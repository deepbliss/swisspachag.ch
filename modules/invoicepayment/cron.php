<?php
/**
 * invoicepayment
 *
 * @author    silbersaiten <info@silbersaiten.de>
 * @copyright 2016 silbersaiten
 * @license   See joined file licence.txt
 * @category  Module
 * @support   silbersaiten <support@silbersaiten.de>
 * @version   3.0.0
 * @link      http://www.silbersaiten.de
 */

require(dirname(__FILE__).'/../../config/config.inc.php');
require(dirname(__FILE__).'/invoicepayment.php');

/* when this is call by cron context is not init */
Context::getContext()->link = new Link();

$ip = new InvoicePayment();
$ip->setReminders();
