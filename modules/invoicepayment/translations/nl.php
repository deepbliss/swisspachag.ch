<?php
global $_MODULE;
$_MODULE = array();
$_MODULE['<{invoicepayment}prestashop>configurator_254f642527b45bc260048e30704edb39'] = 'Configuratie';
$_MODULE['<{invoicepayment}prestashop>configurator_c9cc8cce247e49bae79f15173ce97354'] = 'Opslaan';
$_MODULE['<{invoicepayment}prestashop>confirmation_0db71da7150c27142eef9d22b843b4a9'] = 'Voor vragen of voor meer informatie kunt u contact opnemen met onze';
$_MODULE['<{invoicepayment}prestashop>confirmation_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Uw bestelling op';
$_MODULE['<{invoicepayment}prestashop>confirmation_64430ad2835be8ad60c59e7d44e4b0b1'] = 'klantenondersteuning';
$_MODULE['<{invoicepayment}prestashop>confirmation_75fbf512d744977d62599cc3f0ae2bb4'] = 'voltooid.';
$_MODULE['<{invoicepayment}prestashop>confirmation_e6dc7945b557a1cd949bea92dd58963e'] = 'Uw bestelling zal zeer binnenkort worden verzonden.';
$_MODULE['<{invoicepayment}prestashop>confirmation_ebe4e4e879c37c04d027193e9a912995'] = 'Je hebt het wetsvoorstel gekozen methode.';
$_MODULE['<{invoicepayment}prestashop>group_selects_352bea2689c255007e7d51bc39707a6b'] = 'Mail vertraging';
$_MODULE['<{invoicepayment}prestashop>group_selects_4bc9056ab4105b5bc1b9637c1b48b667'] = 'Nadat een \'betaaldatum\' is bereikt, wordt de klant een herinnering gestuurd als de betaling nog steeds niet binnen het opgegeven aantal dagen is ontvangen.';
$_MODULE['<{invoicepayment}prestashop>group_selects_5d9979cdbad2e7b5de46b1104bcea86f'] = 'Bestellingen worden aangemaakt met de geselecteerde status.';
$_MODULE['<{invoicepayment}prestashop>group_selects_6adf97f83acf6453d4a6a4b1070f3754'] = 'Geen';
$_MODULE['<{invoicepayment}prestashop>group_selects_6df8d168ddb57ba987c08b4552057615'] = 'Status voor geannuleerde bestellingen';
$_MODULE['<{invoicepayment}prestashop>group_selects_6f774631e71968763553dce5190d907b'] = 'Herinnering status';
$_MODULE['<{invoicepayment}prestashop>group_selects_97fb8f888932a2adab1216906baede1b'] = 'Plaats deze URL in crontab of bel het handmatig dagelijks:';
$_MODULE['<{invoicepayment}prestashop>group_selects_a4982afbc84e7f49b9a827e635e6fbc7'] = 'Wanneer een herinnering wordt verzonden, wordt de bestelling de status zal worden veranderd naar deze ene.';
$_MODULE['<{invoicepayment}prestashop>group_selects_ade211683447360c7c797671560b1c8c'] = 'Als u \'geen\' kiest, worden de herinneringen nog steeds verzonden, maar wordt de status van de bestelling niet gewijzigd.';
$_MODULE['<{invoicepayment}prestashop>group_selects_d26b2ee17a5ef036b46c28d96b32ca6c'] = 'Standaard order status';
$_MODULE['<{invoicepayment}prestashop>group_selects_df851401abd05365df1910e431498ea3'] = 'Orders die aan deze status zijn toegewezen, worden niet opgenomen in de bovenstaande regels. Als u \'Geen\' selecteert, worden alle bestellingen opgenomen in de regels.';
$_MODULE['<{invoicepayment}prestashop>group_settings_026ea2becdd42436b71892c1ec22db11'] = 'Geef verschuldigde betaling voor verschillende groepen klanten.';
$_MODULE['<{invoicepayment}prestashop>group_settings_0d42fc4947bdf4c3aef610ee1f412ef6'] = 'Betaalde bestellingen: hier kunt u opgeven hoeveel betaalde bestellingen moet de klant al het invoicepayment geactiveerd in zijn betalingsmethoden (alleen voor bestellingen betaald via Factuur Betaling).';
$_MODULE['<{invoicepayment}prestashop>group_settings_0ea3ce74780a114f03066db41cd26196'] = 'Uitstaande saldo: het maximale bedrag van de onbetaalde bestellingen kan de klant dat het beschikbare betaalmethode. (alleen voor bestellingen betaald via Factuur Betaling).';
$_MODULE['<{invoicepayment}prestashop>group_settings_19890e7801160e90ad426e9131f1c77e'] = 'Dagen: Het aantal dagen gedurende welke u moet betalen voor uw bestelling.';
$_MODULE['<{invoicepayment}prestashop>group_settings_31b757a53de34a4fd1dc97f9c16dc604'] = 'Niet betaalde orders: hier kunt u het aantal niet-betaalde bestellingen (maximaal) kan de klant dat het beschikbare betaalmethode (alleen voor bestellingen betaald via Factuur Betaling).';
$_MODULE['<{invoicepayment}prestashop>group_settings_461a052408e0923006b33ca4213ee814'] = 'Betaalde bestellingen';
$_MODULE['<{invoicepayment}prestashop>group_settings_70222abcfcc9cee89afc1cdb27daab55'] = 'Groepsinstellingen';
$_MODULE['<{invoicepayment}prestashop>group_settings_72f9145d082d692e79305b334e619b1c'] = 'Bestellingslimiet';
$_MODULE['<{invoicepayment}prestashop>group_settings_baa806de63f6866c77173c29c84f7b7d'] = 'Uitstekend saldo';
$_MODULE['<{invoicepayment}prestashop>group_settings_e48e4007e108deaa968c9a20a8298c10'] = 'Orderlimiet: voer de orderlimiet in voor een bedrag voor een klantengroep om de factuurbetalingsmethode geactiveerd te hebben.';
$_MODULE['<{invoicepayment}prestashop>group_settings_e5ee7fd2b237ae91a05c2c7cff4a145f'] = 'Onbetaalde bestellingen';
$_MODULE['<{invoicepayment}prestashop>group_settings_e807d3ccf8d24c8c1a3d86db5da78da8'] = 'dagen';
$_MODULE['<{invoicepayment}prestashop>invoice_63d5049791d9d79d86e9a108b0a999ca'] = 'Referentie';
$_MODULE['<{invoicepayment}prestashop>invoice_cee1f6882f42955618fc1e9dedfa5695'] = 'Te betalen als gevolg van';
$_MODULE['<{invoicepayment}prestashop>invoice_d5cfd0f69cd548e5d3b9edde5ff1b48f'] = 'Betaling informatie';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_0133c7a0fa55069df27f8d9be2c6ab89'] = 'Bestel staten';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_1ab1dfd9dc24ea4729d6c032f7f0342d'] = 'Betalingsinformatie';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_254f642527b45bc260048e30704edb39'] = 'Ivalid orderstatus id #';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_3134ecff4ad87186be94035b2996ea73'] = 'Laat het veld leeg als u de betaalmethode direct bij de eerste bestelling te accepteren.';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_352bea2689c255007e7d51bc39707a6b'] = 'Mail vertraging';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_40c85b7e487330be5060ef9c379e327f'] = '\"Post vertraging\" - onjuiste waarde';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_49ee3087348e8d44e1feda1917443987'] = 'Naam';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_4bc9056ab4105b5bc1b9637c1b48b667'] = 'Na een \"betaling\" datum is bereikt, als de betaling nog steeds niet in het opgegeven aantal dagen wordt ontvangen, een herinnering zal worden verzonden naar de klant. ';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_56d47b27beb03eec010e0dc21bbd6472'] = ' Geef aub na welke bestel uw klanten moet worden gefactureerd.';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_585328c410f1dee93975c12abbd371fe'] = 'Begin na # van orders';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_595f17363413de9a3f777905a783492c'] = 'Betalingsherinnering';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_5d6ad67f2cfd2286268a2e49854e6664'] = 'Fout bij het bijwerken van machtigingen:';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_6adf97f83acf6453d4a6a4b1070f3754'] = 'Geen';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_6da22d7fa7c58e10ab345216d13aef0f'] = 'Kon niet mail templates kopiëren';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_6f774631e71968763553dce5190d907b'] = 'Reminder-status';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_70222abcfcc9cee89afc1cdb27daab55'] = 'Groep instellingen';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_72f9145d082d692e79305b334e619b1c'] = 'Bestel limiet';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_7633e1fe02d7971e8491a0d8b3e5f37b'] = '\'Verstuur e-mail\' - onjuiste waarde';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_81f1465561400b43316891048b0c1c03'] = 'Wachten voor betaling per factuur';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_8db6e6bf29ac75f70973af2e8194d8f1'] = '\"Start na # van orders\" - onjuiste waarde';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_92de6688040a2b8bb4e3198e8f60144b'] = 'Ongeldige orderstatus id #';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_9525c618f3d3c036c6ef845fa16e5309'] = 'Laat leeg of is ingesteld op 0 uit te schakelen, dan zal er geen herinneringen worden verzonden.';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_97fb8f888932a2adab1216906baede1b'] = 'Plaats deze URL in crontab of noem het dagelijks handmatig:';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a19a96670bd330b90bc3d6f62e6fa18e'] = '\"Herinnering Status\" - onjuiste waarde';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a2108b01755fe2b47655de22c8514abc'] = 'Accepteer factuur betalingen';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a2219512f031bd06ec38f95cfb96743d'] = '\'Standaardstatus\' - onjuiste waarde';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a32d306b2a165530e84b4ae71ffb73b2'] = 'Betalen met Factuur Betaling';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a3a02879f4d2a352ec8e39f124bcd0a9'] = 'Gelieve te specificeren betaling dat voor verschillende klantgroepen';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a4982afbc84e7f49b9a827e635e6fbc7'] = 'Wanneer een herinnering wordt verzonden, wordt de status van de bestelling worden veranderd om deze.';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a4c36ccf447ed15e22166322fb210db6'] = 'Ongeldige waarden voorzien voor de groep';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_ac1d69cbf772509f137f1b4db51a60ac'] = 'Você paga com conta';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_ade211683447360c7c797671560b1c8c'] = 'Als u kiest voor \"niemand\", zal de herinneringen nog steeds worden gestuurd, maar de status van de bestelling zal niet worden gewijzigd.';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_c9cc8cce247e49bae79f15173ce97354'] = 'Save';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_e807d3ccf8d24c8c1a3d86db5da78da8'] = 'Dagen';
$_MODULE['<{invoicepayment}prestashop>payment_7691588914c922b50ad0c4b2f661ac7e'] = 'U betalen met factuur';
$_MODULE['<{invoicepayment}prestashop>payment_8781b940179e3869ff36263350a8dce1'] = 'Betalen met factuur';
$_MODULE['<{invoicepayment}prestashop>validation_0881a11f7af33bc1b43e437391129d66'] = 'Bevestig uw bestelling door op \'Ik bevestig mijn bestelling\' te klikken';
$_MODULE['<{invoicepayment}prestashop>validation_0c25b529b4d690c39b0831840d0ed01c'] = 'Bestel sommatie';
$_MODULE['<{invoicepayment}prestashop>validation_1f87346a16cf80c372065de3c54c86d9'] = '(Incl. btw)';
$_MODULE['<{invoicepayment}prestashop>validation_46b9e3665f187c739c55983f757ccda0'] = 'ik bevestig mijn bestelling';
$_MODULE['<{invoicepayment}prestashop>validation_569fd05bdafa1712c4f6be5b153b8418'] = 'Andere betalingsmethoden';
$_MODULE['<{invoicepayment}prestashop>validation_723ec5d2c9cfaaea04c0173f4e4c0ea5'] = 'Bill betaling.';
$_MODULE['<{invoicepayment}prestashop>validation_952c511af786a11129140f537c6bc5b4'] = 'Betaling van facturen';
$_MODULE['<{invoicepayment}prestashop>validation_e0d7a6ec1c1101b1b8b6e5d1b94c38d4'] = 'U hebt de factureringsmethode gekozen.';
$_MODULE['<{invoicepayment}prestashop>validation_e2867a925cba382f1436d1834bb52a1c'] = 'het totaalbedrag van je bestelling is';
$_MODULE['<{invoicepayment}prestashop>order_states_0133c7a0fa55069df27f8d9be2c6ab89'] = 'Bestel staten';
$_MODULE['<{invoicepayment}prestashop>order_states_49ee3087348e8d44e1feda1917443987'] = 'Naam';
$_MODULE['<{invoicepayment}prestashop>order_states_deb5258aaeaf39b285723ec418486f89'] = 'Selecteer uw status met betrekking tot de berekening van de betalingstermijn. De berekening begint op de dag dat de bestelling een van deze statussen bereikt.';
$_MODULE['<{invoicepayment}prestashop>payment_information_1ab1dfd9dc24ea4729d6c032f7f0342d'] = 'Betalingsinformatie';
$_MODULE['<{invoicepayment}prestashop>payment_return_6e81010493404d905f3dd12d95a55112'] = 'Neem voor vragen of voor meer informatie contact op met onze [1] klantenondersteuning [/ 1].';
$_MODULE['<{invoicepayment}prestashop>payment_return_c00373052588907ffdd28d1866a75aee'] = 'We hebben een probleem met uw bestelling opgemerkt. Als u denkt dat dit een fout is, kunt u contact opnemen met ons [1] deskundige klantenserviceteam [/ 1].';
$_MODULE['<{invoicepayment}prestashop>payment_return_e6dc7945b557a1cd949bea92dd58963e'] = 'Uw bestelling wordt zeer binnenkort verzonden.';
$_MODULE['<{invoicepayment}prestashop>payment_return_ebe4e4e879c37c04d027193e9a912995'] = 'U hebt de rekenmethode gekozen.';
$_MODULE['<{invoicepayment}prestashop>send_mail_77d6631d9ac30cf95eb01d32c29f999f'] = 'Activeer deze optie zodat uw klanten een extra e-mail ontvangen met de betalingsinformatie en een betalingsverzoek nadat ze een bestelling hebben geplaatst.';
$_MODULE['<{invoicepayment}prestashop>send_mail_93cba07454f06a4a960172bbd6e2a435'] = 'Ja';
$_MODULE['<{invoicepayment}prestashop>send_mail_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Nee';
$_MODULE['<{invoicepayment}prestashop>send_mail_e31c0c465a632294c280582e7d982aa9'] = 'Stuur een e-mail';
$_MODULE['<{invoicepayment}prestashop>user_advanced_settings_6d7313444f7a9bd81c13469e8cc5122e'] = 'Lijst met landen';
$_MODULE['<{invoicepayment}prestashop>user_advanced_settings_7046ce891a343b3dd3a6ccf47fc0131e'] = 'Toon alle landen';
$_MODULE['<{invoicepayment}prestashop>user_advanced_settings_7eb9f6f7b9187f715aab358a713ac903'] = 'Geavanceerde gebruikersgroepinstellingen';
$_MODULE['<{invoicepayment}prestashop>user_advanced_settings_8b144ffc87beff564884b1c190275539'] = 'Selecteer land';
