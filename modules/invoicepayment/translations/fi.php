<?php
global $_MODULE;
$_MODULE = array();
$_MODULE['<{invoicepayment}prestashop>invoicepayment_ac1d69cbf772509f137f1b4db51a60ac'] = 'Lasku	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a2108b01755fe2b47655de22c8514abc'] = 'Hyväksy Lasku Maksutapana	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_81f1465561400b43316891048b0c1c03'] = 'Odottaa laskun maksua	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a32d306b2a165530e84b4ae71ffb73b2'] = 'Maksa Laskulla	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_92de6688040a2b8bb4e3198e8f60144b'] = 'Väärä tilauksen tila id #	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a4c36ccf447ed15e22166322fb210db6'] = 'Väärät arvot annettu ryhmälle	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_254f642527b45bc260048e30704edb39'] = 'Asetukset	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_70222abcfcc9cee89afc1cdb27daab55'] = 'Ryhmäasetukset	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a3a02879f4d2a352ec8e39f124bcd0a9'] = 'Määritä maksuaika eri asiakasryhmille	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_e807d3ccf8d24c8c1a3d86db5da78da8'] = 'Päivää	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_72f9145d082d692e79305b334e619b1c'] = 'Tilauksia	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_461a052408e0923006b33ca4213ee814'] = 'Maksettuja	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_e5ee7fd2b237ae91a05c2c7cff4a145f'] = 'Maksamattomia	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_baa806de63f6866c77173c29c84f7b7d'] = 'Velan määrä	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a88bee1af5e0a68755fca2de4076c505'] = 'Velan määrä	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_19890e7801160e90ad426e9131f1c77e'] = 'Päivää: Tilauksen maksuaika päivinä.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_e48e4007e108deaa968c9a20a8298c10'] = 'Tilauksia: montako tilausta asiakkaalla pitää olla jotta tämä maksutapa aktivoituu.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_0d42fc4947bdf4c3aef610ee1f412ef6'] = 'Maksettuja: montako maksettua tilausta asiakkaalla pitää olla jotta tämä maksutapa aktivoituu.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_31b757a53de34a4fd1dc97f9c16dc604'] = 'Maksamattomia: montako maksamatonta laskua asiakkaalla voi maksimissaan olla jotta tämä maksutapa aktivoituu.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_0ea3ce74780a114f03066db41cd26196'] = 'Velka: paljonko maksamatonta velkaa asiakkaalla voi maksimissaan olla (laskujen kautta), jotta tämä maksutapa aktivoituu.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_1ab1dfd9dc24ea4729d6c032f7f0342d'] = 'Maksutiedot (tilinumero jne.)	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_0133c7a0fa55069df27f8d9be2c6ab89'] = 'Tilausten tilat	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_49ee3087348e8d44e1feda1917443987'] = 'Nimi	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_deb5258aaeaf39b285723ec418486f89'] = 'Valitse tila eräpäivän laskentaa varten. Eräpäivälaskenta alkaa päivästä jona tilaus menee muutetaan johonkin tässä valituista tiloista.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_352bea2689c255007e7d51bc39707a6b'] = 'Muistutusviive	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_4bc9056ab4105b5bc1b9637c1b48b667'] = 'Kun \"eräpäivä\" on saavutettu, eikä maksu ole saapunut, lähetä sähköpostimuistutus asiakkaalle määritetyn päivän kuluttua eräpäivästä.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_9525c618f3d3c036c6ef845fa16e5309'] = 'Jos et halua lähettää muistutuksia, jätä kenttä tyhjäksi tai laita siihen 0.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_97fb8f888932a2adab1216906baede1b'] = 'Aja URL crontab:lla tai manuaalisesti päivittäin:	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_6f774631e71968763553dce5190d907b'] = 'Tila muistutuksen jälkeen	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_6adf97f83acf6453d4a6a4b1070f3754'] = 'Ei muuteta	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a4982afbc84e7f49b9a827e635e6fbc7'] = 'Kun muistutus on lähetetty, tilauksen tila vaihtuu tähän.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_ade211683447360c7c797671560b1c8c'] = 'Jos valitset \"Ei muuteta\", muistutus lähetetään, mutta tilaa ei muuteta.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_d26b2ee17a5ef036b46c28d96b32ca6c'] = 'Tilauksen oletustila	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_5d9979cdbad2e7b5de46b1104bcea86f'] = 'Tilaukset luodaan valitulla tilalla.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_6df8d168ddb57ba987c08b4552057615'] = 'Peruutettujen tilausten tila	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_df851401abd05365df1910e431498ea3'] = 'Tilaukset joilla on tämä tila jätetään huomioimatta säännöissä ylempänä. Jos valitset \"Ei muuteta\", kaikki tilaukset tottelevat sääntöjä.	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_c9cc8cce247e49bae79f15173ce97354'] = 'Tallenna	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_40c85b7e487330be5060ef9c379e327f'] = '\"Muistutusviive\" - väärä arvo	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a19a96670bd330b90bc3d6f62e6fa18e'] = '\"Muistutuksen tila\" - väärä arvo	';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a2219512f031bd06ec38f95cfb96743d'] = '\"Oletustila\" - väärä arvo	';
$_MODULE['<{invoicepayment}prestashop>validation_723ec5d2c9cfaaea04c0173f4e4c0ea5'] = 'Maksu Laskulla.	';
$_MODULE['<{invoicepayment}prestashop>validation_0c25b529b4d690c39b0831840d0ed01c'] = 'Tilauksen yhteenveto	';
$_MODULE['<{invoicepayment}prestashop>validation_952c511af786a11129140f537c6bc5b4'] = 'Maksu Laskulla	';
$_MODULE['<{invoicepayment}prestashop>validation_e0d7a6ec1c1101b1b8b6e5d1b94c38d4'] = 'Olet valinnut maksutavan.	';
$_MODULE['<{invoicepayment}prestashop>validation_e2867a925cba382f1436d1834bb52a1c'] = 'Tilauksen summa yhteensä on	';
$_MODULE['<{invoicepayment}prestashop>validation_1f87346a16cf80c372065de3c54c86d9'] = '(sis. ALV.)	';
$_MODULE['<{invoicepayment}prestashop>validation_0881a11f7af33bc1b43e437391129d66'] = 'Vahvista tilaus klikkaamalla \'Vahvistan tilauksen\'	';
$_MODULE['<{invoicepayment}prestashop>validation_569fd05bdafa1712c4f6be5b153b8418'] = 'Muut maksutavat	';
$_MODULE['<{invoicepayment}prestashop>validation_46b9e3665f187c739c55983f757ccda0'] = 'Vahvistan tilauksen	';
$_MODULE['<{invoicepayment}prestashop>confirmation_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Tilauksesi	';
$_MODULE['<{invoicepayment}prestashop>confirmation_75fbf512d744977d62599cc3f0ae2bb4'] = 'on valmis.	';
$_MODULE['<{invoicepayment}prestashop>confirmation_ebe4e4e879c37c04d027193e9a912995'] = 'Olet valinnut maksutavaksi laskun.	';
$_MODULE['<{invoicepayment}prestashop>confirmation_e6dc7945b557a1cd949bea92dd58963e'] = 'Tilauksesi lähetetään pian eteenpäin.	';
$_MODULE['<{invoicepayment}prestashop>confirmation_0db71da7150c27142eef9d22b843b4a9'] = 'Jos sinulla on kysyttävää, ota yhteyttä	';
$_MODULE['<{invoicepayment}prestashop>confirmation_64430ad2835be8ad60c59e7d44e4b0b1'] = 'asiakastukeen	';
$_MODULE['<{invoicepayment}prestashop>invoice_cee1f6882f42955618fc1e9dedfa5695'] = 'Eräpäivä	';
$_MODULE['<{invoicepayment}prestashop>invoice_d5cfd0f69cd548e5d3b9edde5ff1b48f'] = 'Maksutiedot	';
$_MODULE['<{invoicepayment}prestashop>payment_8781b940179e3869ff36263350a8dce1'] = 'Maksa laskulla	';
$_MODULE['<{invoicepayment}prestashop>payment_7691588914c922b50ad0c4b2f661ac7e'] = 'Maksat laskulla	';
