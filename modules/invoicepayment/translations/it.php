<?php
global $_MODULE;
$_MODULE = array();
$_MODULE['<{invoicepayment}prestashop>configurator_254f642527b45bc260048e30704edb39'] = 'Configurazione';
$_MODULE['<{invoicepayment}prestashop>configurator_c9cc8cce247e49bae79f15173ce97354'] = 'Salvare';
$_MODULE['<{invoicepayment}prestashop>group_selects_352bea2689c255007e7d51bc39707a6b'] = 'Ritardo di posta';
$_MODULE['<{invoicepayment}prestashop>group_selects_4bc9056ab4105b5bc1b9637c1b48b667'] = 'Dopo un \'pagamento\', la data è raggiunto, se il pagamento non è ancora ricevuto il numero di giorni specificato, verrà inviato un promemoria per il cliente.';
$_MODULE['<{invoicepayment}prestashop>group_selects_5d9979cdbad2e7b5de46b1104bcea86f'] = 'Gli ordini vengono creati con lo stato selezionato.';
$_MODULE['<{invoicepayment}prestashop>group_selects_6adf97f83acf6453d4a6a4b1070f3754'] = 'Nessuno';
$_MODULE['<{invoicepayment}prestashop>group_selects_6df8d168ddb57ba987c08b4552057615'] = 'Stato degli ordini annullati';
$_MODULE['<{invoicepayment}prestashop>group_selects_6f774631e71968763553dce5190d907b'] = 'Stato del promemoria';
$_MODULE['<{invoicepayment}prestashop>group_selects_97fb8f888932a2adab1216906baede1b'] = 'Posto questo URL nel crontab o chiamare manualmente ogni giorno:';
$_MODULE['<{invoicepayment}prestashop>group_selects_a4982afbc84e7f49b9a827e635e6fbc7'] = 'Quando viene mandato un sollecito, lo stato dell\'ordine sarà cambiato per questo.';
$_MODULE['<{invoicepayment}prestashop>group_selects_ade211683447360c7c797671560b1c8c'] = 'Se si sceglie l\'opzione \'nessuno\', il promemoria verrà inviata, ma lo stato dell\'ordine non verrà modificato.';
$_MODULE['<{invoicepayment}prestashop>group_selects_d26b2ee17a5ef036b46c28d96b32ca6c'] = 'Ordine predefinito di stato';
$_MODULE['<{invoicepayment}prestashop>group_selects_df851401abd05365df1910e431498ea3'] = 'Gli ordini assegnati a questo stato non saranno inclusi nelle regole di cui sopra. Se si seleziona \'Nessuno\', tutti gli ordini saranno inclusi nelle regole.';
$_MODULE['<{invoicepayment}prestashop>group_settings_026ea2becdd42436b71892c1ec22db11'] = 'Si prega di specificare al pagamento dovuto per i diversi gruppi di clienti.';
$_MODULE['<{invoicepayment}prestashop>group_settings_0d42fc4947bdf4c3aef610ee1f412ef6'] = 'Gli ordini pagati: qui è possibile specificare la modalità di pagamento gli ordini il cliente deve già avere il invoicepayment attivate nella sua metodi di pagamento (solo per gli ordini pagati tramite il Pagamento di Fatture).';
$_MODULE['<{invoicepayment}prestashop>group_settings_0ea3ce74780a114f03066db41cd26196'] = 'Saldo: l\'importo massimo dell\'ordine non pagato, può essere il cliente che era disponibile metodo di pagamento. (solo per gli ordini pagati tramite il Pagamento di Fatture).';
$_MODULE['<{invoicepayment}prestashop>group_settings_19890e7801160e90ad426e9131f1c77e'] = 'Giorni: Il numero di giorni durante i quali si deve pagare per il vostro ordine.';
$_MODULE['<{invoicepayment}prestashop>group_settings_31b757a53de34a4fd1dc97f9c16dc604'] = 'Ordini non pagati: qui è possibile specificare il numero di ordini non pagati (massimo) può essere il cliente che era disponibile un metodo di pagamento (solo per gli ordini pagati tramite il Pagamento di Fatture).';
$_MODULE['<{invoicepayment}prestashop>group_settings_461a052408e0923006b33ca4213ee814'] = 'Ordini a pagamento';
$_MODULE['<{invoicepayment}prestashop>group_settings_70222abcfcc9cee89afc1cdb27daab55'] = 'Impostazioni di gruppo';
$_MODULE['<{invoicepayment}prestashop>group_settings_72f9145d082d692e79305b334e619b1c'] = 'Limite di ordine';
$_MODULE['<{invoicepayment}prestashop>group_settings_baa806de63f6866c77173c29c84f7b7d'] = 'Saldo eccezionale';
$_MODULE['<{invoicepayment}prestashop>group_settings_e48e4007e108deaa968c9a20a8298c10'] = 'Limite ordine: immettere il limite dell\'ordine per un importo per un gruppo di clienti per attivare il metodo di pagamento delle fatture.';
$_MODULE['<{invoicepayment}prestashop>group_settings_e5ee7fd2b237ae91a05c2c7cff4a145f'] = 'Ordini non pagati';
$_MODULE['<{invoicepayment}prestashop>group_settings_e807d3ccf8d24c8c1a3d86db5da78da8'] = 'giorni';
$_MODULE['<{invoicepayment}prestashop>invoice_63d5049791d9d79d86e9a108b0a999ca'] = 'Riferimento';
$_MODULE['<{invoicepayment}prestashop>invoice_cee1f6882f42955618fc1e9dedfa5695'] = 'Per essere pagato dovuto';
$_MODULE['<{invoicepayment}prestashop>invoice_d5cfd0f69cd548e5d3b9edde5ff1b48f'] = 'Informazioni di pagamento';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_40c85b7e487330be5060ef9c379e327f'] = '\"Ritardo della posta\" - valore errato';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_595f17363413de9a3f777905a783492c'] = 'Promemoria di pagamento';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_5d6ad67f2cfd2286268a2e49854e6664'] = 'Errore durante l\'aggiornamento delle autorizzazioni:';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_7633e1fe02d7971e8491a0d8b3e5f37b'] = '\"Invia mail\" - valore errato ';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_81f1465561400b43316891048b0c1c03'] = 'In attesa di pagamento tramite fattura';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_92de6688040a2b8bb4e3198e8f60144b'] = 'ID stato ordine invalido # ';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a19a96670bd330b90bc3d6f62e6fa18e'] = '\"Stato promemoria\" - valore errato ';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a2108b01755fe2b47655de22c8514abc'] = 'Accetta i pagamenti delle fatture';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a2219512f031bd06ec38f95cfb96743d'] = '\"Stato predefinito\" - valore errato';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a32d306b2a165530e84b4ae71ffb73b2'] = 'Pagamento con pagamento fattura';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_a4c36ccf447ed15e22166322fb210db6'] = 'Valori non validi forniti per group ';
$_MODULE['<{invoicepayment}prestashop>invoicepayment_ac1d69cbf772509f137f1b4db51a60ac'] = 'Pagamento della fattura';
$_MODULE['<{invoicepayment}prestashop>validation_0881a11f7af33bc1b43e437391129d66'] = 'Conferma il tuo ordine facendo clic su \'Confermare il mio ordine\'';
$_MODULE['<{invoicepayment}prestashop>validation_0c25b529b4d690c39b0831840d0ed01c'] = 'Sommatoria dell\'ordine';
$_MODULE['<{invoicepayment}prestashop>validation_1f87346a16cf80c372065de3c54c86d9'] = '(tasse incluse)';
$_MODULE['<{invoicepayment}prestashop>validation_46b9e3665f187c739c55983f757ccda0'] = 'Confermo il mio ordine';
$_MODULE['<{invoicepayment}prestashop>validation_569fd05bdafa1712c4f6be5b153b8418'] = 'Altri metodi di pagamento';
$_MODULE['<{invoicepayment}prestashop>validation_723ec5d2c9cfaaea04c0173f4e4c0ea5'] = 'Pagamento delle bollette.';
$_MODULE['<{invoicepayment}prestashop>validation_952c511af786a11129140f537c6bc5b4'] = 'Pagamento delle bollette';
$_MODULE['<{invoicepayment}prestashop>validation_e0d7a6ec1c1101b1b8b6e5d1b94c38d4'] = 'Hai scelto il metodo di fatturazione.';
$_MODULE['<{invoicepayment}prestashop>validation_e2867a925cba382f1436d1834bb52a1c'] = 'L\'importo totale del tuo ordine è';
$_MODULE['<{invoicepayment}prestashop>order_states_0133c7a0fa55069df27f8d9be2c6ab89'] = 'Stati d\'ordine';
$_MODULE['<{invoicepayment}prestashop>order_states_49ee3087348e8d44e1feda1917443987'] = 'Nome';
$_MODULE['<{invoicepayment}prestashop>order_states_deb5258aaeaf39b285723ec418486f89'] = 'Si prega di selezionare il proprio stato per quanto riguarda il calcolo del termine di pagamento. Il calcolo inizia il giorno in cui l\'ordine raggiunge uno di questi stati.';
$_MODULE['<{invoicepayment}prestashop>payment_information_1ab1dfd9dc24ea4729d6c032f7f0342d'] = 'Informazioni sul pagamento';
$_MODULE['<{invoicepayment}prestashop>payment_return_6e81010493404d905f3dd12d95a55112'] = 'Per qualsiasi domanda o per ulteriori informazioni, si prega di contattare il [1] supporto clienti [/ 1].';
$_MODULE['<{invoicepayment}prestashop>payment_return_c00373052588907ffdd28d1866a75aee'] = 'Abbiamo notato un problema con il tuo ordine. Se ritieni che si tratti di un errore, non esitare a contattare il nostro [1] team di assistenza clienti esperto [/ 1].';
$_MODULE['<{invoicepayment}prestashop>payment_return_e6dc7945b557a1cd949bea92dd58963e'] = 'Il tuo ordine verrà inviato molto presto.';
$_MODULE['<{invoicepayment}prestashop>payment_return_ebe4e4e879c37c04d027193e9a912995'] = 'Hai scelto il metodo di fatturazione.';
$_MODULE['<{invoicepayment}prestashop>send_mail_77d6631d9ac30cf95eb01d32c29f999f'] = 'Attiva questa opzione in modo che i tuoi clienti riceveranno un\'e-mail aggiuntiva con le informazioni di pagamento e una richiesta di pagamento dopo aver effettuato un ordine.';
$_MODULE['<{invoicepayment}prestashop>send_mail_93cba07454f06a4a960172bbd6e2a435'] = 'sì';
$_MODULE['<{invoicepayment}prestashop>send_mail_bafd7322c6e97d25b6299b5d6fe8920b'] = 'No';
$_MODULE['<{invoicepayment}prestashop>send_mail_e31c0c465a632294c280582e7d982aa9'] = 'Invia una email';
$_MODULE['<{invoicepayment}prestashop>user_advanced_settings_6d7313444f7a9bd81c13469e8cc5122e'] = 'Elenco dei Paesi';
$_MODULE['<{invoicepayment}prestashop>user_advanced_settings_7046ce891a343b3dd3a6ccf47fc0131e'] = 'Mostra tutti i paesi';
$_MODULE['<{invoicepayment}prestashop>user_advanced_settings_7eb9f6f7b9187f715aab358a713ac903'] = 'Impostazioni avanzate del gruppo utenti';
$_MODULE['<{invoicepayment}prestashop>user_advanced_settings_8b144ffc87beff564884b1c190275539'] = 'Seleziona il paese';
