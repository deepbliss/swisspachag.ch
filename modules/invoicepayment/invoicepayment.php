<?php
/**
 * invoicepayment
 *
 * @author    silbersaiten <info@silbersaiten.de>
 * @copyright 2018 silbersaiten
 * @license   See joined file licence.txt
 * @category  Module
 * @support   silbersaiten <support@silbersaiten.de>
 * @version   3.0.6
 * @link      http://www.silbersaiten.de
 */

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit;
}

class InvoicePayment extends PaymentModule
{
    public $html_content;
    public $post_errors = array();
    public $post_warnings = array();
    private static $sql_queries = array(
        'invoice_payment_group_settings' => '
        CREATE TABLE IF NOT EXISTS `%PREFIX%invoice_payment_group_settings` (
        `id_group` int(10) unsigned NOT NULL,
        `days_amount` int(10) unsigned DEFAULT NULL,
        `order_limit` decimal(20,6) NOT NULL DEFAULT \'0.000000\',
        `paid_orders` int(10) unsigned DEFAULT NULL,
        `unpaid_orders` int(10) unsigned DEFAULT NULL,
        `out_bal` decimal(20,6) NOT NULL DEFAULT \'0.000000\',
        PRIMARY KEY (`id_group`)
        ) ENGINE=%ENGINE% DEFAULT CHARSET=utf8',

        'invoice_payment_status_settings' => '
        CREATE TABLE IF NOT EXISTS `%PREFIX%invoice_payment_status_settings` (
        `id_order_state` int(10) unsigned NOT NULL,
        PRIMARY KEY (`id_order_state`)
        ) ENGINE=%ENGINE% DEFAULT CHARSET=utf8',

        'invoice_payment_order' => '
        CREATE TABLE IF NOT EXISTS `%PREFIX%invoice_payment_order` (
        `id_order` int(10) unsigned NOT NULL,
        `is_paid` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
        `is_reminded` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
        `date_add` datetime NOT NULL,
        PRIMARY KEY (`id_order`)
        ) ENGINE=%ENGINE% DEFAULT CHARSET=utf8',

        'invoice_payment_permissions' => 'CREATE TABLE IF NOT EXISTS `%PREFIX%invoice_payment_permissions` (
          `id_invoice_payment_permission` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `id_country` INT UNSIGNED NOT NULL,
          `id_group` INT(10) UNSIGNED NOT NULL,
          `id_shop`  INT(11) UNSIGNED DEFAULT \'1\' NOT NULL,
          PRIMARY KEY  (`id_invoice_payment_permission`))'
    );

    public function __construct()
    {
        $this->name = 'invoicepayment';
        $this->tab = 'payments_gateways';
        $this->author = 'silbersaiten';
        $this->version = '3.0.6';
        $this->module_key = '3d5467af7231b92cc5c54bbcbd6f664e';
        $this->bootstrap = true;

        $this->currencies = true;
        $this->currencies_mode = 'checkbox';

        $this->controllers = array('payment', 'validation');
        $this->is_eu_compatible = 1;

        parent::__construct();

        $this->displayName = $this->l('Invoice Payment');
        $this->description = $this->l('Accept invoice payments');
    }

    public function install()
    {
        if (!parent::install()
            || !$this->registerHook('paymentOptions')
            || !$this->registerHook('paymentReturn')
            || !$this->registerHook('displayPDFInvoice')
            || !$this->registerHook('postUpdateOrderStatus')
            || !$this->registerHook('actionObjectOrderAddAfter')
            || !$this->registerHook('displayBackOfficeHeader')) {
            return false;
        }

        foreach (self::$sql_queries as $query) {
            $query = strtr($query, array('%PREFIX%' => _DB_PREFIX_, '%ENGINE%' => _MYSQL_ENGINE_));

            if (!Db::getInstance()->Execute($query)) {
                $this->uninstall();
                return false;
            }
        }

        Configuration::updateValue('PS_DEFAULT_STATUS_ORDER_IP', Configuration::get('PS_OS_PREPARATION'));
        Configuration::updateValue('PS_CANCEL_STATUS_ORDER_IP', Configuration::get('PS_OS_CANCELED'));
        Configuration::updateValue('PS_MAIL_DELAY_DAYS', 3);
        Configuration::updateValue('PS_MAIL_DELAY_OS', 0);
        Configuration::updateValue('PS_MAIL_SEND_IP', 1);

        $create_os = false;
        $create_os_id = 0;
        $os_waiting_invoice = true;

        if ((int)Configuration::get('PS_OS_INVOICE_PAYMENT') > 0) {
            $os_waiting_invoice = new OrderState((int)Configuration::get('PS_OS_INVOICE_PAYMENT'));
        }

        if (!Validate::isLoadedObject($os_waiting_invoice)) {
            $create_os = true;
            $os_waiting_invoice = new OrderState();
            $create_os_id = (int)Configuration::get('PS_OS_INVOICE_PAYMENT');
        }

        if ($create_os_id > 0) {
            $os_waiting_invoice->id = $create_os_id;
        }

        $langs = Language::getLanguages();
        foreach ($langs as $lang) {
            $os_waiting_invoice->name[$lang['id_lang']] = $this->l('Waiting for payment by invoice');
            $os_waiting_invoice->template[$lang['id_lang']] = 'waiting_invoice';
        }
        $os_waiting_invoice->color = '#4169E1';
        $os_waiting_invoice->send_email = 0;
        $os_waiting_invoice->unremovable = 1;
        $os_waiting_invoice->hidden = 0;
        $os_waiting_invoice->logable = 0;
        $os_waiting_invoice->delivery = 0;
        $os_waiting_invoice->shipped = 0;
        $os_waiting_invoice->paid = 0;
        $os_waiting_invoice->module_name = $this->name;
        if ($create_os) {
            if ($os_waiting_invoice->add()) {
                $source = dirname(__FILE__) . '/../../img/os/' . Configuration::get('PS_OS_CHEQUE') . '.gif';
                $destination = dirname(__FILE__) . '/../../img/os/' . (int)$os_waiting_invoice->id . '.gif';
                copy($source, $destination);
            }
            Configuration::updateValue('PS_OS_INVOICE_PAYMENT', $os_waiting_invoice->id);
        } else {
            $os_waiting_invoice->update();
        }

        $this->updateGroups(false, false, false, false, false, true);
        return true;
    }

    public function uninstall()
    {
        if (parent::uninstall()) {
            foreach (array_keys(self::$sql_queries) as $table) {
                Db::getInstance()->Execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . pSQL($table) . '`');
            }

            return true;
        }

        return false;
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->active || !isset($params['cart']) || !$this->checkCart($params['cart']) || !$this->getUserPermissions($params['cart'])) {
            return false;
        }

        $newOption = new PaymentOption();
        $newOption->setCallToActionText($this->l('Pay by Invoice Payment'))
            ->setAction($this->context->link->getModuleLink($this->name, 'validation', array(), true));
        // for display info under paymnet method in order page (cart)
        // ->setAdditionalInformation(
        //$this->context->smarty->fetch('module:bankwire/views/templates/front/payment_infos.tpl'));
        $payment_options = array(
            $newOption,
        );

        return $payment_options;
    }

    private function getUserPermissions($cart)
    {
        $query = new DbQuery();
        $query->select('COUNT(*)');
        $query->from('invoice_payment_permissions', 'ipp');
        $query->leftJoin('customer_group', 'cg', 'cg.id_group = ipp.id_group');
        $query->innerJoin('address', 'adr', 'adr.id_customer = cg.id_customer AND adr.id_country=ipp.id_country');
        $query->where('cg.id_customer = ' . (int)$cart->id_customer);
        $query->where('ipp.id_shop = ' . (int)$cart->id_shop);
        $query->where('adr.id_address = ' . (int)$cart->id_address_invoice);

        return (int)Db::getInstance()->getValue($query);
    }

    public function hookDisplayBackOfficeHeader($params)
    {
        $this->context->controller->addJquery();
        $this->context->controller->addJS(_MODULE_DIR_ . $this->name . '/views/js/invoicepayment_editor.js');
        $this->context->controller->addCSS(_MODULE_DIR_ . $this->name . '/views/css/invoicepayment_editor.css');
    }

    public function hookPaymentReturn()
    {
        if ($this->active) {
            $this->smarty->assign(array(
                'contact_url' => $this->context->link->getPageLink('contact', true),
                'status' => 'ok',
            ));
        } else {
            $this->smarty->assign('status', 'failed');
        }

        return $this->display(__FILE__, 'payment_return.tpl');
    }

    public function hookDisplayPDFInvoice($params)
    {
        $order = new Order((int)$params['object']->id_order);

        if (Validate::isLoadedObject($order) && $order->module == $this->name) {
            $display_tpl = false;
            $valid_statuses = self::getValidStatuses();
            $history = $order->getHistory($order->id_lang);

            foreach ($history as $h) {
                if (in_array($h['id_order_state'], $valid_statuses)) {
                    $shipping_date = $h['date_add'];
                    break;
                }
            }

            if (isset($shipping_date)) {
                $customer = new Customer((int)$order->id_customer);

                if (Validate::isLoadedObject($customer)) {
                    $days = self::getDaysForGroup($customer->id_default_group);

                    if ($days > 0) {
                        $days *= (60 * 60 * 24);
                        $this->context->smarty->assign(
                            'shipping_date',
                            Tools::displayDate(
                                date('Y-m-d h:i:s', (strtotime($shipping_date) + $days)),
                                $order->id_lang
                            )
                        );
                        $display_tpl = true;
                    }
                }
            }

            $payment_info = Configuration::get('INVOICE_PAYMENT_INFO', $order->id_lang);

            if (!Tools::isEmpty($payment_info)) {
                $this->context->smarty->assign(array(
                    'payment_info' => preg_split('/\R/', $payment_info),
                    'order' => $order
                ));
                $display_tpl = true;
            }

            if ($display_tpl) {
                return $this->display(__FILE__, '/views/templates/hook/invoice.tpl');
            }
        }
    }

    public static function getGroupsSettings($id_group = false)
    {
        $prepared = array();
        $groups = Db::getInstance()->ExecuteS(
            'SELECT * FROM `' . _DB_PREFIX_ . 'invoice_payment_group_settings`' .
            ($id_group != false ? ' WHERE `id_group` = ' . (int)$id_group : '')
        );

        if (!$groups || !count($groups)) {
            return $prepared;
        }

        foreach ($groups as $group) {
            $prepared[(int)$group['id_group']] = array(
                'day' => (int)$group['days_amount'],
                'limit' => (float)$group['order_limit'],
                'paid' => (int)$group['paid_orders'],
                'unpaid' => (int)$group['unpaid_orders'],
                'out' => (int)$group['out_bal'],
            );
        }

        return $prepared;
    }

    public static function getDaysForGroup($id_group)
    {
        return (int)Db::getInstance()->getValue(
            'SELECT `days_amount` FROM `' . _DB_PREFIX_ .
            'invoice_payment_group_settings` WHERE `id_group` = ' . (int)$id_group
        );
    }

    public function updateGroups($group_data, $limit_data, $paid_orders_data, $unpaid_orders_data, $out_bal_data, $install = false)
    {
        Db::getInstance()->Execute('TRUNCATE `' . _DB_PREFIX_ . 'invoice_payment_group_settings`');

        if ($install) {
            $default_language = (int)$this->context->language->id;
            $groups = $this->getGroups($default_language);

            foreach ($groups as $group) {
                Db::getInstance()->Execute(
                    'INSERT INTO `' . _DB_PREFIX_ .
                    'invoice_payment_group_settings`
                        (`id_group`, `days_amount`, `order_limit`, `paid_orders`, `unpaid_orders`, `out_bal`)
                    VALUES (' . (int)$group['id_group'] . ', 0, 0, 0, 0, 0)'
                );
            }
        } else {
            foreach ($group_data as $id_group => $days_amount) {
                if (!Validate::isUnsignedId($id_group)
                    || !Validate::isUnsignedInt($days_amount)
                    || !Validate::isPrice($limit_data[$id_group])
                    || !Validate::isInt($paid_orders_data[$id_group])
                    || !Validate::isInt($unpaid_orders_data[$id_group])
                    || !Validate::isInt($out_bal_data[$id_group])) {
                    continue;
                }

                Db::getInstance()->Execute(
                    'INSERT INTO `' . _DB_PREFIX_ .
                    'invoice_payment_group_settings`
                        (`id_group`, `days_amount`, `order_limit`, `paid_orders`, `unpaid_orders`, `out_bal`)
                    VALUES (
                        ' . (int)$id_group . ',
                        ' . (int)$days_amount . ',
                        ' . (float)$limit_data[$id_group] . ',
                        ' . (int)$paid_orders_data[$id_group] . ',
                        ' . (int)$unpaid_orders_data[$id_group] . ',
                        ' . (int)$out_bal_data[$id_group] . ')'
                );
            }
        }
    }


    public function updateStatuses($status_data)
    {
        Db::getInstance()->Execute('TRUNCATE `' . _DB_PREFIX_ . 'invoice_payment_status_settings`');

        if (is_array($status_data)) {
            foreach (array_keys($status_data) as $id_order_state) {
                if (!Validate::isUnsignedId($id_order_state)
                    || !Validate::isLoadedObject(new OrderState((int)$id_order_state))) {
                    $this->post_errors[] = $this->l('Invalid order status id #') . $id_order_state;
                    continue;
                }

                Db::getInstance()->Execute(
                    'INSERT INTO `' . _DB_PREFIX_ . 'invoice_payment_status_settings` (`id_order_state`)
                    VALUES (' . (int)$id_order_state . ')'
                );
            }
        }
    }


    public static function getValidStatuses()
    {
        $prepared_statuses = array();

        $statuses = Db::getInstance()->ExecuteS('SELECT * FROM `' . _DB_PREFIX_ . 'invoice_payment_status_settings`');

        if ($statuses && count($statuses)) {
            foreach ($statuses as $status) {
                array_push($prepared_statuses, $status['id_order_state']);
            }
        }

        return $prepared_statuses;
    }

    private function getModuleLink()
    {
        return Context::getContext()->link->getAdminLink('AdminModules', true) . '&configure=' . $this->name .
            '&tab_module=' . $this->tab . '&module_name=' . $this->name;
    }

    public function getGroups($id_lang)
    {
        $module = Module::getInstanceByName($this->name);
        $shop_id = $this->context->shop->id;

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS(
            'SELECT DISTINCT g.`id_group`, g.`reduction`, g.`price_display_method`, gl.`name`
            FROM `' . _DB_PREFIX_ . 'group` g
            LEFT JOIN `' . _DB_PREFIX_ . 'group_lang` AS gl
                ON (g.`id_group` = gl.`id_group` AND gl.`id_lang` = ' . (int)$id_lang . ')
            LEFT JOIN `' . _DB_PREFIX_ . 'module_group` AS mg ON (g.`id_group` = mg.`id_group`)
            WHERE mg.`id_module` = ' . (int)$module->id . ' AND mg.`id_shop` = ' . (int)$shop_id . '
            ORDER BY g.`id_group` ASC'
        );
    }

    public function checkGroups($groups)
    {
        foreach ($groups as $group) {
            $sett_group = $this->getGroupsSettings($group['id_group']);
            if (!count($sett_group)) {
                $this->post_warnings[] = $this->l('Invalid values provided for group ') . $group['name'];
            }
        }
        $this->outputWarnings();
    }

    public function getContent()
    {
        $default_language = (int)$this->context->language->id;
        $groups = $this->getGroups($default_language);
        $order_states = OrderState::getOrderStates($default_language);
        $languages = Language::getLanguages();


        $this->postProcess();

        $this->checkGroups($groups);

        $textarea_val = array();

        foreach ($languages as $language) {
            $textarea_val[$language['id_lang']] = Tools::htmlentitiesUTF8(Tools::getValue(
                'payment_info_' . $language['id_lang'],
                Configuration::get('INVOICE_PAYMENT_INFO', $language['id_lang'])
            ));
        }

        $countries = Country::getCountries($default_language);
        $all_groups = Group::getGroups($default_language);
        $permissions = $this->getAllPermission();

        $this->context->smarty->assign(array(
            'display_name' => $this->displayName,
            'mail_os' => (int)Configuration::get('PS_MAIL_DELAY_OS'),
            'mail_delay' => Configuration::get('PS_MAIL_DELAY_DAYS'),
            'mail_send' => (int)Configuration::get('PS_MAIL_SEND_IP'),
            'default_os' => Configuration::get('PS_DEFAULT_STATUS_ORDER_IP') ? (int)Configuration::get('PS_DEFAULT_STATUS_ORDER_IP') : (int)Configuration::get('PS_OS_PREPARATION'),
            'cancel_os' => (int)Configuration::get('PS_CANCEL_STATUS_ORDER_IP'),
            'action' => $this->getModuleLink(),
            'group_values' => count($groups) ? self::getGroupsSettings() : array(),
            'groups' => $groups,
            'languages' => $languages,
            'order_states' => $order_states,
            'valid_statuses' => count($order_states) ? self::getValidStatuses() : '',
            'cron_link' => Tools::getShopDomain(true, true) . __PS_BASE_URI__ . 'modules/invoicepayment/cron.php',
            'textarea_val' => $textarea_val,
            'default_language' => $default_language,
            'all_groups' => $all_groups,
            'countries' => $countries,
            'permissions' => $permissions
        ));

        return $this->display(__FILE__, 'configurator.tpl');
    }

    private function getAllPermission()
    {
        $result = DB::getInstance()->executeS('SELECT * FROM `' . _DB_PREFIX_ . 'invoice_payment_permissions` WHERE `id_shop`=' . (int)$this->context->shop->id);
        $format_result = array();

        foreach ($result as $res) {
            $format_result[$res['id_country']][] = $res['id_group'];
        }
        return $format_result;
    }

    private function updatePermissions()
    {
        try {
            $all_groups = Group::getGroups((int)$this->context->language->id);
            Db::getInstance()->delete('invoice_payment_permissions', 'id_shop=' . $this->context->shop->id);

            foreach ($all_groups as $group) {
                foreach (Tools::getValue('permission_group_' . $group['id_group']) as $permission) {
                    Db::getInstance()->insert(
                        'invoice_payment_permissions',
                        array(
                            'id_country' => (int)$permission,
                            'id_group' => $group['id_group'],
                            'id_shop' => (int)$this->context->shop->id,
                        )
                    );
                }
            }
        } catch (Exception $e) {
            $this->post_errors[] = $this->l('Error updating permissions: ') . $e->getMessage();
        }
    }


    private function postProcess()
    {
        $languages = Language::getLanguages();

        if (Tools::isSubmit('btnSaveBill')) {
            $delay_os = Tools::getValue('mail_status');
            $default_os = Tools::getValue('default_status');
            $cancel_os = Tools::getValue('cancel_status');
            $delay_days = Tools::getValue('mail_delay');
            $mail_send = Tools::getValue('mail_send');
            $payment_info = array();

            $this->updatePermissions();

            foreach ($languages as $language) {
                $payment_info[$language['id_lang']] = Tools::getValue('payment_info_' . $language['id_lang']);
            }

            Configuration::updateValue('INVOICE_PAYMENT_INFO', $payment_info);

            if ($delay_days && !Validate::isInt($delay_days)) {
                $this->post_errors[] = $this->l('"Mail delay" - incorrect value');
            }

            if ($delay_os && !Validate::isUnsignedId($delay_os)) {
                $this->post_errors[] = $this->l('"Reminder status" - incorrect value');
            }

            if ($default_os && !Validate::isUnsignedId($default_os)) {
                $this->post_errors[] = $this->l('"Default status" - incorrect value');
            }

            if ($mail_send && !Validate::isUnsignedId($mail_send)) {
                $this->post_errors[] = $this->l('"Send mail" - incorrect value');
            }

            if (!count($this->post_errors)) {
                $this->updateGroups(
                    Tools::getValue('groupDays'),
                    Tools::getValue('groupOrderLimit'),
                    Tools::getValue('groupPaidOrders'),
                    Tools::getValue('groupUnpaidOrders'),
                    Tools::getValue('groupOutOrders')
                );
            }

            if (!count($this->post_errors)) {
                $this->updateStatuses(Tools::getValue('orderStates'));
            }

            if (count($this->post_errors)) {
                $this->outputErrors();
            } else {
                Configuration::updateValue('PS_MAIL_DELAY_DAYS', (int)$delay_days);
                Configuration::updateValue('PS_MAIL_DELAY_OS', (int)$delay_os);
                Configuration::updateValue('PS_DEFAULT_STATUS_ORDER_IP', (int)$default_os);
                Configuration::updateValue('PS_CANCEL_STATUS_ORDER_IP', (int)$cancel_os);
                Configuration::updateValue('PS_MAIL_SEND_IP', (int)$mail_send);

                Tools::redirectAdmin($this->getModuleLink() . '&conf=6');
            }
        }
    }

    public function checkCart(&$cart)
    {
        $currency_order = new Currency((int)$cart->id_currency);
        $currencies_module = $this->getCurrency((int)$cart->id_currency);
        $currency_ok = false;

        if (!is_array($currencies_module)) {
            return false;
        }

        foreach ($currencies_module as $currency_module) {
            if ($currency_order->id == $currency_module['id_currency']) {
                $currency_ok = true;
                break;
            }
        }

        if (!$currency_ok) {
            return false;
        }
        $customer = new Customer($cart->id_customer);
        $customer_group = $customer->id_default_group;
        $group_settings = $this->getGroupsSettings($customer_group);

        $bill_sum = 0;
        if (isset($group_settings[$customer_group])) {
            $bill_sum = $group_settings[$customer_group]['limit'];
        }

        // $orders = Order::getCustomerOrders($customer->id, true);
        $orders = $this->preparedOrders(Order::getCustomerOrders($customer->id, true));

        $orders_ok = $orders_no_ok = array();
        $summ = $summ_out = 0;
        foreach ($orders as $order) {
            if ($order['valid'] == 1) {
                $orders_ok[] = $order;
                $summ += (float)$order['total_paid'];
            } else {
                $obj_order = new Order($order['id_order']);
                if ($obj_order->module == $this->name) {
                    $orders_no_ok[] = $order;
                    $summ_out += (float)$order['total_paid'];
                }
            }
        }

        if (isset($cart)
            && ($cart->getOrderTotal(true, Cart::BOTH) <= $bill_sum || $bill_sum == 0)
            && count($orders_ok) >= $group_settings[$customer_group]['paid']
            && (count($orders_no_ok) < $group_settings[$customer_group]['unpaid']
                || $group_settings[$customer_group]['unpaid'] == 0)
            && ($summ_out <= $group_settings[$customer_group]['out']
                || $group_settings[$customer_group]['out'] == 0)) {
            return true;
        }

        return false;
    }

    public function preparedOrders($orders)
    {
        $result = array();
        $cancel = (int)Configuration::get('PS_CANCEL_STATUS_ORDER_IP');

        foreach ($orders as $order) {
            if ($cancel && $cancel != 0) {
                $states = Db::getInstance()->executeS(
                    'SELECT *
                    FROM `' . _DB_PREFIX_ . 'order_history`
                    WHERE id_order = ' . (int)$order['id_order'] . ' AND id_order_state = ' . (int)$cancel
                );
                if (count($states) == 0) {
                    $result[] = $order;
                }
            } else {
                $result[] = $order;
            }
        }

        return $result;
    }

    private function displayConf($conf)
    {
        return '<div class="conf">
            <img src="../img/admin/ok2.png" alt="" /> ' . $conf . '
        </div>';
    }

    private function outputErrors()
    {
        if (count($this->post_errors)) {
            foreach ($this->post_errors as $error) {
                $this->html_content .= parent::displayError($error);
            }
        }
    }

    private function outputWarnings()
    {
        if (count($this->post_warnings)) {
            foreach ($this->post_warnings as $error) {
                $this->html_content .= '
                <div class="bootstrap">
                <div class="module_error alert alert-warning" >
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    ' . $error . '
                </div>
                </div>';
            }
        }
    }

    public function setReminders()
    {
        $delay = (int)Configuration::get('PS_MAIL_DELAY_DAYS');
        $id_order_state = (int)Configuration::get('PS_MAIL_DELAY_OS');
        $cancel = Configuration::get('PS_CANCEL_STATUS_ORDER_IP');

        if (!$id_order_state) {
            return false;
        }

        $group_delays = self::getGroupsSettings();

        $longest_delay = 0;

        foreach ($group_delays as $days_amount) {
            if ($days_amount['day'] > $longest_delay) {
                $longest_delay = $days_amount['day'];
            }
        }

        $days_delay = $longest_delay + $delay;
        if ($days_delay < 0) {
            $days_delay = 0;
        }

        $order_list = Db::getInstance()->ExecuteS(
            'SELECT `id_order`  FROM `' . _DB_PREFIX_ . 'invoice_payment_order`
            WHERE  DATEDIFF(NOW(),  `date_add`) >= ' . $days_delay . '
            AND `is_paid` = 0 AND `is_reminded` = 0'
        );

        if ($order_list && count($order_list)) {
            $languages = Language::getLanguages(true);

            foreach ($order_list as $o) {
                $order = new Order($o['id_order']);

                if (Validate::isLoadedObject($order)
                    && Validate::isLoadedObject($customer = new Customer($order->id_customer))
                    && !count($order->getHistory($order->id_lang, $cancel))) {
                    if (!array_key_exists($customer->id_default_group, $group_delays)) {
                        continue;
                    }

                    $order_lang = false;

                    foreach ($languages as $language) {
                        if ($language['id_lang'] == $order->id_lang) {
                            $order_lang = $language;
                        }
                    }

                    if (!$order_lang) {
                        continue;
                    }

                    $customer_delay = $group_delays[$customer->id_default_group]['day'] + $delay;
                    if ($customer_delay < 0) {
                        $customer_delay = 0;
                    }

                    if (strtotime($order->date_add) <= strtotime('-' . $customer_delay . ' days')) {
                        $new_history = new OrderHistory();
                        $new_history->id_order = (int)$order->id;
                        $new_history->changeIdOrderState((int)$id_order_state, $order, true);
                        $new_history->add(true);
                    }
                }
            }
        }
    }

    public function hookActionObjectOrderAddAfter($params)
    {
        $order = $params['object'];

        if ($order->module == $this->name) {
            Db::getInstance()->insert('invoice_payment_order', array(
                'id_order' => $order->id,
                'date_add' => $order->date_add
            ));
        }
    }

    public function hookPostUpdateOrderStatus($params)
    {
        $order_state = $params['newOrderStatus'];
        $id_order = $params['id_order'];

        $order = new Order((int)$id_order);
        $invoice_payment_data = Db::getInstance()->getRow(
            'SELECT * FROM `' . _DB_PREFIX_ .
            'invoice_payment_order` WHERE `id_order` = ' . (int)$order->id
        );

        if (Validate::isLoadedObject($order) && ($order->module == $this->name || !empty($invoice_payment_data))) {
            if ($order_state->id == Configuration::get('PS_MAIL_DELAY_OS')) {
                if ($invoice_payment_data['is_paid'] == 0) {
                    $customer = new Customer($order->id_customer);

                    $data = array(
                        '{firstname}' => $customer->firstname,
                        '{lastname}' => $customer->lastname,
                        '{order_reference}' => $order->getUniqReference(),
                        '{order_date}' => Tools::displayDate($order->date_add),
                        '{id_order}' => $order->id
                    );

                    $file_attachment = null;
                    if ($order->invoice_number) {
                        $pdf = new PDF($order->getInvoicesCollection(), PDF::TEMPLATE_INVOICE, $this->context->smarty);
                        $file_attachment['content'] = $pdf->render(false);
                        $file_attachment['name'] = Configuration::get(
                                'PS_INVOICE_PREFIX',
                                (int)$order->id_lang,
                                null,
                                $order->id_shop
                            ) . sprintf('%06d', $order->invoice_number) . '.pdf';
                        $file_attachment['mime'] = 'application/pdf';
                    }

                    Mail::Send(
                        (int)$order->id_lang,
                        'reminder',
                        $this->l('Payment reminder'),
                        $data,
                        $customer->email,
                        $customer->firstname . ' ' . $customer->lastname,
                        null,
                        null,
                        $file_attachment,
                        null,
                        dirname(__FILE__) . '/mails/',
                        false,
                        (int)$order->id_shop
                    );

                    Db::getInstance()->update(
                        'invoice_payment_order',
                        array('is_reminded' => 1),
                        '`id_order` = ' . (int)$order->id
                    );
                }
            } elseif (($order_state->id == Configuration::get('PS_OS_INVOICE_PAYMENT')
                    || $order_state->id == Configuration::get('PS_DEFAULT_STATUS_ORDER_IP'))
                && Configuration::get('PS_MAIL_SEND_IP') == 1) {
                $customer = new Customer($order->id_customer);
                $data = array(
                    '{firstname}' => $customer->firstname,
                    '{lastname}' => $customer->lastname,
                    '{order_name}' => $order->getUniqReference(),
                    '{payment_detail}' => nl2br(Configuration::get('INVOICE_PAYMENT_INFO', $this->context->language->id))
                );

                $file_attachment = null;
                if ($order->invoice_number) {
                    $pdf = new PDF($order->getInvoicesCollection(), PDF::TEMPLATE_INVOICE, $this->context->smarty);
                    $file_attachment['content'] = $pdf->render(false);
                    $file_attachment['name'] = Configuration::get(
                            'PS_INVOICE_PREFIX',
                            (int)$order->id_lang,
                            null,
                            $order->id_shop
                        ) . sprintf('%06d', $order->invoice_number) . '.pdf';
                    $file_attachment['mime'] = 'application/pdf';
                }

                Mail::Send(
                    (int)$order->id_lang,
                    'waiting_invoice',
                    $this->l('Waiting for payment by invoice'),
                    $data,
                    $customer->email,
                    $customer->firstname . ' ' . $customer->lastname,
                    null,
                    null,
                    $file_attachment,
                    null,
                    dirname(__FILE__) . '/mails/',
                    false,
                    (int)$order->id_shop
                );
            } else {
                if ($order_state->paid) {
                    Db::getInstance()->update(
                        'invoice_payment_order',
                        array('is_paid' => 1),
                        '`id_order` = ' . (int)$order->id
                    );
                } else {
                    Db::getInstance()->update(
                        'invoice_payment_order',
                        array('is_paid' => 0),
                        '`id_order` = ' . (int)$order->id
                    );
                }
            }
        }
    }
}
