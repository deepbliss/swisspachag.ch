
[{shop_url}] 

Moi, {firstname} {lastname}, 

käsittelemme tilaustasi {order_name}.

Payment detail:
{payment_detail}

Voit tarkistaa tilauksen tiedot ja ladata laskun "tilaushistoria"
 [{history_url}] -kohdasta, kun olet kirjautunut tilillesi
[{my_account_url}] kaupassa. 

Jos olet käyttänyt vierastiliä, voit seurata tilaustasi tämän linkin kautta
[{guest_tracking_url}?id_order={order_name}]

{shop_name} [{shop_url}] powered by
PrestaShop(tm) [http://www.prestashop.com/] 

