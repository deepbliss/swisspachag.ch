<?php
/**
 * invoicepayment
 *
 * @author    silbersaiten <info@silbersaiten.de>
 * @copyright 2018 silbersaiten
 * @license   See joined file licence.txt
 * @category  Module
 * @support   silbersaiten <support@silbersaiten.de>
 * @version   3.0.5
 * @link      http://www.silbersaiten.de
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_3_0_5($object)
{
    $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'invoice_payment_permissions` (
          `id_invoice_payment_permission` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `id_country` INT UNSIGNED NOT NULL,
          `id_group` INT(10) UNSIGNED NOT NULL,
          `id_shop`  INT(11) UNSIGNED DEFAULT \'1\' NOT NULL,
          PRIMARY KEY  (`id_invoice_payment_permission`))';

    // Set default permissions, as in for this module
    $current_permissions = Db::getInstance()->executeS(
        'SELECT mc.`id_country`, mc.`id_shop`, mg.`id_group` FROM `' . _DB_PREFIX_ . 'module_country` AS mc
        LEFT JOIN `' . _DB_PREFIX_ . 'module_group` AS mg ON mg.`id_module`=mc.`id_module` AND mg.`id_shop`=mc.`id_shop`
        WHERE mc.`id_module`=' . $object->id
    );

    return $object->registerHook('displayBackOfficeHeader') &&
        Db::getInstance()->execute($sql) &&
        Db::getInstance()->insert('invoice_payment_permissions', $current_permissions);
}
