<?php
/**
 * invoicepayment
 *
 * @author    silbersaiten <info@silbersaiten.de>
 * @copyright 2016 silbersaiten
 * @license   See joined file licence.txt
 * @category  Module
 * @support   silbersaiten <support@silbersaiten.de>
 * @version   2.3.3
 * @link      http://www.silbersaiten.de
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_3_3($object)
{
    unset($object);
    return Db::getInstance()->execute(
        'ALTER TABLE `'._DB_PREFIX_.'invoice_payment_group_settings`
        ADD `out_bal` decimal(20,6) NOT NULL DEFAULT \'0.000000\' AFTER `unpaid_orders`'
    );
}
