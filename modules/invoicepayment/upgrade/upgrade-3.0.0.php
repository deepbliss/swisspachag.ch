<?php
/**
 * invoicepayment
 *
 * @author    silbersaiten <info@silbersaiten.de>
 * @copyright 2016 silbersaiten
 * @license   See joined file licence.txt
 * @category  Module
 * @support   silbersaiten <support@silbersaiten.de>
 * @version   3.0.0
 * @link      http://www.silbersaiten.de
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_3_0_0($module)
{
    $result = true;
    $hook_to_remove_ids = array(
        Hook::getIdByName('displayPaymentEU'),
        Hook::getIdByName('payment'),
    );

    foreach ($hook_to_remove_ids as $hook_to_remove_id) {
        $result &= $module->unregisterHook((int)$hook_to_remove_id);
    }

    $result &= $module->registerHook('paymentOptions');

    return $result;
}
