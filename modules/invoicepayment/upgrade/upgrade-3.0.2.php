<?php
/**
 * invoicepayment
 *
 * @author    silbersaiten <info@silbersaiten.de>
 * @copyright 2017 silbersaiten
 * @license   See joined file licence.txt
 * @category  Module
 * @support   silbersaiten <support@silbersaiten.de>
 * @version   3.0.2
 * @link      http://www.silbersaiten.de
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_3_0_2($object)
{
    unset($object);
    return Configuration::updateValue('PS_MAIL_SEND_IP', 1);
}
