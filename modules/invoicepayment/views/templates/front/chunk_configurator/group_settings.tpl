{**
* invoicepayment
*
* @category  Module
* @author    silbersaiten <info@silbersaiten.de>
* @support   silbersaiten <support@silbersaiten.de>
* @copyright 2018 silbersaiten
* @version   3.0.4
* @link      http://www.silbersaiten.de
* @license   See joined file licence.txt
*}
<div class="panel col-lg-12">
    <div class="panel-heading">
        <i class="icon-cogs"></i>
        {l s='Group settings' mod='invoicepayment'}
    </div>
    <div class="panel-body">
        <div class="table-responsive">
                <span id="helpBlock" class="help-block">
                    {l s='Please specify payment due for different customer groups.' mod='invoicepayment'}
                </span>
            <div class="form-group ">
                <label class="control-label col-lg-3"></label>
                <div class="col-lg-1" title="{l s='Days' mod='invoicepayment'}">
                    <strong>{l s='Days' mod='invoicepayment'}</strong>
                </div>
                <div class="col-lg-1" title="{l s='Order limit' mod='invoicepayment'}">
                    <strong>{l s='Order limit' mod='invoicepayment'}</strong>
                </div>
                <div class="col-lg-1" title="{l s='Paid orders' mod='invoicepayment'}">
                    <strong>{l s='Paid orders' mod='invoicepayment'}</strong>
                </div>
                <div class="col-lg-1" title="{l s='Unpaid orders' mod='invoicepayment'}">
                    <strong>{l s='Unpaid orders' mod='invoicepayment'}</strong>
                </div>
                <div class="col-lg-2" title="{l s='Outstanding ballance' mod='invoicepayment'}">
                    <strong>{l s='Outstanding ballance' mod='invoicepayment'}</strong>
                </div>
            </div>
        </div>
        {foreach $groups as $group}
            <div class="form-group ">
                <label class="control-label col-lg-3">{$group.name}</label>
                <div class="col-lg-1" title="{l s='Days' mod='invoicepayment'}">
                    <input type="text" size="3" name="groupDays[{$group.id_group}]"
                           placeholder="{l s='Days' mod='invoicepayment'}"
                           value="{if isset($group_values[$group.id_group].day)}{$group_values[$group.id_group].day}{/if}"/>
                </div>
                <div class=" col-lg-1" title="{l s='Order limit' mod='invoicepayment'}">
                    <input type="text" size="3" name="groupOrderLimit[{$group['id_group']}]"
                           placeholder="{l s='Order limit' mod='invoicepayment'}"
                           value="{if isset($group_values[$group.id_group].limit)}{$group_values[$group.id_group].limit}{/if}"/>
                </div>
                <div class="col-lg-1" title="{l s='Paid orders' mod='invoicepayment'}">
                    <input type="text" size="3" name="groupPaidOrders[{$group['id_group']}]"
                           placeholder="{l s='Paid orders' mod='invoicepayment'}"
                           value="{if isset($group_values[$group.id_group].paid)}{$group_values[$group.id_group].paid}{/if}"/>
                </div>
                <div class="col-lg-1" title="{l s='Unpaid orders' mod='invoicepayment'}">
                    <input type="text" size="3" name="groupUnpaidOrders[{$group['id_group']}]"
                           placeholder="{l s='Unpaid orders' mod='invoicepayment'}"
                           value="{if isset($group_values[$group.id_group].unpaid)}{$group_values[$group.id_group].unpaid}{/if}"/>
                </div>
                <div class="col-lg-2" title="{l s='Outstanding ballance' mod='invoicepayment'}">
                    <input type="text" size="3" name="groupOutOrders[{$group['id_group']}]"
                           placeholder="{l s='Outstanding ballance' mod='invoicepayment'}"
                           value="{if isset($group_values[$group.id_group].out)}{$group_values[$group.id_group].out}{/if}"/>
                </div>
            </div>
        {/foreach}
        <div class="col-lg-offset-3">
                    <span id="helpBlock" class="help-block">
                        {l s='Days: The number of days during which you must pay for your order.' mod='invoicepayment'}
                    </span>
            <span id="helpBlock" class="help-block">
                        {l s='Order Limit: enter the order Limit for a amount for a customer group to have the invoicepayment method activated.' mod='invoicepayment'}
                    </span>
            <span id="helpBlock" class="help-block">
                        {l s='Paid orders: here you can specify how many paid orders must the customer already have to have the invoicepayment activated in his payment methods (only for orders paid via Invoice Payment).' mod='invoicepayment'}
                    </span>
            <span id="helpBlock" class="help-block">
                        {l s='Unpaid orders: here you can specify the number of unpaid orders (maximum) can be the customer that it was available payment method (only for orders paid via Invoice Payment).' mod='invoicepayment'}
                    </span>
            <span id="helpBlock" class="help-block">
                        {l s='Outstanding balance: the maximum amount of unpaid orders can be the customer that it was available payment method. (only for orders paid via Invoice Payment).' mod='invoicepayment'}
                    </span>
        </div>
    </div>
</div>
