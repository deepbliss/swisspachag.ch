{**
* invoicepayment
*
* @category  Module
* @author    silbersaiten <info@silbersaiten.de>
* @support   silbersaiten <support@silbersaiten.de>
* @copyright 2018 silbersaiten
* @version   3.0.4
* @link      http://www.silbersaiten.de
* @license   See joined file licence.txt
*}
<div class="form-group ">
    <label class="control-label col-lg-3">{l s='Order states' mod='invoicepayment'}</label>
    <div class="col-lg-3 ">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th>{l s='Name' mod='invoicepayment'}</th>
                </tr>
            </thead>
            <tbody>
            {foreach $order_states as $order_state}
                <tr>
                    <td>
                        <input type="checkbox"
                               name="orderStates[{$order_state['id_order_state']}]"
                                {if in_array($order_state['id_order_state'], $valid_statuses)} checked="checked" {/if}
                        />
                    </td>
                    <td>
                        <span style="display: block; margin: 3px; width: 24px; box-shadow: 0 0 3px rgba(0,0,0,0.3);
                                    height: 24px; border-radius: 3px; padding: 3px; border: 1px solid #eee; background: #fff;">
                            <span style="display: block; width: 100%; height: 100%; box-shadow: 0 0 3px rgba(0,0,0,0.2);
                                    border-radius: 3px; background-color: {$order_state['color']};"></span>
                        </span>
                    </td>
                    <td>{$order_state['name']}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
        <span id="helpBlock" class="help-block">
            {l s='Please select your status regarding to the payment deadline calculation. The calculation starts on the day the order reaches one of these statuses.' mod='invoicepayment'}
        </span>
    </div>
</div>
