{**
* invoicepayment
*
* @category  Module
* @author    silbersaiten <info@silbersaiten.de>
* @support   silbersaiten <support@silbersaiten.de>
* @copyright 2018 silbersaiten
* @version   3.0.4
* @link      http://www.silbersaiten.de
* @license   See joined file licence.txt
*}
<div class="form-group">
    <label class="control-label col-lg-3">
        {l s='Send e-mail' mod='invoicepayment'}
    </label>
    <div class="col-lg-9">
        <span class="switch prestashop-switch fixed-width-lg">
            <input type="radio" name="mail_send" id="mail_send_on" value="1" {if $mail_send == 1}checked="checked"{/if}>
            <label for="mail_send_on" class="radioCheck">
                {l s='Yes' mod='invoicepayment'}
            </label>
            <input type="radio" name="mail_send" id="mail_send_off" value="0" {if $mail_send == 0}checked="checked"{/if}>
            <label for="mail_send_off" class="radioCheck">
                {l s='No' mod='invoicepayment'}
            </label>
            <a class="slide-button btn"></a>
        </span>
        <span id="helpBlock" class="help-block">
            {l s='Activate this option so that your customers will receive an additional email with the payment information and a payment request after they have placed an order.' mod='invoicepayment'}
        </span>
    </div>
</div>
