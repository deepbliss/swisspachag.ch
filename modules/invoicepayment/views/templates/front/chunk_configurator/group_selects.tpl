{**
* invoicepayment
*
* @category  Module
* @author    silbersaiten <info@silbersaiten.de>
* @support   silbersaiten <support@silbersaiten.de>
* @copyright 2018 silbersaiten
* @version   3.0.6
* @link      http://www.silbersaiten.de
* @license   See joined file licence.txt
*}
<div class="form-group ">
    <label class="control-label col-lg-3">
        {l s='Mail delay' mod='invoicepayment'}
    </label>
    <div class="col-lg-9 ">
        <input type="text" name="mail_delay" value="{$mail_delay}"/>
        <span id="helpBlock" class="help-block">
                <p>
                    {l s='After a "payment due" date is reached, if the payment is still not received in the specified number of days, a reminder will be sent to customer.' mod='invoicepayment'}
                </p>
                <p>
                    {l s='Place this URL in crontab or call it manually daily:' mod='invoicepayment'}
                    </br>
                    <b>{$cron_link}</b>
                </p>
            </span>
    </div>
</div>
<div class="form-group ">
    <label class="control-label col-lg-3">
        {l s='Reminder status' mod='invoicepayment'}
    </label>
    <div class="col-lg-9 ">
        <select name="mail_status">
            <option value="0" {if $mail_os == 0} selected="selected" {/if}>
                {l s='None' mod='invoicepayment'}
            </option>
            {foreach $order_states as $order_state}
                <option value="{$order_state['id_order_state']}"
                        {if $mail_os == $order_state['id_order_state']}selected="selected"{/if}
                >
                    {$order_state['name']}
                </option>
            {/foreach}
        </select>
        <span id="helpBlock" class="help-block">
            {l s='When a reminder is sent, the order status will be changed to this one.' mod='invoicepayment'}
            {l s='If you choose "none", the reminders will still be sent, but the order status will not be changed.' mod='invoicepayment'}
        </span>
    </div>
</div>
<div class="form-group ">
    <label class="control-label col-lg-3">
        {l s='Default order status' mod='invoicepayment'}
    </label>
    <div class="col-lg-9">
        <select name="default_status">
            {foreach $order_states as $order_state}
                <option value="{$order_state['id_order_state']}"
                        {if $default_os == $order_state['id_order_state']} selected="selected" {/if}
                >
                    {$order_state['name']}
                </option>
            {/foreach}
        </select>
        <span id="helpBlock" class="help-block">
            {l s='Orders will be created with the selected status.' mod='invoicepayment'}
        </span>
    </div>
</div>
<div class="form-group ">
    <label class="control-label col-lg-3">
        {l s='Status for canceled orders' mod='invoicepayment'}
    </label>
    <div class="col-lg-9 ">
        <select name="cancel_status">
            <option value="0"
                    {if $cancel_os == 0}selected="selected"{/if}
            >
                {l s='None' mod='invoicepayment'}
            </option>
            {foreach $order_states as $order_state}
                <option value="{$order_state.id_order_state}"
                        {if $cancel_os == $order_state.id_order_state} selected="selected" {/if}
                >
                    {$order_state.name}
                </option>
            {/foreach}
        </select>
        <span id="helpBlock" class="help-block">
            {l s='Orders that are assigned to this status will not be included in the rules above. If you select "None", all orders will be included in the rules.' mod='invoicepayment'}
        </span>
    </div>
</div>
