{**
* invoicepayment
*
* @category  Module
* @author    silbersaiten <info@silbersaiten.de>
* @support   silbersaiten <support@silbersaiten.de>
* @copyright 2018 silbersaiten
* @version   3.0.4
* @link      http://www.silbersaiten.de
* @license   See joined file licence.txt
*}
<div class="form-group ">
    <div class="col-lg-3 text-right">
        <label class="control-label">{l s='Payment information' mod='invoicepayment'}</label>
    </div>
    <div class="col-lg-9">
        {foreach $languages as $language}
            <div class="row translatable-field lang-{$language.id_lang}"
                    {if $language['id_lang'] != $default_language} style="display:none;"{/if}
            >
                <div class="col-lg-4">
                    <textarea name="payment_info_{$language.id_lang}"
                              id="payment_info_{$language.id_lang}" cols="60" rows="10"
                              class="rte autoload_rte textarea-autosize">{$textarea_val[$language.id_lang]}</textarea>
                </div>
                {if count($languages) > 1}
                    <div class="col-lg-2">
                        <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1"
                                data-toggle="dropdown">
                            {$language.iso_code}
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            {foreach $languages as $language}
                                <li>
                                    <a href="javascript:hideOtherLanguage({$language.id_lang});"
                                       tabindex="-1">{$language.name}</a>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                {/if}
            </div>
        {/foreach}
    </div>
</div>
