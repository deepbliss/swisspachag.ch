{**
* invoicepayment
*
* @category  Module
* @author    silbersaiten <info@silbersaiten.de>
* @support   silbersaiten <support@silbersaiten.de>
* @copyright 2018 silbersaiten
* @version   3.0.5
* @link      http://www.silbersaiten.de
* @license   See joined file licence.txt
*}
<div class="panel col-lg-12">
    <div class="panel-heading">
        <i class="icon-cogs"></i>
        {l s='Advanced user group settings' mod='invoicepayment'}
    </div>
    <div class="panel-body">
        <div class="form-group">
            <div class="row">
                <div class="checker pull-left">
                    <span><input type="checkbox" id="show_all_countries" name="show_all_countries" value="1"></span>
                </div>
                <label for="show_all_countries">
                    {l s='Show all countries' mod='invoicepayment'}
                </label>
            </div>
            <label class="control-label col-lg-3">
                <p class="text-left">{l s='Select country' mod='invoicepayment'}</p>
                {foreach from=$countries key=key item=i name=country}
                    {if $smarty.foreach.country.first }
                        <select id="select_country">
                            {foreach $countries as $country}
                                <option value="{$country.id_country}">
                                    {$country.name}
                                </option>
                            {/foreach}
                        </select>
                    {/if}
                {/foreach}
            </label>
            <div class="col-lg-9 ">
                <table class="table" id="permissions_group">
                    <thead>
                    <tr>
                        <th><span class="title_box">{l s='Countries list' mod='invoicepayment'}</span></th>
                        {foreach $all_groups as $group}
                            <th class="text-center">
                                <a href="javascript:InvoicePaymentEditor.checkBoxes('permission_group_{$group.id_group}')">
                                    {$group.name}
                                </a>
                            </th>
                        {/foreach}
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $countries as $country}
                        <tr id="{$country.id_country}">
                            <td>
                                <span>
                                    {$country.name}
                                </span>
                            </td>
                            {foreach $all_groups as $group}
                                <td class="text-center">
                                    <input type="checkbox"
                                           name="permission_group_{$group.id_group}[]"
                                           value="{$country.id_country}"
                                            {if isset($permissions[$country.id_country]) && in_array($group.id_group , $permissions[$country.id_country])}
                                                checked="checked"
                                            {/if}
                                    >
                                </td>
                            {/foreach}
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
