{**
* invoicepayment
*
* @category  Module
* @author    silbersaiten <info@silbersaiten.de>
* @support   silbersaiten <support@silbersaiten.de>
* @copyright 2018 silbersaiten
* @version   3.0.4
* @link      http://www.silbersaiten.de
* @license   See joined file licence.txt
*}
{capture name=path}{l s='Bill payment.' mod='invoicepayment'}{/capture}
<h1 class="page-heading">{l s='Order summation' mod='invoicepayment'}</h1>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

<form action="{$path_validation|escape:'html':'UTF-8'}" method="post">
<div class="box cheque-box">
<h3 class="page-subheading">{l s='Bill payment' mod='invoicepayment'}</h3>
	<input type="hidden" name="confirm" value="1" />
	<p class="cheque-indent"><strong class="dark">{l s='You have chosen the billing method.' mod='invoicepayment'}</strong></p>
	<p>- {l s='The total amount of your order is' mod='invoicepayment'} <b id="amount_{$currencies.0.id_currency|escape:'htmlall':'UTF-8'}" class="price">{convertPrice price=$total}</b> {l s='(tax incl.)' mod='invoicepayment'}</p>
	<p>- {l s='Please confirm your order by clicking \'I confirm my order\'' mod='invoicepayment'}.</p>
</div>
    <p class="cart_navigation clearfix" id="cart_navigation">
        <a class="button-exclusive btn btn-default" href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}"><i class="icon-chevron-left"></i>{l s='Other payment methods' mod='invoicepayment'}</a>
        <button class="button btn btn-default button-medium"  type="submit"><span>{l s='I confirm my order' mod='invoicepayment'}<i class="icon-chevron-right right"></i></span></button>
    </p>
</form>
