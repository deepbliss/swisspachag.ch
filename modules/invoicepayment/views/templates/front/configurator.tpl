{**
* invoicepayment
*
* @category  Module
* @author    silbersaiten <info@silbersaiten.de>
* @support   silbersaiten <support@silbersaiten.de>
* @copyright 2018 silbersaiten
* @version   3.0.5
* @link      http://www.silbersaiten.de
* @license   See joined file licence.txt
*}
<h2>{$display_name}</h2>
<form action="{$action}" method="post" class="form-horizontal clearfix">
    <div class="panel col-lg-12">
        <div class="panel-heading">
            <i class="icon-cogs"></i>
            {l s='Configuration' mod='invoicepayment'}
        </div>
        <div class="panel-body">
            {*{if count($group_values)}*}
                {include file="./chunk_configurator/group_settings.tpl"}
            {*{/if}*}
            <div class="clearfix"></div>

            {include file="./chunk_configurator/payment_information.tpl"}

            {if count($order_states)}
                {include file="./chunk_configurator/order_states.tpl"}
            {/if}

            {include file="./chunk_configurator/group_selects.tpl"}
            {include file="./chunk_configurator/send_mail.tpl"}

            {include file="./chunk_configurator/user_advanced_settings.tpl"}
        </div>
        <div class="panel-footer">
            <button class="btn btn-default pull-right" name="btnSaveBill"
                    value="{l s='Save' mod='invoicepayment'}"
                    type="submit">
                <i class="process-icon-save"></i>
                {l s='Save' mod='invoicepayment'}
            </button>
        </div>
    </div>
</form>
