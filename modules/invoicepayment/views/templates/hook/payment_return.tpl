{**
* invoicepayment
*
* @category  Module
* @author    silbersaiten <info@silbersaiten.de>
* @support   silbersaiten <support@silbersaiten.de>
* @copyright 2016 silbersaiten
* @version   3.0.0
* @link      http://www.silbersaiten.de
* @license   See joined file licence.txt
*}

{if $status == 'ok'}
<div class="box cheque-box">
	<p class="cheque-indent">
		<strong class="dark">{l s='You have chosen the bill method.' mod='invoicepayment'}</strong>
	</p>
	<p>- {l s='Your order will be sent very soon.' mod='invoicepayment'}</p>
	<p>- {l s='For any questions or for further information, please contact our [1]customer support[/1].' mod='invoicepayment' tags=["<a href='{$contact_url}'>"]}</p>
</div>
{else}
<p class="warning">
	{l s='We noticed a problem with your order. If you think this is an error, feel free to contact our [1]expert customer support team[/1].' mod='invoicepayment' tags=["<a href='{$contact_url}'>"]}
</p>
{/if}