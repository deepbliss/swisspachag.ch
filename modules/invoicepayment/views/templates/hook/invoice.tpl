{**
* invoicepayment
*
* @category  Module
* @author    silbersaiten <info@silbersaiten.de>
* @support   silbersaiten <support@silbersaiten.de>
* @copyright 2018 silbersaiten
* @version   3.0.3
* @link      http://www.silbersaiten.de
* @license   See joined file licence.txt
*}
{if isset($shipping_date)}
<p>{l s='To be paid due' mod='invoicepayment'}: {$shipping_date}</p>
{/if}
{if isset($payment_info)}
<table style="border: solid 1pt black; padding:0 10pt">
	<tr><td></td></tr>
	<tr>
		<td>
			<h3>&nbsp;&nbsp;{l s='Payment info' mod='invoicepayment'}</h3>
		</td>
	</tr>
	<tr><td></td></tr>
	<tr>
		<td>
			<p>
                {foreach from=$payment_info item=payment}
                    {$payment}<br>
                {/foreach}
			{l s='Reference' mod='invoicepayment'}: {$order->reference}
			</p>
		</td>
	</tr>
	<tr><td></td></tr>
</table>
{/if}
