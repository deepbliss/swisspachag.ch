/**
 * invoicepayment
 *
 * @category  Module
 * @author    silbersaiten <info@silbersaiten.de>
 * @support   silbersaiten <support@silbersaiten.de>
 * @copyright 2018 silbersaiten
 * @version   3.0.5
 * @link      http://www.silbersaiten.de
 * @license   See joined file licence.txt
 */

var InvoicePaymentEditor = {
    init: function () {
        this.listeners();
    },
    listeners: function () {
        $('#show_all_countries').on('click', function () {
            if ($(this).is(':checked')) {
                $('#permissions_group tbody tr').show();
            } else {
                $('#permissions_group tbody tr').hide();
            }
        });
        $("#select_country").change(function () {
            $('#permissions_group tbody tr').hide();
            $('#' + $("#select_country option:selected").val()).show();
        });
    },
    checkBoxes: function (name) {
        var inputs = $('input[name="' + name + '[]"]');

        if (inputs.is(':checked')) {
            inputs.removeProp("checked");
        } else {
            inputs.attr('checked', 'checked');
        }
    }

};

$(function () {
    $(document).ready(function () {
        InvoicePaymentEditor.init();
    })
});
