# Invoicepayment 3.0.6
## copyright 2018 silbersaiten.de

## Changelog

#### 3.0.6 (31.10.2018):
* Minor fixes

#### 3.0.5 (14.09.2018):
* Added advanced user group settings
* The change in order statuses was corrected (for the module name in the order was changed)

#### 3.0.4 (30.07.2018):
* Added payment info to the waiting invoice template
* Added description to the send mail trigger

#### 3.0.3 (18.05.2018):
* Fix generate "Payment info" block (without cache)

#### 3.0.2 (19.09.2017):
* added FR language
* minor bug fixed
* minor improvements

#### 3.0.0 (24.11.2016):
* init for Prestashop 1.7

#### 2.3.4 (27.10.2016):
* bug fixes

#### 2.3.3 (29.09.2016):
* added field "Status for canceled orders"
* added settings "Outstanding balance" for groups
* change field "Payment information" (design)
* minor bug fixes

#### 2.3.2 (28.06.2016):
* added field "Order default status"

#### 2.3.1 (23.02.2016):
* major bug fixes
