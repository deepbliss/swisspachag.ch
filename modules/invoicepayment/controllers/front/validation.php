<?php
/**
 * invoicepayment
 *
 * @author    silbersaiten <info@silbersaiten.de>
 * @copyright 2016 silbersaiten
 * @license   See joined file licence.txt
 * @category  Module
 * @support   silbersaiten <support@silbersaiten.de>
 * @version   3.0.0
 * @link      http://www.silbersaiten.de
 */

class InvoicePaymentValidationModuleFrontController extends ModuleFrontController
{
    public $ssl = true;

    public function init()
    {
        $this->display_column_left = false;
        parent::init();
    }

    public function postProcess()
    {
        $cart = $this->context->cart;

        if ($cart->id_customer == 0
            || $cart->id_address_delivery == 0
            || $cart->id_address_invoice == 0
            || !$this->module->active) {
            Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');
        }

        // Check that this payment option is still available in case the customer
        // changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'invoicepayment') {
                $authorized = true;
                break;
            }
        }

        if (!$authorized) {
            die(Tools::displayError('This payment method is not available.'));
        }

        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer)) {
            Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');
        }

        $total = $cart->getOrderTotal(true, Cart::BOTH);
        $currency = $this->context->currency;

        $this->module->validateOrder(
            (int)$cart->id,
            (Configuration::get('PS_DEFAULT_STATUS_ORDER_IP')
                ? (int)Configuration::get('PS_DEFAULT_STATUS_ORDER_IP')
                : (int)Configuration::get('PS_OS_PREPARATION')),
            $total,
            $this->module->displayName,
            null,
            array(),
            (int)$currency->id,
            false,
            $customer->secure_key
        );
        Tools::redirectLink(
            __PS_BASE_URI__.'order-confirmation.php?key='.$customer->secure_key.'&id_cart='.
            (int)$cart->id.'&id_module='.
            (int)$this->module->id.'&id_order='.(int)$this->module->currentOrder
        );
    }
}
