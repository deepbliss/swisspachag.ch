<?php
class blocknewsadvfunctions extends Module{
	
	
	private $_width = 400;
	private $_height = 400;
	private $_name = 'blocknewsadv';
	
	
		public function getItemsBlock(){

			global $cookie;
			$current_language = (int)$cookie->id_lang;
			$id_shop = $this->getIdShop();
			//var_dump(Configuration::get($this->name.'number_ni'));
			$limit  = Configuration::get($this->_name.'number_ni');
			$sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'blocknewsadv` pc 
			LEFT JOIN `'._DB_PREFIX_.'blocknewsadv_data` pc_d
			ON(pc.id = pc_d.id_item) 
			WHERE pc.status = 1 and pc.id_shop = '.$id_shop.' 
			and pc_d.id_lang = '.$current_language.' ORDER BY pc.`id` DESC LIMIT '.$limit;
			
			$items = Db::getInstance()->ExecuteS($sql);
			$items_tmp = array();
			foreach($items as $k => $_item){
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'blocknewsadv_data` pc
				WHERE pc.id_item = '.$_item['id'].'
				');
				
				
				
				foreach ($items_data as $item_data){
		    		if($current_language == $item_data['id_lang']){
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['title'] = $item_data['title'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['content'] = $item_data['content'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['seo_url'] = $item_data['seo_url'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['img'] = $_item['img'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['time_add'] = $_item['time_add'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['id'] = $_item['id'];
		    		}
		    	}
		    	
			}
		return array('items' => $items_tmp );
	}
	
	
	public function getHomeLastNews($data){
		global $cookie;
			$current_language = (int)$cookie->id_lang;
			$id_shop = $this->getIdShop();
			
			$limit  = Configuration::get($this->_name.'hnumber_ni');
			$sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'blocknewsadv` pc 
			LEFT JOIN `'._DB_PREFIX_.'blocknewsadv_data` pc_d
			ON(pc.id = pc_d.id_item) 
			WHERE pc.status = 1 and pc.id_shop = '.$id_shop.' 
			and pc_d.id_lang = '.$current_language.' ORDER BY pc.`id` DESC LIMIT '.$limit;
			
			$items = Db::getInstance()->ExecuteS($sql);
			$items_tmp = array();
			foreach($items as $k => $_item){
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'blocknewsadv_data` pc
				WHERE pc.id_item = '.$_item['id'].'
				');
				
				
				
				foreach ($items_data as $item_data){
		    		if($current_language == $item_data['id_lang']){
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['title'] = $item_data['title'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['content'] = $item_data['content'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['seo_url'] = $item_data['seo_url'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['img'] = $_item['img'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['time_add'] = $_item['time_add'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['id'] = $_item['id'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['id_lang'] = $item_data['id_lang'];
		    		}
		    	}
		    	
			}
		return array('items' => $items_tmp );
		
		
	}
	
	public function getItems($_data = null){
		
		$id_shop = $this->getIdShop();
		$admin = isset($_data['admin'])?$_data['admin']:0;
		$start = isset($_data['start'])?$_data['start']:0;
		$step = isset($_data['step'])?$_data['step']:10;
		if($admin){
			
			$sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'blocknewsadv` pc
			WHERE pc.id_shop = '.$id_shop.'
			ORDER BY pc.`id` DESC
			LIMIT '.$start.' ,'.$step.'';
			$items = Db::getInstance()->ExecuteS($sql);
			
			
			
			foreach($items as $k => $_item){
				
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'blocknewsadv_data` pc
				WHERE pc.id_item = '.$_item['id'].'
				');
				
				global $cookie;
				$defaultLanguage =  $cookie->id_lang;
				
				$tmp_title = '';
				$tmp_link = '';
				
				foreach ($items_data as $item_data){
		    		
		    		$title = isset($item_data['title'])?$item_data['title']:'';
		    		if(strlen($tmp_title)==0){
		    			if(strlen($title)>0)
		    					$tmp_title = $title; 
		    		}
		    		
		    		
		    		if($defaultLanguage == $item_data['id_lang']){
		    			$items[$k]['title'] = $item_data['title'];
		    		} 
		    	}
		    	
		    	if(@strlen($items[$k]['title'])==0)
		    		$items[$k]['title'] = $tmp_title;
		    	
			}
			

			$data_count = Db::getInstance()->getRow('
			SELECT COUNT(`id`) AS "count"
			FROM `'._DB_PREFIX_.'blocknewsadv` pc where pc.id_shop = '.$id_shop.'
			');
			
		} else {
			$step = Configuration::get($this->_name.'news_page');
			
			global $cookie;
			$current_language = (int)$cookie->id_lang;
			
			$sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'blocknewsadv` pc 
			LEFT JOIN `'._DB_PREFIX_.'blocknewsadv_data` pc_d
			on(pc.id = pc_d.id_item)
			WHERE pc.status = 1 and pc.id_shop = '.$id_shop.' and pc_d.id_lang = '.$current_language.' 
			ORDER BY pc.`id` DESC
			LIMIT '.$start.' ,'.$step.'';
			$items_tmp = Db::getInstance()->ExecuteS($sql);
			
			$items = array();
			
			foreach($items_tmp as $k => $_item){
				
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'blocknewsadv_data` pc
				WHERE pc.id_item = '.$_item['id'].'
				');
				
				
				
				foreach ($items_data as $item_data){
		    		
		    		if($current_language == $item_data['id_lang']){
		    			$items[$k]['title'] = $item_data['title'];
		    			$items[$k]['content'] = $item_data['content'];
		    			$items[$k]['seo_description'] = $item_data['seo_description'];
		    			$items[$k]['seo_keywords'] = $item_data['seo_keywords'];
		    			$items[$k]['seo_url'] = $item_data['seo_url'];
		    			$items[$k]['id'] = $_item['id'];
		    			$items[$k]['img'] = $_item['img'];
		    			$items[$k]['time_add'] = $_item['time_add'];
		    		} 
		    	}
		    }
			

			$data_count = Db::getInstance()->getRow('
			SELECT COUNT(pc.`id`) AS "count"
			FROM `'._DB_PREFIX_.'blocknewsadv` pc LEFT JOIN `'._DB_PREFIX_.'blocknewsadv_data` pc_d
			on(pc.id = pc_d.id_item)
			WHERE pc.status = 1 and pc.id_shop = '.$id_shop.' and pc_d.id_lang = '.$current_language.' 
			');
			
		}
		return array('items' => $items, 'count_all' => $data_count['count']);
	}
	
private function  _translit( $str )
	{
    $str  = str_replace(array("'",'"','`','?','!','.','=',':','&','+',',','’', ')', '(', '$', '{', '}'), array(''), $str );
		
	$arrru = array (" ","-",",","«","»","+","/","(",")",".");

    $arren = array ("-","-","","","","","","","","");

	$textout = '';
    $textout = str_replace($arrru,$arren,$str);
    
    $textout = str_replace("--","-",$textout);
    return strtolower($textout);
	}
	
	public function saveItem($data){
	
		$error = 0;
		$error_text = '';
		$item_status = $data['item_status'];
			
		$id_shop = $this->getIdShop();
		
		$sql = 'INSERT into `'._DB_PREFIX_.'blocknewsadv` SET
							   `status` = \''.pSQL($item_status).'\',
							   `id_shop`= '.$id_shop.'
							   ';
		$result = Db::getInstance()->Execute($sql);
		
		$item_id = Db::getInstance()->Insert_ID();
		
		foreach($data['data_title_content_lang'] as $language => $item){
		
		$title = $item['title'];
		$content = $item['content'];
		$seokeywords = $item['seokeywords'];
	    $seodescription = $item['seodescription'];
	    $seo_url_pre = strlen($item['seo_url'])>0?$item['seo_url']:$title;
	    $translite_seo = $this->_translit($seo_url_pre);
		$seo_url = $translite_seo;
	   
		$sql = 'SELECT count(*) as count
				FROM `'._DB_PREFIX_.'blocknewsadv_data` pc
				WHERE seo_url = "'.mysql_escape_string($seo_url).'"';
		$data_seo_url = Db::getInstance()->GetRow($sql);
		if($data_seo_url['count']!=0)
			$seo_url = "news".strtolower(Tools::passwdGen(6));
		
		$sql = 'INSERT into `'._DB_PREFIX_.'blocknewsadv_data` SET
							   `id_item` = \''.pSQL($item_id).'\',
							   `id_lang` = \''.pSQL($language).'\',
							   `title` = \''.pSQL($title).'\',
							   `content` = "'.mysql_escape_string($content).'",
							   `seo_keywords` = "'.mysql_escape_string($seokeywords).'",
							   `seo_description` = "'.mysql_escape_string($seodescription).'",
							   `seo_url` = "'.mysql_escape_string($seo_url).'"
							   ';
		
		$result = Db::getInstance()->Execute($sql);
		
		}
		
		$data_save_img = $this->saveImage(array('post_id' => $item_id));
		
		if(!$error && strlen($error_text)==0){
			$error = $data_save_img['error'];
			$error_text  = $data_save_img['error'];
		}
		
		return array('error' => $error,
					 'error_text' => $error_text);
		
	}
	
	public function saveImage($data = null){
		
		$error = 0;
		$error_text = '';
		
		$post_id = $data['post_id'];
		$post_images = isset($data['post_images'])?$data['post_images']:'';
		
		$files = $_FILES['news_image'];
		
		############### files ###############################
		if(!empty($files['name']))
			{
		      if(!$files['error'])
		      {
				  $type_one = $files['type'];
				  $ext = explode("/",$type_one);
				  
				  if(strpos('_'.$type_one,'image')<1)
				  {
				  	$error_text = $this->l('Invalid file type, please try again!');
				  	$error = 1;

				  }elseif(!in_array($ext[1],array('png','x-png','gif','jpg','jpeg','pjpeg'))){
				  	$error_text = $this->l('Wrong file format, please try again!');
				  	$error = 1;
				  	
				  } else {
				  	
				  		$info_post = $this->getItem(array('id'=>$post_id));
				  		$post_item = $info_post['item'];
				  		$img_post = $post_item[0]['img'];
				  		
				  		if(strlen($img_post)>0){
				  			// delete old avatars
				  			$name_thumb = current(explode(".",$img_post));
				  			unlink(dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR."blocknewsadv".DIRECTORY_SEPARATOR.$name_thumb.".jpg");
				  			
				  		} 
							
					  	srand((double)microtime()*1000000);
					 	$uniq_name_image = uniqid(rand());
					 	$type_one = substr($type_one,6,strlen($type_one)-6);
					 	$filename = $uniq_name_image.'.'.$type_one; 
					 	
						move_uploaded_file($files['tmp_name'], dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR."blocknewsadv".DIRECTORY_SEPARATOR.$filename);
						
						$this->copyImage(array('dir_without_ext'=>dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR."blocknewsadv".DIRECTORY_SEPARATOR.$uniq_name_image,
												'name'=>dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR."blocknewsadv".DIRECTORY_SEPARATOR.$filename)
										);
						
						
						$img_return = $uniq_name_image.'.jpg';
			  		
				  		$this->_updateImgToItem(array('post_id' => $post_id,
				  									  'img' =>  $img_return
				  									  )
				  								);

				  }
				}
				else
					{
					### check  for errors ####
			      	switch($files['error'])
						{
							case '999':
							default:
								$error_text = $this->l('Error downloading file!');
							break;
						}
						$error = 1;
			      	########
					   
					}
			}  else {
				if($post_images != "on"){
				$this->_updateImgToItem(array('post_id' => $post_id,
				  							  'img' =>  ""
				  							  )
				  						);
				}
			}
		
			
		return array('error' => $error,
					 'error_text' => $error_text);
	
	
	}
	
	private function _updateImgToItem($data = null){
		
		$post_id = $data['post_id'];
		$img = $data['img'];
		
		$id_shop = $this->getIdShop();
			
		// update
		$sql = 'UPDATE `'._DB_PREFIX_.'blocknewsadv` SET
							   `img` = \''.pSQL($img).'\'
							   WHERE id = '.$post_id.' and id_shop = '.$id_shop.'
							   ';
		$result = Db::getInstance()->Execute($sql);
		
	}
	
	public function deleteItem($data){
		
		
		$id = $data['id'];
		$sql = 'DELETE FROM `'._DB_PREFIX_.'blocknewsadv`
					   WHERE id ='.$id.'';
			$result = Db::getInstance()->Execute($sql);
		
		$sql = 'DELETE FROM `'._DB_PREFIX_.'blocknewsadv_data`
					   WHERE id_item ='.$id.'';
			$result = Db::getInstance()->Execute($sql);
	}
	
	public function deleteImg($data = null){
		$id = $data['id'];
		
		$info_post = $this->getItem(array('id'=>$id));
  		$img = $info_post['item'][0]['img'];
				  		
		$this->_updateImgToItem(array('post_id' => $id,
				  					  'img' =>  ""
				  					 )
				  				);
				  				
		@unlink(dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR."blocknewsadv".DIRECTORY_SEPARATOR.$img);
		
	}
	
	public function getItem($_data){
		$id = $_data['id'];
		$site = isset($_data['site'])?$_data['site']:0;
		$id_shop = $this->getIdShop();
		if($site){
			global $cookie;
			$current_language = (int)$cookie->id_lang;
			
			$sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'blocknewsadv` pc 
			LEFT JOIN  `'._DB_PREFIX_.'blocknewsadv_data` pc_d
			ON(pc_d.id_item = pc.id)
			WHERE pc.id = '.$id.' and pc.id_shop = '.$id_shop.' 
			AND pc.status = 1 and pc_d.id_lang = '.$current_language.' ';
			
			$item = Db::getInstance()->ExecuteS($sql);
			
			foreach($item as $k => $_item){
				
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'blocknewsadv_data` pc
				WHERE pc.id_item = '.$_item['id'].'
				');
				
				foreach ($items_data as $item_data){
					if($current_language == $item_data['id_lang']){
						
		    			$item[$k]['title'] = $item_data['title'];
		    			$item[$k]['content'] = $item_data['content'];
		    			$item[$k]['seo_url'] = $item_data['seo_url'];
		    			$item[$k]['seo_description'] = $item_data['seo_description'];
		    			$item[$k]['seo_keywords'] = $item_data['seo_keywords'];
		    			
						}
		    	}
		    	
			}
			
		} else { 	
			
			
			$sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'blocknewsadv` pc
			WHERE id = '.$id.' and id_shop = '.$id_shop;
			$item = Db::getInstance()->ExecuteS($sql);
			
			foreach($item as $k => $_item){
				
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'blocknewsadv_data` pc
				WHERE pc.id_item = '.$_item['id'].'
				');
				
				foreach ($items_data as $item_data){
		    			$item['data'][$item_data['id_lang']]['title'] = $item_data['title'];
		    			$item['data'][$item_data['id_lang']]['content'] = $item_data['content'];
		    			$item['data'][$item_data['id_lang']]['seo_url'] = $item_data['seo_url'];
		    			$item['data'][$item_data['id_lang']]['seo_description'] = $item_data['seo_description'];
		    			$item['data'][$item_data['id_lang']]['seo_keywords'] = $item_data['seo_keywords'];
		    	}
		    	
			}
		}
			
	   return array('item' => $item);
	}
	
	
	public function updateItem($data){
		
		$id = $data['id'];
		
		$item_status = $data['item_status'];
		$post_images = $data['post_images'];
		
		$id_shop = $this->getIdShop();
		
		// update
		$sql = 'UPDATE `'._DB_PREFIX_.'blocknewsadv` SET
							   `status` = \''.pSQL($item_status).'\'
							   WHERE id = '.$id.' and id_shop = '.$id_shop.'
							   ';
			$result = Db::getInstance()->Execute($sql);
		
		/// delete tabs data
		$sql = 'DELETE FROM `'._DB_PREFIX_.'blocknewsadv_data` WHERE id_item = '.$id.'';
		$result = Db::getInstance()->Execute($sql);
		
		foreach($data['data_title_content_lang'] as $language => $item){
		
		$title = $item['title'];
		$content = $item['content'];
		$seokeywords = $item['seokeywords'];
	    $seodescription = $item['seodescription'];
	    
	    $seo_url_pre = strlen($item['seo_url'])>0?$item['seo_url']:$title;
	    $translite_seo = $this->_translit($seo_url_pre);
		$seo_url = $translite_seo;
	    
	    //$translite_seo = $this->_translit($item['seo_url']);
	    //$seo_url = strlen($translite_seo)>0?$translite_seo:"news".Tools::passwdGen(6);
	   
		$sql = 'SELECT count(*) as count
				FROM `'._DB_PREFIX_.'blocknewsadv_data` pc
				WHERE seo_url = "'.mysql_escape_string($seo_url).'"';
		$data_seo_url = Db::getInstance()->GetRow($sql);
		if($data_seo_url['count']!=0)
			$seo_url = "news".strtolower(Tools::passwdGen(6));
		
		$sql = 'INSERT into `'._DB_PREFIX_.'blocknewsadv_data` SET
							   `id_item` = \''.pSQL($id).'\',
							   `id_lang` = \''.pSQL($language).'\',
							   `title` = \''.pSQL($title).'\',
							   `content` = "'.mysql_escape_string($content).'",
							   `seo_keywords` = "'.mysql_escape_string($seokeywords).'",
							   `seo_description` = "'.mysql_escape_string($seodescription).'",
							   `seo_url` = "'.mysql_escape_string($seo_url).'"
							   ';
		$result = Db::getInstance()->Execute($sql);
		}
		
		$this->saveImage(array('post_id' => $id,'post_images' => $post_images ));
	}
	
	
	public function copyImage($data){
	
		$filename = $data['name'];
		$dir_without_ext = $data['dir_without_ext'];
		$width = $this->_width;
		$height = $this->_height;
		
		if (!$width){ $width = 85; };
		if (!$height){ $height = 85; };
		// Content type
		$size_img = getimagesize($filename);
		// Get new dimensions
		list($width_orig, $height_orig) = getimagesize($filename);
		$ratio_orig = $width_orig/$height_orig;
		
		if($width_orig>$height_orig){
		$height =  $width/$ratio_orig;
		}else{ 
		$width = $height*$ratio_orig;
		}
		if($width_orig<$width){
			$width = $width_orig;
			$height = $height_orig;
		}
	
			$image_p = imagecreatetruecolor($width, $height);
		$bgcolor=ImageColorAllocate($image_p, 255, 255, 255);
		//   
		imageFill($image_p, 5, 5, $bgcolor);
	
		if ($size_img[2]==2){ $image = imagecreatefromjpeg($filename);}                         
		else if ($size_img[2]==1){  $image = imagecreatefromgif($filename);}                         
		else if ($size_img[2]==3) { $image = imagecreatefrompng($filename); }
	
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
		// Output
		$users_img = $dir_without_ext.'.jpg';
		if ($size_img[2]==2)  imagejpeg($image_p, $users_img, 100);                         
		else if ($size_img[2]==1)  imagejpeg($image_p, $users_img, 100);                        
		else if ($size_img[2]==3)  imagejpeg($image_p, $users_img, 100);
		imageDestroy($image_p);
		imageDestroy($image);
		unlink($filename);

	}
	
	
public function PageNav($start,$count,$step, $_data =null )
	{
		$_admin = isset($_data['admin'])?$_data['admin']:null;
		$post_id = isset($_data['post_id'])?$_data['post_id']:0;
		
		
		$res = '';
		$product_count = $count;
		
			$res .= '<div class="pages" id="page_nav">';
			if($_admin){
				$data_translate = $this->getTranslateText();
				$res .= '<span>'.$data_translate['page'].':</span>';
			}
			$res .= '<span class="nums">';
		$start1 = $start;
			for ($start1 = ($start - $step*4 >= 0 ? $start - $step*4 : 0); $start1 < ($start + $step*5 < $product_count ? $start + $step*5 : $product_count); $start1 += $step)
				{
					$par = (int)($start1 / $step) + 1;
					if ($start1 == $start)
						{
							$res .= '<b>'. $par .'</b>';
						}
					else
						{
							if($_admin){
								$currentIndex = $_data['currentIndex'];
								$token = $_data['token'];
								$item = $_data['item'];
								$res .= '<a href="'.$currentIndex.'&page'.$item.'='.($start1 ? $start1 : 0).$token.'" >'.$par.'</a>';
							} else {
								$res .= '<a href="javascript:void(0)" onclick="go_page_news( '.($start1 ? $start1 : 0).' )">'.$par.'</a>';
							}
						}
				}
		
		$res .= '</span>';
		$res .= '</div>';
		
		return $res;
	}
	
	public function getTranslateText(){
		include_once(dirname(__FILE__) . '/../blocknewsadv.php');
		$obj = new blocknewsadv();
    	$data = $obj->translateText();
    	
		return array('seo_text'=> $data['seo_text'],
					 'page'=>$data['page']);
	}
	
	public function getIdShop(){
		if(substr(_PS_VERSION_,0,3) == '1.5'){
    		$id_shop = Context::getContext()->shop->id;
    	} else {
    		$id_shop = 0;
    	}
    	return $id_shop;
	}
	
	public function getTransformSEOURLtoID($_data){
		
	if(Configuration::get($this->_name.'urlrewrite_on') == 1 && !is_numeric($_data['id'])){
			$id = $_data['id'];
			$sql = '
					SELECT pc.id_item as id
					FROM `'._DB_PREFIX_.'blocknewsadv_data` pc
					WHERE seo_url = "'.$id.'"';
			$data_id = Db::getInstance()->GetRow($sql);
			$id = $data_id['id'];
		} else {
			$id = (int)$_data['id'];
		}
		
		return $id;
	}	
	
	
	public function createRSSFile($post_title,$post_description,$post_link)
	{
		$returnITEM = "<item>\n";
		# this will return the Title of the Article.
		$returnITEM .= "<title>".$post_title."</title>\n";
		# this will return the Description of the Article.
		$returnITEM .= "<description>".$post_description."</description>\n";
		# this will return the URL to the post.
		$returnITEM .= "<link>".$post_link."</link>\n";
		$returnITEM .= "</item>\n";
		return $returnITEM;
	}
	
	public function getItemsForRSS(){
			$id_shop = $this->getIdShop();
		
			$step = Configuration::get($this->_name.'number_rssitems');
			
			global $cookie;
			$current_language = (int)$cookie->id_lang;
			
			$sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'blocknewsadv` pc 
			LEFT JOIN `'._DB_PREFIX_.'blocknewsadv_data` pc_d
			on(pc.id = pc_d.id_item)
			WHERE pc.status = 1 and pc.id_shop = '.$id_shop.' and pc_d.id_lang = '.$current_language.' 
			ORDER BY pc.`id` DESC
			LIMIT 0 ,'.$step.'';
			$items_tmp = Db::getInstance()->ExecuteS($sql);
			
			$items = array();
			
			foreach($items_tmp as $k => $_item){
				
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'blocknewsadv_data` pc
				WHERE pc.id_item = '.$_item['id'].'
				');
				
				
				
				foreach ($items_data as $item_data){
		    		
		    		if($current_language == $item_data['id_lang']){
		    			$items[$k]['title'] = $item_data['title'];
		    			$items[$k]['content'] = $item_data['content'];
		    			$items[$k]['seo_description'] = $item_data['seo_description'];
		    			$items[$k]['seo_keywords'] = $item_data['seo_keywords'];
		    			$items[$k]['seo_url'] = $item_data['seo_url'];
		    			$items[$k]['id'] = $_item['id'];
		    			$items[$k]['img'] = $_item['img'];
		    			$items[$k]['time_add'] = $_item['time_add'];
		    		} 
		    	}
		    }
			

			return array('items' => $items);
	}
	
	
	public function getfacebooklocale()
	{
		$locales = array();

		if (($xml=simplexml_load_file(_PS_MODULE_DIR_ . $this->_name."/lib/facebook_locales.xml")) === false)
			return $locales;
			
		$result = $xml->xpath('/locales/locale/codes/code/standard/representation');

		foreach ($result as $locale)
		{
			list($k, $node) = each($locale);
			$locales[] = $node;
		}
			
		return $locales;
	}
	
 	public function getfacebooklib($id_lang){
    	
    	$lang = new Language((int)$id_lang);
		
    	$lng_code = isset($lang->language_code)?$lang->language_code:$lang->iso_code;
    	if(strstr($lng_code, '-')){
			$res = explode('-', $lng_code);
			$language_iso = strtolower($res[0]).'_'.strtoupper($res[1]);
			$rss_language_iso = strtolower($res[0]);
		} else {
			$language_iso = strtolower($lng_code).'_'.strtoupper($lng_code);
			$rss_language_iso = $lng_code;
		}
			
			
		if (!in_array($language_iso, $this->getfacebooklocale()))
			$language_iso = "en_US";
		
		if (Configuration::get('PS_SSL_ENABLED') == 1)
			$url = "https://";
		else
			$url = "http://";
		
		
		
		return array('url'=>$url . 'connect.facebook.net/'.$language_iso.'/all.js#xfbml=1',
					  'lng_iso' => $language_iso, 'rss_language_iso' => $rss_language_iso);
    }
	
	
}