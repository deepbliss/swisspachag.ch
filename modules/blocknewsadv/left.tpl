{if $blocknewsadvlast_news == "left"}

	<div class="block blockmanufacturer" id="manufacturers_block_left">
		<h4 align="center">
			<div class="blocknewsadv-float-left">
			{if $blocknewsadvurlrewrite_on == 1}
			<a href="{$base_dir}news/">
			{else}
			<a href="{$base_dir}modules/blocknewsadv/news.php">
			{/if}
				{l s='Last News' mod='blocknewsadv'}
			</a>
			</div>
			<div class="blocknewsadv-float-right">
			{if $blocknewsadvrsson == 1}
				<a href="{$module_dir}rss.php" title="{l s='RSS Feed' mod='blocknewsadv'}">
					<img src="{$module_dir}i/feed.png" alt="{l s='RSS Feed' mod='blocknewsadv'}" />
				</a>
			{/if}
			</div>
			<div class="blocknewsadv-clear"></div>
		</h4>
		<div class="block_content block-items-data">
		{if count($blocknewsadvitemsblock) > 0}
	    <ul>
	    {foreach from=$blocknewsadvitemsblock item=items name=myLoop1}
	    	{foreach from=$items.data item=item name=myLoop}
	    	<li class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if}">
	    	
		    	<table width="100%">
		    				<tr>
		    				{if strlen($item.img)>0}
		    					<td align="center">
		    						{if $blocknewsadvurlrewrite_on == 1}
			    						<a href="{$base_dir}news/{$item.seo_url}/" 
			    		  				title="{$item.title|escape:"htmlall":"UTF-8"}">
		    						{else}
			    						<a href="{$base_dir}modules/blocknewsadv/news-item.php?id={$item.id}" 
			    		  				title="{$item.title|escape:"htmlall":"UTF-8"}">
		    		  				{/if}
		    						<img src="{$base_dir}upload/blocknewsadv/{$item.img}" 
		    								  title="{$item.title|escape:"htmlall":"UTF-8"}" style="height:45px" />
		    						</a>
		    					</td>
		    				{/if}
		    					<td style="{if strlen($item.img)==0} width:100%{else} width:80%{/if}">
		    						<table width="100%">
		    							<tr>
		    								<td align="left">
		    									{if $blocknewsadvurlrewrite_on == 1}
						    						<a href="{$base_dir}news/{$item.seo_url}/" 
						    		  				title="{$item.title|escape:"htmlall":"UTF-8"}">
					    						{else}
						    						<a href="{$base_dir}modules/blocknewsadv/news-item.php?id={$item.id}" 
						    		  				title="{$item.title|escape:"htmlall":"UTF-8"}">
					    		  				{/if}
		    									<b>{$item.title}</b>
		    									</a>
		    								</td>
		    							</tr>
		    							<tr>
		    								<td align="left" class="blocknewsadv-date">{$item.time_add|date_format}</td>
		    							</tr>
		    						</table>
			    					
		    					</td>
		    				</tr>
		    		</table>
	    	</li>
	    		{/foreach}
	    	{/foreach}
	    </ul>
	    
	    <p class="blocnewsadv-view-all">
	    	{if $blocknewsadvurlrewrite_on == 1}
	    		<a href="{$base_dir}news/" class="button_large"
	    		title="{l s='View all news' mod='blocknewsadv'}">
	    	{else}
				<a class="button_large" title="{l s='View all news' mod='blocknewsadv'}" 
					href="{$module_dir}news.php">
			{/if}
			{l s='View all news' mod='blocknewsadv'}</a>
		</p>
	    
	    {else}
		<div class="blocknewsadv-block-noitems">
			{l s='News not found.' mod='blocknewsadv'}
		</div>
		{/if}
		</div>
	</div>
{/if}