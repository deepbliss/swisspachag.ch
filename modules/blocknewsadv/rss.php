<?php
include(dirname(__FILE__) . '/../../config/config.inc.php');
include(dirname(__FILE__) . '/../../init.php');
include_once(dirname(__FILE__) . '/classes/blocknewsadvfunctions.class.php');
$obj_blocknewshelp = new blocknewsadvfunctions();

$_name = "blocknewsadv";

global $cookie;
$id_lang = (int)$cookie->id_lang;
$data_language = $obj_blocknewshelp->getfacebooklib($id_lang);
$rss_title =  Configuration::get($_name.'rssname_'.$id_lang);;
$rss_description =  Configuration::get($_name.'rssdesc_'.$id_lang);;

if (Configuration::get('PS_SSL_ENABLED') == 1)
	$url = "https://";
else
	$url = "http://";

$site = $_SERVER['HTTP_HOST'];
			
// Lets build the page
$rootURL = $url.$site."/feeds/";
$latestBuild = date("r");

// Lets define the the type of doc we're creating.
$createXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";


if(Configuration::get($_name.'rsson') == 1){

$createXML .= "<rss version=\"0.92\">\n";
$createXML .= "<channel>
	<title>".$rss_title."</title>
	<link>$rootURL</link>
	<description>".$rss_description."</description>
	<lastBuildDate>$latestBuild</lastBuildDate>
	<docs>http://backend.userland.com/rss092</docs>
	<language>".$data_language['rss_language_iso']."</language>
";

$data_rss_items = $obj_blocknewshelp->getItemsForRSS();
foreach($data_rss_items['items'] as $_item)
{
	if(Configuration::get($_name.'urlrewrite_on') == 1)
		$page =  $url.$site."/news/".$_item['seo_url']."/";
	else
		$page = $url.$site.'/modules/blocknewsadv/news-item.php?id='.$_item['id'];
		
	$description = $_item['seo_description'];
	$title = $_item['title'];
	
	$createXML .= $obj_blocknewshelp->createRSSFile($title,$description,$page);
}
$createXML .= "</channel>\n </rss>";
// Finish it up
}

echo $createXML;















?>