{capture name=path}
{$meta_title}
{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}

{if $count_all > 0}

<p class="blocnewsadv-header"><span class="bold">{l s='News' mod='blocknewsadv'}  ( {$count_all} )</span></p>


<ul id="manufacturers_list" class="blocknewsadv-list">
{foreach from=$posts item=post name=myLoop}
	<li class="clearfix"> 
				<div class="left_side" style="width:100%">

				<h2>
				{if $blocknewsadvurlrewrite_on == 1}
					<a href="{$base_dir}news/{$post.seo_url}/" 
						   title="{$post.title|escape:'htmlall':'UTF-8'}"
							  >
				
				{else}
					<a href="{$base_dir}modules/blocknewsadv/news-item.php?id={$post.id}" 
					   title="{$post.title|escape:'htmlall':'UTF-8'}"
						  >
				{/if}
						{$post.title|escape:'htmlall':'UTF-8'}
					</a>
                   <div class="right_side" style="color: #fff;  font-size: 13px; font-weight: normal;">
						<span class="blocknewsadv-time">{$post.time_add|date_format:"%d.%m.%Y"}</span>
					</div>
				</h2>
                
                <div class="logo">
				{if strlen($post.img)>0}
					
					{if $blocknewsadvurlrewrite_on == 1}
						<a class="lnk_img" href="{$base_dir}news/{$post.seo_url}/" 
					   	   title="{$post.title|escape:'htmlall':'UTF-8'}">
					{else}
						<a class="lnk_img" href="{$base_dir}modules/blocknewsadv/news-item.php?id={$post.id}" 
					   	   title="{$post.title|escape:'htmlall':'UTF-8'}">
					{/if}
							<img src="{$base_dir}upload/blocknewsadv/{$post.img}" 
							     title="{$post.title|escape:'htmlall':'UTF-8'}" 
							      width="120" height="120" />
						</a>
					
				{/if}
			</div>
					
			
			<p class="description rte" style="font: normal 13px/18px Arial; color:#222; float:left;">
				{$post.content|substr:0:500}
				{if strlen($post.content)>500}...{/if}
				{if $blocknewsadvurlrewrite_on == 1}
				<a href="{$base_dir}news/{$post.seo_url}/" 
					   title="{$post.title|escape:'htmlall':'UTF-8'}" style="color: #0C55C3;font-family: noto sans; font-size: 11px;font-weight: 700; text-transform: uppercase;">
				{else}
				<a href="{$base_dir}modules/blocknewsadv/news-item.php?id={$post.id}" 
					   title="{$post.title|escape:'htmlall':'UTF-8'}" style="color: #0C55C3;font-family: noto sans; font-size: 11px;font-weight: 700; text-transform: uppercase;">
				{/if}
						{l s='mehr' mod='blocknews'}
				</a>
				<br/>
			</p>
				
			</div>
			<div class="blocknewsadv-clear"></div>
		</li>
{/foreach}
</ul>

			
		
{$paging}
	

{else}
	<div class="blocnewsadv-noitems">
	{l s='There are not news yet' mod='blocknewsadv'}
	</div>
{/if}


{literal}
<script type="text/javascript">
//<![CDATA[
	var baseDir = '{/literal}{$base_dir_ssl}{literal}';
//]]>
</script>
<script type="text/javascript">
function go_page_news(page,item_id){
	
	$('#manufacturers_list').css('opacity',0.5);
	$.post(baseDir + 'modules/blocknewsadv/process.php', {
		action:'pagenav',
		page : page,
		item_id : item_id
	}, 
	function (data) {
		if (data.status == 'success') {
		
		$('#manufacturers_list').css('opacity',1);
		
		$('#manufacturers_list').html('');
		$('#manufacturers_list').prepend(data.params.content);
		
		$('#page_nav').html('');
		$('#page_nav').prepend(data.params.page_nav);
		
		
		
	    } else {
			$('#manufacturers_list').css('opacity',1);
			alert(data.message);
		}
		
	}, 'json');

}
</script>
{/literal}

