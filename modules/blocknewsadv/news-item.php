<?php
$_GET['controller'] = 'all'; 
$_GET['fc'] = 'module';
$_GET['module'] = 'blocknewsadv';
require_once(dirname(__FILE__) . '/../../config/config.inc.php');
require_once(dirname(__FILE__) . '/../../init.php');


$name_module = 'blocknewsadv';

include_once(dirname(__FILE__) . '/classes/blocknewsadvfunctions.class.php');
$obj_blocknewshelp = new blocknewsadvfunctions();

$post_id = isset($_REQUEST['id'])?$_REQUEST['id']:0;

$post_id = $obj_blocknewshelp->getTransformSEOURLtoID(array('id'=>$post_id));

$_info_cat = $obj_blocknewshelp->getItem(array('id' => $post_id,'site'=>1));

$title = isset($_info_cat['item'][0]['title'])?$_info_cat['item'][0]['title']:'';
$seo_description = isset($_info_cat['item'][0]['seo_description'])?$_info_cat['item'][0]['seo_description']:'';
$seo_keywords = isset($_info_cat['item'][0]['seo_keywords'])?$_info_cat['item'][0]['seo_keywords']:''; 


if(sizeof($_info_cat['item'])==0){
	$title = 'There are not news yet ';
}

$smarty->assign('meta_title' , $title);
$smarty->assign('meta_description' , $seo_description);
$smarty->assign('meta_keywords' , $seo_keywords);

$smarty->assign($name_module.'urlrewrite_on', Configuration::get($name_module.'urlrewrite_on'));


if (version_compare(_PS_VERSION_, '1.5', '>')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				// header
				$oController->setMedia();
				@$oController->displayHeader();
			}
			else {
				include_once(dirname(__FILE__) . '/../../header.php');
			}


$smarty->assign(array('posts' => $_info_cat['item']));

include_once(dirname(__FILE__) . '/blocknewsadv.php');
$obj_blocknews = new blocknewsadv();

if(defined('_MYSQL_ENGINE_')){
	echo $obj_blocknews->renderTplItem();
} else {
	echo Module::display(dirname(__FILE__).'/blocknewsadv.php', 'news-item.tpl');
}
if (version_compare(_PS_VERSION_, '1.5', '>')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				// footer
				@$oController->displayFooter();
			}
			else {
				include_once(dirname(__FILE__) . '/../../footer.php');
			}

?>