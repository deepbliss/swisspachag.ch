<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_ed909ff67326a562e9eaa817b11d127e'] = 'News avanzata';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_83218ac34c1834c26781fe4bde918ee4'] = 'Benvenuto';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_3c658b7439354cde67ba29e40e83846d'] = 'Notizie Impostazioni';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_abc6501eea0bb6c90321e536aef6457e'] = 'Notizie moderato';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_8c2cc9ff98500d80f1e0bbb39da0485f'] = 'SEO URL Rewriting';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b707cfbc5f0be0ca6bd3718039924d57'] = 'Aiuto / Documentazione';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_f0bb0ebfeca956f686198b108183b3e7'] = 'SEO URL Rewriting Impostazioni';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_3500c4da9eb05f1162aca06fd467f308'] = 'Attivare o disattivare la riscrittura degli URL';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Abilitato';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b9f5c797ebbf55adccdd8539a65a0241'] = 'Disabile';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_da82a2002384409477655085d5c4136e'] = 'Attivare solo se il server permette la riscrittura degli URL (consigliato)';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b17f3f4dcf653a5776792498a9b44d6a'] = 'Aggiornare le impostazioni';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_5032a9b1df91e2463d4eaa26a4007ecc'] = 'URL di configurazione riscrittura:';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b1a90c50aac7924c336d4ce647fabcea'] = 'Aggiungi al tuo file .htaccess nella sezione';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_148e1d225291af33bd03f34125b9814a'] = 'seguenti linee';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_1065adff73b41482e043cbe8b9632f73'] = 'Aggiungere righe con le regole di riscrittura degli URL prima sezione Dispatcher.';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_81eeab9506186e2dca8faefa78d54067'] = 'Esempio:';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_1a7ed12f2dc51d30549a13c48817431b'] = 'Posizione di blocco Ultime Notizie';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_945d5e233cf7d6240f6b783b36a374ff'] = 'Sinistra';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_92b09c7c48c520c3c55e497875da437c'] = 'Destra';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_6adf97f83acf6453d4a6a4b1070f3754'] = 'Nessuna';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_a68f9efb7b3b19713059332fab915bf4'] = 'Numero di elementi nel Block Last News';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_15a82e8c6ff514b82cd2144350e989a8'] = 'Numero di notizie per pagina';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_5c3c75d15923e3fa4090bc5cf0981983'] = 'Attivazione o disattivazione del blocco Ultime notizie su Home page';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_37a4043ead6b883e918eaeb006cb94bb'] = 'Numero di articoli nel Blocco Ultime notizie su Home page';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_28c87c1ca3a27ff5311bf5f974de8f32'] = 'Attiva o Disattiva RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_dd7a8d6cc867186b8ef1325e0fa767e4'] = 'Titolo del tuo feed RSS';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_5c0d43a744ca1634d75ce7539e85c2d5'] = 'Descrizione del tuo RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_94bba4749e8d87dec57a63411fe4490e'] = 'Numero di articoli in RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_51ec9bf4aaeab1b25bb57f9f8d4de557'] = 'Titolo:';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_53a6aebc600183ad851f3a65e00028ea'] = 'Identifier (SEO URL)';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_295e66436239b5b7ddcf687616aaf90f'] = 'SEO keywords';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_40f33930d53eaa0a079d478c5bb72523'] = 'SEO Descrizione';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_bc87ef2144ae15ef4f78211e73948051'] = 'Logo Immagine';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_f2a6c498fb90ee345d997f888fce3b18'] = 'Elimina';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_f18074f8b138ed9e289a2210c2b86838'] = 'Contenuto:';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_ec53a8c4f07baed5d8825072c89799be'] = 'Stato';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_ea4788705e6873b424c65e91c2846b19'] = 'Annullare';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_06933067aafd48425d67bcb01bba5cb6'] = 'Aggiornare';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_f97f483a827121621eafd93657f6adb1'] = 'Aggiungi nuovo elemento';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b78a3223503896721cca1303f776159b'] = 'Titolo';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_a8de732739faee31ed691ee2127ad7bb'] = 'Consenti i formati';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_f15c1cae7882448b3fb0404682e17e61'] = 'Contenuto';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_c9cc8cce247e49bae79f15173ce97354'] = 'Salvare';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_be53a0541a6d36f6ecb879fa2c584b08'] = 'Immagine';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_44749712dbec183e983dcd78a7736c41'] = 'Data';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_004bf6c9a40003140292e97330236c53'] = 'Azione';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_7dce122004969d56ae2e0245cb754d35'] = 'Modifica';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_d4e616754739b63930f2fc8e0ae97b0e'] = 'Sei sicuro di voler eliminare questo elemento?';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_0c07d90dc51ebd2efbed4003d3528e70'] = 'Le voci non trovato';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_8e4e0f8cd68815acbb2fc7b305f5ea5c'] = 'MODULO DI DOCUMENTAZIONE';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_03e795452e58b24739e732c4d3b5539a'] = 'CONTATTARE';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_43862dd0c694c96fc775348a37656532'] = 'Benvenuti e grazie per aver acquistato il modulo.';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_648cdd0c2ecca5626a6ad17a277add1d'] = 'Il modulo consente agli utenti di aggiungere notizie sul sito.';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_91762c47d4a12a65e5a110c237419579'] = 'Per configurare il modulo si prega di leggere';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_dd1ba1872df91985ed1ca4cde2dfe669'] = 'Notizie';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_193cfc9be3b995831c6af2fea6650e60'] = 'Pagina';
$_MODULE['<{blocknewsadv}prestashop>home_19ca87c447ab2f6ec2e05d84fa662c5b'] = 'Ultime Notizie';
$_MODULE['<{blocknewsadv}prestashop>home_7eca39de358415bfb69b727a9557cd1c'] = 'RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>home_d6654a5804a4db50220aa2c602842ce9'] = 'News non trovata.';
$_MODULE['<{blocknewsadv}prestashop>left_19ca87c447ab2f6ec2e05d84fa662c5b'] = 'Ultime Notizie';
$_MODULE['<{blocknewsadv}prestashop>left_7eca39de358415bfb69b727a9557cd1c'] = 'RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>left_b5956867d9e5f8785698e2500b84cf38'] = 'Vedi tutte le news';
$_MODULE['<{blocknewsadv}prestashop>left_d6654a5804a4db50220aa2c602842ce9'] = 'News non trovata.';
$_MODULE['<{blocknewsadv}prestashop>news-item_e8a02be1b6972a8c7876fa828b4fc4af'] = 'Non ci sono ancora notizie';
$_MODULE['<{blocknewsadv}prestashop>news-list_addec426932e71323700afa1911f8f1c'] = 'più';
$_MODULE['<{blocknewsadv}prestashop>news_dd1ba1872df91985ed1ca4cde2dfe669'] = 'Notizie';
$_MODULE['<{blocknewsadv}prestashop>news_addec426932e71323700afa1911f8f1c'] = 'più';
$_MODULE['<{blocknewsadv}prestashop>news_e8a02be1b6972a8c7876fa828b4fc4af'] = 'Non ci sono ancora notizie';
$_MODULE['<{blocknewsadv}prestashop>right_19ca87c447ab2f6ec2e05d84fa662c5b'] = 'Ultime Notizie';
$_MODULE['<{blocknewsadv}prestashop>right_7eca39de358415bfb69b727a9557cd1c'] = 'RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>right_b5956867d9e5f8785698e2500b84cf38'] = 'Vedi tutte le news';
$_MODULE['<{blocknewsadv}prestashop>right_d6654a5804a4db50220aa2c602842ce9'] = 'News non trovata.';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadvfunctions.class_afc07f802259850134c46d9d2d5b65fd'] = 'Tipo di file non valido, si prega di riprovare!';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadvfunctions.class_bb8c7518d70d3ea5bafdce4f5dba0708'] = 'Formato di file errato, riprova!';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadvfunctions.class_3197baa1fee7404b3880f50e76d1fb35'] = 'Errore durante il download di file!';
