{foreach from=$posts item=post name=myLoop}
	<li class="clearfix">
				<div class="left_side" style="width:100%">
                    <h3>
                {if $blocknewsadvurlrewrite_on == 1}
                    <a href="{$base_dir}news/{$post.seo_url}/"
                           title="{$post.title|escape:'htmlall':'UTF-8'}"
                              >

                {else}
                    <a href="{$base_dir}modules/blocknewsadv/news-item.php?id={$post.id}"
                       title="{$post.title|escape:'htmlall':'UTF-8'}"
                          >
                {/if}
                        {$post.title|escape:'htmlall':'UTF-8'}
                    </a>
                </h3>
			<div class="logo">
				{if strlen($post.img)>0}

					{if $blocknewsadvurlrewrite_on == 1}
						<a class="lnk_img" href="{$base_dir}news/{$post.seo_url}/"
					   	   title="{$post.title|escape:'htmlall':'UTF-8'}">
					{else}
						<a class="lnk_img" href="{$base_dir}modules/blocknewsadv/news-item.php?id={$post.id}"
					   	   title="{$post.title|escape:'htmlall':'UTF-8'}">
					{/if}
							<img src="{$base_dir}upload/blocknewsadv/{$post.img}"
							     title="{$post.title|escape:'htmlall':'UTF-8'}"
							      width="70" height="70" />
						</a>

				{/if}
			</div>
			


			<p class="description rte">
				{$post.content|substr:0:140}
				{if strlen($post.content)>140}...{/if}
				{if $blocknewsadvurlrewrite_on == 1}
				<a href="{$base_dir}news/{$post.seo_url}/"
					   title="{$post.title|escape:'htmlall':'UTF-8'}" style="font-size:11px;text-decoration:underline;">
				{else}
				<a href="{$base_dir}modules/blocknewsadv/news-item.php?id={$post.id}"
					   title="{$post.title|escape:'htmlall':'UTF-8'}" style="font-size:11px;text-decoration:underline;">
				{/if}
						{l s='mehr' mod='blocknews'}
				</a>
				<br/>
			</p>

			</div>
			<div class="blocknewsadv-clear"></div>
			<div class="right_side" style="margin-top:5px">
			<span class="blocknewsadv-time">{$post.time_add}</span>
			</div>
		</li>
{/foreach}