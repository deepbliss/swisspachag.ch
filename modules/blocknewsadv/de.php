<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_ed909ff67326a562e9eaa817b11d127e'] = 'News Erweiterte';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_83218ac34c1834c26781fe4bde918ee4'] = 'Willkommen';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_3c658b7439354cde67ba29e40e83846d'] = 'News Einstellungen';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_abc6501eea0bb6c90321e536aef6457e'] = 'Moderate Nachrichten';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_8c2cc9ff98500d80f1e0bbb39da0485f'] = 'SEO URL Rewriting';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b707cfbc5f0be0ca6bd3718039924d57'] = 'Hilfe / Dokumentation';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_f0bb0ebfeca956f686198b108183b3e7'] = 'SEO URL Rewriting Einstellungen';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_3500c4da9eb05f1162aca06fd467f308'] = 'Aktivieren oder Deaktivieren der URL Rewriting';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Aktiviert';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b9f5c797ebbf55adccdd8539a65a0241'] = 'Behindert';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_da82a2002384409477655085d5c4136e'] = 'Aktivieren Sie nur, wenn Ihr Server erlaubt URL Rewriting (empfohlen)';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b17f3f4dcf653a5776792498a9b44d6a'] = 'Update einstellungen';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_5032a9b1df91e2463d4eaa26a4007ecc'] = 'URL rewriting Konfiguration:';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b1a90c50aac7924c336d4ce647fabcea'] = 'Hinzufügen zu .htaccess Datei im Abschnitt';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_148e1d225291af33bd03f34125b9814a'] = 'folgenden zeilen';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_1065adff73b41482e043cbe8b9632f73'] = 'Fügen Sie Zeilen mit den Regeln für URL-Rewriting, bevor Abschnitt Dispatcher.';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_81eeab9506186e2dca8faefa78d54067'] = 'Beispiel:';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_1a7ed12f2dc51d30549a13c48817431b'] = 'Positionen Block Last News';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_945d5e233cf7d6240f6b783b36a374ff'] = 'Links';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_92b09c7c48c520c3c55e497875da437c'] = 'Rechten';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_6adf97f83acf6453d4a6a4b1070f3754'] = 'None';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_a68f9efb7b3b19713059332fab915bf4'] = 'Anzahl der Elemente in der Block Last News';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_15a82e8c6ff514b82cd2144350e989a8'] = 'Anzahl der News pro Seite';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_5c3c75d15923e3fa4090bc5cf0981983'] = 'Aktivieren oder Deaktivieren Blockieren Letzte Nachrichten zum Startseite';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_37a4043ead6b883e918eaeb006cb94bb'] = 'Anzahl der Artikel bei Block Neue Nachrichten auf Startseite';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_28c87c1ca3a27ff5311bf5f974de8f32'] = 'Aktivieren oder Deaktivieren von RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_dd7a8d6cc867186b8ef1325e0fa767e4'] = 'Titel Ihrer RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_5c0d43a744ca1634d75ce7539e85c2d5'] = 'Beschreibung deines RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_94bba4749e8d87dec57a63411fe4490e'] = 'Anzahl der Einträge in RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_51ec9bf4aaeab1b25bb57f9f8d4de557'] = 'Titel:';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_53a6aebc600183ad851f3a65e00028ea'] = 'Identifier (SEO URL)';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_295e66436239b5b7ddcf687616aaf90f'] = 'SEO Keywords';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_40f33930d53eaa0a079d478c5bb72523'] = 'SEO Beschreibung';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_bc87ef2144ae15ef4f78211e73948051'] = 'Logo Bild';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_f2a6c498fb90ee345d997f888fce3b18'] = 'Delete';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_f18074f8b138ed9e289a2210c2b86838'] = 'Inhalt:';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_ec53a8c4f07baed5d8825072c89799be'] = 'Status';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_ea4788705e6873b424c65e91c2846b19'] = 'Stornieren';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_06933067aafd48425d67bcb01bba5cb6'] = 'Aktualisieren';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_f97f483a827121621eafd93657f6adb1'] = 'Neues Element hinzufügen';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b78a3223503896721cca1303f776159b'] = 'Titel';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_a8de732739faee31ed691ee2127ad7bb'] = 'Erlauben Formate';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_f15c1cae7882448b3fb0404682e17e61'] = 'Inhalt';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_c9cc8cce247e49bae79f15173ce97354'] = 'Sparen';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_be53a0541a6d36f6ecb879fa2c584b08'] = 'Bild';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_44749712dbec183e983dcd78a7736c41'] = 'Datum';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_004bf6c9a40003140292e97330236c53'] = 'Handlung';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_7dce122004969d56ae2e0245cb754d35'] = 'Editieren';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_d4e616754739b63930f2fc8e0ae97b0e'] = 'Sind Sie sicher, dass Sie diesen Artikel zu entfernen?';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_0c07d90dc51ebd2efbed4003d3528e70'] = 'Artikel nicht gefunden';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_8e4e0f8cd68815acbb2fc7b305f5ea5c'] = 'Modul dokumentation';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_03e795452e58b24739e732c4d3b5539a'] = 'KONTAKT';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_43862dd0c694c96fc775348a37656532'] = 'Willkommen und vielen Dank für den Kauf des Moduls.';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_648cdd0c2ecca5626a6ad17a277add1d'] = 'Das Modul erlaubt es Benutzern hinzufügen Nachrichten vor Ort.';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_91762c47d4a12a65e5a110c237419579'] = 'Um Modul konfigurieren lesen';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_dd1ba1872df91985ed1ca4cde2dfe669'] = 'Nachrichten';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadv_193cfc9be3b995831c6af2fea6650e60'] = 'Seite';
$_MODULE['<{blocknewsadv}prestashop>home_19ca87c447ab2f6ec2e05d84fa662c5b'] = 'Letzte News';
$_MODULE['<{blocknewsadv}prestashop>home_7eca39de358415bfb69b727a9557cd1c'] = 'RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>home_d6654a5804a4db50220aa2c602842ce9'] = 'Nachrichten nicht gefunden.';
$_MODULE['<{blocknewsadv}prestashop>left_19ca87c447ab2f6ec2e05d84fa662c5b'] = 'Letzte News';
$_MODULE['<{blocknewsadv}prestashop>left_7eca39de358415bfb69b727a9557cd1c'] = 'RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>left_b5956867d9e5f8785698e2500b84cf38'] = 'Alle News';
$_MODULE['<{blocknewsadv}prestashop>left_d6654a5804a4db50220aa2c602842ce9'] = 'Nachrichten nicht gefunden.';
$_MODULE['<{blocknewsadv}prestashop>news-item_e8a02be1b6972a8c7876fa828b4fc4af'] = 'Es gibt keine Neuigkeit noch';
$_MODULE['<{blocknewsadv}prestashop>news-list_addec426932e71323700afa1911f8f1c'] = 'mehr';
$_MODULE['<{blocknewsadv}prestashop>news_dd1ba1872df91985ed1ca4cde2dfe669'] = 'News';
$_MODULE['<{blocknewsadv}prestashop>news_addec426932e71323700afa1911f8f1c'] = 'mehr';
$_MODULE['<{blocknewsadv}prestashop>news_e8a02be1b6972a8c7876fa828b4fc4af'] = 'Es gibt keine Neuigkeit noch';
$_MODULE['<{blocknewsadv}prestashop>right_19ca87c447ab2f6ec2e05d84fa662c5b'] = 'Letzte News';
$_MODULE['<{blocknewsadv}prestashop>right_7eca39de358415bfb69b727a9557cd1c'] = 'RSS Feed';
$_MODULE['<{blocknewsadv}prestashop>right_b5956867d9e5f8785698e2500b84cf38'] = 'Alle News';
$_MODULE['<{blocknewsadv}prestashop>right_d6654a5804a4db50220aa2c602842ce9'] = 'Nachrichten nicht gefunden.';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadvfunctions.class_afc07f802259850134c46d9d2d5b65fd'] = 'Ungültiger Dateityp, bitte versuchen Sie es erneut!';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadvfunctions.class_bb8c7518d70d3ea5bafdce4f5dba0708'] = 'Falsches Dateiformat, bitte versuchen Sie es erneut!';
$_MODULE['<{blocknewsadv}prestashop>blocknewsadvfunctions.class_3197baa1fee7404b3880f50e76d1fb35'] = 'Fehler beim Herunterladen der Datei!';
