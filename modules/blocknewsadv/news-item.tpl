
{capture name=path}
{$meta_title}
{/capture}

{include file="$tpl_dir./breadcrumb.tpl"}


{if count($posts) > 0}

<script src="/js/jquery/plugins/fancybox/jquery.fancybox.js" ></script>
<link href='/js/jquery/plugins/fancybox/jquery.fancybox.css' rel='stylesheet' type='text/css'>
<script type="text/javascript">
function formatTitle(title, currentArray, currentIndex, currentOpts) {
                return '<div id="tip7-title" style="text-align:left;">' + (title && title.length ? '<b>' + title + '</b>' : '' ) + ' <span class="closeFancyBox" style="position:static;float:right;"><a href="javascript:;" onclick="$.fancybox.close();"><img src="/img/fancybox/close.gif" /></a></span></div>';
}

$(document).ready(function() {
	$(".productImage").fancybox({
		'showCloseButton'	: false,
		'titlePosition' 	: 'inside',
		'titleFormat'		: formatTitle
	});
});
</script>

<ul id="manufacturers_list">
{foreach from=$posts item=post name=myLoop}
	<li class="clearfix" style="border:none;">
				<div class="left_side" style="width:100%;">
			
			<table width="100%">
                
                <tr>
                    <td colspan="2" style="margin-top:-15px;">
                         <h2 style="text-indent: 10px;">
                                {$post.title|escape:'htmlall':'UTF-8'}
                                <div class="right_side" style="color: #fff;  font-size: 13px; font-weight: normal;">
                                <span class="blocknewsadv-time" style="float:right;">{$post.time_add|date_format:"%d.%m.%Y"}</span>
								</div>
                        </h2>
                        

                    </td>
                </tr>
					
				<tr>
                	<td style="vertical-align:top;background:none;border-bottom:none;">
                        {if strlen($post.img)>0}
							<div class="logo">
								<a href="{$base_dir}upload/blocknewsadv/{$post.img}" rel="" class="productImage img">
									<img src="{$base_dir}upload/blocknewsadv/{$post.img}" alt="" width="120" height="120" />
								</a>
							</div>
						{/if}
                     </td>
					<td style="vertical-align:top;background:none;border-bottom:none;">
                        <duv style="font: normal 13px/18px Arial">
                            {$post.content}
                        </duv>
					</td>
					
				</tr>
			</table>
			<br/>

			</div>
			<div class="blocknewsadv-clear"></div>

			{*<br/><br/>*}
			{*<table width="100%" cellpadding="0" cellspacing="0" border="0">*}
				{*<tr>*}
					{*<td>*}
						{*{literal}*}
						{*<script type="text/javascript">*}
						{*/*<![CDATA[*/*}
						{*var fb_button = '<div class="fb-like" data-href="http://{/literal}{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}{literal}"'+ *}
								 		{*'data-send="false" data-layout="button_count" data-width="50" data-show-faces="true"></div>';*}
						{*document.write(fb_button);*}
						{*/*]]>*/*}
						{*</script>*}
						{*{/literal}*}
					{*</td>*}
					{*<td>*}
					{*{literal}*}
					{*<script type="text/javascript">*}
					{*/*<![CDATA[*/*}
					{*var tw_button = '<a href="http://twitter.com/share" class="twitter-share-button"'+ *}
						   			{*'data-url="http://{/literal}{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}{literal}"'+*}
					           		{*'data-counturl="http://{/literal}{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}{literal}"'+*}
					           		{*'data-via="#"'+*}
							   		{*'data-count="horizontal">Tweet</a>';*}
					{*document.write(tw_button);*}
					{*/*]]>*/*}
					{*</script>*}
					{*{/literal}*}
					{*</td>*}
					{*<td>*}
						{*<!-- Place this render call where appropriate -->*}
						{*{literal}*}
						{*<script type="text/javascript">*}
						  {*(function() {*}
						    {*var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;*}
						    {*po.src = 'https://apis.google.com/js/plusone.js';*}
						    {*var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);*}
						  {*})();*}
						{*</script>*}
						{*<script type="text/javascript">*}
							{*/*<![CDATA[*/*}
							{*var g_button = '<div class="g-plusone" data-size="medium" href="http://{/literal}{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}{literal}" data-count="true"></div>';*}
							{*document.write(g_button);*}
							{*/*]]>*/*}
						{*</script>*}
						{*{/literal}*}
					{*</td>*}
					{*<td>*}
					{*{literal}*}
					{*<script type="IN/Share" data-url="http://{/literal}{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}{literal}" *}
							{*data-counter="right"></script>*}
					{*{/literal}*}
					{*</td>*}
					{*<td>*}
					{*{literal}*}
					{*<script type="text/javascript">*}
					{*/*<![CDATA[*/*}
					{*var pin_button = '<a href="http://pinterest.com/pin/create/button/?url=http://{/literal}{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}{literal}&media={/literal}{if strlen($post.img)>0}{$base_dir}upload/blocknewsadv/{$post.img}{else}{$base_dir}img/logo.jpg{/if}{literal}&description={/literal}{$meta_title|escape:'htmlall':'UTF-8'}{literal}"'+ *}
									 {*'class="pin-it-button" count-layout="horizontal">'+*}
					 				 {*'<img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>';*}
					{*document.write(pin_button);*}
					{*/*]]>*/*}
					{*</script>*}
					{*{/literal}*}
					{*</td>*}
				{*</tr>*}
			{*</table>*}

			{*<div id="fb-root"></div>*}
			{*{literal}*}

			{*<script>(function(d, s, id) {*}
			  {*var js, fjs = d.getElementsByTagName(s)[0];*}
			  {*if (d.getElementById(id)) return;*}
			  {*js = d.createElement(s); js.id = id;*}
			  {*js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";*}
			  {*fjs.parentNode.insertBefore(js, fjs);*}
			{*}(document, 'script', 'facebook-jssdk'));</script>*}

		{*<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>*}
		{*<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>*}
		{*<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>*}
		{*{/literal}	*}
		</li>
{/foreach}
</ul>

{else}
	<div class="blocnewsadv-noitems">
		{l s='There are not news yet' mod='blocknewsadv'}
	</div>
{/if}