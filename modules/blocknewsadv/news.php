<?php
$_GET['controller'] = 'all'; 
$_GET['fc'] = 'module';
$_GET['module'] = 'blocknewsadv';
require_once(dirname(__FILE__) . '/../../config/config.inc.php');
require_once(dirname(__FILE__) . '/../../init.php');

include_once(dirname(__FILE__) . '/classes/blocknewsadvfunctions.class.php');
$obj_blocknewshelp = new blocknewsadvfunctions();

$_data = $obj_blocknewshelp->getItems();
$name_module = 'blocknewsadv';
$step = Configuration::get($name_module.'news_page');

$paging = $obj_blocknewshelp->PageNav(0,$_data['count_all'],$step);

// strip tags for content
foreach($_data['items'] as $_k => $_item){
	$_data['items'][$_k]['content'] = strip_tags($_item['content']);
}

$smarty->assign(array('posts' => $_data['items'], 
					  'count_all' => $_data['count_all'],
					  'paging' => $paging
					  )
				);

$smarty->assign($name_module.'urlrewrite_on', Configuration::get($name_module.'urlrewrite_on'));
				
$seo_text_data = $obj_blocknewshelp->getTranslateText();
$seo_text = $seo_text_data['seo_text'];
$smarty->assign('meta_title' , $seo_text);
$smarty->assign('meta_description' , $seo_text);
$smarty->assign('meta_keywords' , $seo_text);
				
if (version_compare(_PS_VERSION_, '1.5', '>')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				// header
				$oController->setMedia();
				@$oController->displayHeader();
			}
			else {
				include_once(dirname(__FILE__) . '/../../header.php');
			}


include_once(dirname(__FILE__) . '/blocknewsadv.php');
$obj_blocknews = new blocknewsadv();

if(defined('_MYSQL_ENGINE_')){
	echo $obj_blocknews->renderTplItems();
} else {
	echo Module::display(dirname(__FILE__).'/blocknewsadv.php', 'news.tpl');
}

if (version_compare(_PS_VERSION_, '1.5', '>')) {
				if (isset(Context::getContext()->controller)) {
					$oController = Context::getContext()->controller;
				}
				else {
					$oController = new FrontController();
					$oController->init();
				}
				// footer
				@$oController->displayFooter();
			}
			else {
				include_once(dirname(__FILE__) . '/../../footer.php');
			}