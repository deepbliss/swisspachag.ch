<?php
 
class BlocknewsadvAllModuleFrontController extends ModuleFrontController
{
	
	public function init()
	{
		header('Location: '.$_SERVER['HTTP_REFERER']);
		parent::init();
	}
	
	public function setMedia()
	{
		parent::setMedia();
	}

	
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		
		parent::initContent();
		
		$this->setTemplate('all.tpl');
		
	}
}