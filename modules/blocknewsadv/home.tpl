

<div class="block-last-blocknewsadvs block blockmanufacturer" style="margin-top:10px">
		<h4>
			
			<div class="blocknewsadv-float-left">
				{if $blocknewsadvurlrewrite_on == 1}
				<a href="{$base_dir}news/">
				{else}
				<a href="{$base_dir}modules/blocknewsadv/news.php">
				{/if}
					{l s='Last News' mod='blocknewsadv'}
				</a>
			</div>
			<div class="blocknewsadv-float-right">
			{if $blocknewsadvrsson == 1}
				<a href="{$module_dir}rss.php" title="{l s='RSS Feed' mod='blocknewsadv'}">
					<img src="{$module_dir}i/feed.png" alt="{l s='RSS Feed' mod='blocknewsadv'}" />
				</a>
			{/if}
			</div>
			<div class="blocknewsadv-clear"></div>
		</h4>
		<div class="block_content">
			{if count($blocknewsadvitems_home)>0}
			<ul class="items-last-blocknewsadvs">
			{foreach from=$blocknewsadvitems_home item=items name=myLoop}
				{foreach from=$items.data item=item name=myLoop}
	    		<li>
	    			
	    			<table width="100%">
	    				<tr>
	    				{if strlen($item.img)>0}
	    						<td align="center" style="width:20%">
	    							{if $blocknewsadvurlrewrite_on == 1}
	    								<a href="{$base_dir}news/{$item.seo_url}/" 
			    		  				   title="{$item.title|escape:"htmlall":"UTF-8"}">
	    							{else}
			    						<a href="{$base_dir}modules/blocknewsadv/news-item.php?id={$item.id}" 
			    		  				   title="{$item.title|escape:"htmlall":"UTF-8"}">
			    		  			{/if}
		    						<img src="{$base_dir}upload/blocknewsadv/{$item.img}" 
		    								  title="{$item.title|escape:"htmlall":"UTF-8"}" style="height:45px" />
		    						</a>
		    					</td>
	    					
	    				{/if}
	    					<td class="blocknewsadv-title-and-content">
	    						<table width="100%">
	    							<tr>
	    								<td>
	    									{if $blocknewsadvurlrewrite_on == 1}
			    								<a href="{$base_dir}news/{$item.seo_url}/" 
					    		  				   title="{$item.title|escape:"htmlall":"UTF-8"}">
			    							{else}
					    						<a href="{$base_dir}modules/blocknewsadv/news-item.php?id={$item.id}" 
					    		  				   title="{$item.title|escape:"htmlall":"UTF-8"}">
					    		  			{/if}
	    										<b>{$item.title}</b>
	    									</a>
	    								</td>
	    							</tr>
	    							
	    							<tr>
				    					<td>
				    						{if $blocknewsadvurlrewrite_on == 1}
				    							<a href="{$base_dir}news/{$item.seo_url}/" 
						    		  			   title="{$item.content|strip_tags|substr:0:150|escape:"htmlall":"UTF-8"}{if strlen($item.content)>150}...{/if}"
				    		   	   			   	   style="font-size:11px">
				    		   	   			
				    						{else}
					    						<a href="{$base_dir}modules/blocknewsadv/news-item.php?id={$item.id}" 
							    		  		   title="{$item.content|strip_tags|substr:0:150|escape:"htmlall":"UTF-8"}{if strlen($item.content)>150}...{/if}"
					    		   	   			   style="font-size:11px">
					    		   	   		{/if}
				    		   	   				{$item.content|strip_tags|substr:0:150|escape:"htmlall":"UTF-8"}{if strlen($item.content)>150}...{/if}
				    						</a>
				    					</td>
	    							</tr>
	    				
	    						</table>
	    					</td>
	    				</tr>
	    				</table>
	    		   	
	    		</li>
	    		{/foreach}
	    	{/foreach}
	    	</ul>
	    	{else}
	    		<div class="blocknewsadv-block-noitems">
					{l s='News not found.' mod='blocknewsadv'}
				</div>
	    	{/if}
	    </div>
</div>


