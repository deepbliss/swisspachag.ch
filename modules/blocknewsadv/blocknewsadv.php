<?php
class blocknewsadv extends Module{

	private $_step = 10;
	private $_is15;

	function __construct(){

		$this->name = 'blocknewsadv';
 	 	$this->version = '1.0.0';
 	 	$this->tab = 'blocknewsadv';
		$this->module_key = 'e12d5ba9b99d59ebd0ecda8ec7841096';
		$this->author = "mitrocops";
		
		if(substr(_PS_VERSION_,0,3) == '1.5')
			$this->_is15 = 1;
		else
			$this->_is15 = 0;
			
		parent::__construct();
		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('News Advanced');
		$this->description = $this->l('News Advanced');
		
	}
	
	public function install(){

		if (!parent::install()) return false;
		
		
		Configuration::updateValue($this->name.'urlrewrite_on', 0);
		Configuration::updateValue($this->name.'last_news', 'right');
		Configuration::updateValue($this->name.'number_ni', 5);
		Configuration::updateValue($this->name.'news_page', 5);
		Configuration::updateValue($this->name.'newshomeon', 1);
		Configuration::updateValue($this->name.'hnumber_ni', 5);
		Configuration::updateValue($this->name.'rsson', 1);
		Configuration::updateValue($this->name.'number_rssitems', 10);
		
		
		
		$languages = Language::getLanguages(false);
    	foreach ($languages as $language){
    		$i = $language['id_lang'];
    		$iso = strtoupper(Language::getIsoById($i));
    		
    		$rssname = Configuration::get('PS_SHOP_NAME');
    		Configuration::updateValue($this->name.'rssname_'.$i, $rssname);
			$rssdesc = Configuration::get('PS_SHOP_NAME');
			Configuration::updateValue($this->name.'rssdesc_'.$i, $rssdesc);
		}
		
		if (!$this->registerHook('leftColumn') 
			OR !$this->registerHook('rightColumn')
			OR !$this->registerHook('Header') 
			OR !$this->registerHook('home') 
			OR !$this->_installDatabaseTable()
			OR !$this->_createFolderAndSetPermissions()
			 )
			return false;

		return true;
	}
	
	public function uninstall(){
		
		if (!parent::uninstall()) return false;
		
		return true;
	}
	
	private function _installDatabaseTable(){
		
		$db = Db::getInstance();
		$sql = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'blocknewsadv` (
							  `id` int(11) NOT NULL auto_increment,
							  `img` text, 
							  `status` int(11) NOT NULL default \'1\',
							  `id_shop` int(11) NOT NULL default \'0\',
							  `time_add` timestamp NOT NULL default CURRENT_TIMESTAMP,
							  PRIMARY KEY  (`id`)
							) ENGINE='.(defined('_MYSQL_ENGINE_')?_MYSQL_ENGINE_:"MyISAM").' DEFAULT CHARSET=utf8;';
		if (!Db::getInstance()->Execute($sql))
			return false;
			
		$query = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'blocknewsadv_data` (
							  `id_item` int(11) NOT NULL,
							  `id_lang` int(11) NOT NULL,
							  `seo_description` text,
							  `seo_keywords` varchar(5000) default NULL,
							  `seo_url` varchar(5000) default NULL,
							  `title` varchar(5000) NOT NULL,
							  `content` text NOT NULL,
							  KEY `id_item` (`id_item`)
							) ENGINE='.(defined('_MYSQL_ENGINE_')?_MYSQL_ENGINE_:"MyISAM").' DEFAULT CHARSET=utf8';
		Db::getInstance()->Execute($query);
		return true;
		
	}
	
	private function _createFolderAndSetPermissions(){
		
		$prev_cwd = getcwd();
		
		$module_dir = dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR;
		@chdir($module_dir);
		//folder avatars
		$module_dir_img = $module_dir."blocknewsadv".DIRECTORY_SEPARATOR; 
		@mkdir($module_dir_img, 0777);

		@chdir($prev_cwd);
		
		return true;
	} 
	
	function hookHeader($params){
    	global $smarty;
    	
    	$smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));
    	
    	return $this->display(__FILE__, 'head.tpl');
    }
    
	
	function hookLeftColumn($params)
	{
		global $smarty,$cookie;
		include_once(dirname(__FILE__) . '/classes/blocknewsadvfunctions.class.php');
		$obj_blocknewshelp = new blocknewsadvfunctions();
    	$_data = $obj_blocknewshelp->getItemsBlock();
		
    	$smarty->assign(array($this->name.'itemsblock' => $_data['items']
							  )
						);
		
		$smarty->assign($this->name.'last_news', Configuration::get($this->name.'last_news'));
		$smarty->assign($this->name.'urlrewrite_on', Configuration::get($this->name.'urlrewrite_on'));
		$smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));
	
		$current_language = (int)$cookie->id_lang;
		$smarty->assign($this->name.'current_language', $current_language);
		
		return $this->display(__FILE__, 'left.tpl');		
	}
	
	function hookRightColumn($params)
	{
		global $smarty;
		include_once(dirname(__FILE__) . '/classes/blocknewsadvfunctions.class.php');
		$obj_blocknewshelp = new blocknewsadvfunctions();
    	$_data = $obj_blocknewshelp->getItemsBlock();
		
    	$smarty->assign(array($this->name.'itemsblock' => $_data['items']
							  )
						);
		
		$smarty->assign($this->name.'last_news', Configuration::get($this->name.'last_news'));
		$smarty->assign($this->name.'urlrewrite_on', Configuration::get($this->name.'urlrewrite_on'));
		$smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));
		
		return $this->display(__FILE__, 'right.tpl');	
	}
	
	public function hookHome($params){
		if(Configuration::get($this->name.'newshomeon') == 1){
			
			global $smarty,$cookie;;
			include_once(dirname(__FILE__) . '/classes/blocknewsadvfunctions.class.php');
			$obj = new blocknewsadvfunctions();
	    	$data = $obj->getHomeLastNews(array('start'=>0,
	    											'step'=>Configuration::get($this->name.'hnumber_ni')
	    											)
	    									);
	    	$current_language = (int)$cookie->id_lang;
			// strip tags for content
			foreach($data['items'] as $_k => $_item){
				if($current_language == @$_item['data'][$_k]['id_lang']){
				$data['items'][$_k]['data'][$current_language]['content'] = strip_tags($_item['data'][$_k]['content']);
				}
			}
			$smarty->assign(array($this->name.'items_home' => $data['items']));
			$smarty->assign($this->name.'urlrewrite_on', Configuration::get($this->name.'urlrewrite_on'));
			$smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));
			
			$smarty->assign($this->name.'is_ps15', $this->_is15);
			return $this->display(__FILE__, 'home.tpl');
		}
	
	}
	
	private $_html;
	
	public function getContent()
    {
    	global $currentIndex, $cookie;
    	include_once(dirname(__FILE__) . '/classes/blocknewsadvfunctions.class.php');
    	$blocknewsadvfunctions_obj = new blocknewsadvfunctions();
		
    	$this->_html = '';
    	
    	$this->_html .= $this->_jsandcss();	
    	
    	if(substr(_PS_VERSION_,0,3) == '1.5'){
    		$id_shop = Context::getContext()->shop->id;
    	} else {
    		$id_shop = 0;
    	}
    	
    	$pageitems_url = isset($_GET["pageitems"])?$_GET["pageitems"]:'';
   	    if (strlen($pageitems_url)>0) {
        	$this->_html .= '<script>init_tabs(4);</script>';
        }
        
   		 if (Tools::isSubmit('cancel_item'))
        {
        	$page = Tools::getValue("pageitems");
        	Tools::redirectAdmin($currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)).'&pageitems='.$page);
		}
        
    	if (Tools::isSubmit('update_item'))
        {
        	$id = Tools::getValue("id");
     		
        	$languages = Language::getLanguages(false);
	    	$data_title_content_lang = array();
	    	$seo_url = Tools::getValue("seo_url");
	    	foreach ($languages as $language){
	    		$id_lang = $language['id_lang'];
	    		$title = Tools::getValue("title_".$id_lang);
	    		$content = Tools::getValue("content_".$id_lang);
	    		$seokeywords = Tools::getValue("seokeywords_".$id_lang);
	    		$seodescription = Tools::getValue("seodescription_".$id_lang);
	    		
	    		if(strlen($title)>0 && strlen($content)>0)
	    		{
	    			$data_title_content_lang[$id_lang] = array('title' => $title,
	    									 				   'content' => $content,
	    													   'seokeywords' => $seokeywords,
	    													   'seodescription' => $seodescription,
	    													   'seo_url' => $seo_url
	    													    );		
	    		}
	    	}
        	
         	$item_status = Tools::getValue("item_status");
        	$post_images = Tools::getValue("post_images");
        	
         	$data = array('data_title_content_lang'=>$data_title_content_lang,
        				  'id' => $id,
         				  'item_status' => $item_status,
         				  'post_images' => $post_images
         				 );
         	if(sizeof($data_title_content_lang)>0)
         		$blocknewsadvfunctions_obj->updateItem($data);
         		
         	$page = Tools::getValue("pageitems");
         	Tools::redirectAdmin($currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)).'&pageitems='.$page);
		
        }
    	
    	if (Tools::isSubmit('submit_item'))
        {
        	$languages = Language::getLanguages(false);
	    	$data_title_content_lang = array();
	    	$seo_url = Tools::getValue("seo_url");
	    	foreach ($languages as $language){
	    		$id_lang = $language['id_lang'];
	    		$title = Tools::getValue("title_".$id_lang);
	    		$content = Tools::getValue("content_".$id_lang);
	    		$seokeywords = Tools::getValue("seokeywords_".$id_lang);
	    		$seodescription = Tools::getValue("seodescription_".$id_lang);
	    		
	    		if(strlen($title)>0 && strlen($content)>0)
	    		{
	    			$data_title_content_lang[$id_lang] = array('title' => $title,
	    									 				   'content' => $content,
	    													   'seokeywords' => $seokeywords,
	    													   'seodescription' => $seodescription,
	    													   'seo_url' => $seo_url
	    													    );		
	    		}
	    	}
	    	
        	$item_status = Tools::getValue("item_status");
        	
        	$data = array( 'data_title_content_lang'=>$data_title_content_lang,
         				 	'item_status' => $item_status
         				  );
         	//echo "<pre>"; var_dump($data); exit;
         	if(sizeof($data_title_content_lang)>0)
         		$blocknewsadvfunctions_obj->saveItem($data);
        		
         	$page = (int) Tools::getValue("pageitems");
			Tools::redirectAdmin($currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)).'&pageitems='.$page);
		}
    	
		
    	// delete news item
    	if (Tools::isSubmit("delete_item")) {
			if (Validate::isInt(Tools::getValue("id"))) {
				
				$data = array('id' => Tools::getValue("id"));
				$blocknewsadvfunctions_obj->deleteItem($data);
				
				$page = Tools::getValue("pageitems");
				Tools::redirectAdmin($currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)).'&pageitems='.$page);
			}
		}
		// delete news item
		
    	
    	// url rewriting settings
    	$urlrewriting_settingsset = isset($_GET["urlrewriting_settingsset"])?$_GET["urlrewriting_settingsset"]:'';
   	    if (strlen($urlrewriting_settingsset)>0) {
        	$this->_html .= '<script>init_tabs(5);</script>';
        }
    	if (Tools::isSubmit('submit_urlrewriting'))
        {
        	Configuration::updateValue($this->name.'urlrewrite_on', Tools::getValue('urlrewrite_on'));
        	$url = $currentIndex.'&tab=AdminModules&urlrewriting_settingsset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }
        // url rewriting settings
       
    	// news settings
   	    $news_settingsset = isset($_GET["news_settingsset"])?$_GET["news_settingsset"]:'';
   	    if (strlen($news_settingsset)>0) {
        	$this->_html .= '<script>init_tabs(3);</script>';
        }
        
        if (Tools::isSubmit('news_settings'))
        {
    		Configuration::updateValue($this->name.'last_news', Tools::getValue('last_news'));
			Configuration::updateValue($this->name.'number_ni', Tools::getValue('number_ni'));
			Configuration::updateValue($this->name.'news_page', Tools::getValue('news_page'));
			Configuration::updateValue($this->name.'newshomeon', Tools::getValue('newshomeon'));
			Configuration::updateValue($this->name.'hnumber_ni', Tools::getValue('hnumber_ni'));
			Configuration::updateValue($this->name.'rsson', Tools::getValue('rsson'));
			Configuration::updateValue($this->name.'number_rssitems', Tools::getValue('number_rssitems'));
			
			
        	$languages = Language::getLanguages(false);
        	foreach ($languages as $language){
    			$i = $language['id_lang'];
        		Configuration::updateValue($this->name.'rssname_'.$i, Tools::getValue('rssname_'.$i));
        		Configuration::updateValue($this->name.'rssdesc_'.$i, Tools::getValue('rssdesc_'.$i));
        	}
			
        	$url = $currentIndex.'&tab=AdminModules&news_settingsset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }
        // news settings
        
        
        
        $this->_html .= $this->_displayForm();
        return $this->_html;
    }
    

    
	
	
	public function _displayForm(){
   		$_html = '';
    	
    	$_html .= '
		<fieldset '.(($this->_is15 == 1)?"class=\"ps15-width\"":"").'>
					<legend><img src="../modules/'.$this->name.'/logo.gif"  />
					'.$this->displayName.'</legend>
					
		<ul class="leftMenu">
			<li><a href="javascript:void(0)" onclick="tabs_custom(1)" id="tab-menu-1" class="selected"><img src="../modules/'.$this->name.'/i/btn/ico-news.gif" />'.$this->l('Welcome').'</a></li>
			<li><a href="javascript:void(0)" onclick="tabs_custom(3)" id="tab-menu-3"><img src="../modules/'.$this->name.'/i/btn/ico-news.gif" />'.$this->l('News Settings').'</a></li>
			<li><a href="javascript:void(0)" onclick="tabs_custom(4)" id="tab-menu-4"><img src="../modules/'.$this->name.'/i/btn/ico-news.gif" />'.$this->l('Moderate News').'</a></li>
			<li><a href="javascript:void(0)" onclick="tabs_custom(5)" id="tab-menu-5"><img src="../modules/'.$this->name.'/i/btn/ico-news.gif" />'.$this->l('SEO URL Rewriting').'</a></li>
			<li><a href="javascript:void(0)" onclick="tabs_custom(6)" id="tab-menu-6"><img src="../modules/'.$this->name.'/i/btn/ico-help.gif" />'.$this->l('Help / Documentation').'</a></li>
		</ul>
		';
    	$_html .= '<div style="clear:both"></div>';
		$_html .= '<div class="reviewsadv-content">
						<div class="menu-content" id="tabs-1">'.$this->_welcome().'</div>';
		$_html .= '<div class="menu-content" id="tabs-3">'.$this->_newsSettings().'</div>';
		$_html .= '<div class="menu-content" id="tabs-4">'.$this->_moderateNews().'</div>';
		$_html .= '<div class="menu-content" id="tabs-5">'.$this->_UrlRewriteSettings().'</div>';
		$_html .= '<div class="menu-content" id="tabs-6">'.$this->_help_documentation().'</div>';
		$_html .= '<div style="clear:both"></div>';
		$_html .= '</div>';
		
		
		$_html .= '</fieldset>';
    	
    	return $_html;
    }
    
    private function _UrlRewriteSettings(){
    	$_html = '';
    	
    	$_html .= '<h3 class="title-block-content">'.$this->l('SEO URL Rewriting Settings').'</h3>';
    	
    	
    	$_html .= '<form method="post" action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'">';
    	
    	
    	$_html .= '<table style="width:100%">';
    	
    	
    	$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:40%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Enable or Disable URL rewriting').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="radio" value="1" id="text_list_on" name="urlrewrite_on" onclick="enableOrDisable(1)"
							'.(Tools::getValue('urlrewrite_on', Configuration::get($this->name.'urlrewrite_on')) ? 'checked="checked" ' : '').'>
					<label for="dhtml_on" class="t"> 
						<img alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="../img/admin/enabled.gif">
					</label>
					
					<input type="radio" value="0" id="text_list_off" name="urlrewrite_on"  onclick="enableOrDisable(0)"
						   '.(!Tools::getValue('urlrewrite_on', Configuration::get($this->name.'urlrewrite_on')) ? 'checked="checked" ' : '').'>
					<label for="dhtml_off" class="t">
						<img alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="../img/admin/disabled.gif">
					</label>
					
					<p class="clear">'.$this->l('Enable only if your server allows URL rewriting (recommended)').'.</p>
				';
		$_html .= '<script type="text/javascript">
			    	function enableOrDisable(id)
						{
						if(id==0){
							$("#block-url-settings").hide(200);
						} else {
							$("#block-url-settings").show(200);
						}
							
						}
					</script>';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		$_html .= '</table>';
    	
    	$_html .= '<div id="block-url-settings" '.(Configuration::get($this->name.'urlrewrite_on')==1?'style="display:block"':'style="display:none"').'>';
    	
    	if(substr(_PS_VERSION_,0,3) == '1.5'){
    	$_html .= $this->_hint15();
    		 
    	} else{
    	$_html .= $this->_hint();
    	}	
    	
    	$_html .= '</div>';
    	
    	$_html .= '<p class="center" style="padding: 10px; margin-top: 10px;">
					<input type="submit" name="submit_urlrewriting" value="'.$this->l('Update settings').'" 
                		   class="button"  />
                	</p>';
    	
    	$_html .= '</form>';
    	return $_html;
    }
    
    private function _hint(){
    	$_html = '';
    	
    	$_html .= '<p style="display: block; font-size: 11px; width: 95%; margin-bottom:20px;position:relative" class="hint clear">
    	<b>'.$this->l('URL rewriting configuration:').'</b>
    	<br/><br/>
    	'.$this->l('Add to your .htaccess file in section ').'&lt;IfModule mod_rewrite.c&gt;&lt;/IfModule&gt;'.$this->l(' following lines').':
    	<br/><br/>
    	<code>
		RewriteRule ^news/([0-9a-zA-Z-_]+)/?$ /modules/blocknewsadv/news-item.php?id=$1 [QSA,L]
		</code>
		<br/>
		<code>
		RewriteRule ^news/?$ /modules/blocknewsadv/news.php [QSA,L] 
		</code>
		
			<br/><br/>
		</p>';
    	
    	return $_html;
    }
    
    private function _hint15(){
    	$_html = '';
    	
    	$_html .= '<p style="display: block; font-size: 11px; width: 95%; margin-bottom:20px;position:relative" class="hint clear">
    	<b>'.$this->l('URL rewriting configuration:').'</b>
    	<br/><br/>
    	'.$this->l('Add to your .htaccess file in section ').'&lt;IfModule mod_rewrite.c&gt;&lt;/IfModule&gt;'.$this->l(' following lines').':
    	<br/><br/>
    	
    	<b>
    	<code>
		RewriteRule ^news/([0-9a-zA-Z-_]+)/?$ /modules/blocknewsadv/news-item.php?id=$1 [QSA,L]
		</code>
		<br/>
		<code>
		RewriteRule ^news/?$ /modules/blocknewsadv/news.php [QSA,L] 
		</code>
		
		</b>
		<br/><br/>
		'.$this->l('Add rows with the rules for URL rewriting before section Dispatcher.').'
		<br/><br/>
		<b style="font-size:13px">'.$this->l('Example:').'</b>
			<br/><br/>
			<code>
			### module News Advanced
			</code>
			<br/>
			<code>
			RewriteRule ^news/([0-9a-zA-Z-_]+)/?$ /modules/blocknewsadv/news-item.php?id=$1 [QSA,L]
			</code>
			<br/>
			<code>
			RewriteRule ^news/?$ /modules/blocknewsadv/news.php [QSA,L] 
			</code>
			<br/><br/>
			<code>			
			# Dispatcher
			</code>
			<br/>
			<code>
			RewriteCond %{REQUEST_FILENAME} -s [OR]
			</code>
			<br/>
			<code>
			RewriteCond %{REQUEST_FILENAME} -l [OR]
			</code>
			<br/>
			<code>
			RewriteCond %{REQUEST_FILENAME} -d
			</code>
			<br/>
			<code>
			RewriteCond %{HTTP_HOST} ^yoursite.com$
			</code>
			<br/>
			<code>
			RewriteRule ^.*$ - [NC,L]
			</code>
			<br/>
			<code>
			RewriteCond %{HTTP_HOST} ^yoursite.com$
			</code>
			<br/>
			<code>
			RewriteRule ^.*$ %{ENV:REWRITEBASE}index.php [NC,L]
			</code>
			<br/>
			<code>
			&lt;/IfModule&gt;
			</code>
			<br/><br/>
			<code>
			#If rewrite mod isn\'t enabled
    		</code>
    		<br/>
    		<code>
			ErrorDocument 404 /index.php?controller=404
			</code>
			<br/><br/>
			<code>
			# ~~end~~ Do not remove this comment, Prestashop will keep automatically the code outside this comment when .htaccess will be generated again
			</code>
		</p>';
    	
    	return $_html;
    }
    
    
    private function _newsSettings(){
    	
    	
    	
    	$_html = '';
    	
    	$_html .= '<h3 class="title-block-content">'.$this->l('News Settings').'</h3>';
    	
    	
    	$_html .= '<form method="post" action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'">';
    	
		
		$_html .= '<table style="width:100%">';
		
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:40%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Position Block Last News').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<select class="select" name="last_news" 
							id="last_news">
						<option '.(Tools::getValue('last_news', Configuration::get($this->name.'last_news'))  == "left" ? 'selected="selected" ' : '').' value="left">'.$this->l('Left').'</option>
						<option '.(Tools::getValue('last_news', Configuration::get($this->name.'last_news')) == "right" ? 'selected="selected" ' : '').' value="right">'.$this->l('Right').'</option>
						<option '.(Tools::getValue('last_news', Configuration::get($this->name.'last_news')) == "none" ? 'selected="selected" ' : '').' value="none">'.$this->l('None').'</option>
					
					</select>
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Number of items in the Block Last News').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="text" name="number_ni"  
			               value="'.Tools::getValue('number_ni', Configuration::get($this->name.'number_ni')).'"
			               >
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Number of news per page').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="text" name="news_page"  
			               value="'.Tools::getValue('news_page', Configuration::get($this->name.'news_page')).'"
			               >
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		
		
    	
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	$_html .= '<b>'.$this->l('Enable or Disable Block Last News on Home page').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="radio" value="1" id="text_list_on" name="newshomeon" 
							'.(Tools::getValue('newshomeon', Configuration::get($this->name.'newshomeon')) ? 'checked="checked" ' : '').'>
					<label for="dhtml_on" class="t"> 
						<img alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="../img/admin/enabled.gif">
					</label>
					
					<input type="radio" value="0" id="text_list_off" name="newshomeon" 
						   '.(!Tools::getValue('newshomeon', Configuration::get($this->name.'newshomeon')) ? 'checked="checked" ' : '').'>
					<label for="dhtml_off" class="t">
						<img alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="../img/admin/disabled.gif">
					</label>
				';
		$_html .= '</td>';
		$_html .= '</tr>';
    	
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Number of items in Block Last News on Home page').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="text" name="hnumber_ni"  
			               value="'.Tools::getValue('hnumber_ni', Configuration::get($this->name.'hnumber_ni')).'"
			               >
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	$_html .= '<b>'.$this->l('Enable or Disable RSS Feed').':</b>';
    	
    	$_html .= '<script type="text/javascript">
			    	function enableOrDisableRSS(id)
						{
						if(id==0){
							$("#block-rss-settings").hide(200);
						} else {
							$("#block-rss-settings").show(200);
						}
							
						}
					</script>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="radio" value="1" id="text_list_on" name="rsson" onclick="enableOrDisableRSS(1)"
							'.(Tools::getValue('rsson', Configuration::get($this->name.'rsson')) ? 'checked="checked" ' : '').'>
					<label for="dhtml_on" class="t"> 
						<img alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="../img/admin/enabled.gif">
					</label>
					
					<input type="radio" value="0" id="text_list_off" name="rsson" onclick="enableOrDisableRSS(0)"
						   '.(!Tools::getValue('rsson', Configuration::get($this->name.'rsson')) ? 'checked="checked" ' : '').'>
					<label for="dhtml_off" class="t">
						<img alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="../img/admin/disabled.gif">
					</label>
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
		
		$_html .= '</table>';
		
		
		
		$_html .= '<div id="block-rss-settings" '.(Configuration::get($this->name.'rsson')==1?'style="display:block"':'style="display:none"').'>';
    	
		$_html .= '<table style="width:100%">';
		
		
		$_html .= '<tr>';
		$_html .= '<td style="text-align:right;width:40%;padding:0 20px 0 0">';
		$divLangName = "rssname¤srssdesc";
		
    	// Title of your RSS Feed
		
		$_html .= '<b>'.$this->l('Title of your RSS Feed').':</b>';
    			
		$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
    	
    	$_html .= '<div class="margin-form1">';
		
    		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language){
			$id_lng = (int)$language['id_lang'];
	    	$rssname = Configuration::get($this->name.'rssname'.'_'.$id_lng);
	    	
	    	
			$_html .= '	<div id="rssname_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

						<input type="text" style="width:400px"   
								  id="rssname_'.$language['id_lang'].'" 
								  name="rssname_'.$language['id_lang'].'" 
								  value="'.htmlentities(stripslashes($rssname), ENT_COMPAT, 'UTF-8').'"/>
						</div>';
	    	}
			$_html .= '';
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'rssname');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
			
			$_html .= '</div>';
		$_html .= '</td>';
		$_html .= '</tr>';	
		
		
		
    	// Description of your RSS Feed
    	
		
		$_html .= '<tr>';
		$_html .= '<td style="text-align:right;width:40%;padding:0 20px 0 0">';
		
    	$_html .= '<b>'.$this->l('Description of your RSS Feed').':</b>';
    			
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
    	
    	$_html .= '<div class="margin-form1">';
		
    		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language){
			$id_lng = (int)$language['id_lang'];
	    	$rssdesc = Configuration::get($this->name.'rssdesc_'.$id_lng);
	    	
	    	
			$_html .= '	<div id="srssdesc_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

							 <input type="text" style="width:400px"   
								  id="rssdesc_'.$language['id_lang'].'" 
								  name="rssdesc_'.$language['id_lang'].'" 
								  value="'.htmlentities(stripslashes($rssdesc), ENT_COMPAT, 'UTF-8').'"/>
								  
					</div>';
	    	}
			$_html .= '';
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'srssdesc');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
			
			$_html .= '</div>';
		$_html .= '</td>';
		$_html .= '</tr>';		
    	// Description of your RSS Feed
		
		
		$_html .= '<tr>';
    	$_html .= '<td style="text-align:right;width:35%;padding:0 20px 0 0">';
    	
    	$_html .= '<b>'.$this->l('Number of items in RSS Feed').':</b>';
    	
    	$_html .= '</td>';
    	$_html .= '<td style="text-align:left">';
		$_html .=  '
					<input type="text" name="number_rssitems"  
			               value="'.Tools::getValue('number_rssitems', Configuration::get($this->name.'number_rssitems')).'"
			               >
				';
		$_html .= '</td>';
		$_html .= '</tr>';
		
    	$_html .= '</table>';
    	
    	$_html .= '</div>';
			
			$_html .= '<p class="center" style="padding: 10px; margin-top: 10px;">
					<input type="submit" name="news_settings" value="'.$this->l('Update settings').'" 
                		   class="button"  />
                	</p>';
					
					
		
		$_html .= '</form>';
		return $_html;
    }
    
    private function _moderateNews(){
    
    global $currentIndex, $cookie;
    $_html = '';
    	
    $_html .= '<h3 class="title-block-content">'.$this->l('Moderate News').'</h3>';
    
    	
    include_once(dirname(__FILE__) . '/classes/blocknewsadvfunctions.class.php');
		$obj_blocknews = new blocknewsadvfunctions();
		
		$i=0;
		$paging = '';
		
		if(Tools::isSubmit("edit_item")){
			
			//$divLangName = "ccontent¤title";
			$divLangName = "ccontent¤title¤seokeywords¤seodescription";
			
			$_data = $obj_blocknews->getItem(array('id'=>(int)Tools::getValue("id")));
			
			$id = $_data['item'][0]['id'];
			$img = $_data['item'][0]['img'];
			$status = $_data['item'][0]['status'];
			$data_content = isset($_data['item']['data'])?$_data['item']['data']:'';
			
			$_html .= '<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">';
    		
    		$_html .= '<label>'.$this->l('Title:').'</label>
    					<div class="margin-form">';
    		
			$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language){
			$id_lng = (int)$language['id_lang'];
	    	$title = isset($data_content[$id_lng]['title'])?$data_content[$id_lng]['title']:"";
			
			$_html .= '	<div id="title_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

						<input type="text" style="width:400px"   
								  id="title_'.$language['id_lang'].'" 
								  name="title_'.$language['id_lang'].'" 
								  value="'.htmlentities(stripslashes($title), ENT_COMPAT, 'UTF-8').'"/>
						</div>';
	    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'title');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
    		$_html .= '<div style="clear:both"></div>';
							
			$_html .= '</div>';
			
			
			
			if(Configuration::get($this->name.'urlrewrite_on') == 1){
			// identifier
			$current_lng =  $cookie->id_lang;
			$seo_url = isset($data_content[$current_lng]['seo_url'])?$data_content[$current_lng]['seo_url']:"";
	   
		    	
			$_html .= '<label>'.$this->l('Identifier (SEO URL)').'</label>';
	    	
	    	$_html .= '<div class="margin-form">';
	    	
				
				$_html .= '
							<input type="text" style="width:400px"   
									  id="seo_url" 
									  name="seo_url" 
									  value="'.$seo_url.'"/>
							<p>(eg: domain.com/news/identifier)</p>
							';
		    $_html .=  '</div>';
			}
			
			
			$_html .= '<label>'.$this->l('SEO Keywords').'</label>';
    			
	    	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		    $languages = Language::getLanguages(false);
	    	
	    	$_html .= '<div class="margin-form">';
	    	
			foreach ($languages as $language){
				$id_lng = (int)$language['id_lang'];
		    	$seo_keywords = isset($data_content[$id_lng]['seo_keywords'])?$data_content[$id_lng]['seo_keywords']:"";
	   
		    	
				$_html .= '	<div id="seokeywords_'.$language['id_lang'].'" 
								 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
								 >
							<textarea id="seokeywords_'.$language['id_lang'].'" 
									  name="seokeywords_'.$language['id_lang'].'" 
									  cols="80" rows="7"  
				                	   >'.htmlentities(stripslashes($seo_keywords), ENT_COMPAT, 'UTF-8').'</textarea>
							
							</div>';
		    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'seokeywords');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
				        
			$_html .=  '</div>';
			
			
			$_html .= '<label>'.$this->l('SEO Description').'</label>';
	    			
	    	
	    	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		    $languages = Language::getLanguages(false);
	    	
	    	$_html .= '<div class="margin-form">';
	    	
			foreach ($languages as $language){
				$id_lng = (int)$language['id_lang'];
		    	$seo_description = isset($data_content[$id_lng]['seo_description'])?$data_content[$id_lng]['seo_description']:"";
	       	
				$_html .= '	<div id="seodescription_'.$language['id_lang'].'" 
								 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
								 >
							<textarea id="seodescription_'.$language['id_lang'].'" 
									  name="seodescription_'.$language['id_lang'].'" 
									  cols="80" rows="7"  
				                	   >'.htmlentities(stripslashes($seo_description), ENT_COMPAT, 'UTF-8').'</textarea>
							
							</div>';
		    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'seodescription');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
				        
			$_html .=  '</div>';
		
    		
    		$_html .= '<label>'.$this->l('Logo Image').'</label>
    			
    				<div class="margin-form">
					<input type="file" name="news_image" id="news_image" />
					<p>Allow formats *.jpg; *.jpeg; *.png; *.gif.</p>';

    		if(strlen($img)>0){
	    	$_html .= '<div id="post_images_list">';
	    		$_html .= '<div style="float:left;margin:10px" id="post_images_id">';
	    		$_html .= '<table width=100%>';
	    		
	    		$_html .= '<tr><td align="left">';
	    		$_html .= '<input type="radio" checked name="post_images"/>';
	    		
	    		$_html .= '</td>';
	    		
	    		$_html .= '<td align="right">';
	    		
	    			$_html .= '<a href="javascript:void(0)" title="'.$this->l('Delete').'"  
	    						onclick = "delete_img('.$id.');"><img src="'._PS_ADMIN_IMG_.'delete.gif" alt="" /></a>
	    					';
	    		
	    		$_html .= '</td>';
	    		
	    		$_html .= '<tr>';
	    		$_html .= '<td colspan=2>';
	    		$_html .= '<img src="../upload/'.$this->name.'/'.$img.'" style="width:50px;height:50px"/>';
	    		$_html .= '</td>';
	    		$_html .= '</tr>';
	    		
	    		$_html .= '</table>';
	    		
	    		$_html .= '</div>';
	    	//}
	    	$_html .= '<div style="clear:both"></div>';
	    	$_html .= '</div>';
	    	}
	    	
	    	$_html .= '</div>';
    		
    		
    		if(defined('_MYSQL_ENGINE_')){
	    		
    		$_html .= '<label>'.$this->l('Content:').'</label>
	    					<div class="margin-form" >';
    		
    		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language){
	    	$id_lng = (int)$language['id_lang'];
	    	$content = isset($data_content[$id_lng]['content'])?$data_content[$id_lng]['content']:"";
			
			$_html .= '	<div id="ccontent_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

						<textarea class="rte" cols="25" rows="10" style="width:400px"   
								  id="content_'.$language['id_lang'].'" 
								  name="content_'.$language['id_lang'].'">'.htmlentities(stripslashes($content), ENT_COMPAT, 'UTF-8').'</textarea>

					</div>';
	    	}
	    	
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'ccontent');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
	    	
			$_html .= '<div style="clear:both"></div>';
			
			$_html .= '</div>';
	    	}else{
	    		
	    		$_html .= '<label>'.$this->l('Content:').'</label>
	    					<div class="margin-form">';
	    					
	    		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		    	$languages = Language::getLanguages(false);
		    	$divLangName = "ccontent";
		    	
		    	foreach ($languages as $language){
				$id_lng = (int)$language['id_lang'];
	    		$content = isset($data_content[$id_lng]['content'])?$data_content[$id_lng]['content']:"";
			
				$_html .= '	<div id="ccontent_'.$language['id_lang'].'" 
								 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
								 >
	
							<textarea class="rte" cols="25" rows="10" style="width:400px"   
									  id="content_'.$language['id_lang'].'" 
									  name="content_'.$language['id_lang'].'">'.htmlentities(stripslashes($content), ENT_COMPAT, 'UTF-8').'</textarea>
	
						</div>';
		    	}
				ob_start();
				$this->displayFlags($languages, $defaultLanguage, $divLangName, 'ccontent');
				$displayflags = ob_get_clean();
				$_html .= $displayflags;
		    	
				$_html .= '<div style="clear:both"></div>';
				
				$_html .= '</div>';
	    	}
	    	
	    	$_html .= '<label>'.$this->l('Status').'</label>
				<div class = "margin-form">';
				
			$_html .= '<select name="item_status" style="width:100px">
						<option value=1 '.(($status==1)?"selected=\"true\"":"").'>'.$this->l('Enabled').'</option>
						<option value=0 '.(($status==0)?"selected=\"true\"":"").'>'.$this->l('Disabled').'</option>
					   </select>';
				
				
			$_html .= '</div>';
			
    	
    		$_html .= '<label>&nbsp;</label>
						<div class = "margin-form"  style="margin-top:10px">
						<input type="submit" name="cancel_item" value="'.$this->l('Cancel').'" 
                		   class="button"  />&nbsp;&nbsp;&nbsp;
						<input type="submit" name="update_item" value="'.$this->l('Update').'" 
                		   class="button"  />
                		  </div>';
    		
    		$_html .= '</form>';
    		
		} else {
			
			$divLangName = "ccontent¤title¤seokeywords¤seodescription";
			$name = "";
			$content = "";
			
    		$_html .= '<a href="javascript:void(0)" onclick="$(\'#add-question-form\').show(200);$(\'#link-add-question-form\').hide(200)"
    					id="link-add-question-form"	
					  style="border: 1px solid #F05F5D; padding: 5px; margin-bottom: 10px; display: block; font-size: 16px; color: #F05F5D; text-align: center; font-weight: bold; text-decoration: underline;"
					  >'.$this->l('Add New Item').'</a>';
    		
    		$_html .= '<div style="border: 1px solid rgb(222, 222, 222);padding-top:10px;display:none" id="add-question-form">';
			$_html .= '<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">';
    		
    		$_html .= '<label>'.$this->l('Title').'</label>
    					<div class="margin-form">';
    		
    		
    		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language){
			$id_lng = (int)$language['id_lang'];
	    	
			$_html .= '	<div id="title_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

						<input type="text" style="width:400px"   
								  id="title_'.$language['id_lang'].'" 
								  name="title_'.$language['id_lang'].'" 
								  value="'.htmlentities(stripslashes($name), ENT_COMPAT, 'UTF-8').'"/>
						</div>';
	    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'title');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
    		
			$_html .= '</div>';
			
			if(Configuration::get($this->name.'urlrewrite_on') == 1){
			// identifier
			$current_lng =  $cookie->id_lang;
			$seo_url = "";
		    	
			$_html .= '<label>'.$this->l('Identifier (SEO URL)').'</label>';
	    	
	    	$_html .= '<div class="margin-form">';
	    	
				
				$_html .= '
							<input type="text" style="width:400px"   
									  id="seo_url" 
									  name="seo_url" 
									  value="'.$seo_url.'"/>
							<p>(eg: domain.com/news/identifier)</p>
							';
		    $_html .=  '</div>';
			}
			
			
			$_html .= '<label>'.$this->l('SEO Keywords').'</label>';
    			
	    	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		    $languages = Language::getLanguages(false);
	    	
	    	$_html .= '<div class="margin-form">';
	    	
			foreach ($languages as $language){
				$id_lng = (int)$language['id_lang'];
		    	$seo_keywords = "";
		    	
				$_html .= '	<div id="seokeywords_'.$language['id_lang'].'" 
								 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
								 >
							<textarea id="seokeywords_'.$language['id_lang'].'" 
									  name="seokeywords_'.$language['id_lang'].'" 
									  cols="80" rows="7"  
				                	   >'.htmlentities(stripslashes($seo_keywords), ENT_COMPAT, 'UTF-8').'</textarea>
							
							</div>';
		    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'seokeywords');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
				        
			$_html .=  '</div>';
			
			
			$_html .= '<label>'.$this->l('SEO Description').'</label>';
	    			
	    	
	    	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		    $languages = Language::getLanguages(false);
	    	
	    	$_html .= '<div class="margin-form">';
	    	
			foreach ($languages as $language){
				$id_lng = (int)$language['id_lang'];
		    	$seo_description = "";
		    	
				$_html .= '	<div id="seodescription_'.$language['id_lang'].'" 
								 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
								 >
							<textarea id="seodescription_'.$language['id_lang'].'" 
									  name="seodescription_'.$language['id_lang'].'" 
									  cols="80" rows="7"  
				                	   >'.htmlentities(stripslashes($seo_description), ENT_COMPAT, 'UTF-8').'</textarea>
							
							</div>';
		    	}
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'seodescription');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
				        
			$_html .=  '</div>';
			
    		
    		$_html .= '<label>'.$this->l('Logo Image').'</label>
    			
    				<div class="margin-form">
					<input type="file" name="news_image" id="news_image" />
					<p>'.$this->l('Allow formats').' *.jpg; *.jpeg; *.png; *.gif.</p>';
    	
	    	$_html .= '</div>';
    		
    		if(defined('_MYSQL_ENGINE_')){
	    	
    			
    		$_html .= '<label>'.$this->l('Content').'</label>
	    					<div class="margin-form" >';
    			$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language)

			$_html .= '	<div id="ccontent_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

						<textarea class="rte" cols="25" rows="10" style="width:400px"   
								  id="content_'.$language['id_lang'].'" 
								  name="content_'.$language['id_lang'].'">'.htmlentities(stripslashes($content), ENT_COMPAT, 'UTF-8').'</textarea>

					</div>';
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'ccontent');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
			$_html .= '</div>';
			
	    	}else{
	    		
	    	$_html .= '<label>'.$this->l('Content:').'</label>
	    					<div class="margin-form" >';
    			$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	    	$languages = Language::getLanguages(false);
	    	
	    	foreach ($languages as $language)

			$_html .= '	<div id="ccontent_'.$language['id_lang'].'" 
							 style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;"
							 >

						<textarea class="rte" cols="25" rows="10" style="width:400px"   
								  id="content_'.$language['id_lang'].'" 
								  name="content_'.$language['id_lang'].'">'.htmlentities(stripslashes($content), ENT_COMPAT, 'UTF-8').'</textarea>

					</div>';
			ob_start();
			$this->displayFlags($languages, $defaultLanguage, $divLangName, 'ccontent');
			$displayflags = ob_get_clean();
			$_html .= $displayflags;
			$_html .= '<div style="clear:both"></div>';
			$_html .= '</div>';
	    	}
	    	
	    	
		$_html .= '<label>'.$this->l('Status').'</label>
				<div class = "margin-form" style="padding: 0pt 0pt 10px 130px;">';
				
		$_html .= '<select name="item_status">
					<option value=1 selected="true">'.$this->l('Enabled').'</option>
					<option value=0>'.$this->l('Disabled').'</option>
				   </select>';
			
				
			$_html .= '</div>';
    	
    		$_html .= '<label>&nbsp;</label>
						<div class = "margin-form"  style="margin-top:10px">
						<input type="button" value="'.$this->l('Cancel').'" 
                		   class="button"  
                		   onclick="$(\'#link-add-question-form\').show(200);$(\'#add-question-form\').hide(200);" 
                		   />&nbsp;&nbsp;&nbsp;
						<input type="submit" name="submit_item" value="'.$this->l('Save').'" 
                		   class="button"  />
                		  </div>';
    		
    		$_html .= '</form>';
    		$_html .= '</div>';
		
    		$_html .= '<br/>';
    		
			$_html .= '<table class = "table" width = 100%>
			<tr>
				<th width="5%">'.$this->l('ID').'</th>
				<th width="10%">'.$this->l('Image').'</th>
				<th>'.$this->l('Title').'</th>
				<th width="20%">'.$this->l('Date').'</th>
				<th width="5%">'.$this->l('Status').'</th>
				<th width = "7%">'.$this->l('Action').'</th>
			</tr>';
			
			$start = (int)Tools::getValue("pageitems");
			$_data = $obj_blocknews->getItems(array('admin'=>1,'start' => $start,'step'=>$this->_step));
			$_items = $_data['items'];
			
			$paging = $obj_blocknews->PageNav($start,$_data['count_all'],$this->_step, 
											array('admin' => 1,'currentIndex'=>$currentIndex,
												  'token' => '&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)),
												  'item' => 'items'
											));
			
			if(sizeof($_items)>0){
				
				foreach($_items as $_item){
					$i=1;		
					$id = $_item['id'];
					$title = $_item['title'];
					$img = $_item['img'];
					$status = $_item['status'];
					$date = $_item['time_add'];
			
					$_html .= 
						'<tr>
						<td style = "color:black;">'.$id.'</td>
						<td style = "color:black;" align="center"><img src="../upload/'.$this->name.'/'.$img.'" style="width:40px;height:40px" /></td>
						<td style = "color:black;">'.$title.'</td>';
					$_html .= '<td style = "color:black;">'.$date.'</td>';
					if($status)
						$_html .= '<td align="center"><img alt="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="../img/admin/enabled.gif"></td>';
					else
						$_html .= '<td align="center"><img alt="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="../img/admin/disabled.gif"></td>';
				
			
					$_html .= '<td>
				
								 <input type = "hidden" name = "id" value = "'.$id.'"/>
								 <a href="'.$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)).'&edit_item&id='.(int)($id).'&pageitems='.$start.'" title="'.$this->l('Edit').'"><img src="'._PS_ADMIN_IMG_.'edit.gif" alt="" /></a> 
								 <a href="'.$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.intval(Tab::getIdFromClassName('AdminModules')).intval($cookie->id_employee)).'&delete_item&id='.(int)($id).'&pageitems='.$start.'" title="'.$this->l('Delete').'"  onclick = "javascript:return confirm(\''.$this->l('Are you sure you want to remove this item?').'\');"><img src="'._PS_ADMIN_IMG_.'delete.gif" alt="" /></a>'; 
								 $_html .= '</form>
							 </td>';
					$_html .= '</tr>';
				}
			} else {
			$_html .= '<tr><td colspan="6" style="text-align:center;font-weight:bold;padding:10px">'.$this->l('Items not found').'</td></tr>';	
			}
			
			$_html .= '</table>';
		}
			
		     if($i!=0){
		    	$_html .= '<div style="margin:5px">';
		    	$_html .= $paging;
		    	$_html .= '</div>';
		    	}
		   return $_html;
    }
    
 private function _help_documentation(){
    	return '<h3 class="title-block-content">'.$this->l('Help / Documentation').'</h3>'.
    			'<b style="text-transform:uppercase">'.$this->l('MODULE DOCUMENTATION ').':</b>&nbsp;<a target="_blank" href="../modules/'.$this->name.'/readme.pdf" style="text-decoration:underline;font-weight:bold">readme.pdf</a>
    			<br/><br/>
    			<b style="text-transform:uppercase">'.$this->l('CONTACT ').':</b>&nbsp;<a target="_blank" href="mailto:developersaddons@gmail.com" style="text-decoration:underline;font-weight:bold">developersaddons@gmail.com</a>';
    }
   
    
    
 private function _welcome(){
    	return  '<h3 class="title-block-content">'.$this->l('Welcome').'</h3>'.
    			''
    			.$this->l('Welcome and thank you for purchasing the module.').
    			'<br/><br/>'
    			.$this->l('The module allows users add news on site.').
    			'<br/><br/>'
    			.$this->l('To configure module please read').'&nbsp;<b><a style="text-decoration:underline" id="tab-menu-6" onclick="tabs_custom(6)" href="javascript:void(0)">'.$this->l('Help / Documentation').'</a></b>';
    }
    
	
     private function _jsandcss(){
    	$_html = '';
    	
    	$_html .= '<link rel="stylesheet" href="../modules/'.$this->name.'/css/menu.css" type="text/css" />';
    	$_html .= '<script type="text/javascript" src="../modules/'.$this->name.'/js/menu.js"></script>';
    	
    	
     global $cookie,$smarty;
    	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		$iso = Language::getIsoById((int)($cookie->id_lang));
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
		$ad = dirname($_SERVER["PHP_SELF"]);
		
		if(defined('_MYSQL_ENGINE_') && substr(_PS_VERSION_,0,3) != '1.5'){
		$_html .=  '
			<script type="text/javascript">	
			var iso = \''.$isoTinyMCE.'\' ;
			var pathCSS = \''._THEME_CSS_DIR_.'\' ;
			var ad = \''.$ad.'\' ;
			</script>';
			$_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
			<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';
		$_html .= '
		<script type="text/javascript">id_language = Number('.$defaultLanguage.');</script>';
		}
		
		if(substr(_PS_VERSION_,0,3) == '1.5' || !defined('_MYSQL_ENGINE_')){
			if(substr(_PS_VERSION_,0,3) == '1.5'){
				$_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>';
			} else {
				$_html .= '<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>';
			}
		$_html .= '<script type="text/javascript">
					tinyMCE.init({
						mode : "specific_textareas",
						theme : "advanced",
						editor_selector : "rte",';
		if(substr(_PS_VERSION_,0,3) == '1.5'){
			 $_html .= 'skin:"cirkuit",';
		 	$_html  .=  'editor_selector : "rte",';
		
		}
			$_html  .=  'editor_deselector : "noEditor",';
			$_html  .=  '
						plugins : "safari,pagebreak,style,layer,table,advimage,advlink,inlinepopups,media,searchreplace,contextmenu,paste,directionality,fullscreen",
						// Theme options
						theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",';
			if(substr(_PS_VERSION_,0,3) == '1.5'){
				$_html  .= 	'theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,media,backcolor",';
			} else {
					$_html  .= 	'theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",';
			
			}
			$_html  .= 	'theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
						theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "bottom",
						theme_advanced_resizing : false,
						content_css : "'.__PS_BASE_URI__.'themes/'._THEME_NAME_.'/css/global.css",
						document_base_url : "'.__PS_BASE_URI__.'",
						width: "650",
						height: "auto",
						font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
						// Drop lists for link/image/media/template dialogs
						template_external_list_url : "lists/template_list.js",
						external_link_list_url : "lists/link_list.js",
						external_image_list_url : "lists/image_list.js",
						media_external_list_url : "lists/media_list.js",';
				if(substr(_PS_VERSION_,0,3) == '1.5'){
					$_html .= 'elements : "nourlconvert,ajaxfilemanager",
							  file_browser_callback : "ajaxfilemanager",';
				}else{
					$_html .= 'elements : "nourlconvert",';
				}
				$_html .= 'entity_encoding: "raw",
						convert_urls : false,
						language : "'.(file_exists(_PS_ROOT_DIR_.'/js/tinymce/jscripts/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en').'"
						
					});
					
	
		</script>';
		
		}
		
		if(substr(_PS_VERSION_,0,3) == '1.5'){
			//echo "<pre>"; var_dump($cookie); exit;
			$_html .= '<script type="text/javascript">';
			$_html .= 'var ad = "'.$ad.'";'; 
			$_html .= 'function ajaxfilemanager(field_name, url, type, win) {
						var ajaxfilemanagerurl = ad+"/ajaxfilemanager/ajaxfilemanager.php";
						switch (type) {
							case "image":
								break;
							case "media":
								break;
							case "flash": 
								break;
							case "file":
								break;
							default:
								return false;
					}
				    tinyMCE.activeEditor.windowManager.open({
				        url: ajaxfilemanagerurl,
				        width: 782,
				        height: 440,
				        inline : "yes",
				        close_previous : "no"
				    },{
				        window : win,
				        input : field_name
				    });
				  }
				    ';
		
			$_html .= '</script>';
		}
		
		
    	return $_html;
    }	
    
    
	public function translateText(){
		return array('seo_text'=> $this->l('News'),
					 'page'=>$this->l('Page'));
	}
	
	public function renderTplItems(){
		return Module::display(dirname(__FILE__).'/blocknewsadv.php', 'news.tpl');
	}
	
	public function renderTplListCat(){
		return Module::display(dirname(__FILE__).'/blocknewsadv.php', 'news-list.tpl');
	}
	
	public function renderTplItem(){
		return Module::display(dirname(__FILE__).'/blocknewsadv.php', 'news-item.tpl');
	}
    

}