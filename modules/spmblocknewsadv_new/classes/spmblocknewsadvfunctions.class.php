<?php
/**
 * 2011 - 2018 StorePrestaModules SPM LLC.
 *
 * MODULE spmblocknewsadv
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.2.2
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class spmblocknewsadvfunctions extends Module{
	
	
	private $_width = 1000;
	private $_height = 1000;
	private $_name = 'spmblocknewsadv';
	
	private $_block_img_width;
	private $_lists_img_width;
	private $_item_img_width;
	
	private $_http_host;
	
	private $_id_shop;

    private $_is_cloud;
    private $path_img_cloud;
	
	public function __construct(){
		$this->_block_img_width = Configuration::get($this->_name.'n_block_img_w');
		$this->_lists_img_width = Configuration::get($this->_name.'lists_img_w');
		$this->_item_img_width = Configuration::get($this->_name.'item_img_w');


        if (defined('_PS_HOST_MODE_'))
            $this->_is_cloud = 1;
        else
            $this->_is_cloud = 0;


        // for test
        //$this->_is_cloud = 1;
        // for test

        if($this->_is_cloud){
            $this->path_img_cloud = DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR;
        } else {
            $this->path_img_cloud = DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR.$this->_name.DIRECTORY_SEPARATOR;

        }
		
		$this->_http_host = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;

		$this->_id_shop = Context::getContext()->shop->id;

		

		$this->initContext();
		
	}
	
	private function initContext()
	{
	  $this->context = Context::getContext();
	}
	
	public function getItemsBlock(){

			$cookie = $this->context->cookie;
			$current_language = (int)$cookie->id_lang;

			$id_shop = $this->getIdShop();
			$limit  = Configuration::get($this->_name.'number_ni');

			$sql = '
			SELECT pc.*,
			(select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 1) as count_like,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 1 and ip = \''.pSQL($_SERVER['REMOTE_ADDR']).'\') as is_liked_news,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 0) as count_unlike,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 0 and ip = \''.pSQL($_SERVER['REMOTE_ADDR']).'\') as is_unliked_news
			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc
			LEFT JOIN `'._DB_PREFIX_.'spmblocknewsadv_data` pc_d
			ON(pc.id = pc_d.id_item) 
			WHERE pc.status = 1 and FIND_IN_SET('.(int)($id_shop).',pc.id_shop) 
			and pc_d.id_lang = '.(int)($current_language).' ORDER BY pc.`time_add` DESC LIMIT '.(int)($limit);
			
			$items = Db::getInstance()->ExecuteS($sql);
			$items_tmp = array();
			foreach($items as $k => $_item){
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
				WHERE pc.id_item = '.(int)($_item['id']).'
				');
				
				
				
				foreach ($items_data as $item_data){
		    		if($current_language == $item_data['id_lang']){
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['title'] = $item_data['title'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['content'] = $item_data['content'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['seo_url'] = $item_data['seo_url'];
		    			
		    			if(Tools::strlen($_item['img'])>0){
		    				$this->generateThumbImages(array('img'=>$_item['img'], 
		    												 'width'=>$this->_block_img_width,
		    												 'height'=>$this->_block_img_width 
		    												)
		    											);
		    				$img = Tools::substr($_item['img'],0,-4)."-".$this->_block_img_width."x".$this->_block_img_width.".jpg";
		    			} else {
		    				$img = $_item['img'];
		    			}
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['img'] = $img;
		    			
		    			
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['time_add'] = $_item['time_add'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['id'] = $_item['id'];

                        $items_tmp[$k]['data'][$item_data['id_lang']]['count_like'] = $_item['count_like'];
                        $items_tmp[$k]['data'][$item_data['id_lang']]['is_liked_news'] = $_item['is_liked_news'];
                        $items_tmp[$k]['data'][$item_data['id_lang']]['count_unlike'] = $_item['count_unlike'];
                        $items_tmp[$k]['data'][$item_data['id_lang']]['is_unliked_news'] = $_item['is_unliked_news'];
		    		}
		    	}
		    	
			}
		return array('items' => $items_tmp );
	}
	
	
	public function getHomeLastNews(){
		$cookie = $this->context->cookie;
			$current_language = (int)$cookie->id_lang;
			$id_shop = $this->getIdShop();
			
			$limit  = Configuration::get($this->_name.'hnumber_ni');


            $block_img_width = (int)Configuration::get($this->_name.'items_w_h');
            //var_dump($block_img_width);exit;

			$sql = '
			SELECT pc.*,
			(select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 1) as count_like,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 1 and ip = \''.pSQL($_SERVER['REMOTE_ADDR']).'\') as is_liked_news,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 0) as count_unlike,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 0 and ip = \''.pSQL($_SERVER['REMOTE_ADDR']).'\') as is_unliked_news

			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc
			LEFT JOIN `'._DB_PREFIX_.'spmblocknewsadv_data` pc_d
			ON(pc.id = pc_d.id_item) 
			WHERE pc.status = 1 and FIND_IN_SET('.(int)($id_shop).',pc.id_shop) 
			and pc_d.id_lang = '.(int)($current_language).' ORDER BY pc.`time_add` DESC LIMIT '.(int)($limit);
			
			$items = Db::getInstance()->ExecuteS($sql);
			$items_tmp = array();
			foreach($items as $k => $_item){
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
				WHERE pc.id_item = '.(int)($_item['id']).'
				');
				
				
				
				foreach ($items_data as $item_data){
		    		if($current_language == $item_data['id_lang']){

                        if(Tools::strlen($_item['img'])>0){
                            $this->generateThumbImages(array('img'=>$_item['img'],
                                    'width'=>$block_img_width,
                                    'height'=>$block_img_width
                                )
                            );
                            $img = Tools::substr($_item['img'],0,-4)."-".$block_img_width."x".$block_img_width.".jpg";
                        } else {
                            $img = $_item['img'];
                        }
                        $items_tmp[$k]['data'][$item_data['id_lang']]['img'] = $img;

		    			$items_tmp[$k]['data'][$item_data['id_lang']]['title'] = $item_data['title'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['content'] = $item_data['content'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['seo_url'] = $item_data['seo_url'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['time_add'] = $_item['time_add'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['id'] = $_item['id'];
		    			$items_tmp[$k]['data'][$item_data['id_lang']]['id_lang'] = $item_data['id_lang'];

                        $items_tmp[$k]['data'][$item_data['id_lang']]['count_like'] = $_item['count_like'];
                        $items_tmp[$k]['data'][$item_data['id_lang']]['is_liked_news'] = $_item['is_liked_news'];
                        $items_tmp[$k]['data'][$item_data['id_lang']]['count_unlike'] = $_item['count_unlike'];
                        $items_tmp[$k]['data'][$item_data['id_lang']]['is_unliked_news'] = $_item['is_unliked_news'];
		    		}
		    	}
		    	
			}
		return array('items' => $items_tmp );
		
		
	}
	
	public function getItems($_data = null){
		$cookie = $this->context->cookie;
		$id_shop = $this->getIdShop();
		$admin = isset($_data['admin'])?$_data['admin']:0;
		$start = isset($_data['start'])?$_data['start']:0;
		$step = isset($_data['step'])?$_data['step']:10;
		if($admin){
			
			
			$is_all_shop_select = 0;
			if(version_compare(_PS_VERSION_, '1.5', '>')){
				$current_shop_id = Shop::getContextShopID();
					
				if(!$current_shop_id){
					$is_all_shop_select = 1;
				}
				
			}
			
			if(Configuration::get($this->_name.'switch_s') && !$is_all_shop_select){
				$cond = ' WHERE FIND_IN_SET('.(int)($id_shop).',pc.id_shop)';
			} else {
				$cond = '';
			}
			
			
			
			$sql = '
			SELECT pc.*,
			    (select count(*) as count from `'._DB_PREFIX_.'spmblocknewsadv_news_like` pclike
				    WHERE pclike.news_id = pc.id) as count_likes

			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc '.$cond.'
			ORDER BY pc.`id` DESC
			LIMIT '.(int)($start).' ,'.(int)($step).'';
			//echo $sql;
			$items = Db::getInstance()->ExecuteS($sql);
			
			
			
			foreach($items as $k => $_item){
				
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
				WHERE pc.id_item = '.(int)($_item['id']).'
				');
				
				
				$defaultLanguage =  $cookie->id_lang;
				
				$tmp_title = '';
			
				
				// languages
				$languages_tmp_array = array();
				
				
				foreach ($items_data as $item_data){
		    		$languages_tmp_array[] = $item_data['id_lang'];
					
		    		$title = isset($item_data['title'])?$item_data['title']:'';
		    		if(Tools::strlen($tmp_title)==0){
		    			if(Tools::strlen($title)>0)
		    					$tmp_title = $title; 
		    		}
		    		
		    		
		    		if($defaultLanguage == $item_data['id_lang']){
		    			$items[$k]['title'] = $item_data['title'];
		    		} 
		    	}
		    	
		    	// languages
		    	$items[$k]['ids_lng'] = $languages_tmp_array;

                $items[$k]['count_likes'] = $_item['count_likes'];
		    	
		    	
		    	if(@Tools::strlen($items[$k]['title'])==0)
		    		$items[$k]['title'] = $tmp_title;
		    	
			}
			

			$data_count = Db::getInstance()->getRow('
			SELECT COUNT(`id`) AS "count"
			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc  '.$cond.'
			');
			
		} else {

            $is_search = isset($_data['is_search'])?$_data['is_search']:0;
            $search = isset($_data['search'])?$_data['search']:'';
            $is_arch = isset($_data['is_arch'])?$_data['is_arch']:0;
            $month = isset($_data['month'])?$_data['month']:0;
            $year = isset($_data['year'])?$_data['year']:0;

            $sql_condition = '';
            if($is_search == 1){
                $sql_condition = "AND (
	    		   LOWER(pc_d.title) LIKE BINARY LOWER('%".pSQL(trim($search))."%')
	    		   OR
	    		   LOWER(pc_d.content) LIKE BINARY LOWER('%".pSQL(trim($search))."%')
	    		   ) ";
            }

            if($is_arch == 1){
                $sql_condition = 'AND YEAR(pc.time_add) = "'.pSQL($year).'" AND
    							  MONTH(pc.time_add) = "'.pSQL($month).'"';
            }

			$step = Configuration::get($this->_name.'news_page');
			
			$current_language = (int)$cookie->id_lang;
			
			$sql = '
			SELECT pc.*,
			(select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 1) as count_like,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 1 and ip = \''.pSQL($_SERVER['REMOTE_ADDR']).'\') as is_liked_news,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 0) as count_unlike,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 0 and ip = \''.pSQL($_SERVER['REMOTE_ADDR']).'\') as is_unliked_news

			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc
			LEFT JOIN `'._DB_PREFIX_.'spmblocknewsadv_data` pc_d
			on(pc.id = pc_d.id_item)
			WHERE pc.status = 1 
			AND FIND_IN_SET('.(int)($id_shop).',pc.id_shop) '.$sql_condition.'
			and pc_d.id_lang = '.(int)($current_language).' 
			ORDER BY pc.`time_add` DESC
			LIMIT '.(int)($start).' ,'.(int)($step).'';
			$items_tmp = Db::getInstance()->ExecuteS($sql);
			
			$items = array();
			
			foreach($items_tmp as $k => $_item){
				
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
				WHERE pc.id_item = '.(int)($_item['id']).'
				');
				
				
				
				foreach ($items_data as $item_data){

		    		if($current_language == $item_data['id_lang']){
		    			$items[$k]['title'] = $item_data['title'];
		    			$items[$k]['content'] = $item_data['content'];
		    			$items[$k]['seo_description'] = $item_data['seo_description'];
		    			$items[$k]['seo_keywords'] = $item_data['seo_keywords'];
		    			$items[$k]['seo_url'] = $item_data['seo_url'];
		    			$items[$k]['id'] = $_item['id'];

		    			######### img #############
		    			if(Tools::strlen($_item['img'])>0){
							$this->generateThumbImages(array('img'=>$_item['img'],
				    												 'width'=>$this->_lists_img_width,
				    												 'height'=>$this->_lists_img_width
				    												)
				    											);
				    		$img = Tools::substr($_item['img'],0,-4)."-".$this->_lists_img_width."x".$this->_lists_img_width.".jpg";
				    	} else {
				    		$img = $_item['img'];
						}

				    	$items[$k]['img'] = $img;
		    			######### img #############

		    			$items[$k]['time_add'] = $_item['time_add'];

                        $items[$k]['count_like'] = $_item['count_like'];
                        $items[$k]['is_liked_news'] = $_item['is_liked_news'];
                        $items[$k]['count_unlike'] = $_item['count_unlike'];
                        $items[$k]['is_unliked_news'] = $_item['is_unliked_news'];
		    		}
		    	}
		    }
			

			$data_count = Db::getInstance()->getRow('
			SELECT COUNT(pc.`id`) AS "count"
			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc LEFT JOIN `'._DB_PREFIX_.'spmblocknewsadv_data` pc_d
			on(pc.id = pc_d.id_item)
			WHERE pc.status = 1 AND FIND_IN_SET('.(int)($id_shop).',pc.id_shop) '.$sql_condition.' and pc_d.id_lang = '.(int)($current_language).'
			');
			
		}
		return array('items' => $items, 'count_all' => $data_count['count']);
	}

    private function  _translit( $str )
    {
        $str  = str_replace(array("®","'",'"','`','?','!','.','=',':','&','+',',','’', ')', '(', '$', '{', '}'), array(''), $str );

        $arrru = array ("А","а","Б","б","В","в","Г","г","Д","д","Е","е","Ё","ё","Ж","ж","З","з","И","и","Й","й","К","к","Л","л","М","м","Н","н", "О","о","П","п","Р","р","С","с","Т","т","У","у","Ф","ф","Х","х","Ц","ц","Ч","ч","Ш","ш","Щ","щ","Ъ","ъ","Ы","ы","Ь", "ь","Э","э","Ю","ю","Я","я",
            " ","-",",","«","»","+","/","(",")",".");

        $arren = array ("a","a","b","b","v","v","g","g","d","d","e","e","e","e","zh","zh","z","z","i","i","y","y","k","k","l","l","m","m","n","n", "o","o","p","p","r","r","s","s","t","t","u","u","ph","f","h","h","c","c","ch","ch","sh","sh","sh","sh","","","i","i","","","e", "e","yu","yu","ya","ya",
            "-","-","","","","","","","","");

        $textout = '';
        $textout = str_replace($arrru,$arren,$str);

        //$textout = preg_replace("/[0-9]/u", "", $textout);

        $textout = str_replace("--","-",$textout);


        $separator = "-";
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array( '&' => 'and');
        $textout = mb_strtolower( trim( $textout ), 'UTF-8' );
        $textout = str_replace( array_keys($special_cases), array_values( $special_cases), $textout );
        $textout = preg_replace( $accents_regex, '$1', htmlentities( $textout, ENT_QUOTES, 'UTF-8' ) );
        $textout = preg_replace("/[^a-z0-9]/u", "$separator", $textout);
        $textout = preg_replace("/[$separator]+/u", "$separator", $textout);

        if(Tools::strlen($textout)==0)
            $textout = Tools::strtolower(Tools::passwdGen(6,NO_NUMERIC));

        return Tools::strtolower($textout);
    }
	
	public function saveItem($data){
	
		$error = 0;
		$error_text = '';
		$item_status = $data['item_status'];
		$time_add = date('Y-m-d H:i:s',strtotime($data['time_add']));


        $related_posts = $data['ids_related_posts']?$data['ids_related_posts']:array();

        $related_posts = implode(",",$related_posts);
        if(Tools::strlen($related_posts)==0) $related_posts = 0;


		$related_products = $data['related_products'];
		$related_products = explode("-",$related_products,-1);
	    $related_products = implode(",",$related_products);
		if(Tools::strlen($related_products)==0) $related_products = 0;
		
			
		
		$ids_shops = implode(",",$data['cat_shop_association']);

        $iscomments = $data['iscomments'];


        Db::getInstance()->Execute('BEGIN');
		
		
		$sql = 'INSERT into `'._DB_PREFIX_.'spmblocknewsadv` SET
							   `status` = \''.(int)($item_status).'\',
							   `id_shop`= "'.pSQL($ids_shops).'",
							   `related_products` = "'.pSQL($related_products).'",
							   `related_posts` = "'.pSQL($related_posts).'",
							   `time_add` = "'.pSQL($time_add).'",
							   `is_comments` = '.(int)$iscomments.'
							   ';
		Db::getInstance()->Execute($sql);
		
		$item_id = Db::getInstance()->Insert_ID();
		
		foreach($data['data_title_content_lang'] as $language => $item){
		
		$title = $item['title'];
		$content = $item['content'];
		$seokeywords = $item['seokeywords'];
	    $seodescription = $item['seodescription'];
	    
		
		$seo_url_pre = Tools::strlen($item['seo_url'])>0?$item['seo_url']:$title;
	    $seo_url = $this->_translit($seo_url_pre);
		
	    
		$sql = 'INSERT into `'._DB_PREFIX_.'spmblocknewsadv_data` SET
							   `id_item` = \''.(int)($item_id).'\',
							   `id_lang` = \''.(int)($language).'\',
							   `title` = \''.pSQL($title).'\',
							   `content` = "'.pSQL($content, true).'",
							   `seo_keywords` = "'.pSQL($seokeywords).'",
							   `seo_description` = "'.pSQL($seodescription).'",
							   `seo_url` = "'.pSQL($seo_url).'"
							   ';
		
		Db::getInstance()->Execute($sql);
		
		}



        include_once(_PS_MODULE_DIR_.$this->_name.'/spmblocknewsadv.php');
        $obj = new spmblocknewsadv();

        if($obj->is_demo == 0) {
            $data_save_img = $this->saveImage(array('post_id' => $item_id));
            $error = $data_save_img['error'];
            $error_text  = $data_save_img['error_text'];

        }



        if($error && Tools::strlen($error_text)>0){
            Db::getInstance()->Execute('ROLLBACK');
        }  else {
            Db::getInstance()->Execute('COMMIT');
        }
		
		### posts API ###
			
			$_info = $this->getItem(array('id' => $item_id,'site'=>1));
			$news_name = isset($_info['item'][0]['title'])?$_info['item'][0]['title']:'';
			$picture = isset($_info['item'][0]['img'])?$_info['item'][0]['img']:'';
			$seo_url  = isset($_info['item'][0]['seo_url'])?$_info['item'][0]['seo_url']:'';
			
			
    		include_once(_PS_MODULE_DIR_.$this->_name.'/classes/postshelp.class.php');
			$postshelp = new postshelp();
			$cookie = $this->context->cookie;
			$id_lang = (int)($cookie->id_lang);

            $data_url = $this->getSEOURLs();
            $news_item_url = $data_url['news_item_url'];



            if(Configuration::get($this->_name.'rew_on')){
                $product_link = $news_item_url.$seo_url;
            } else {
                $product_link = $news_item_url.'?id='.$item_id;
            }


            if(Tools::strlen($picture)>0) {
                if ($this->_is_cloud) {
                    $image_path = _PS_MODULE_DIR_ .$this->_name . "/upload/" . $picture;

                    $picture = $this->_http_host . "modules/" . $this->_name . "/upload/" . $picture;

                } else {
                    $image_path = _PS_ROOT_DIR_. "/upload/" . $this->_name . "/" . $picture;

                    $picture = $this->_http_host . "upload/" . $this->_name . "/" . $picture;

                }
            }else {
                $picture = '';
                $image_path = '';
            }


			$data = array(
								  		'name'=>$this->_name,
								  		'product_name'=>$news_name,
								  		'product_link'=>$product_link,
								  		'image' => $picture,
								  		'id_lang'=>$id_lang,
                                        'image_path'=>$image_path,
								  		);

        //var_dump($data);exit;
			$postshelp->postToAPI($data);
			### posts API	




        $this->_clearSmartyCache();

		return array('error' => $error,
					 'error_text' => $error_text);
		
	}
	
	public function saveImage($data = null){
		
		$error = 0;
		$error_text = '';
		
		$post_id = $data['post_id'];
		$post_images = isset($data['post_images'])?$data['post_images']:'';
		
		$files = $_FILES['news_image'];


		############### files ###############################
		if(!empty($files['name']))
			{
		      if(!$files['error'])
		      {
				  $type_one = $files['type'];
				  $ext = explode("/",$type_one);
				  
				  if(strpos('_'.$type_one,'image')<1)
				  {
				  	$error_text = $this->l('Invalid file type, please try again!');
				  	$error = 1;

				  }elseif(!in_array($ext[1],array('png','x-png','gif','jpg','jpeg','pjpeg'))){
				  	$error_text = $this->l('Wrong file format, please try again!');
				  	$error = 1;
				  	
				  } else {
				  	
				  		$info_post = $this->getItem(array('id'=>$post_id));
				  		$post_item = $info_post['item'];
				  		$img_post = $post_item[0]['img'];
				  		
				  		if(Tools::strlen($img_post)>0){


                            $name_thumb = current(explode(".",$img_post));
                            unlink(dirname(__FILE__).$this->path_img_cloud.$name_thumb.".jpg");

                            $posts_block_img = dirname(__FILE__).$this->path_img_cloud.$name_thumb.'-'.$this->_lists_img_width.'x'.$this->_lists_img_width.'.jpg';
                            @unlink($posts_block_img);

                            $lists_img = dirname(__FILE__).$this->path_img_cloud.$name_thumb.'-'.$this->_item_img_width.'x'.$this->_item_img_width.'.jpg';
                            @unlink($lists_img);

                            $post_img = dirname(__FILE__).$this->path_img_cloud.$name_thumb.'-'.$this->_block_img_width.'x'.$this->_block_img_width.'.jpg';
                            @unlink($post_img);
				  			
				  		} 
							
					  	srand((double)microtime()*1000000);
					 	$uniq_name_image = uniqid(rand());
					 	$type_one = Tools::substr($type_one,6,Tools::strlen($type_one)-6);
					 	$filename = $uniq_name_image.'.'.$type_one; 
					 	
						move_uploaded_file($files['tmp_name'], dirname(__FILE__).$this->path_img_cloud.$filename);
						
						
						$name_img = dirname(__FILE__).$this->path_img_cloud.$filename;
						$dir_without_ext = dirname(__FILE__).$this->path_img_cloud.$uniq_name_image;
						
						$this->copyImage(array('dir_without_ext'=>$dir_without_ext,
												'name'=>$name_img)
										);
										
						
						//Image in the block "Left Right Home Footer"				
						$this->copyImage(array('dir_without_ext'=>$dir_without_ext,
											   'name'=>$name_img,
											   'width'=>$this->_block_img_width,
											   'height'=>$this->_block_img_width
											   )
										);
						// Image in the block "Left Right Home Footer"
						
						// Image in lists				
						$this->copyImage(array('dir_without_ext'=>$dir_without_ext,
											   'name'=>$name_img,
											   'width'=>$this->_lists_img_width,
											   'height'=>$this->_lists_img_width
											   )
										);
						// Image in lists					
										
						// Image on item page			
						$this->copyImage(array('dir_without_ext'=>$dir_without_ext,
											   'name'=>$name_img,
											   'width'=>$this->_item_img_width,
											   'height'=>$this->_item_img_width
											   )
										);
						// Image on item page					
						
						// delete original image				
						@unlink(dirname(__FILE__).$this->path_img_cloud.$uniq_name_image.".".$ext[1]);
						
						
						$img_return = $uniq_name_image.'.jpg';

                      //var_dump($img_return);
			  		
				  		$this->_updateImgToItem(array('post_id' => $post_id,
				  									  'img' =>  $img_return
				  									  )
				  								);

				  }
				}
				else
					{
					### check  for errors ####
			      	switch($files['error'])
						{
							case '999':
							default:
								$error_text = $this->l('Error downloading file!');
							break;
						}
						$error = 1;
			      	########
					   
					}
			}  else {
				if($post_images != "on"){
				$this->_updateImgToItem(array('post_id' => $post_id,
				  							  'img' =>  ""
				  							  )
				  						);
				}
			}
		
			
		return array('error' => $error,
					 'error_text' => $error_text);
	
	
	}
	
	private function _updateImgToItem($data = null){
		
		$post_id = $data['post_id'];
		$img = $data['img'];
		
		$id_shop = $this->getIdShop();
			
		// update
		$sql = 'UPDATE `'._DB_PREFIX_.'spmblocknewsadv` SET
							   `img` = \''.pSQL($img).'\'
							   WHERE id = '.(int)($post_id).' and id_shop = '.(int)($id_shop).'
							   ';
		Db::getInstance()->Execute($sql);
		
	}


    public function updateItemStatus($data){

        $id = (int)$data['id'];
        $status = (int)$data['status'];

        $sql_update = 'UPDATE `'._DB_PREFIX_.'spmblocknewsadv` SET
						`status` = '.(int)$status.'
						WHERE id ='.(int)$id;
        Db::getInstance()->Execute($sql_update);

        $this->_clearSmartyCache();


    }


    public function updateItemComments($data){

        $id = (int)$data['id'];
        $status = (int)$data['status'];

        $sql_update = 'UPDATE `'._DB_PREFIX_.'spmblocknewsadv` SET
						`is_comments` = '.(int)$status.'
						WHERE id ='.(int)$id;
        Db::getInstance()->Execute($sql_update);

        $this->_clearSmartyCache();

    }
	
	public function deleteItem($data){
		
		
		$id = $data['id'];

        include_once(_PS_MODULE_DIR_.$this->_name.'/spmblocknewsadv.php');
        $obj = new spmblocknewsadv();

        if($obj->is_demo == 0) {
            $this->deleteImg(array('id' => $id));
        }
		
		$sql = 'DELETE FROM `'._DB_PREFIX_.'spmblocknewsadv`
					   WHERE id ='.(int)($id).'';
		Db::getInstance()->Execute($sql);
		
		$sql = 'DELETE FROM `'._DB_PREFIX_.'spmblocknewsadv_data`
					   WHERE id_item ='.(int)($id).'';
		Db::getInstance()->Execute($sql);

        $this->_clearSmartyCache();
		
	}
	
	public function deleteImg($data = null){
		$id = $data['id'];
		
		$info_post = $this->getItem(array('id'=>$id));
  		$img = $info_post['item'][0]['img'];
				  		
		$this->_updateImgToItem(array('post_id' => $id,
				  					  'img' =>  ""
				  					 )
				  				);
				  				
		@unlink(dirname(__FILE__).$this->path_img_cloud.$img);
		
		//var_dump($img); exit;
		$name_thumb = current(explode(".",$img));
		
		$items_block_img = dirname(__FILE__).$this->path_img_cloud.$name_thumb.'-'.$this->_block_img_width.'x'.$this->_block_img_width.'.jpg';
		@unlink($items_block_img);
						
		$lists_img = dirname(__FILE__).$this->path_img_cloud.$name_thumb.'-'.$this->_lists_img_width.'x'.$this->_lists_img_width.'.jpg';
		@unlink($lists_img);
						
		$item_img = dirname(__FILE__).$this->path_img_cloud.$name_thumb.'-'.$this->_item_img_width.'x'.$this->_item_img_width.'.jpg';
		@unlink($item_img);

        $this->_clearSmartyCache();
		
	}
	
	public function getItem($_data){
		$id = $_data['id'];
		$site = isset($_data['site'])?$_data['site']:0;
		$id_shop = $this->getIdShop();
		$cookie = $this->context->cookie;
		if($site){
			
			$current_language = (int)$cookie->id_lang;
			
			$sql = '
			SELECT pc.*,
			(select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 1) as count_like,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 1 and ip = \''.pSQL($_SERVER['REMOTE_ADDR']).'\') as is_liked_news,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 0) as count_unlike,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 0 and ip = \''.pSQL($_SERVER['REMOTE_ADDR']).'\') as is_unliked_news

			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc
			LEFT JOIN  `'._DB_PREFIX_.'spmblocknewsadv_data` pc_d
			ON(pc_d.id_item = pc.id)
			WHERE pc.id = '.(int)($id).' AND FIND_IN_SET('.(int)($id_shop).',pc.id_shop)  
			AND pc.status = 1 and pc_d.id_lang = '.(int)($current_language).' ';
			
			$item = Db::getInstance()->ExecuteS($sql);
			
			foreach($item as $k => $_item){
				
				if(Tools::strlen($_item['img'])>0){
					$item[$k]['img_orig'] = $_item['img'];	
					$this->generateThumbImages(array('img'=>$_item['img'], 
		    										 'width'=>$this->_item_img_width,
		    										 'height'=>$this->_item_img_width 
		    												)
		    											);
		    		$img = Tools::substr($_item['img'],0,-4)."-".$this->_item_img_width."x".$this->_item_img_width.".jpg";
				} else {
		    		$img = $_item['img'];
				}
		    	
		    	$item[$k]['img'] = $img;
				
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
				WHERE pc.id_item = '.(int)($_item['id']).'
				');
				
				foreach ($items_data as $item_data){
					if($current_language == $item_data['id_lang']){
						
		    			$item[$k]['title'] = $item_data['title'];
		    			$item[$k]['content'] = $item_data['content'];
		    			$item[$k]['seo_url'] = $item_data['seo_url'];
		    			$item[$k]['seo_description'] = $item_data['seo_description'];
		    			$item[$k]['seo_keywords'] = $item_data['seo_keywords'];


		    			
						}
		    	}



                $item[$k]['count_like'] = $_item['count_like'];
                $item[$k]['is_liked_news'] = $_item['is_liked_news'];
                $item[$k]['count_unlike'] = $_item['count_unlike'];
                $item[$k]['is_unliked_news'] = $_item['is_unliked_news'];

                $item[$k]['time_add_rss'] = date(DATE_ATOM,strtotime($_item['time_add']));
		    	
			}
			
		} else { 	
			
			
			$sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc
			WHERE id = '.(int)($id).' ';
			$item = Db::getInstance()->ExecuteS($sql);
			
			foreach($item as $k => $_item){
				
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
				WHERE pc.id_item = '.(int)($_item['id']).'
				');

                $languages_tmp_array = array();

				foreach ($items_data as $item_data){
                        $languages_tmp_array[] = $item_data['id_lang'];

		    			$item['data'][$item_data['id_lang']]['title'] = $item_data['title'];
		    			$item['data'][$item_data['id_lang']]['content'] = $item_data['content'];
		    			$item['data'][$item_data['id_lang']]['seo_url'] = $item_data['seo_url'];
		    			$item['data'][$item_data['id_lang']]['seo_description'] = $item_data['seo_description'];
		    			$item['data'][$item_data['id_lang']]['seo_keywords'] = $item_data['seo_keywords'];




                }

                $item['data'][$k]['ids_lng'] = $languages_tmp_array;

                $lang_for_category = array();
                foreach($languages_tmp_array as $lng_id){
                    $data_lng = Language::getLanguage($lng_id);
                    $lang_for_category[] = $data_lng['iso_code'];
                }
                $lang_for_category = implode(",",$lang_for_category);

                $item['data'][$k]['iso_lang'] = $lang_for_category;
		    	
			}
		}
			
	   return array('item' => $item);
	}
	
	
	public function updateItem($data){

        $error = 0;
        $error_text = '';

		
		$id = $data['id'];
		
		$item_status = $data['item_status'];
		$post_images = $data['post_images'];
		
		$time_add = date('Y-m-d H:i:s',strtotime($data['time_add']));
		
		
		$ids_shops = implode(",",$data['cat_shop_association']);

        $ids_related_posts = ($data['ids_related_posts'])?$data['ids_related_posts']:array();
		
		$related_posts = implode(",",$ids_related_posts);
		if(Tools::strlen($related_posts)==0) $related_posts = 0;
		
		$related_products = $data['related_products'];
		$related_products = explode("-",$related_products,-1);
	    $related_products = implode(",",$related_products);
		if(Tools::strlen($related_products)==0) $related_products = 0;
		
		$iscomments = $data['iscomments'];

        Db::getInstance()->Execute('BEGIN');

		// update
		$sql = 'UPDATE `'._DB_PREFIX_.'spmblocknewsadv` SET
							   `status` = \''.(int)($item_status).'\',
							   id_shop = "'.pSQL($ids_shops).'",
							   `related_products` = "'.pSQL($related_products).'",
							   `related_posts` = "'.pSQL($related_posts).'",
							   `time_add` = "'.pSQL($time_add).'",
							   `is_comments` = '.(int)$iscomments.'
							   WHERE id = '.(int)($id).'
							   ';
		//echo $sql; exit;
		Db::getInstance()->Execute($sql);
		
		/// delete tabs data
		$sql = 'DELETE FROM `'._DB_PREFIX_.'spmblocknewsadv_data` WHERE id_item = '.(int)($id).'';
		Db::getInstance()->Execute($sql);
		
		foreach($data['data_title_content_lang'] as $language => $item){
		
		$title = $item['title'];
		$content = $item['content'];
		$seokeywords = $item['seokeywords'];
	    $seodescription = $item['seodescription'];
	    $seo_url_pre = Tools::strlen($item['seo_url'])>0?$item['seo_url']:$title;


	    $translite_seo = $this->_translit($seo_url_pre);
        $seo_url = $translite_seo;
	    
		
	    
		
		$sql = 'INSERT into `'._DB_PREFIX_.'spmblocknewsadv_data` SET
							   `id_item` = \''.(int)($id).'\',
							   `id_lang` = \''.(int)($language).'\',
							   `title` = \''.pSQL($title).'\',
							   `content` = "'.pSQL($content, true).'",
							   `seo_keywords` = "'.pSQL($seokeywords).'",
							   `seo_description` = "'.pSQL($seodescription).'",
							   `seo_url` = "'.pSQL($seo_url).'"
							   ';

		Db::getInstance()->Execute($sql);
		}


        include_once(_PS_MODULE_DIR_.$this->_name.'/spmblocknewsadv.php');
        $obj = new spmblocknewsadv();

        if($obj->is_demo == 0) {
            $data_save_img = $this->saveImage(array('post_id' => $id, 'post_images' => $post_images));

            $error = $data_save_img['error'];
            $error_text  = $data_save_img['error_text'];
        }


        if($error && Tools::strlen($error_text)>0){
            Db::getInstance()->Execute('ROLLBACK');
        } else {
            Db::getInstance()->Execute('COMMIT');
            $this->_clearSmartyCache();
        }


        return array('error' => $error,
            'error_text' => $error_text);


	}
	
	

	
	public function copyImage($data){
	
		$filename = $data['name'];
		$dir_without_ext = $data['dir_without_ext'];
		
		$is_height_width = 0;
		if(isset($data['width']) && isset($data['height'])){
			$is_height_width = 1;
		}
			
		
		$width = isset($data['width'])?$data['width']:$this->_width;
		$height = isset($data['height'])?$data['height']:$this->_height;
		
		$width_orig_custom = $width;
		$height_orig_custom = $height;
		
		if (!$width){ $width = 85;}
		if (!$height){ $height = 85;}
		// Content type
		$size_img = getimagesize($filename);
		// Get new dimensions
		list($width_orig, $height_orig) = getimagesize($filename);
		$ratio_orig = $width_orig/$height_orig;
		
		if($width_orig>$height_orig){
		$height =  $width/$ratio_orig;
		}else{ 
		$width = $height*$ratio_orig;
		}
		if($width_orig<$width){
			$width = $width_orig;
			$height = $height_orig;
		}
	
		$image_p = imagecreatetruecolor($width, $height);
		$bgcolor=ImageColorAllocate($image_p, 255, 255, 255);
		//   
		imageFill($image_p, 5, 5, $bgcolor);
	
		if ($size_img[2]==2){ $image = imagecreatefromjpeg($filename);}                         
		else if ($size_img[2]==1){  $image = imagecreatefromgif($filename);}                         
		else if ($size_img[2]==3) { $image = imagecreatefrompng($filename); }
	
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
		// Output
		
		if ($is_height_width)
			$users_img = $dir_without_ext.'-'.$width_orig_custom.'x'.$height_orig_custom.'.jpg';
		else
		 	$users_img = $dir_without_ext.'.jpg';
		
		if ($size_img[2]==2)  imagejpeg($image_p, $users_img, 100);                         
		else if ($size_img[2]==1)  imagejpeg($image_p, $users_img, 100);                        
		else if ($size_img[2]==3)  imagejpeg($image_p, $users_img, 100);
		imageDestroy($image_p);
		imageDestroy($image);
		//unlink($filename);

	}
	

public function PageNav($start,$count,$step, $_data =null )
	{
		$_admin = isset($_data['admin'])?$_data['admin']:null;

        $all_items = isset($_data['all_items'])?$_data['all_items']:'';
        $is_search = isset($_data['is_search'])?$_data['is_search']:0;

        $is_arch = isset($_data['is_arch'])?$_data['is_arch']:0;
        $month = isset($_data['month'])?$_data['month']:0;
        $year = isset($_data['year'])?$_data['year']:0;

        $data_url = $this->getSEOURLs();

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/PageNav.phtml');
        $res = ob_get_clean();

		return $res;
	}

	public function getTranslateText(){
		include_once(_PS_MODULE_DIR_.$this->_name.'/spmblocknewsadv.php');
		$obj = new spmblocknewsadv();
    	$data = $obj->translateText();
    	
		return array('seo_text'=> $data['seo_text'],
					 'page'=>$data['page']);
	}
	
	public function getIdShop(){
		$id_shop = Context::getContext()->shop->id;
    	return $id_shop;
	}
	
	public function getTransformSEOURLtoID($_data){
	
	if(Configuration::get($this->_name.'rew_on') == 1 && !is_numeric($_data['id'])){
			$id = $_data['id'];
			$sql = '
					SELECT pc.id_item as id
					FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
					WHERE seo_url = "'.pSQL($id).'"';
			$data_id = Db::getInstance()->GetRow($sql);
			$id = $data_id['id'];
		} else {
			$id = (int)$_data['id'];
		}
		
		return $id;
	}	
	
 	public function isURLRewriting(){
    	$_is_rewriting_settings = 0;
    	if(Configuration::get('PS_REWRITING_SETTINGS') && Configuration::get($this->_name.'rew_on') == 1){
			$_is_rewriting_settings = 1;
		} 
		return $_is_rewriting_settings;
    }

    public function getLangISO(){
        $cookie = $this->context->cookie;

        $id_shop = Context::getContext()->shop->id;
        if($id_shop) {
            $all_laguages = Language::getLanguages(true,$id_shop);
        } else {
            $all_laguages = Language::getLanguages(true);
        }

        $id_lang = (int)$cookie->id_lang;

        if($this->isURLRewriting() && sizeof($all_laguages)>1)
            $iso_lang = Language::getIsoById((int)($id_lang))."/";
        else
            $iso_lang = '';

        return $iso_lang;

    }


    public function createRSSFile($post_title,$post_description,$post_link,$post_pubdate,$img)
	{
		


        $data_url = $this->getSEOURLs();
        $news_item_url = $data_url['news_item_url'];
        if(Configuration::get($this->_name.'rew_on')){
            $post_link = $news_item_url.$post_link;
        } else {

            $post_link = $news_item_url.$post_link;
        }
		

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/createRSSFile.phtml');
        $returnITEM = ob_get_clean();

		return $returnITEM;
	}
	
	public function getItemsForRSS(){
			$id_shop = $this->getIdShop();
		
			$step = Configuration::get($this->_name.'number_rssitems');
			
			$cookie = $this->context->cookie;
			$current_language = (int)$cookie->id_lang;
			
			$sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc
			LEFT JOIN `'._DB_PREFIX_.'spmblocknewsadv_data` pc_d
			on(pc.id = pc_d.id_item)
			WHERE pc.status = 1 and pc.id_shop = '.(int)($id_shop).' and pc_d.id_lang = '.(int)($current_language).' 
			ORDER BY pc.`id` DESC
			LIMIT 0 ,'.(int)($step).'';
			$items_tmp = Db::getInstance()->ExecuteS($sql);
			
			$items = array();
			
			foreach($items_tmp as $k => $_item){
				
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
				WHERE pc.id_item = '.(int)($_item['id']).'
				');
				
				if(Tools::strlen($_item['img'])>0){
					$this->generateThumbImages(array('img'=>$_item['img'], 
		    												 'width'=>$this->_lists_img_width,
		    												 'height'=>$this->_lists_img_width 
		    												)
		    											);
		    		$img = Tools::substr($_item['img'],0,-4)."-".$this->_lists_img_width."x".$this->_lists_img_width.".jpg";
		    	} else {
		    		$img = $_item['img'];
				}
		    		
		    	$items[$k]['img'] = $img;
				
				
				foreach ($items_data as $item_data){
		    		
		    		if($current_language == $item_data['id_lang']){
		    			$items[$k]['title'] = $item_data['title'];
		    			$items[$k]['content'] = $item_data['content'];
		    			$items[$k]['seo_description'] = $item_data['seo_description'];
		    			$items[$k]['seo_keywords'] = $item_data['seo_keywords'];
		    			
		    			$items[$k]['seo_url'] = $item_data['seo_url'];
		    			$items[$k]['id'] = $_item['id'];
		    			//$items[$k]['img'] = $_item['img'];
		    			$items[$k]['time_add'] = $_item['time_add'];
		    			$items[$k]['pubdate'] = date('D, d M Y H:i:s +0000',strtotime($_item['time_add']));
		    			
		    		} 
		    	}
		    }
			

			return array('items' => $items);
	}
	
	
	public function getfacebooklocale()
	{
        $locales = array();

        if (($xml=simplexml_load_file(_PS_MODULE_DIR_ . $this->_name."/lib/facebook_locales.xml")) === false)
            return $locales;

        for ($i=0;$i<sizeof($xml);$i++)
        {
            $locale = $xml->locale[$i]->codes->code->standard->representation;
            $locales[]= $locale;
        }

        return $locales;
	}
	
 	public function getfacebooklib($id_lang){
    	
    	$lang = new Language((int)$id_lang);
		
    	$lng_code = isset($lang->language_code)?$lang->language_code:$lang->iso_code;
    	if(strstr($lng_code, '-')){
			$res = explode('-', $lng_code);
			$language_iso = Tools::strtolower($res[0]).'_'.Tools::strtoupper($res[1]);
			$rss_language_iso = Tools::strtolower($res[0]);
		} else {
			$language_iso = Tools::strtolower($lng_code).'_'.Tools::strtoupper($lng_code);
			$rss_language_iso = $lng_code;
		}
			
			
		if (!in_array($language_iso, $this->getfacebooklocale()))
			$language_iso = "en_US";
		
		if (Configuration::get('PS_SSL_ENABLED') == 1)
			$url = "https://";
		else
			$url = "http://";
		
		
		
		return array('url'=>$url . 'connect.facebook.net/'.$language_iso.'/all.js#xfbml=1',
					  'lng_iso' => $language_iso, 'rss_language_iso' => $rss_language_iso);
    }
    
    
    public function generateThumbImages($data){
		
		$filename = $data['img'];
		$orig_name_img= $data['img'];
		$width = $data['width'];
		$height = $data['height'];
		
		$filename = Tools::substr($filename,0,-4)."-".$width."x".$height.".jpg";
		
		$name_img = dirname(__FILE__).$this->path_img_cloud.$filename;
		
		
		if(@filesize($name_img)==0){
		
		$uniq_name_image_without_ext = current(explode(".",$orig_name_img));
		
		$dir_without_ext = dirname(__FILE__).$this->path_img_cloud.$uniq_name_image_without_ext;
						
		$this->copyImage(
			array('dir_without_ext'=>$dir_without_ext,
			      'name'=>dirname(__FILE__).$this->path_img_cloud.$orig_name_img,
				  'width'=>$width,
				  'height'=>$height
				  )
				    );	
		}
		
		
						
	}
	
	
    private function _getCategoriesForSitemap(){
    		$id_shop = $this->getIdShop();
    		
    		
    		$is_all_shop_select = 0;
    		if(version_compare(_PS_VERSION_, '1.5', '>')){
    			$current_shop_id = Shop::getContextShopID();
    				
    			if(!$current_shop_id){
    				$is_all_shop_select = 1;
    			}
    		
    		}
    			
    		if(!$is_all_shop_select){
    			$cond = ' FIND_IN_SET('.(int)($id_shop).',pc.id_shop) and ';
    		} else {
    			$cond = '';
    		}
    			
    		
			$sql = '
			SELECT count(*) as count
			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc
			WHERE  '.$cond.' status = 1
			';
			$items = Db::getInstance()->getRow($sql);
			
			$count_news = $items['count'];
			
			$items_tmp = array();
			if($count_news != 0){
				
							
				if (Configuration::get('PS_REWRITING_SETTINGS')) {

				$sql = '
						SELECT distinct id_lang
						FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
						';
						$items = Db::getInstance()->ExecuteS($sql);
				foreach($items as $_item){
					
				$_is_friendly_url = $this->isURLRewriting();
				if($_is_friendly_url)
					$_iso_lng = Language::getIsoById((int)($_item['id_lang']))."/";
				else
					$_iso_lng = '';
		    				
		    		$url = $this->_http_host.$_iso_lng.'news/';
		    		$items_tmp['data']['url'][] = $url;	
				}	
					
				
		    	} else {
		    		$url = $this->_http_host.'/modules/'.$this->_name.'/news.php';
		    		$items_tmp['data']['url'] = $url;
		    	}
			}
			//echo "<pre>"; var_Dump($items_tmp); exit;
			return array('all_categories'=>$items_tmp);
    }
    
    
    private function _getPostsForSitemap(){
		
		
			$id_shop = $this->getIdShop();
			
			$is_all_shop_select = 0;
			if(version_compare(_PS_VERSION_, '1.5', '>')){
				$current_shop_id = Shop::getContextShopID();
			
				if(!$current_shop_id){
					$is_all_shop_select = 1;
				}
			
			}
			 
			if(!$is_all_shop_select){
				$cond = ' FIND_IN_SET('.(int)($id_shop).',pc.id_shop) and ';
			} else {
				$cond = '';
			}

        $data_url = $this->getSEOURLs();
        $news_item_url = $data_url['news_item_url'];

			$sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc
			WHERE '.$cond.' pc.status = 1
			ORDER BY pc.`time_add` DESC';
			
			$items = Db::getInstance()->ExecuteS($sql);
			$items_tmp = array();
			foreach($items as $_item){
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
				WHERE pc.id_item = '.(int)($_item['id']).'
				');
				foreach ($items_data as $item_data){
		    		//if($current_language == $item_data['id_lang']){
		    			
		    			/*if (Configuration::get('PS_REWRITING_SETTINGS')) {
		    				$_is_friendly_url = $this->isURLRewriting();
							if($_is_friendly_url)
								$_iso_lng = Language::getIsoById((int)($item_data['id_lang']))."/";
							else
								$_iso_lng = '';
								
		    				$url = $this->_http_host.$_iso_lng.'news/'.$item_data['seo_url'];
		    				$items_tmp[]['data'][$item_data['id_lang']]['url'] = $url;
		    			} else {
		    				$url = $this->_http_host.$_iso_lng.'modules/'.$this->_name.'/news-item.php?id='.$_item['id'];
		    				$items_tmp[]['data'][$item_data['id_lang']]['url'] = $url;
		    			}*/





                    if(Configuration::get($this->_name.'rew_on')){
                        $url = $news_item_url.$item_data['seo_url'];
                    } else {
                        $url = $news_item_url.'?id='.$_item['id'];
                    }
                    $items_tmp[]['data'][$item_data['id_lang']]['url'] = $url;


                    //}
		    	}
		    	
			}
			
		return array('all_posts' => $items_tmp );
	} 
    
	
  public function generateSitemap(){
               
        $filename = dirname(__FILE__).$this->path_img_cloud."news.xml";
                
                //if(@filesize($filename)==0){
                	$new_sitemap = '<?xml version="1.0" encoding="UTF-8"?>
									<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

									</urlset>';
                	file_put_contents($filename,$new_sitemap);
                //}
                $xml = simplexml_load_file($filename);
                unset($xml->url);
                $sxe = new SimpleXMLElement($xml->asXML());
                
                   
                    
                    $all_categories_data = $this->_getCategoriesForSitemap();
                    $all_categories = $all_categories_data['all_categories'];
                    	
                    	foreach($all_categories as $item_cat){
                        $catlink_array = $item_cat['url'];
                        	foreach($catlink_array as $catlink){
                        
	                        $catlink = str_replace('&','&amp;', $catlink);
	                        $url = $sxe->addChild('url');
	                        $url->addChild('loc', $catlink);
	                        $url->addChild('priority','0.6');
	                        $url->addChild('changefreq','monthly');
	                        
                        	}
                    	}
                    
                    
                    
   					$all_posts_data = $this->_getPostsForSitemap();
                    $all_posts = $all_posts_data['all_posts'];
                    
                    foreach($all_posts as $post){
                    	
                    	foreach($post['data'] as $item_post){
                    	$postlink = $item_post['url'];
                        $postlink = str_replace('&','&amp;', $postlink);
                        
                        $url = $sxe->addChild('url');
                        $url->addChild('loc', $postlink);
                        $url->addChild('priority','0.6');
                        $url->addChild('changefreq','monthly');
                    	}
                    }
                    
                    $sxe->asXML($filename); 
             
    }
	
public function getIdItemifFriendlyURLEnable($data){
			$seo_url = $data['seo_url'];
		    $id_lang = $data['id_lang'];
			$sql = '
					SELECT pc.id_item as id_item
					FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
					WHERE pc.seo_url = "'.pSQL($seo_url).'" and pc.id_lang = '.(int)($id_lang);
			//echo $sql;
			$data_id = Db::getInstance()->GetRow($sql);
			$id_item = $data_id['id_item'];
			
			return $id_item;
	}
	
	public function getSEOFriendlyURLifFriendlyURLEnable($data){
			$id_post = $data['id_post'];
		    $id_lang = $data['id_lang'];
			$sql = '
					SELECT pc.seo_url
					FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
					WHERE pc.id_item = "'.(int)($id_post).'" and pc.id_lang = '.(int)($id_lang);
			//echo $sql;exit;
			$data_id = Db::getInstance()->GetRow($sql);
			$seo_url = $data_id['seo_url'];
			
			return $seo_url;
	}
	
	
public function getProducts($related_products) {

        $cookie = $this->context->cookie;
        $id_lang = $cookie->id_lang;

        $query = 'SELECT distinct p.`id_product`, p.`reference`, pl.`name`, 
                    pl.`description_short`, pl.`link_rewrite`
                    FROM  `' . _DB_PREFIX_ . 'product` p 
                    LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
                            p.`id_product` = pl.`id_product`
                            AND pl.`id_lang` = ' . (int)($id_lang) . '
                    )
                    
                    WHERE p.`id_product` IN('.pSQL($related_products).')';

            $query .= ' AND p.`active` = 1 ';

        $query .= ' ORDER BY pl.`name` DESC';
        return Db::getInstance()->executeS($query);
    }
	
	
public function getRelatedPosts($_data){
		$admin = isset($_data['admin'])?$_data['admin']:null;
		$items = array();
		if($admin){
			$id = isset($_data['id'])?$_data['id']:0;
			$cookie = $this->context->cookie;
			$current_language = (int)$cookie->id_lang;
			
			
			$sql = '
			SELECT pc.* 
			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc
			WHERE pc.status = 1 AND id != '.(int)($id).'
			AND FIND_IN_SET('.(int)($this->_id_shop).',pc.id_shop) 
			ORDER BY pc.`id` DESC';
			
			
			$posts = Db::getInstance()->ExecuteS($sql);
			
			
			foreach($posts as $k => $_item){
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
				WHERE pc.id_item = '.(int)($_item['id']).'
				');
				
				
				$tmp_title = '';
				$tmp_id = '';
				$tmp_time_add = '';

				// languages
				$languages_tmp_array = array();
				
				foreach ($items_data as $item_data){
					$languages_tmp_array[] = $item_data['id_lang'];
		    		
		    		$title = isset($item_data['title'])?$item_data['title']:'';
		    		$id = isset($item_data['id_item'])?$item_data['id_item']:'';
		    		$time_add = isset($posts[$k]['time_add'])?$posts[$k]['time_add']:'';
		    		
		    		if(Tools::strlen($tmp_title)==0){
		    			if(Tools::strlen($title)>0)
		    					$tmp_title = $title; 
		    		}
		    		
					if(Tools::strlen($tmp_id)==0){
		    			if(Tools::strlen($id)>0)
		    					$tmp_id = $id; 
		    		}
		    		
					if(Tools::strlen($tmp_time_add)==0){
		    			if(Tools::strlen($time_add)>0)
		    					$tmp_time_add = $time_add; 
		    		}
		    		
		    		if($current_language == $item_data['id_lang']){
		    			$items[$k]['title'] = $item_data['title'];
		    			$items[$k]['seo_url'] = $item_data['seo_url'];
		    			$items[$k]['id'] = $id;
		    			$items[$k]['time_add'] = $time_add;
		    		}
		    		
		    	}
		    	
		    	if(@Tools::strlen($items[$k]['title'])==0)
		    		$items[$k]['title'] = $tmp_title;
		    		
		    	if(@Tools::strlen($items[$k]['id'])==0)
		    		$items[$k]['id'] = $tmp_id;
		    		
		    	if(@Tools::strlen($items[$k]['time_add'])==0)
		    		$items[$k]['time_add'] = $tmp_time_add;
		    	
		    	
		    	// languages
		    	$items[$k]['ids_lng'] = $languages_tmp_array;



                $lang_for_category = array();
                foreach($languages_tmp_array as $lng_id){
                    $data_lng = Language::getLanguage($lng_id);
                    $lang_for_category[] = $data_lng['iso_code'];
                }
                $lang_for_category = implode(",",$lang_for_category);

                $items[$k]['iso_lang'] = $lang_for_category;
			}
			
			$data_count_related_posts = Db::getInstance()->getRow('
			SELECT COUNT(`id`) AS "count"
			FROM `'._DB_PREFIX_.'spmblocknewsadv` WHERE status = 1
			');
			
		} else {
			
			$id = isset($_data['id'])?$_data['id']:0;
			$cookie = $this->context->cookie;
			$current_language = (int)$cookie->id_lang;
			$related_data = $_data['related_data'];
			
			$sql = '
			SELECT pc.*,
			 (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 1) as count_like,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 1 and ip = \''.pSQL($_SERVER['REMOTE_ADDR']).'\') as is_liked_news,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 0) as count_unlike,
		    (select count(*) from `'._DB_PREFIX_.'spmblocknewsadv_news_like` al where al.news_id = pc.id and al.like = 0 and ip = \''.pSQL($_SERVER['REMOTE_ADDR']).'\') as is_unliked_news

			FROM `'._DB_PREFIX_.'spmblocknewsadv` pc
			WHERE pc.status = 1 AND pc.id IN('.pSQL($related_data).')
			AND FIND_IN_SET('.(int)($this->_id_shop).',pc.id_shop) 
			ORDER BY pc.`time_add` DESC';
			
			$posts = Db::getInstance()->ExecuteS($sql);
			
			
			foreach($posts as $k => $_item){
				$items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmblocknewsadv_data` pc
				WHERE pc.id_item = '.pSQL($_item['id']).' AND id_lang='.pSQL($current_language).'
				');

                if(Tools::strlen($posts[$k]['img'])>0){
                    $items[$k]['img_orig'] = $posts[$k]['img'];
                    $this->generateThumbImages(array('img'=>$posts[$k]['img'],
                            'width'=>Configuration::get($this->_name.'rp_img_width'),
                            'height'=>Configuration::get($this->_name.'rp_img_width')
                        )
                    );
                    $img = Tools::substr($posts[$k]['img'],0,-4)."-".Configuration::get($this->_name.'rp_img_width')."x".Configuration::get($this->_name.'rp_img_width').".jpg";
                } else {
                    $img = $posts[$k]['img'];
                }

                $items[$k]['img'] = $img;
				
				$tmp_title = '';
				$tmp_id = '';
				$tmp_time_add = '';

               foreach ($items_data as $item_data){

		    		$title = isset($item_data['title'])?$item_data['title']:'';
		    		$id = isset($item_data['id_item'])?$item_data['id_item']:'';
		    		$time_add = isset($posts[$k]['time_add'])?$posts[$k]['time_add']:'';
		    		
		    		if(Tools::strlen($tmp_title)==0){
		    			if(Tools::strlen($title)>0)
		    					$tmp_title = $title; 
		    		}
		    		
					if(Tools::strlen($tmp_id)==0){
		    			if(Tools::strlen($id)>0)
		    					$tmp_id = $id; 
		    		}
		    		
					if(Tools::strlen($tmp_time_add)==0){
		    			if(Tools::strlen($time_add)>0)
		    					$tmp_time_add = $time_add; 
		    		}
		    		
		    		if($current_language == $item_data['id_lang']){
		    			$items[$k]['title'] = $item_data['title'];
		    			$items[$k]['seo_url'] = $item_data['seo_url'];
		    			$items[$k]['id'] = $id;
		    			$items[$k]['time_add'] = $time_add;
		    		}
		    		
		    	}
		    	
		    	if(@Tools::strlen($items[$k]['title'])==0)
		    		$items[$k]['title'] = $tmp_title;
		    		
		    	if(@Tools::strlen($items[$k]['id'])==0)
		    		$items[$k]['id'] = $tmp_id;
		    		
		    	if(@Tools::strlen($items[$k]['time_add'])==0)
		    		$items[$k]['time_add'] = $tmp_time_add;


                $items[$k]['count_like'] = $posts[$k]['count_like'];
                $items[$k]['is_liked_news'] = $posts[$k]['is_liked_news'];

                $items[$k]['count_unlike'] = $posts[$k]['count_unlike'];
                $items[$k]['is_unliked_news'] = $posts[$k]['is_unliked_news'];

		    	
		    	
		  	}
			
			$data_count_related_posts = Db::getInstance()->getRow('
			SELECT COUNT(`id`) AS "count"
			FROM `'._DB_PREFIX_.'spmblocknewsadv` WHERE status = 1  AND FIND_IN_SET('.(int)($this->_id_shop).',id_shop)
			');	
		} 
		
		
		return array('related_posts' => $items, 'count_all' => $data_count_related_posts['count'] );
	}
	
	

    
    public function getSiteURL($id_shop = null){
    	$http_host = $this->_http_host;
    	$uri = '';
    	if(Tools::strlen($id_shop)!=0){
    		$shops = Shop::getShops(false);
			foreach($shops as $shop){
				if($id_shop == $shop['id_shop']){
					$uri = $shop['uri'];
					$uri = Tools::substr($uri, 1);
					break;
				}
			}
    	}
				
    	return $http_host.$uri;
    }
	
	public function productData($data){
		$product = $data['product'];
        $is_related_products = isset($data['is_related_products'])?$data['is_related_products']:0;
		
		$cookie = $this->context->cookie;
		$id_lang = (int)($cookie->id_lang);	
		
			/* Product URL */
			if (version_compare(_PS_VERSION_, '1.5', '>'))
				$link = Context::getContext()->link;
			else
				$link = new Link();
				
			$category = new Category((int)($product->id_category_default), $id_lang);

            if (version_compare(_PS_VERSION_, '1.5.5', '>=')) {
                   $product_url = $link->getProductLink((int)$product->id, null, null, null, 
                    									 $id_lang, null, 0, false);
             }
             elseif (version_compare(_PS_VERSION_, '1.5', '>')) {
               if (Configuration::get('PS_REWRITING_SETTINGS')) {
                     $product_url = $link->getProductLink((int)$product->id, null, null, null, 
                     									 $id_lang, null, 0, true);
               }
                else {
                    $product_url = $link->getProductLink((int)$product->id, null, null, null, 
                     									 $id_lang, null, 0, false);
                 }
            }
            else {
                  $product_url = $link->getProductLink((int)$product->id, @$product->link_rewrite[$id_lang], 
                 									 $category->link_rewrite, $product->ean13, $id_lang);
            }
            
            
			if (version_compare(_PS_VERSION_, '1.5', '>'))
				$link = Context::getContext()->link;
			else
				$link = new Link();

			/* Image */
			$image = Image::getCover((int)($product->id));

			if ($image)
			{
				



                if($is_related_products){
                    $type_img = Configuration::get($this->_name.'img_size_rp');
                } else {
                    $available_types = ImageType::getImagesTypes('products');

                    foreach ($available_types as $type) {
                        $width = $type['width'];

                        if (version_compare(_PS_VERSION_, '1.5', '>')) {
                            if ($width < 400) {
                                $type_img = $type['name'];
                                break;
                            }
                        } else {
                            if ($width < 100) {
                                $type_img = $type['name'];
                                break;
                            }
                        }
                    }
                }
				
					$image_link = $link->getImageLink(@$product->link_rewrite[$id_lang], (int)($product->id).'-'.(int)($image['id_image']),$type_img);
			
				/* version 1.4 */
				if (strpos($image_link, 'http://') === FALSE && strpos($image_link, 'https://') === FALSE && version_compare(_PS_VERSION_, '1.4', '<'))
				{
					$image_link = 'http://'.$_SERVER['HTTP_HOST'].$image_link;
				}
			}
			else
			{
				$image_link = false;
				
			}
            
            return array('product_url'=>$product_url,'image_link'=>$image_link);
	}
	
	public function getRelatedProducts($data){
		$cookie = $this->context->cookie;
		$id_lang = (int)($cookie->id_lang);
		
		$related_data = explode(",",$data['related_data']);
		
		$data_products = array();
		
		foreach($related_data as $_product_id){
			
		if($_product_id != 0){	
			$_obj_product = new Product($_product_id,false,$id_lang);
	    	
	    	$data_product = $this->productData(array('product'=>$_obj_product,'is_related_products'=>1));
			
	    	$picture = $data_product['image_link'];
			$product_url = $data_product['product_url'];
	    		
	    	$productname = addslashes($_obj_product->name);
	    	
	    	$desc = ($_obj_product->description_short != "") ? $_obj_product->description_short : $_obj_product->description;
	    	$desc = Tools::htmlentitiesUTF8(strip_tags($desc));
			
	    	$data_products[] = array('title'=>$productname,'description'=>$desc,'picture'=>$picture,'product_url'=>$product_url);
		}
		
		}
		return $data_products;
	}
	
	public function getRelatedPostsForPost($data){
		
			$related_data = $data['related_data'];
			$post_id = $data['post_id'];
		
			$data_rel_posts  = $this->getRelatedPosts(array('id'=>$post_id,'related_data'=>$related_data)); 
				
			$data_posts = array();
			
			foreach($data_rel_posts['related_posts'] as $_item){
				$name = isset($_item['title'])?$_item['title']:'';
				$id_post = isset($_item['id'])?$_item['id']:'';
				$seo_url = $_item['seo_url'];
                $count_like = $_item['count_like'];
                $is_liked_news = $_item['is_liked_news'];
                $is_unliked_news = $_item['is_unliked_news'];
                $count_unlike = $_item['count_unlike'];
                $time_add = $_item['time_add'];
                $img = $_item['img'];


                $data_posts[] = array('title'=>$name,'url'=>$seo_url,'id'=>$id_post,
                    'count_like'=>$count_like,
                    'count_unlike'=>$count_unlike,
                    'is_liked_news'=>$is_liked_news,
                    'is_unliked_news'=>$is_unliked_news,
                    'time_add'=>$time_add, 'img'=>$img,);
		       
			}
		
		return $data_posts;
	}


    public function getSEOURLs(){


        $cookie = $this->context->cookie;
        $id_lang = (int)($cookie->id_lang);
        $id_shop = $this->_id_shop;
        $link = new Link();

        $is_ssl = false;
        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (bool)Configuration::get('PS_SSL_ENABLED'))
            $is_ssl = true;

        if(Configuration::get($this->_name.'rew_on')==1){

            $iso_code = $this->getLangISO();
            $news_url = $this->getHttpost() . $iso_code. 'news';
            $news_item_url = $this->getHttpost() .  $iso_code.'news/';

        } else {



                $delimeter_rewrite = "&";
                if(Configuration::get('PS_REWRITING_SETTINGS')){
                    $delimeter_rewrite = "?";
                }


                $news_url = $link->getModuleLink($this->_name, 'news',  array(), $is_ssl, $id_lang, $id_shop);

                $news_item_url = $link->getModuleLink($this->_name, 'news',  array(), $is_ssl, $id_lang, $id_shop);
                $news_item_url .= $delimeter_rewrite.'id=';


        }

        $process_url = $link->getModuleLink($this->_name, 'process', array(), $is_ssl, $id_lang, $id_shop);
        $rss_url = $link->getModuleLink($this->_name, 'rss', array(), $is_ssl, $id_lang, $id_shop);
        $newssitemap_url = $link->getModuleLink($this->_name, 'newssitemap', array(), $is_ssl, $id_lang, $id_shop);


        return array(
            'news_url' => $news_url, 'news_item_url'=>$news_item_url,
            'process_url'=>$process_url,'rss_url'=>$rss_url,
            'newssitemap_url'=>$newssitemap_url,

        );
    }

    public function getHttpost(){

            $custom_ssl_var = 0;
            if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (bool)Configuration::get('PS_SSL_ENABLED'))
                $custom_ssl_var = 1;


            if ($custom_ssl_var == 1)
                $_http_host = _PS_BASE_URL_SSL_.__PS_BASE_URI__;
            else
                $_http_host = _PS_BASE_URL_.__PS_BASE_URI__;


        return $_http_host;
    }

    public function like($data){
        $like = $data['like'];
        $id = $data['id'];
        $ip = $data['ip'];
        $sql_exists = "select count(*) as count from `"._DB_PREFIX_."spmblocknewsadv_news_like` where ip = '".pSQL($ip)."' and news_id = ".(int)$id." and `like` = ".pSQL($like);
        $is_exists = Db::getInstance()->getRow($sql_exists);


        $error = 'error';

        include_once(_PS_MODULE_DIR_.$this->_name.'/spmblocknewsadv.php');
        $obj = new spmblocknewsadv();
        $_data_translate = $obj->translateItems();
        $message = htmlspecialchars($_data_translate['message_like']);

        $count = 0;

        if($is_exists['count'] == 0){
            $error = 'success';
            $message = "";
            $sql = "insert into `"._DB_PREFIX_."spmblocknewsadv_news_like` set news_id = ".(int)$id.", `like` = ".(int)$like.", ip = '".pSQL($ip)."'";
            Db::getInstance()->Execute($sql);

            $sql_count_likes = "select count(*) as count from `"._DB_PREFIX_."spmblocknewsadv_news_like` where news_id = ".(int)$id." and `like` = ".pSQL($like);
            $count_likes = Db::getInstance()->getRow($sql_count_likes);
            $count = $count_likes['count'];
        }

        $this->_clearSmartyCache();

        return array('error'=>$error, 'message'=>$message, 'count'=>$count);
    }

    public function getArchives(){

        $cookie = $this->context->cookie;
        $current_language = (int)$cookie->id_lang;

        $sql = 'SELECT
						    YEAR(`time_add`) AS YEAR,
						    MONTH(`time_add`) AS MONTH,
						    COUNT(*) AS TOTAL ,
						    time_add
						FROM  `'._DB_PREFIX_.'spmblocknewsadv` pc
						LEFT JOIN `'._DB_PREFIX_.'spmblocknewsadv_data` pc_d
						ON(pc.id = pc_d.id_item)
						WHERE pc.status = 1 AND FIND_IN_SET('.(int)$this->_id_shop.',pc.id_shop)
						AND pc_d.id_lang = '.(int)$current_language.'
						GROUP BY YEAR desc, MONTH';

        $items = Db::getInstance()->ExecuteS($sql);
        $items_tmp = array();


        foreach($items as $_item){
            $year = $_item['YEAR'];
            $month = $_item['MONTH'];
            $total = $_item['TOTAL'];
            $time_add = $_item['time_add'];


            $items_tmp[$year][] = array('year'=>$year,
                'month'=>$month,
                'total' =>$total,
                'time_add' => $time_add
            );

        }

        return array('items' => $items_tmp );
    }



    private function _clearSmartyCache($data = null){

        include_once(_PS_MODULE_DIR_.$this->_name.'/classes/cachespmblocknewsadv.class.php');
        $obj = new cachespmblocknewsadv();
        $obj->clearSmartyCacheModule($data);
    }

}