<?php
/**
 * 2011 - 2018 StorePrestaModules SPM LLC.
 *
 * MODULE spmblocknewsadv
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.2.2
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmblocknewsadvNewsModuleFrontController extends ModuleFrontController
{
	
	public function init()
	{

		parent::init();
	}
	
	public function setMedia()
	{
		parent::setMedia();
    }

	
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();

        $id = Tools::getValue('id');
        $p = Tools::getValue('p');


        if($id && !$p
            //&& !ctype_digit($id)
        ){
            $this->_item(array('id'=>$id));
        } else {
            $this->_list();
        }


	}

    private function _item($data){
            $post_id = $data['id'];


            $name_module = 'spmblocknewsadv';

            include_once(_PS_MODULE_DIR_.$name_module.'/classes/spmblocknewsadvfunctions.class.php');
            $obj_blocknewshelp = new spmblocknewsadvfunctions();

            include_once(_PS_MODULE_DIR_.$name_module.'/spmblocknewsadv.php');
            $obj_blocknews = new spmblocknewsadv();

             $obj_blocknews->setSEOUrls();



            $post_id = (int) $obj_blocknewshelp->getTransformSEOURLtoID(array('id'=>$post_id));

            $_info_cat = $obj_blocknewshelp->getItem(array('id' => $post_id,'site'=>1));



            $title = isset($_info_cat['item'][0]['title'])?$_info_cat['item'][0]['title']:'';
            $seo_description = isset($_info_cat['item'][0]['seo_description'])?$_info_cat['item'][0]['seo_description']:'';
            $seo_keywords = isset($_info_cat['item'][0]['seo_keywords'])?$_info_cat['item'][0]['seo_keywords']:'';


            if(sizeof($_info_cat['item'])==0){

                header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
                header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");

                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                header("HTTP/1.1 301 Moved Permanently");


                $data_url = $obj_blocknewshelp->getSEOURLs();
                $news_url = $data_url['news_url'];
                $obj_blocknews->tools_redirect($news_url);

                exit;
            }


        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->context->smarty->tpl_vars['page']->value['meta']['title'] = $title;
            $this->context->smarty->tpl_vars['page']->value['meta']['description'] = $seo_description;
            $this->context->smarty->tpl_vars['page']->value['meta']['keywords'] = $seo_keywords;
        }


            $this->context->smarty->assign('meta_title' , $title);
            $this->context->smarty->assign('meta_description' , $seo_description);
            $this->context->smarty->assign('meta_keywords' , $seo_keywords);

            $this->context->smarty->assign($name_module.'rew_on', Configuration::get($name_module.'rew_on'));

            $this->context->smarty->assign($name_module.'i_display_date', Configuration::get($name_module.'i_display_date'));

            $this->context->smarty->assign($name_module.'is16' , 1);


            ### related products ####
            $related_products = $_info_cat['item'][0]['related_products'];
            $data_related_products = $obj_blocknewshelp->getRelatedProducts(array('related_data'=>$related_products));
            ### related products ####


            ### related posts ###
            $related_posts = $_info_cat['item'][0]['related_posts'];
            $data_related_posts = $obj_blocknewshelp->getRelatedPostsForPost(array('related_data'=>$related_posts,'post_id'=>$post_id));
            ### related posts ###

            $id_lang = $obj_blocknews->getIdLang();

            $data_locale = $obj_blocknewshelp->getfacebooklib($id_lang);
            $lng_iso =$data_locale['lng_iso'];


        $is_ssl = false;
        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (bool)Configuration::get('PS_SSL_ENABLED'))
            $is_ssl = true;


            $this->context->smarty->assign(array('posts' => $_info_cat['item'],
                'related_products'=>$data_related_products,
                'related_posts'=>$data_related_posts,

                $name_module.'snip_publisher' => Configuration::get('PS_SHOP_NAME'),
                $name_module.'snip_width'=>Configuration::get($name_module.'item_img_w'),
                $name_module.'snip_height'=>Configuration::get($name_module.'item_img_w'),
                $name_module.'is_soc_but'=>Configuration::get($name_module.'is_soc_but'),

                $name_module.'item_rp_tr'=>Configuration::get($name_module.'item_rp_tr'),

                $name_module.'is_comments'=>Configuration::get($name_module.'is_comments'),
                $name_module.'number_fc'=>Configuration::get($name_module.'number_fc'),
                $name_module.'lng_iso'=>$lng_iso,

                $name_module.'appid'=>Configuration::get($name_module.'appid'),

                $name_module.'is_ssl' => $is_ssl,
            ));


        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->setTemplate('module:'.$name_module.'/views/templates/front/news-item17.tpl');
        }else {
            $this->setTemplate('news-item.tpl');
        }



    }

    private function _list(){


        $name_module = 'spmblocknewsadv';

        include_once(_PS_MODULE_DIR_.$name_module.'/classes/spmblocknewsadvfunctions.class.php');
        $obj_blocknewshelp = new spmblocknewsadvfunctions();

        include_once(_PS_MODULE_DIR_.$name_module.'/spmblocknewsadv.php');
        $obj_spmblocknewsadv = new spmblocknewsadv();

        $obj_spmblocknewsadv->setSEOUrls();



        $step = Configuration::get($name_module.'news_page');
        $p = (int)Tools::getValue('p');

        if($p == 0)
            $p = (int)Tools::getValue('id');



        $search = Tools::getValue("search");
        $is_search = 0;

        ### search ###
        if(Tools::strlen($search)>0){
            $is_search = 1;
        }

        ### archives ####
        $year = (int)Tools::getValue("y");
        $month = (int)Tools::getValue("m");
        $is_arch = 0;
        if($year!=0 && $month!=0){
            $is_arch = 1;
        }

        $start = (int)(($p - 1)*$step);
        if($start<0)
            $start = 0;




        $_data = $obj_blocknewshelp->getItems(array('start'=>$start,'step'=>$step,
                                                    'is_search'=>$is_search,'search'=>$search,
                                                    'is_arch'=>$is_arch,'month'=>$month,'year'=>$year
                                                )
        );



        $paging = $obj_blocknewshelp->PageNav($start,$_data['count_all'],$step,
                                                array('all_items'=>1,
                                                    'is_search'=>$is_search,'search'=>$search,
                                                    'is_arch'=>$is_arch,'month'=>$month,'year'=>$year
                                                )
        );

        // strip tags for content
        foreach($_data['items'] as $_k => $_item){
            $_data['items'][$_k]['content'] = strip_tags($_item['content']);
        }

        $this->context->smarty->assign(array('posts' => $_data['items'],
                'count_all' => $_data['count_all'],
                'paging' => $paging,
                $name_module.'is_search' => $is_search,
                $name_module.'search' => $search
            )
        );

        $this->context->smarty->assign($name_module.'rew_on', Configuration::get($name_module.'rew_on'));

        $this->context->smarty->assign($name_module.'l_display_date', Configuration::get($name_module.'l_display_date'));


        $this->context->smarty->assign($name_module.'is16' , 1);

        $seo_text_data = $obj_blocknewshelp->getTranslateText();
        $seo_text = $seo_text_data['seo_text'];


        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->context->smarty->tpl_vars['page']->value['meta']['title'] = $seo_text;
            $this->context->smarty->tpl_vars['page']->value['meta']['description'] = $seo_text;
            $this->context->smarty->tpl_vars['page']->value['meta']['keywords'] = $seo_text;
        }


        $this->context->smarty->assign('meta_title' , $seo_text);
        $this->context->smarty->assign('meta_description' , $seo_text);
        $this->context->smarty->assign('meta_keywords' , $seo_text);

        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->setTemplate('module:'.$name_module.'/views/templates/front/news17.tpl');
        }else {
            $this->setTemplate('news.tpl');
        }



    }
}