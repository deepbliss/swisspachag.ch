/**
 * SPM
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 *
 /*
 *
 * @author    SPM
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SPM
 * @license   SPM
 */

function init_tabs(id){
    $('document').ready( function() {

        if(id == 3){
            $('#navtabs16 a[href="#newssettings"]').tab('show');
        }

        if(id == 5){
            $('#navtabs16 a[href="#seourlrewrite"]').tab('show');
        }

        if(id == 8){
            $('#navtabs16 a[href="#autoposts"]').tab('show');
        }

    });
}



function tabs_custom(id){


    if(id == 6){
        $('#navtabs16 a[href="#info"]').tab('show');
    }





}


