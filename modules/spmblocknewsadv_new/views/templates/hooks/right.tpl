{*
/**
 * SPM
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    SPM
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SPM
 * @license   SPM
 */
 *}
 
 {if $spmblocknewsadvrightnews == 1}

     <div id="spmblocknewsadvnews_block_right"
          class="block
                {if $spmblocknewsadvbnews_slider == 1}owl_news_latest_news_type_carousel{/if}
                {if $spmblocknewsadvis17 == 1}block-categories hidden-sm-down{/if}
                {if $spmblocknewsadvis16 == 1 && $spmblocknewsadvis17 == 0}blockmanufacturer16{/if}
                spmblocknewsadv-block" >
         <h4 class="title_block {if $spmblocknewsadvis17 == 1}text-uppercase{/if}">

             <a href="{$spmblocknewsadvnews_url|escape:'htmlall':'UTF-8'}" title="{l s='Latest News' mod='spmblocknewsadv'}"
                     >{l s='Latest News' mod='spmblocknewsadv'}</a>

             {if $spmblocknewsadvrsson == 1}
                 <a  class="margin-left-left-10" href="{$spmblocknewsadvrss_url|escape:'htmlall':'UTF-8'}" title="{l s='RSS Feed' mod='spmblocknewsadv'}" target="_blank">
                     <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmblocknewsadv/views/img/feed.png" alt="{l s='RSS Feed' mod='spmblocknewsadv'}" />
                 </a>
             {/if}

         </h4>
         <div class="block_content">

             {if count($spmblocknewsadvitemsblock) > 0}
                 <div class="items-articles-block">

                     {if $spmblocknewsadvbnews_slider == 1 && (count($spmblocknewsadvitemsblock) > $spmblocknewsadvbnews_sl)}<ul class="owl-carousel owl-theme">{/if}

                     {foreach from=$spmblocknewsadvitemsblock item=items name=myLoop1}
                         {foreach from=$items.data item=item name=myLoop}

                             {if $spmblocknewsadvbnews_slider == 1}
                                {if ($smarty.foreach.myLoop1.index % $spmblocknewsadvbnews_sl == 0) || $smarty.foreach.myLoop1.first}
                                    <div>
                                {/if}

                            {/if}

                             <div class="current-item-block">
                                 {if $spmblocknewsadvblock_display_img == 1}
                                     {if strlen($item.img)>0}
                                         <div class="block-side">
                                             <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$spmblocknewsadvpic|escape:'htmlall':'UTF-8'}{$item.img|escape:'htmlall':'UTF-8'}"
                                                  title="{$item.title|escape:'htmlall':'UTF-8'}" alt="{$item.title|escape:'htmlall':'UTF-8'}"  />

                                         </div>
                                     {/if}
                                 {/if}

                                 <div class="block-content">
                                     <a class="item-article" title="{$item.title|escape:'htmlall':'UTF-8'}"
                                        href="{if $spmblocknewsadvrew_on == 1}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.seo_url|escape:'htmlall':'UTF-8'}{else}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.id|escape:'htmlall':'UTF-8'}{/if}"
                                             >{$item.title|escape:'htmlall':'UTF-8'}</a>

                                     <div class="clr"></div>
                                     {if $spmblocknewsadvb_display_date == 1}
                                         <span class="float-left block-item-date"><i class="fa fa-clock-o fa-lg"></i>&nbsp;{$item.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}</span>
                                     {/if}
                                     <span class="float-right comment block-item-like">

                             {if $spmblocknewsadvis_like == 1}
                            {if $item.is_liked_news}
                                <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)
                            {else}
                                <span class="block-item-like-{$item.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="spmblocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                            {/if}
                             {/if}
                                         &nbsp;
                             {if $spmblocknewsadvis_unlike == 1}
                             {if $item.is_unliked_news}
                                             <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)
                            {else}
                                <span class="block-item-unlike-{$item.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="spmblocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                             {/if}
                             {/if}

                        </span>

                                     <div class="clr"></div>
                                 </div>


                             {if $spmblocknewsadvbnews_slider == 1}

                                 {if ($smarty.foreach.myLoop1.index % $spmblocknewsadvbnews_sl == $spmblocknewsadvbnews_sl - 1) || $smarty.foreach.myLoop1.last}
                                     </div>
                                 {/if}

                             {/if}


                             </div>

                         {/foreach}
                     {/foreach}

                         {if $spmblocknewsadvbnews_slider == 1  && (count($spmblocknewsadvitemsblock) > $spmblocknewsadvbnews_sl)}</ul>{/if}

                     <p class="block-view-all">
                         <a href="{$spmblocknewsadvnews_url|escape:'htmlall':'UTF-8'}" title="{l s='View all news' mod='spmblocknewsadv'}"
                            class="button {if $spmblocknewsadvis17 == 1}button-small-spmblocknewsadv{/if}"
                                 ><b>{l s='View all news' mod='spmblocknewsadv'}</b></a>
                     </p>

                 </div>
             {else}
                 <div class="block-no-items">
                     {l s='News not found.' mod='spmblocknewsadv'}
                 </div>
             {/if}

         </div>
     </div>

 {/if}


{if $spmblocknewsadvarch_right_n == 1}
    <div id="spmblocknewsadvarch_block_right" class="block {if $spmblocknewsadvis17 == 1}block-categories hidden-sm-down{/if} {if $spmblocknewsadvis16 == 1 && $spmblocknewsadvis17 == 0}blockmanufacturer16{/if} search_items" >
        <h4 class="title_block {if $spmblocknewsadvis17 == 1}text-uppercase{/if}">{l s='News Archives' mod='spmblocknewsadv'}</h4>

        <div class="block_content{if $spmblocknewsadvis17 == 1}17{/if}">
            {if sizeof($spmblocknewsadvarch)>0}
                <ul class="bullet">
                    {foreach from=$spmblocknewsadvarch item=items key=year name=myarch}
                        <li><a class="arch-category" href="javascript:void(0)"
                               onclick="show_arch({$smarty.foreach.myarch.index|escape:'htmlall':'UTF-8'},'right')">{$year|escape:'htmlall':'UTF-8'}</a></li>
                        <div id="arch{$smarty.foreach.myarch.index|escape:'htmlall':'UTF-8'}right"
                             {if $smarty.foreach.myarch.first}{else}class="display-none"{/if}
                                >
                            {foreach from=$items item=item name=myLoop1}
                                <li class="arch-subcat">
                                    <a class="arch-subitem" href="{$spmblocknewsadvnews_url|escape:'htmlall':'UTF-8'}{if $spmblocknewsadvrew_on == 1}?{else}&{/if}y={$year|escape:'htmlall':'UTF-8'}&m={$item.month|escape:'htmlall':'UTF-8'}">
                                        {$item.time_add|date_format:"%B"|escape:'htmlall':'UTF-8'}&nbsp;({$item.total|escape:'htmlall':'UTF-8'})
                                    </a>
                                </li>
                            {/foreach}
                        </div>
                    {/foreach}
                </ul>
            {else}
                {l s='There are not Archives yet.' mod='spmblocknewsadv'}
            {/if}

        </div>

    </div>
{/if}

{if $spmblocknewsadvsearch_right_n == 1}
    <div id="spmblocknewsadvsearch_block_right" class="block {if $spmblocknewsadvis17 == 1}block-categories hidden-sm-down{/if} {if $spmblocknewsadvis16 == 1 && $spmblocknewsadvis17 == 0}blockmanufacturer16{/if} search_items" >
        <h4 class="title_block {if $spmblocknewsadvis17 == 1}text-uppercase{/if}">{l s='Search News' mod='spmblocknewsadv'}</h4>
        <form method="get" action="{$spmblocknewsadvnews_url|escape:'htmlall':'UTF-8'}">
            <div class="block_content{if $spmblocknewsadvis17 == 1}17{/if}">
                <input type="text" value="" class="search_items {if $spmblocknewsadvis17 == 1}search-spmblocknewsadv17{/if} {if $spmblocknewsadvis_ps15 == 0}search_text{/if}" name="search" >
                <input type="submit" value="go" class="button_mini {if $spmblocknewsadvis17 == 1}button-mini-spmblocknewsadv{/if} {if $spmblocknewsadvis_ps15 == 0}search_go{/if}"/>
                {if $spmblocknewsadvis_ps15 == 0}<div class="clear"></div>{/if}
            </div>
        </form>
    </div>
{/if}