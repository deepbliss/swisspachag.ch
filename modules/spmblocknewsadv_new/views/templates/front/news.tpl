{*
/**
 * SPM
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    SPM
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SPM
 * @license   SPM
 */
 *}

{capture name=path}
{$meta_title|escape:'htmlall':'UTF-8'}
{/capture}

{if $spmblocknewsadvis16 == 0}

    {capture name=path}
    {$meta_title|escape:'htmlall':'UTF-8'}
    {/capture}



{else}

    <h1 class="page-heading">{$meta_title|escape:'htmlall':'UTF-8'}</h1>

{/if}

{if $spmblocknewsadvis_search == 1}
    <div class="clear"></div>
    <h3 class="float-right">{l s='Results for' mod='spmblocknewsadv'} "{$spmblocknewsadvsearch|escape:'htmlall':'UTF-8'}"</h3>
    <div class="clear"></div>
{/if}


{if $count_all > 0}

<p class="blocnewsadv-header"><span class="bold">{l s='News' mod='spmblocknewsadv'}  ( {$count_all|escape:"htmlall":"UTF-8"} )</span></p>


<ul id="manufacturers_list" class="spmblocknewsadv-list">
{foreach from=$posts item=post name=myLoop}
	<li class="clearfix"> 
				<div class="left_side">
			
			<div class="logo">
				{if strlen($post.img)>0}
					
						<a class="lnk_img"
                           href="{if $spmblocknewsadvrew_on == 1}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.seo_url|escape:'htmlall':'UTF-8'}{else}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}{/if}"

                           title="{$post.title|escape:'htmlall':'UTF-8'}">

							<img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$spmblocknewsadvpic|escape:'htmlall':'UTF-8'}{$post.img|escape:'htmlall':'UTF-8'}"
							     title="{$post.title|escape:'htmlall':'UTF-8'}" 
							      />
						</a>
					
				{/if}
			</div>
				<h3>
					<a href="{if $spmblocknewsadvrew_on == 1}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.seo_url|escape:'htmlall':'UTF-8'}{else}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}{/if}"

                       title="{$post.title|escape:'htmlall':'UTF-8'}"
							  >
						{$post.title|escape:'htmlall':'UTF-8'}
					</a>
				</h3>
					
			
			<p class="description rte">
				{$post.content|substr:0:140|escape:"htmlall":"UTF-8"}
				{if strlen($post.content)>140}...{/if}
				<a href="{if $spmblocknewsadvrew_on == 1}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.seo_url|escape:'htmlall':'UTF-8'}{else}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}{/if}"
					   title="{$post.title|escape:'htmlall':'UTF-8'}" class="item-more">
						{l s='more' mod='spmblocknewsadv'}
				</a>
				<br/>
			</p>
				
			</div>
			<div class="spmblocknewsadv-clear"></div>


            <div class="clr"></div>
            {if $spmblocknewsadvl_display_date == 1}
                <span class="float-left block-item-date"><i class="fa fa-clock-o fa-lg"></i>&nbsp;{$post.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}</span>
            {/if}
            <span class="float-right comment block-item-like">

                             {if $spmblocknewsadvis_like == 1}
                                 {if $post.is_liked_news}
                                     <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_like|escape:'htmlall':'UTF-8'}</span>)
                            {else}
                                <span class="block-item-like-{$post.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="spmblocknewsadv_like_news({$post.id|escape:'htmlall':'UTF-8'},1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                                 {/if}
                             {/if}
            &nbsp;
            {if $spmblocknewsadvis_unlike == 1}
                {if $post.is_unliked_news}
                    <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_unlike|escape:'htmlall':'UTF-8'}</span>)
                            {else}
                                <span class="block-item-unlike-{$post.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="spmblocknewsadv_like_news({$post.id|escape:'htmlall':'UTF-8'},0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_unlike|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                {/if}
            {/if}

            </span>

		</li>
{/foreach}
</ul>

			
		
{$paging|escape:'quotes':'UTF-8' nofilter}
	

{else}
	<div class="blocnewsadv-noitems">
	{l s='There are not news yet' mod='spmblocknewsadv'}
	</div>
{/if}



