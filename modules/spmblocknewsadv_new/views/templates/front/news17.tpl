{*
/**
 * SPM
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    SPM
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SPM
 * @license   SPM
 */
 *}

{extends file='page.tpl'}

{block name="page_content"}

    {include file="modules/spmblocknewsadv/views/templates/front/news.tpl"}

{/block}
