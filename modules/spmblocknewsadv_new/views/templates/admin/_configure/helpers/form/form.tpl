{*
 *
 * SERG
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 *
 /*
 * 
 * @author    SERG
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SERG
 * @license   SERG
 *
*}

{extends file="helpers/form/form.tpl"}
{block name="field"}

    {if $input.type == 'checkbox_custom'}
    <div class="col-lg-9 {$input.name|escape:'htmlall':'UTF-8'}" style="padding: 7px">

        <input type="checkbox" name="{$input.name|escape:'htmlall':'UTF-8'}" id="{$input.name|escape:'htmlall':'UTF-8'}"
               value="1" {if $input.values.value == 1} checked="checked"{/if} />



        {if isset($input.desc) && !empty($input.desc)}
            <p class="help-block">
                {$input.desc|escape:'htmlall':'UTF-8'}
            </p>
        {/if}
    </div>
    {elseif $input.type == 'text_fcom'}
        <div class="input-group col-lg-9">
                <a target="_blank" href="http://www.facebook.com/apps/application.php?id={$input.f_appid|escape:'htmlall':'UTF-8'}" class="btn btn-primary pull">{l s='Check My App' mod='spmblocknewsadv'}</a>
                &nbsp;&nbsp;
                <a target="_blank" href="http://www.facebook.com/developers/editapp.php?app_id={$input.f_appid|escape:'htmlall':'UTF-8'}&amp;view=web" class="btn btn-primary pull">{l s='Configure App' mod='spmblocknewsadv'}</a>
                &nbsp;&nbsp;
                <a target="_blank" href="http://developers.facebook.com/setup" class="btn btn-primary pull">{l s='Create an App' mod='spmblocknewsadv'}</a>

        </div>
    {elseif $input.type == 'text_fcom_set'}
        <div class="input-group col-lg-9">


                <a target="_blank" href="http://developers.facebook.com/tools/comments?id={$input.f_appid|escape:'htmlall':'UTF-8'}&amp;view=edit_settings" class="btn btn-primary pull">{l s='Settings Comments' mod='spmblocknewsadv'}</a>
            &nbsp;&nbsp;
                <a target="_blank" href="http://developers.facebook.com/tools/comments?id={$input.f_appid|escape:'htmlall':'UTF-8'}&amp;view=queue" class="btn btn-primary pull">{l s='Moderate Comments' mod='spmblocknewsadv'}</a>
            &nbsp;&nbsp;
                <a target="_blank" href="http://developers.facebook.com/tools/comments?id={$input.f_appid|escape:'htmlall':'UTF-8'}" class="btn btn-primary pull">{l s='Check new comments' mod='spmblocknewsadv'}</a>

        </div>
    {elseif $input.type == 'text_truncate'}
        <div class="input-group col-lg-2">
            <input type="text" name="{$input.name|escape:'htmlall':'UTF-8'}" id="{$input.name|escape:'htmlall':'UTF-8'}"
                   value="{$input.value|escape:'htmlall':'UTF-8'}" />
            <span class="input-group-addon">&nbsp;{l s='chars' mod='spmblocknewsadv'}</span>


        </div>
    {elseif $input.type == 'checkbox_custom_blocks'}
        <div class="col-lg-9 {$input.name|escape:'htmlall':'UTF-8'}">

            {foreach $input.values.query as $value}
                {assign var=id_checkbox value=$value[$input.values.id]}
                <div class="checkbox{if isset($input.expand) && strtolower($input.expand.default) == 'show'} hidden{/if}">
                    {strip}
                        <label for="{$id_checkbox|escape:'htmlall':'UTF-8'}">
                            <input type="checkbox" name="{$id_checkbox|escape:'htmlall':'UTF-8'}" id="{$id_checkbox|escape:'htmlall':'UTF-8'}"
                                   class="{if isset($input.class)}{$input.class}{/if}"{if isset($value.val)}
                            value="{$value.val|escape:'htmlall':'UTF-8'}"{/if}{if isset($fields_value[$id_checkbox]) && $fields_value[$id_checkbox]} checked="checked"{/if} />
                            {$value[$input.values.name] nofilter}
                        </label>
                    {/strip}
                </div>
            {/foreach}

            {if isset($input.desc) && !empty($input.desc)}
                <p class="help-block">
                    {$input.desc|escape:'htmlall':'UTF-8'}
                </p>
            {/if}
        </div>
    {elseif $input.type == 'image_custom_px'}
        <div class="input-group col-lg-1">
            <input type="text" name="{$input.name|escape:'htmlall':'UTF-8'}" id="{$input.name|escape:'htmlall':'UTF-8'}"
                   value="{$input.value|escape:'htmlall':'UTF-8'}" />
            <span class="input-group-addon">&nbsp;px</span>


        </div>
    {elseif $input.type == 'text_autopost'}
        <div class="col-lg-{if isset($input.col)}{$input.col|intval}{else}9{/if}{if !isset($input.label)} col-lg-offset-3{/if}">


        {if isset($input.lang) AND $input.lang}

            {if $languages|count > 1}
                <div class="form-group">
            {/if}
                {foreach $languages as $language}
                    {assign var='value_text' value=$fields_value[$input.name][$language.id_lang]}
                    {if $languages|count > 1}
                        <div class="translatable-field lang-{$language.id_lang|escape:'htmlall':'UTF-8'}" {if $language.id_lang != $defaultFormLanguage}style="display:none"{/if}>
                        <div class="col-lg-9">
                    {/if}
                    {if strlen($input.text_before)>0}
                    <span style="float:left;margin:7px 5px 0 0;font-weight: bold">{$input.text_before|escape:'htmlall':'UTF-8'}</span>
                    {/if}
                    <input type="text" style="float:left;margin-right:5px;width:40%"
                           id="{if isset($input.id)}{$input.id|escape:'htmlall':'UTF-8'}_{$language.id_lang|escape:'htmlall':'UTF-8'}{else}{$input.name|escape:'htmlall':'UTF-8'}_{$language.id_lang|escape:'htmlall':'UTF-8'}{/if}"
                           name="{$input.name|escape:'htmlall':'UTF-8'}_{$language.id_lang|escape:'htmlall':'UTF-8'}"
                           class="{if isset($input.class)}{$input.class|escape:'htmlall':'UTF-8'}{/if}{if $input.type == 'tags'} tagify{/if}"
                           value="{if isset($input.string_format) && $input.string_format}{$value_text|string_format:$input.string_format|escape:'htmlall':'UTF-8'}{else}{$value_text|escape:'htmlall':'UTF-8'}{/if}"
                           onkeyup="if (isArrowKey(event)) return ;updateFriendlyURL();"
                            {if isset($input.size)} size="{$input.size|escape:'htmlall':'UTF-8'}"{/if}
                            {if isset($input.maxchar) && $input.maxchar} data-maxchar="{$input.maxchar|intval}"{/if}
                            {if isset($input.maxlength) && $input.maxlength} maxlength="{$input.maxlength|intval}"{/if}
                            {if isset($input.readonly) && $input.readonly} readonly="readonly"{/if}
                            {if isset($input.disabled) && $input.disabled} disabled="disabled"{/if}
                            {if isset($input.autocomplete) && !$input.autocomplete} autocomplete="off"{/if}
                            {if isset($input.required) && $input.required} required="required" {/if}
                            {if isset($input.placeholder) && $input.placeholder} placeholder="{$input.placeholder|escape:'htmlall':'UTF-8'}"{/if} />
                    <span style="float:left;margin:7px 5px 0 0;font-weight: bold">{$input.text_after|escape:'htmlall':'UTF-8'}</span>
                    {if $languages|count > 1}
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
                                {$language.iso_code|escape:'htmlall':'UTF-8'}
                                <i class="icon-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu">
                                {foreach from=$languages item=language}
                                    <li><a href="javascript:hideOtherLanguage({$language.id_lang|escape:'htmlall':'UTF-8'});" tabindex="-1">{$language.name|escape:'htmlall':'UTF-8'}</a></li>
                                {/foreach}
                            </ul>
                        </div>
                        </div>
                    {/if}
                {/foreach}




            {if $languages|count > 1}
                </div>
            {/if}

        {/if}

            {if isset($input.desc) && !empty($input.desc)}
                <p class="help-block">
                    {$input.desc|escape:'htmlall':'UTF-8'}
                </p>
            {/if}
         </div>


        {else}

		{$smarty.block.parent}
	{/if}
{/block}





