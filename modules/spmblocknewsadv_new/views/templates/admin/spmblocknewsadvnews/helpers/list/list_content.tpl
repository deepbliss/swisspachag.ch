{*
/**
 * SPM
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    SPM
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SPM
 * @license   SPM
 */
*}

{extends file="helpers/list/list_content.tpl"}
    {block name="td_content"}
        {if isset($params.type_custom) && $params.type_custom == 'title_item'}
            {if isset($tr[$key])}
                <span class="label-tooltip" data-original-title="{l s='Click here to see news on your site' mod='spmblocknewsadv'}" data-toggle="tooltip">

                    <a href="
                    {if $params.is_rewrite == 0}
                        {$link->getModuleLink('spmblocknewsadv', 'news', [], true, {$tr.id_lang|escape:'htmlall':'UTF-8'}, {$tr.id_shop|escape:'htmlall':'UTF-8'})|escape:'htmlall':'UTF-8'}{if $params.is16 == 1}&{else}?{/if}id={$tr['id']|escape:'htmlall':'UTF-8'}
                    {else}
                        {$params.base_dir_ssl|escape:'htmlall':'UTF-8'}{$params.iso_code|escape:'htmlall':'UTF-8'}/news/{$tr.seo_url|escape:'htmlall':'UTF-8'}
                    {/if}
                    "
                       style="text-decoration:underline" target="_blank">
                        {$tr[$key]|escape:'htmlall':'UTF-8'}
                    </a>
                </span>
            {/if}

        {elseif isset($params.type_custom) && $params.type_custom == 'is_active'}

        {literal}
            <script type="text/javascript">

                var ajax_link_spmblocknewsadv = '{/literal}{$params.ajax_link nofilter}{literal}';

            </script>
        {/literal}

            <span id="activeitem{$tr['id']|escape:'htmlall':'UTF-8'}">
                    <span class="label-tooltip" data-original-title="{l s='Click here to activate or deactivate news on your site' mod='spmblocknewsadv'}" data-toggle="tooltip">
                    <a href="javascript:void(0)" onclick="spmblocknewsadv_list({$tr['id']|escape:'htmlall':'UTF-8'},'active',{$tr[$key]|escape:'htmlall':'UTF-8'},'news','{$params.token_custom|escape:'htmlall':'UTF-8'}');" style="text-decoration:none">
                        <img src="../img/admin/../../modules/spmblocknewsadv/views/img/{if $tr[$key] == 1}ok.gif{else}no_ok.gif{/if}"  />
                    </a>
                </span>
            </span>


        {elseif isset($params.type_custom) && $params.type_custom == 'is_comments'}

        {literal}
            <script type="text/javascript">

                var ajax_link_spmblocknewsadv = '{/literal}{$params.ajax_link nofilter}{literal}';

            </script>
        {/literal}


            <span id="activeitemc{$tr['id']|escape:'htmlall':'UTF-8'}">
                    <span class="label-tooltip" data-original-title="{l s='Click here to activate or deactivate comments for news' mod='spmblocknewsadv'}" data-toggle="tooltip">
                    <a href="javascript:void(0)" onclick="spmblocknewsadv_list({$tr['id']|escape:'htmlall':'UTF-8'},'active',{$tr[$key]|escape:'htmlall':'UTF-8'},'comment','{$params.token_custom|escape:'htmlall':'UTF-8'}');" style="text-decoration:none">
                        <img src="../img/admin/../../modules/spmblocknewsadv/views/img/{if $tr[$key] == 1}ok.gif{else}no_ok.gif{/if}"  />
                    </a>
                </span>
            </span>

        {elseif isset($params.type_custom) && $params.type_custom == 'img'}
            {if strlen($tr[$key])>0}
            <img src="{$params.logo_img_path|escape:'htmlall':'UTF-8'}{$tr[$key]|escape:'htmlall':'UTF-8'}" class="img-thumbnail" style="width: 80px"/>
            {else}
                ---
            {/if}


        {else}
            {$smarty.block.parent}
        {/if}


    {/block}