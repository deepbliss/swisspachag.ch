<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/collection.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/objectmodel/SupplierReference.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/lib/Yaml.php';

class AIExport
{
    protected $xml;
    protected $page = 1;
    protected $pageSize;
    protected $tree = array();
    protected $languages;
    protected $shop = null;
    protected $type;
    protected $class;
    protected $schema;
    protected $meta = true;
    protected $ignore = array();
    protected $proceded = false;
    protected $collection = null;
    protected $limitToSupplierReference = false;
    protected $parent = null;
    protected $parentType = null;

    public function __construct($xml, $type, $class)
    {
        $this->xml = $xml;
        $this->type = $type;
        $this->class = $class;

        $this->schema = array();
        if (file_exists(_PS_MODULE_DIR_.'advancedimporter/classes/schema/'.$class.'.yml')) {
            $yaml = new Yaml();
            $this->schema = $yaml->load(_PS_MODULE_DIR_.'advancedimporter/classes/schema/'.$class.'.yml');
        }

        $this->languages = array();
        foreach (Language::getIsoIds(false) as $lang) {
            $this->languages[$lang['id_lang']] = $lang['iso_code'];
        }

        return $this;
    }

    public function noMeta()
    {
        $this->meta = false;

        return $this;
    }

    public function supplierReferenceOnly()
    {
        $this->limitToSupplierReference = true;

        return $this;
    }

    public function setParent($type, $parent)
    {
        $this->parentType = $type;
        $this->parent = $parent;

        return $this;
    }

    public function setPage($page)
    {
        $this->page = (int) $page;

        return $this;
    }

    public function setPageSize($size)
    {
        $this->pageSize = (int) $size;

        return $this;
    }

    public function setShop($shop)
    {
        $this->shop = (int) $shop;
        Context::getContext()->shop->id = $this->shop;
        Shop::setContext(Shop::CONTEXT_SHOP, $this->shop);

        return $this;
    }

    public function setEntityTree($tree)
    {
        $this->tree = $tree;

        return $this;
    }

    public function addIgnore($ignore)
    {
        $this->ignore[] = $ignore;

        return $this;
    }


    public function getXml()
    {
        if (!$this->proceded) {
            $this->process();
        }

        return $this->xml;
    }

    protected function getCollection()
    {
        if (is_null($this->collection)) {
            $this->collection = new AICollection($this->class);
            $this->setPagination($this->collection);
            $this->setQuerySelect($this->collection);
        }

        return $this->collection;
    }

    public function setCollection($collection)
    {
        $this->collection = $collection;

        return $this;
    }

    protected function process()
    {
        $this->proceded = true;

        if ($this->meta) {
            $total = $this->getTotal();
            $this->xml->addAttribute('total', $total);
            $this->xml->addAttribute('page', $this->page);
            if ($this->pageSize) {
                $this->xml->addAttribute('page-size', $this->pageSize);
            }
            $this->xml->addAttribute('last-page', ceil($total / $this->pageSize));
        }

        foreach ($this->getCollection() as $entity) {
            $this->mapEntity($entity);
        }
    }

    protected function setPagination($collection)
    {
        if (!$this->pageSize) {
            return $this;
        }

        if ($this->class::$definition['primary'] === 'id_concat') {
            $collection->setPageSize($this->pageSize * count($this->languages));
            $collection->setPageNumber($this->page);
        } else {
            $primary = $this->class::$definition['primary'];
            $start = ($this->page - 1) * $this->pageSize;
            if ($this->limitToSupplierReference) {
                $table = _DB_PREFIX_.'advancedimporter_supplierreference';
                $query = 'SELECT min(id_object) as `min`, max(id_object) as `max`
                    FROM (
                        SELECT `id_object`
                        FROM `'.bqSql($table).'`
                        WHERE `object_type` = "'.pSql(Tools::ucfirst(Tools::strtolower($this->class))).'"
                        ORDER BY `id_object`
                        LIMIT '.(int) $start.', '.(int) $this->pageSize.') as a';
            } else {
                $table = _DB_PREFIX_.$this->class::$definition['table'];
                $query = "SELECT min($primary) as `min`, max($primary) as `max`
                    FROM (
                        SELECT ".bqSql($primary)." FROM `".bqSql($table)."`
                        ORDER BY ".bqSql($primary)."
                        LIMIT ".(int) $start.", ".(int) $this->pageSize.") as a";
            }

            $res = Db::getInstance()->getRow($query);
            $collection->where($primary, '>=', $res['min']);
            $collection->where($primary, '<=', $res['max']);
        }
    }

    protected function getTotal()
    {
        $table = _DB_PREFIX_.$this->class::$definition['table'];
        if ($this->limitToSupplierReference) {
            $supplierReferenceTable = _DB_PREFIX_.'advancedimporter_supplierreference';
            $query = 'SELECT count(distinct `'.bqSql($this->class::$definition['primary']).'`)
                FROM  `'.bqSql($table).'` as t
                INNER JOIN `'.bqSql($supplierReferenceTable).'` as sr
                ON t.`'.bqSql($this->class::$definition['primary']).'` = sr.`id_object`
                AND sr.`object_type` = "'.pSql(Tools::ucfirst(Tools::strtolower($this->class))).'"';
        } else {
            $query = "SELECT count(*) FROM  `".bqSql($table)."`";
        }
        return Db::getInstance()->getValue($query);
    }

    protected function setQuerySelect($collection)
    {
        if ($this->class::$definition['primary'] !== 'id_concat') {
            return;
        }

        $select = array();

        $primary = $this->class::$definition['primary'];
        $concat = array();
        foreach ($this->class::$definition['fields'] as $key => $value) {
            if (strpos($key, 'id_') === 0) {
                $concat[] = $key;
            }
            $select[] = "`$key`";
        }

        $select[] = 'CONCAT('.implode(',', $concat).') as id_concat';

        $collection->getQuery()->select(implode(',', $select));
    }

    protected function mapEntity($entity)
    {
        $definition = $this->class::$definition['fields'];

        if (empty($entity->id)) {
            return;
        }

        if (!empty($entity->id)) {
            try {
                $supplierReference = SupplierReference::getByObjectId($entity->id, $this->type);
                $node = $this->xml->addChild($this->type);
                $node->addAttribute('supplier-reference', $supplierReference->supplier_reference);
            } catch (SupplierReferenceException $e) {
                if ($this->limitToSupplierReference) {
                    return;
                }
                $node = $this->xml->addChild($this->type);
                $node->addChild('id', $entity->id);
            }
        }

        if ($this->shop) {
            $node->addAttribute('shop', $this->shop);
        }

        foreach ($definition as $field => $type) {
            if (in_array($field, $this->ignore)) {
                continue;
            }

            if (isset($type['lang']) && $type['lang']) {
                foreach ($entity->$field as $key => $value) {
                    $subnode = $node->addChild($field, '<![CDATA['.htmlspecialchars($value).']]>');
                    $subnode->addAttribute('lang', $this->languages[$key]);
                }
            } else {
                if ($this->schema != false && isset($this->schema['fields'][$field]) && $entity->$field != 0) {
                    $type = $this->schema['fields'][$field]['entity'];
                    try {
                        $supplierReference = SupplierReference::getByObjectId($entity->$field, $type);
                        $subnode = $node->addChild($field, $supplierReference->supplier_reference);
                        $subnode->addAttribute('supplier-reference', $type);
                    } catch (SupplierReferenceException $e) {
                        if ($this->limitToSupplierReference) {
                            continue;
                        }
                        $node->addChild($field, $entity->$field);
                    }
                } else {
                    $value = $entity->$field;
                    if ($this->schema != false && isset($this->schema['operation'])
                        && isset($this->schema['operation'][$field])
                        && $this->parentType == $this->schema['operation'][$field]['parent_type']
                    ) {
                        switch ($this->schema['operation'][$field]['operator']) {
                            case 'sum':
                                $parentField = $this->schema['operation'][$field]['parent_attribute'];
                                $value += $this->parent->$parentField;
                                break;
                        }
                    }
                    $node->addChild($field, $value);
                }
            }
        }

        //foreach (array_keys($this->tree) as $link) {
        if ($this->schema != false && isset($this->schema['links']/*[$link]*/)) {
            foreach (array_keys($this->schema['links']) as $link) {
                switch ($this->schema['links'][$link]['type']) {
                    case 'n-1':
                        $class = $this->schema['links'][$link]['entity'];
                        $key = $this->schema['links'][$link]['key'];
                        $collection = array(
                            new $class($entity->$key),
                        );
                        break;
                    case '1-n':
                        $class = $this->schema['links'][$link]['entity'];
                        $collection = new AICollection($class);
                        $idAttribute = 'id';
                        if (!empty($this->schema['links'][$link]['initial_key'])) {
                            $idAttribute = $this->schema['links'][$link]['initial_key'];
                        }
                        $collection->where($this->schema['links'][$link]['key'], '=', $entity->$idAttribute);
                        break;
                    case 'n-n':
                        $table = _DB_PREFIX_.$this->schema['links'][$link]['link_table'];
                        $query = 'SELECT `'.bqSql($this->schema['links'][$link]['id_child']).'` as id
                            FROM  `'.bqSql($table).'`
                            WHERE `'.bqSql($this->schema['links'][$link]['id_parent']).'` = '.(int) $entity->id;
                        $ids = array();
                        foreach (Db::getInstance()->executeS($query) as $row) {
                            $ids[] = (int) $row['id'];
                        }


                        if (empty($ids)) {
                            continue 2;
                        }

                        $class = $this->schema['links'][$link]['entity'];
                        $collection = new AICollection($class);
                        $collection->where($this->schema['links'][$link]['id_main'], 'in', $ids);
                        break;
                    default:
                        throw new Exception('Unknow link type "'.$this->schema['links'][$link]['type'].'"');
                }

                $exportClass = new AIExport(
                    $node,
                    $link,
                    $class
                );
                $exportClass->noMeta();
                if ($this->limitToSupplierReference) {
                    $exportClass->supplierReferenceOnly();
                }
                if (!empty($this->tree[$link])) {
                    $exportClass->setEntityTree($this->tree[$link]);
                }

                $exportClass->setCollection($collection);

                $exportClass->setParent($this->type, $entity);

                if (isset($this->schema['links'][$link]['ignore_fields'])) {
                    foreach ($this->schema['links'][$link]['ignore_fields'] as $ignore) {
                        $exportClass->addIgnore($ignore);
                    }
                }

                $exportClass->getXml();
            }
        }

        $this->mapExtra($node, $entity);
    }

    protected function mapExtra($node, $entity)
    {
        switch ($this->class) {
            case 'Image':
                $subpath = implode('/', str_split($entity->id));
                $node->addChild(
                    'url',
                    Tools::getShopDomain(true).__PS_BASE_URI__.'img/p/'.$subpath.'/'.$entity->id.'.jpg'
                );
                break;
        }
    }
}
