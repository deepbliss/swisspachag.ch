<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class class CategoryShop */
class CategoryShop extends ObjectModel
{
    /** @var int category ID */
    public $id_category;

    /** @var int shop ID */
    public $id_shop;

    /** @var int position */
    public $position;

    public static $definition = array(
        'fields' => array(
            'id_category' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'position' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
        ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        if (empty($id)) {
            return $this;
        }

        list($this->id_shop, $this->id_category) = explode('-', $id);

        return $this;
    }

    public function save($null_values = false, $autodate = true)
    {
        if (empty($this->id_category)) {
            throw new Exception('Missing category id');
        }
        if (empty($this->id_shop)) {
            throw new Exception('Missing shop id');
        }

        $this->id = $this->id_shop.'-'.$this->id_category;

        if (!$this->position) {
            $res = Db::getInstance()->getValue(
                'SELECT position FROM `'._DB_PREFIX_."category_shop`
                WHERE
                    `id_category` = ".(int) $this->id_category."
                    AND `id_shop` = ".(int) $this->id_shop
            );
            if ($res) {
                $this->position = (int) $res;
            }
        }

        // Remove link
        $this->delete();

        // Create link
        $row = array(
            'id_category' => (int) $this->id_category,
            'id_shop' => (int) $this->id_shop,
            'position' => (int) $this->position,
        );
        Db::getInstance()->insert('category_shop', $row);
    }

    public function delete()
    {
        if (empty($this->id_category)) {
            throw new Exception('Missing category id');
        }
        if (empty($this->id_shop)) {
            throw new Exception('Missing shop id');
        }

        Db::getInstance()->execute(
            'DELETE FROM `'._DB_PREFIX_.'category_shop`
            WHERE `id_category` = '.(int) $this->id_category
            .' AND `id_shop` = '.(int) $this->id_shop
        );
    }
}
