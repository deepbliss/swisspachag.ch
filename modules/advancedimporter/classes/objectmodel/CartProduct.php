<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class class CartProduct */
class CartProduct extends ObjectModel
{
    /** @var int Id cart */
    public $id_cart;

    /** @var int Id product */
    public $id_product;

    /** @var int Id address delivery */
    public $id_address_delivery;

    /** @var int Id shop */
    public $id_shop;

    /** @var int Id product attribute */
    public $id_product_attribute;

    /** @var int Quantity */
    public $quantity;

    /** @var string date_add */
    public $date_add;

    public $table = 'cart_product';

    public $ids = array(
        'id_cart',
        'id_product',
        'id_address_delivery',
        'id_shop',
        'id_product_attribute',
    );

    public static $definition = array(
        'fields' => array(
            'id_cart' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => true,
            ),
            'id_product' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => true,
            ),
            'id_address_delivery' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => false,
            ),
            'id_shop' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => false,
            ),
            'id_product_attribute' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => false,
            ),
            'quantity' => array(
                'type' => self::TYPE_INT,
                'validate' => 'isUnsignedInt',
                'required' => true,
            ),
            'date_add' => array(
                'type' => self::TYPE_DATE,
                'validate' => 'isDateFormat',
                'required' => true,
            ),
        ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        $res = false;
        if (!empty($id)) {
            $id_values = explode('-', $id);
            foreach ($this->ids as $position => $key) {
                $this->$key = (int) $id_values[$position];
            }

            $res = Db::getInstance()->executeS(
                'SELECT * FROM `'._DB_PREFIX_.bqSql($this->table).'`
                WHERE
                    `id_cart` = '.(int) $this->id_cart.'
                    AND `id_product` = '.(int) $this->id_product.'
                    AND `id_address_delivery` = '.(int) $this->id_address_delivery.'
                    AND `id_shop` = '.(int) $this->id_shop.'
                    AND `id_product_attribute` = '.(int) $this->id_product_attribute,
                true,
                false
            );
        }

        if ($res) {
            $this->date_add = $res['date_add'];
            $this->quantity = (int) $res['quantity'];
        } else {
            $this->date_add = date('Y-m-d H:i:s');
            $this->id_shop = 1;
        }

        $this->def = ObjectModel::getDefinition($this);

        return $this;
    }

    public function save($null_values = false, $autodate = true)
    {
        $this->validateFields();

        $this->id = $this->id_cart
            .'-'.$this->id_product
            .'-'.$this->id_address_delivery
            .'-'.$this->id_shop
            .'-'.$this->id_product_attribute;

        // Remove row
        $this->delete();

        // Create row
        $row = array();
        foreach (self::$definition['fields'] as $field => $details) {
            $row[$field] = $this->$field;
        }
        Db::getInstance()->insert($this->table, $row);
    }

    public function delete()
    {
        foreach ($this->ids as $key) {
            if (empty($this->$key)) {
                throw new Exception('Missing '.$key);
            }
        }

        $where = array();
        foreach ($this->ids as $key) {
            $where[] = '`'.bqSql($key).'` = '.(int) $this->$key;
        }

        Db::getInstance()->execute(
            'DELETE FROM `'._DB_PREFIX_.bqSql($this->table).'`
            WHERE '.implode(' AND ', $where)
        );
    }
}
