<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/classfactory.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/supplierreferenceexception.php';

/** Class SupplierReference */
class SupplierReference extends ObjectModel
{
    public $id_object;
    public $object_type;
    public $supplier_reference;
    public $date_add;
    public $date_upd;
    public $instance = null;
    protected $origData = null;
    protected static $cache = array();
    protected static $needStoreCache = false;

    public static $definition = array(
        'table' => 'advancedimporter_supplierreference',
        'primary' => 'id_advancedimporter_supplierreference',
        'fields' => array(
            'id_object' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),
            'object_type' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),
            'supplier_reference' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),
            'date_add' => array('type' => self::TYPE_DATE, 'required' => true, 'validate' => 'isDateFormat'),
            'date_upd' => array('type' => self::TYPE_DATE, 'required' => true, 'validate' => 'isDateFormat'),
        ),
    );

    public static function preloadCache()
    {
        if (!Configuration::getGlobalValue('AI_PRELOAD_SR_CACHE')) {
            return;
        }

        if (!empty(self::$cache)) {
            return;
        }

        if (file_exists(_PS_MODULE_DIR_.'advancedimporter/classes/tmp/suppierreference.php')) {
            require(_PS_MODULE_DIR_.'advancedimporter/classes/tmp/suppierreference.php');
            self::$cache = $cache;
            return;
        }

        $query = new DbQuery();
        $query->select('id_advancedimporter_supplierreference as id, id_object, object_type, supplier_reference');
        $query->from('advancedimporter_supplierreference');
        $rows = Db::getInstance()->executeS($query, true, false);
        foreach ($rows as $row) {
            $sr = new SupplierReference();
            $sr->id = $row['id'];
            $sr->id_object = $row['id_object'];
            $sr->object_type = $row['object_type'];
            $sr->supplier_reference = $row['supplier_reference'];

            self::addToCache($sr);
        }

        self::storeCache();
    }

    public static function storeCache()
    {
        if (!self::$needStoreCache) {
            return;
        }

        $cacheFileContent = '<?php $cache = '.var_export(self::$cache, true).';';
        $fp = fopen(_PS_MODULE_DIR_.'advancedimporter/classes/tmp/suppierreference.php', "w");
        fwrite($fp, $cacheFileContent);
        fclose($fp);

        self::$needStoreCache = false;
    }

    public static function addToCache(SupplierReference $sr)
    {
        if (!Configuration::getGlobalValue('AI_USE_SR_CACHE')) {
            return;
        }

        if (!isset(self::$cache[$sr->object_type])) {
            self::$cache[$sr->object_type] = array();
        }
        if (!isset(self::$cache[$sr->object_type][$sr->supplier_reference])) {
            self::$cache[$sr->object_type][$sr->supplier_reference] = array(
                'id' => $sr->id,
                'id_object' => $sr->id_object,
                'object_type' => $sr->object_type,
                'supplier_reference' => $sr->supplier_reference,
            );
            self::$needStoreCache = true;
        }
    }

    public static function loadFromCache($supplier_reference, $object_type)
    {
        if (!Configuration::getGlobalValue('AI_USE_SR_CACHE')) {
            return false;
        }

        if (isset(self::$cache[$object_type])
            && isset(self::$cache[$object_type][$supplier_reference])
        ) {
            $sr = new SupplierReference();
            $sr->id = self::$cache[$object_type][$supplier_reference]['id'];
            $sr->id_object = self::$cache[$object_type][$supplier_reference]['id_object'];
            $sr->object_type = self::$cache[$object_type][$supplier_reference]['object_type'];
            $sr->supplier_reference = self::$cache[$object_type][$supplier_reference]['supplier_reference'];
            $sr->setOrigData();
            if (!$sr->getInstance()->id) {
                $sr->delete();
                return false;
            }

            return $sr;
        }

        return false;
    }

    public function getInstance()
    {
        if (is_null($this->instance)) {
            $class = $this->object_type;
            $this->instance = AIClassFactory::loadObjectModel($class, $this->id_object);
        }

        return $this->instance;
    }

    public static function getBySupplierReference($supplier_reference, $object_type)
    {
        $start = microtime(true);
        self::preloadCache();
        $sr = self::loadFromCache($supplier_reference, $object_type);
        if ($sr) {
            return $sr;
        }

        $query = new DbQuery();
        $query->select('er.id_advancedimporter_supplierreference');
        $query->from('advancedimporter_supplierreference', 'er');
        $query->where('er.supplier_reference = \''.pSQL(trim($supplier_reference)).'\'');
        $query->where('er.object_type = \''.pSQL($object_type).'\'');

        $id = Db::getInstance()->getValue($query);
        try {
            if ($id) {
                return self::getById($id);
            }
        } catch (SupplierReferenceException $e) {
            return self::getBySupplierReference($supplier_reference, $object_type);
        }

        throw new SupplierReferenceException(
            'Unknow Supplier Reference "'.$supplier_reference.'" of object type "'.$object_type.'"'
        );
    }

    public static function getByObjectId($object_id, $object_type)
    {
        $query = new DbQuery();
        $query->select('sr.id_advancedimporter_supplierreference');
        $query->from('advancedimporter_supplierreference', 'sr');
        $query->where('sr.id_object = \''.((int) $object_id).'\'');
        $query->where('sr.object_type = \''.pSQL($object_type).'\'');

        $id = Db::getInstance()->getValue($query);
        try {
            if ($id) {
                return self::getById($id);
            }
        } catch (SupplierReferenceException $e) {
            throw new SupplierReferenceException(
                'Unknow Supplier Reference for object id #"'.$object_id.'" of object type "'.$object_type.'"'
            );
        }

        throw new SupplierReferenceException(
            'Unknow Supplier Reference for object id #"'.$object_id.'" of object type "'.$object_type.'"'
        );
    }

    public static function getById($id)
    {
        $reference = new self($id);
        if ($reference->getInstance()->id) {
            self::addToCache($reference);
            return $reference;
        } else {
            // The object do not exists
            // the reference must be removed
            $reference->delete();
        }

        throw new SupplierReferenceException(
            'Object do not exists'
        );
    }

    public function __construct($id = null, $id_lang = null, $id_shop = null, $translator = null)
    {
        parent::__construct($id, $id_lang, $id_shop, $translator);
        $this->setOrigData();
    }

    public function setOrigData()
    {
        $this->origData = array(
            'id_object' => $this->id_object,
            'object_type' => $this->object_type,
            'supplier_reference' => $this->supplier_reference,
        );
    }

    public function save($null_values = false, $auto_date = true)
    {
        $data = array(
            'id_object' => $this->id_object,
            'object_type' => $this->object_type,
            'supplier_reference' => $this->supplier_reference,
        );

        if ($data == $this->origData) {
            return true;
        }

        self::addToCache($this);
        return parent::save($null_values, $auto_date);
    }
}
