<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/supplierreferenceexception.php';

/** Class SupplierReferenceSource */
class SupplierReferenceSource extends ObjectModel
{
    public $id_object;
    public $object_type;
    public $supplier_reference;
    public $supplier;
    public $to_delete = 0;
    public $date_add;
    public $date_upd;

    public static $definition = array(
        'table' => 'advancedimporter_supplierreference_source',
        'primary' => 'id_advancedimporter_supplierreference_source',
        'fields' => array(
            'object_type' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),
            'supplier_reference' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),
            'supplier' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),
            'to_delete' => array('type' => self::TYPE_BOOL),
            'date_add' => array('type' => self::TYPE_DATE, 'required' => true, 'validate' => 'isDateFormat'),
            'date_upd' => array('type' => self::TYPE_DATE, 'required' => true, 'validate' => 'isDateFormat'),
        ),
    );

    public static function getBySupplierReference($supplier_reference, $object_type)
    {
        $query = new DbQuery();
        $query->select('er.id_advancedimporter_supplierreference_source');
        $query->from('advancedimporter_supplierreference_source', 'er');
        $query->where('er.supplier_reference = \''.pSQL(trim($supplier_reference)).'\'');
        $query->where('er.object_type = \''.pSQL($object_type).'\'');

        $id = Db::getInstance()->getValue($query);

        if (!$id) {
            $obj = new self();
            $obj->supplier_reference = $supplier_reference;
            $obj->object_type = $object_type;

            return $obj;
        }

        return new self($id);
    }

    public static function getToDeleteFromSupplier($supplier)
    {
        if (class_exists('PrestaShopCollection')) {
            $collection = new PrestaShopCollection('SupplierReferenceSource');
        } else {
            $collection = new Collection('SupplierReferenceSource');
        }
        $collection->where('supplier', '=', $supplier);
        $collection->where('to_delete', '=', true);

        return $collection;
    }

    public static function flagToDeleteSupplier($supplier)
    {
        Db::getInstance()->execute(
            'UPDATE `'._DB_PREFIX_.'advancedimporter_supplierreference_source`
            SET to_delete = 1
            WHERE supplier = "'.pSql($supplier).'"'
        );
    }
}
