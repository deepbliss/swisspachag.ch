<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/extractor/interface.php';

class AICsvExtractor implements AIExtractorInterface
{
    public static function getName()
    {
        return 'CSV';
    }

    public function extract($file, $template)
    {
        // Manage CR line break
        ini_set('auto_detect_line_endings', true);

        $first_line = true;

        $xml = new SimpleXmlElement('<csv />');
        if (($handle = fopen($file, 'r')) !== false) {
            while (($line = fgetcsv($handle, 0, $template->delimiter, $template->enclosure, $template->escape)) !== false) {
                if ($template->ignore_first_line && $first_line) {
                    $first_line = false;
                    continue;
                }
                $node_line = $xml->addChild('line');
                foreach ($line as $col => $value) {
                    $node_line->addChild($this->convertIntToLetters($col), htmlspecialchars($value));
                }
            }
        } else {
            throw new Exception("Cannot open file {$file}");
        }

        // Debug
        /*
        echo $xml->asXml();
        die();
        // */

        return $xml;
    }

    public function convertIntToLetters($number)
    {
        $result = '';

        do {
            $rest = $number % 26;
            $number = ($number - $rest) / 26;

            if (!empty($result)) {
                $rest--;
            }
            $result = chr(65 + $rest).$result;
        } while ($number != 0);

        return $result;
    }
}
