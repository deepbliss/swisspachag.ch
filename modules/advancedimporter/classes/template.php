<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class AITemplate extends ObjectModel
{
    // General
    public $name;
    public $date_add;
    public $date_upd;
    public $xslt = '';
    public $extractor = 'AIXmlExtractor';

    // Data extracted from the XML for the assistant
    public $item_root;
    public $nodes = array();
    public $schema = array();
    public $nodes_serialized;
    public $schema_serialized;

    // Spécific to CSV
    public $ignore_first_line = 1;
    public $enclosure = '"';
    public $delimiter = ',';
    public $escape = '\\';
    public $encoding = 'UTF-8';

    public static $definition = array(
        'table' => 'advancedimporter_template',
        'primary' => 'id_advancedimporter_template',
        'fields' => array(
            // General
            'name' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 50),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
            'xslt' => array('type' => self::TYPE_HTML, 'required' => true),
            'extractor' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 50),
            // Spécific to CSV
            'enclosure' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 1),
            'delimiter' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 1),
            'escape' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 1),
            'encoding' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 10),
            'ignore_first_line' => array('type' => self::TYPE_INT, 'required' => true),
            // Data extracted from the XML for the assistant
            'item_root' => array('type' => self::TYPE_STRING, 'required' => false, 'size' => 255),
            'nodes_serialized' => array('type' => self::TYPE_HTML, 'required' => false),
            'schema_serialized' => array('type' => self::TYPE_HTML, 'required' => false),
        ),
    );

    public function __construct($id = null, $id_lang = null, $id_shop = null)
    {
        parent::__construct($id, $id_lang, $id_shop);

        $this->nodes = $this->unserialize($this->nodes_serialized);
        $this->schema = $this->unserialize($this->schema_serialized);

        if (empty($this->nodes)) {
            $this->nodes = array();
        }

        if (empty($this->schema)) {
            $this->schema = array();
        }
    }

    public function save($null_values = false, $auto_date = true)
    {
        $this->nodes_serialized = serialize($this->nodes);
        $this->schema_serialized = serialize($this->schema);
        return parent::save($null_values, $auto_date);
    }

    protected function unserialize($string)
    {
        return Tools::unserialize($string);

        // This stuff introduct bugs
        /*
        $string2 = preg_replace_callback(
            '!s:(\d+):"(.*?)";!s',
            function ($m) {
                $len = Tools::strlen($m[2]);
                $result = "s:$len:\"{$m[2]}\";";
                return $result;
            },
            $string
        );
        return Tools::unserialize($string2);
         */
    }

    public function convert($xml, $file)
    {
        $xslt_document = new SimpleXMLElement($this->xslt);
        $errors = libxml_get_errors();
        if (count($errors)) {
            foreach ($errors as &$error) {
                $error = (string) $error->message;
            }

            throw new Exception(
                "Xslt #{$this->id} is not formatted correctly: "
                .implode(', ', $errors)
            );
        }
        $proc = new XSLTProcessor();
        $proc->importStylesheet($xslt_document);
        libxml_use_internal_errors(true);
        $xml_transformed = $proc->transformToXML($xml);
        if ($xml_transformed === false) {
            throw new Exception("Xslt if the template #{$this->id} do not match with file $file");
        }

        $xml = new SimpleXMLElement($xml_transformed);
        $errors = libxml_get_errors();
        if (count($errors)) {
            foreach ($errors as &$error) {
                $error = (string) $error->message;
            }

            throw new Exception(
                "Result of the template #{$this->id} of the file $file is not a valid XML: "
                .implode(', ', $errors)
            );
        }

        return $xml;
    }

    public function loadFile($file)
    {
        $extractor_class = $this->extractor;

        $filename = Tools::strtolower(Tools::substr(Tools::substr($extractor_class, 2), 0, -9));
        $filepath = _PS_MODULE_DIR_.'advancedimporter/classes/extractor/'.$filename.'.php';
        include_once $filepath;

        $extractor = new $extractor_class;
        $xml = $extractor->extract($file, $this);

        if (!empty($this->xslt)) {
            $xml = $this->convert($xml, $file);
        }

        return $xml;
    }
}
