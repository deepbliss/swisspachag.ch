<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class AIIdentifier */
class AIIdentifier
{
    /**
     * With identifier, return object id
     *
     * @param string $identifier Column name
     * @param string $class Object class
     * @param string $value Value of the identifier
     *
     * @return int Id of the object
     * @exception Exception If no object found
     */
    public static function get($identifier, $class, $value)
    {
        $query = 'SELECT `'.bqSql($class::$definition['primary']).'`
            FROM `'._DB_PREFIX_.$class::$definition['table'].'`
            WHERE `'.pSql($identifier).'` = "'.pSql($value).'"';
        $id =  (int) Db::getInstance()->getValue($query);
        if (!$id) {
            throw new Exception("No identifier $identifier found for class $class and value $value");
        }

        return $id;
    }
}
