<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class class HelperString */
class HelperString
{
    public static function remove($values, $entity, string $attribute)
    {
        $value = $values[0];
        $pattern = $values[1];
        $sub = isset($values[2]) ? $values[2] : 'Usi';

        if (is_object($sub)) {
            $sub = 'Usi';
        }

        $pattern = str_replace('/', '\/', $pattern);

        return preg_replace('/'.$pattern.'/'.$sub, '', $value);
    }

    public static function replace($values, $entity, string $attribute)
    {
        $value = $values[0];
        $pattern = $values[1];
        $replacement = $values[2];
        $sub = isset($values[3]) ? $values[3] : 'Usi';

        $pattern = str_replace('/', '\/', $pattern);

        return preg_replace('/'.$pattern.'/'.$sub, $replacement, $value);
    }

    public static function transcode($values, $entity, string $attribute)
    {
        $origValue = $values[0];
        $values = $values[1];

        $newValues = explode('|', $values);
        foreach ($newValues as $patterns) {
            list($value, $newValue) = explode(':', $patterns);
            if ($value  == $origValue) {
                return $newValue;
            }
        }
    }

    public static function truncate($values, $entity, string $attribute)
    {
        $value = $values[0];
        $limit = $values[1];

        return Tools::substr($value, 0, $limit);
    }
}
