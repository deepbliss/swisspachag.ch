<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class class HelperNumber */
class HelperNumber
{
    public static function commaFloatFormat($values, $entity, string $attribute)
    {
        $values[0] = str_replace(' ', '', $values[0]);
        $values[0] = str_replace("'", '', $values[0]);
        $values[0] = str_replace('.', '', $values[0]);
        $values[0] = str_replace(',', '.', $values[0]);

        return (float) $values[0];
    }

    public static function pointFormat($values, $entity, string $attribute)
    {
        $values[0] = str_replace(' ', '', $values[0]);
        $values[0] = str_replace(',', '', $values[0]);

        return (float) $values[0];
    }

    public static function momayyezFormat($values, $entity, string $attribute)
    {
        $values[0] = str_replace('-', '.', $values[0]);

        return (float) $values[0];
    }

    public static function calculate($value)
    {
        if (!preg_match('/^\s*([\(\s]*\d+([\.,]\d+)?[\)\s]*\s*[+*\/-]\s*)*[\(\s]*\d+([\.,]\d+)?[\)\s]*$/Usi', $value)) {
            return 0;
        }

        $value = str_replace(' ', '', $value);
        $value = '('.$value.')';

        while (preg_match('/\(([^\(\)]+)\)/', $value, $match)) {
            $result = self::caculateExpr($match[1]);
            $value = str_replace('('.$match[1].')', $result, $value);
        }

        return (float) $value;
    }

    protected static function caculateExpr($expr)
    {
        while (preg_match('/((\d+(?:[\.,]\d+)?)([\*\/])(\d+(?:[\.,]\d+)?))/', $expr, $match)) {
            switch ($match[3]) {
                case '*':
                    $expr = str_replace($match[1], $match[2] * $match[4], $expr);
                    break;
                case '/':
                    $expr = str_replace($match[1], $match[2] / $match[4], $expr);
                    break;
            }
        }
        while (preg_match('/((\d+(?:[\.,]\d+)?)([+-])(\d+(?:[\.,]\d+)?))/', $expr, $match)) {
            switch ($match[3]) {
                case '+':
                    $expr = str_replace($match[1], $match[2] + $match[4], $expr);
                    break;
                case '-':
                    $expr = str_replace($match[1], $match[2] - $match[4], $expr);
                    break;
            }
        }

        return (float) $expr;
    }

    public static function neg($values, $entity, string $attribute)
    {
        return -(float) $values[0];
    }

    public static function abs($values, $entity, string $attribute)
    {
        return abs((float) $values[0]);
    }

    public static function sum($values, $entity, string $attribute)
    {
        return round((float) $values[0] + (float) $values[1], 2);
    }

    public static function sub($values, $entity, string $attribute)
    {
        return round((float) $values[0] - (float) $values[1], 2);
    }

    public static function multiply($values, $entity, string $attribute)
    {
        return round((float) $values[0] * (float) $values[1], 2);
    }

    public static function divide($values, $entity, string $attribute)
    {
        return round((float) $values[0] / (float) $values[1], 2);
    }

    public static function modulo($values, $entity, string $attribute)
    {
        return round((float) $values[0] % (float) $values[1], 2);
    }

    /**
     * $values[0] * $values[1]%.
     */
    public static function percentage($values, $entity, string $attribute)
    {
        return round((float) $values[0] * ((float) $values[1] / 100), 2);
    }

    /**
     * $values[0] - $values[0] * $values[1]%.
     */
    public static function percentageComplement($values, $entity, string $attribute)
    {
        return round((float) $values[0] * (1 - ((float) $values[1] / 100)), 2);
    }

    /**
     * $values[0] * (100% + $values[1]%)
     * or
     * $values[0] + $values[0] * $values[1]%.
     */
    public static function addPercentage($values, $entity, string $attribute)
    {
        return round((float) $values[0] * (1 + ((float) $values[1] / 100)), 2);
    }

    /**
     * $values[0] / $values[1].
     */
    public static function rate($values, $entity, string $attribute)
    {
        return round(1 - (float) $values[0] / (float) $values[1], 2);
    }
}
