<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class AIEntity_Category_Image extends AIEntity_Abstract
{
    protected $tmpfile;

    public function execute()
    {
        // Copy image
        $this->tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');

        $modifier = '';
        if (isset($this->block['modifier'])) {
            $modifier = (string) $this->block['modifier'];
        }
        $url = $this->modify((string) $this->block, $modifier, $this);
        if (strpos($url, 'http') !== 0) {
            $url = _PS_ROOT_DIR_.$url;
        }

        if (!copy((string) $url, $this->tmpfile)) {
            throw new Exception("Cannot save image $url");
        }

        $path = _PS_CAT_IMG_DIR_.(int) $this->getParent()->getObject()->id;
        ImageManager::resize($this->tmpfile, $path.'.jpg');
        $images_types = ImageType::getImagesTypes('categories');

        foreach ($images_types as $image_type) {
            ImageManager::resize(
                $this->tmpfile,
                $path.'-'.Tools::stripslashes($image_type['name']).'.jpg',
                $image_type['width'],
                $image_type['height']
            );
        }

        unlink($this->tmpfile);
    }
}
