<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class AIEntity_Categorypath extends AIEntity_Abstract
{
    protected $category;
    protected $final_category_id;

    public function execute()
    {
        $separator = (string) $this->block['separator'] ?: '>';
        $modifier = (string) $this->block->modifier;

        $categorypath = array_map(
            'trim',
            explode(
                $separator,
                trim((string) $this->block)
            )
        );

        foreach ($categorypath as $id => $name) {
            if (empty($name)) {
                unset($categorypath[$id]);
                continue;
            }
            $categorypath[$id] = $this->modify($name, $modifier, $this);
        }

        if (count($categorypath) === 0) {
            return;
        }

        $root_category = Category::getRootCategory();
        $parent_category = $root_category;
        foreach ($categorypath as $category_name) {
            $parent_category = $this->getCategory($category_name, $parent_category);
        }

        $this->final_category_id = $parent_category->id;
    }

    public function getCategory($category_name, $parent_category)
    {
        if ($id = Db::getInstance()->getValue(
            'SELECT c.id_category
            FROM '._DB_PREFIX_.'category as c
            INNER JOIN '._DB_PREFIX_.'category_lang as l
            ON c.id_category = l.id_category
            WHERE l.name  = "'.pSql($category_name).'"
            AND c.id_parent = '.(int) $parent_category->id
        )) {
            $category = new Category($id);
        } else {
            $category = new Category();

            $languages = Language::getLanguages(false);
            foreach ($languages as $language) {
                $category->name[$language['id_lang']] = $category_name;
                $link_rewrite = $this->str2url($category_name);
                if (empty($link_rewrite)) {
                    $link_rewrite = uniqid();
                }
                $category->link_rewrite[$language['id_lang']] = $link_rewrite;
                $category->description[$language['id_lang']] = $category_name;
            }

            $category->id_parent = $parent_category->id;
            $category->save();
        }

        return $category;
    }


    /**
     * Defined a custom str2url.
     */
    protected function str2url($str)
    {
        static $allow_accented_chars = null;
        static $has_mb_strtolower = null;

        if ($has_mb_strtolower === null) {
            $has_mb_strtolower = function_exists('mb_strtolower');
        }

        if (!is_string($str)) {
            return false;
        }

        if ($str == '') {
            return '';
        }

        if ($allow_accented_chars === null) {
            $allow_accented_chars = Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL');
        }

        $return_str = trim($str);

        if ($has_mb_strtolower) {
            $return_str = mb_strtolower($return_str, 'utf-8');
        }
        if (!$allow_accented_chars) {
            $return_str = Tools::replaceAccentedChars($return_str);
        }

        // Remove all non-whitelist chars.
        if ($allow_accented_chars) {
            $return_str = preg_replace('/[^a-zA-Z0-9\s\'\:\/\[\]\-\p{L}]/u', '', $return_str);
        } else {
            $return_str = preg_replace('/[^a-zA-Z0-9\s\'\:\/\[\]\-]/', '', $return_str);
        }

        $return_str = preg_replace('/[\s\'\:\/\[\]\-]+/', ' ', $return_str);
        $return_str = str_replace(array(' ', '/'), '-', $return_str);

        // If it was not possible to lowercase the string with mb_strtolower, we do it after the transformations.
        // This way we lose fewer special chars.
        if (!$has_mb_strtolower) {
            $return_str = Tools::strtolower($return_str);
        }

        return $return_str;
    }
}
