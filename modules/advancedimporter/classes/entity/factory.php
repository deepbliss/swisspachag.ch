<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/entity/interface.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/classfactory.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/entity/abstract.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/entity/objectabstract.php';
class AIEntity_Factory
{
    protected static $instance = null;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new AIEntity_Factory();
        }

        return self::$instance;
    }

    public function getEntity($type, $block, $id_block, $id_flow, $parent = null)
    {
        // Try to get the type
        $class = 'AIEntity_'.Tools::ucfirst(str_replace('/', '_', Tools::strtolower($type)));
        $class = implode('_', array_map('ucfirst', explode('_', $class)));
        if (!class_exists($class)) {
            $file = _PS_MODULE_DIR_.'advancedimporter/classes/entity/'.Tools::strtolower($type).'.php';
            if (!file_exists($file)) {
                if (substr_count($type, '/') > 1) {
                    $types = explode('/', $type);
                    $new_types = array();
                    for ($i = 0; $i < count($type) - 1; $i++) {
                        $new_types[] = $types[$i];
                    }
                    $new_types[] = end($types);
                    $type = end($types);
                    try {
                        return self::getEntity(implode('/', $new_types), $block, $id_block, $id_flow, $parent);
                    } catch (Exception $e) {
                        throw new Exception(
                            sprintf('Unknow entity "%s"', $type)
                            .sprintf(' Make sure class "%s" or file "%s" exists.', $class, $file)
                            .' '.$e->getMessage()
                        );
                    }
                } elseif (substr_count($type, '/') === 1) {
                    $types = explode('/', $type);
                    $type = end($types);
                    try {
                        return self::getEntity($type, $block, $id_block, $id_flow, $parent);
                    } catch (Exception $e) {
                        throw new Exception(
                            sprintf('Unknow entity "%s"', $type)
                            .sprintf(' Make sure class "%s" or file "%s" exists.', $class, $file)
                            .' '.$e->getMessage()
                        );
                    }
                } else {
                    $subtypes = explode('_', $type);
                    $subtype = end($subtypes);
                    if (class_exists(Tools::ucfirst($subtype))) {
                        AIClassFactory::loadEntity($class);
                    } else {
                        throw new Exception(
                            sprintf('Unknow entity "%s"', $type)
                            .sprintf(' Make sure class "%s" or file "%s" exists.', $class, $file)
                        );
                    }
                }
            } else {
                require $file;
            }
        }

        $interfaces = class_implements($class);
        if (!in_array('AIEntity_Interface', $interfaces)) {
            throw new Exception(
                sprintf('Class "%s" do not implement AIEntity_Interface"', $class)
            );
        }


        return new $class($block, $id_block, $id_flow, $parent);
    }
}
