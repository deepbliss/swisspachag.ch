<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/objectmodel/SupplierReference.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/identifier.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/history.php';

abstract class AIEntity_ObjectAbstract extends AIEntity_Abstract
{
    protected $supplier_reference = null;
    protected $object = null;
    protected $errors = array();
    protected $original_shop;
    protected $processLocked = false;

    public function execute()
    {
        $this->switchShop();

        try {
            $this->beforeExecute();

            if ($this->processLocked) {
                return;
            }

            // Don't create object if mode attribute is update
            if (isset($this->block['mode'])
                && $this->block['mode'] == 'update'
                && !$this->getObject()->id
            ) {
                $this->beforeEnd();
                return;
            }

            if ($this->processLocked) {
                return;
            }

            // Don't update object if mode attribute is create
            if (isset($this->block['mode'])
                && $this->block['mode'] == 'create'
                && $this->getObject()->id
            ) {
                if ($this->hasSupplierReference()) {
                    // Save supplier reference
                    $this->getSupplierReferenceObject()->supplier_reference = $this->getSupplierReference();
                    $this->getSupplierReferenceObject()->object_type = $this->getClass();
                    $this->getSupplierReferenceObject()->id_object = (int) $this->getObject()->id;
                    $this->getSupplierReferenceObject()->save();
                }
                $this->beforeEnd();
                return;
            }

            if ($this->processLocked) {
                return;
            }

            $this->beforeSetFields();

            if ($this->processLocked) {
                return;
            }

            $this->setFields();

            if ($this->processLocked) {
                return;
            }

            $this->executeEntities(false);

            if ($this->processLocked) {
                return;
            }

            $this->beforeValidate();

            if ($this->processLocked) {
                return;
            }

            // Object validation
            $this->validateFields();
            if (count($this->errors)) {
                throw new Exception('Some fields are missing or not valid: '.implode('; ', $this->errors));
            }

            if ($this->processLocked) {
                return;
            }

            $this->beforeSave();

            if ($this->processLocked) {
                return;
            }

            $this->getObject()->save();

            if ($this->hasSupplierReference()) {
                // Save supplier reference
                $this->getSupplierReferenceObject()->supplier_reference = $this->getSupplierReference();
                $this->getSupplierReferenceObject()->object_type = $this->getClass();
                $this->getSupplierReferenceObject()->id_object = (int) $this->getObject()->id;
                $this->getSupplierReferenceObject()->save();
            }

            AIHistory::create(
                $this->getType(),
                $this->id_advancedimporter_block,
                $this->id_advancedimporter_flow,
                $this->getClass(),
                $this->getObject()->id,
                $this->getSupplierReference()
            );


            if ($this->processLocked) {
                return;
            }

            $this->afterSave();

            if ($this->processLocked) {
                return;
            }

            $this->executeEntities(true);

            if ($this->processLocked) {
                return;
            }

            $this->afterExecuteEntities();

            $class = $this->getClass();
            Log::sys(
                $this->id_advancedimporter_block,
                $this->id_advancedimporter_flow,
                "Object {$class} {$this->getObject()->id} imported"
            );

            if ($this->processLocked) {
                return;
            }

            $this->afterExecute();

            if ($this->processLocked) {
                return;
            }

            $this->beforeEnd();
        } catch (Exception $e) {
            $this->restoreShop();
            throw $e;
        }

        $this->restoreShop();
    }

    protected function getSupplierReferenceObject()
    {
        if (empty($this->supplier_reference)) {
            $class = $this->getClass();
            try {
                $this->supplier_reference = SupplierReference::getBySupplierReference(
                    $this->getSupplierReference(),
                    $class
                );

                if (!empty($this->block->id) && $this->block->id != $this->supplier_reference->id_object) {
                    throw new Exception(
                        "Supplier Reference \"{$this->getSupplierReference()}\" already exist for the id "
                        ."{$this->supplier_reference->id_object}."
                    );
                }

                $this->block->id = $this->supplier_reference->id_object;
                $this->object = $this->supplier_reference->getInstance();
            } catch (SupplierReferenceException $e) {
                $this->supplier_reference = new SupplierReference();
            }
        }

        return $this->supplier_reference;
    }

    public function getObject()
    {
        // @TODO Add multishop management
        if (is_null($this->object)) {
            $class = $this->getClass();

            if (!class_exists($class)) {
                if (!file_exists(_PS_MODULE_DIR_.'advancedimporter/classes/objectmodel/'.$class.'.php')) {
                    throw new Exception("class \"{$class}\" is not a valid class.");
                }

                require_once _PS_MODULE_DIR_.'advancedimporter/classes/objectmodel/'.$class.'.php';
            }

            if ($this->hasSupplierReference()) {
                if ($this->getSupplierReferenceObject()->id_object) {
                    if (!empty($this->block->id) && $this->block->id != $this->supplier_reference->id_object) {
                        Log::notice(
                            $this->id_advancedimporter_block,
                            $this->id_advancedimporter_flow,
                            "Supplier Reference \"{$this->getSupplierReference()}\" already exist for the id "
                            ."{$this->supplier_reference->id_object}."
                        );
                    }

                    $this->block->id = $this->supplier_reference->id_object;
                    $this->object = $this->supplier_reference->getInstance();
                } else {
                    Log::notice(
                        $this->id_advancedimporter_block,
                        $this->id_advancedimporter_flow,
                        "Supplier Reference \"{$this->getSupplierReference()}\" is unknow"
                    );
                }
            }

            $this->getObjectAfterSupplierReference($class);

            // Try to get the entity using the defined identifier
            if (isset($this->block['identifier'])) {
                $identifier = (string) $this->block['identifier'];

                if (!empty($this->block->$identifier)) {
                    try {
                        $this->block->id = AIIdentifier::get(
                            (string) $this->block['identifier'],
                            $class,
                            (string) $this->block->$identifier
                        );
                    } catch (Exception $e) {
                        // Nothing to do
                    }
                }
            }

            if (!isset($this->object)) {
                if (isset($this->block->id)) {
                    if (empty($this->block->id)) {
                        throw new Exception("Id could not be empty, for the object \"{$class}\".");
                    }

                    $this->object = AIClassFactory::loadObjectModel($class, $this->block->id);
                } else {
                    $this->object = AIClassFactory::loadObjectModel($class);
                }
            }
        }
        return $this->object;
    }

    /**
     * Validate fields of an object.
     */
    public function validateFields()
    {
        $class = $this->getClass();
        if (empty($class::$definition['fields'])) {
            return;
        }

        foreach ($class::$definition['fields'] as $field => $detail) {
            if (isset($detail['lang']) && $detail['lang']) {
                if (empty($this->getObject()->$field)) {
                    if (($res = $this->getObject()->validateField($field, null)) !== true) {
                        $this->errors[] = $res;
                    }
                } else {
                    foreach ($this->getObject()->$field as $id_lang => $value) {
                        if (($res = $this->getObject()->validateField($field, $value, $id_lang)) !== true) {
                            $this->errors[] = $res;
                        }
                    }
                }
            } elseif (($res = $this->getObject()->validateField($field, $this->getObject()->$field)) !== true) {
                $this->errors[] = $res;
            }
        }

        return $this->errors;
    }

    protected function executeEntities($needParentSave)
    {
        $class = $this->getClass();

        foreach ($this->block as $type => $block) {
            if (in_array($type, array_keys($class::$definition['fields']))
                || $type === 'id'
            ) {
                continue;
            }
            $entity_type = $this->getType().'/'.$type;
            try {
                $entity = AIEntity_Factory::getInstance()->getEntity(
                    $entity_type,
                    $block,
                    $this->id_advancedimporter_block,
                    $this->id_advancedimporter_flow,
                    $this
                );
            } catch (Exception $e) {
                throw new Exception("Unknow entity \"$entity_type\". {$e->getMessage()}");
            }

            if ($entity->needParentSave === $needParentSave) {
                $entity->execute();
            }
        }
    }

    protected function setFields()
    {
        $class = $this->getClass();

        foreach ($class::$definition['fields'] as $field => $value) {
            if (isset($this->block->$field)) {
                foreach ($this->block->$field as $node) {
                    if (isset($node['validator'])) {
                        $this->getObject()->setValidator($field, (string) $node['validator']);
                    }

                    $attributes = array_keys((array) $node->attributes());

                    if (in_array('mode', $attributes)
                        && $node->$field['mode'] == 'create'
                        && $this->getObject()->id
                    ) {
                        continue;
                    }

                    if (in_array('mode', $attributes)
                        && $this->block->$field['mode'] == 'update'
                        && !$this->getObject()->id
                    ) {
                        continue;
                    }
                }

                if (!empty($node['supplier-reference'])) {
                    $supplierReference = SupplierReference::getBySupplierReference(
                        (string) $node,
                        (string) $node['supplier-reference']
                    );

                    $this->block->$field = $supplierReference->id_object;
                }

                if (!empty($node['identifier']) && !empty($node['type'])) {
                    $this->block->$field = AIIdentifier::get(
                        (string) $node['identifier'],
                        (string) $node['type'],
                        (string) $node
                    );
                }

                if (isset($value['lang']) && $value['lang']) {
                    if (!is_array($this->getObject()->$field)) {
                        $values = array();
                    } else {
                        $value = $this->getObject()->$field;
                    }
                    
                    foreach ($this->block->$field as $node) {
                        $modifier = '';
                        if (isset($node['modifier'])) {
                            $modifier = (string) $node['modifier'];
                        }
                        $value = $this->modify((string) $node, $modifier, $field);
                        if (isset($node['lang'])) {
                            $values[$this->getLanguages()[(string) $node['lang']]] = $value;
                        } else {
                            foreach ($this->getLanguages() as $lang_id) {
                                $values[$lang_id] = $value;
                            }
                        }
                    }
                    $this->getObject()->$field = $values;
                } else {
                    $node = $this->block->$field;
                    $this->setField($field, $node);
                }
            }
        }
    }

    public function setField($field, $node, $object = null)
    {
        if (is_null($object)) {
            $object = $this->getObject();
        }

        $modifier = '';
        if (isset($node['modifier'])) {
            $modifier = (string) $node['modifier'];
        }
        // @TODO Manage decimal
        $object->$field = $this->modify((string) $node, $modifier, $field);
    }

    protected function getLanguages()
    {
        static $languages = array();

        if (empty($languages)) {
            foreach (Language::getIsoIds(false) as $lang) {
                $languages[$lang['iso_code']] = $lang['id_lang'];
            }
        }

        return $languages;
    }

    protected function beforeExecute()
    {
    }

    protected function beforeSetFields()
    {
    }

    protected function afterExecuteEntities()
    {
    }

    protected function afterExecute()
    {
    }

    protected function beforeValidate()
    {
    }

    protected function beforeSave()
    {
    }

    protected function afterSave()
    {
    }

    protected function getObjectAfterSupplierReference($class)
    {
    }

    protected function beforeEnd()
    {
    }

    protected function getClass()
    {
        $type = $this->getType();
        $types = explode('/', $type);
        return Tools::ucfirst(end($types));
    }

    protected function getSupplierReference()
    {
        $supplier_reference = (string) $this->block['supplier-reference'];
        if (!empty($supplier_reference)) {
            return $supplier_reference;
        }
        return $this->getDefaultSupplierReference();
    }

    protected function hasSupplierReference()
    {
        $supplier_reference = $this->getSupplierReference();
        return !empty($supplier_reference);
    }

    protected function getDefaultSupplierReference()
    {
    }
}
