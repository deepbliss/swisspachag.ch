<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class AIEntity_Product extends AIEntity_ObjectAbstract
{
    public $imageIds = array();
    public $categoryIds = array();
    public $combinationIds = array();
    public $tagIds = array();
    public $accessoryIds = array();
    public $carrierIds = array();

    protected function beforeSetFields()
    {
        if (!isset($this->block->active) && !$this->getObject()->id) {
            $this->block->active = true;
        }
        if (!isset($this->block->available_for_order) && !$this->getObject()->id) {
            $this->block->available_for_order = true;
        }
    }

    protected function beforeValidate()
    {
        // Fix for price when store is not defined
        if (Context::getContext()->shop->id == 0 && $this->getObject()->id
            && is_null($this->getObject()->price)
        ) {
            $newContext = Context::getContext()->cloneContext();
            $newContext->shop->id = 1;
            $this->getObject()->price = Product::getPriceStatic(
                (int) $this->getObject()->id,
                false,
                null,
                6,
                null,
                false,
                true,
                1,
                false,
                null,
                null,
                null,
                $this->getObject()->specificPrice,
                true,
                true,
                $newContext
            );
        }

        if (empty($this->getObject()->link_rewrite) && !empty($this->getobject()->name)) {
            $link_rewrite = array();
            foreach ($this->getObject()->name as $key => $name) {
                $link_rewrite[$key] = $this->str2url($name);
            }
            $this->getObject()->link_rewrite = $link_rewrite;
        }

        if (isset($this->block->price) && $this->block->price['tax'] == 'include') {
            if ($this->getObject()->price) {
                $this->getObject()->price = Tools::ps_round(
                    $this->getObject()->price / (1 + ($this->getTaxPercentage() / 100)),
                    6
                );
            }
        }

        if (isset($this->block->wholesale_price) && $this->block->wholesale_price['tax'] == 'include') {
            if ($this->getObject()->wholesale_price) {
                $this->getObject()->wholesale_price = Tools::ps_round(
                    $this->getObject()->wholesale_price / (1 + ($this->getTaxPercentage() / 100)),
                    6
                );
            }
        }
    }

    public function getTaxPercentage()
    {
        if (empty($this->getObject()->id_tax_rules_group)) {
            return 0;
        }

        $tax_manager = TaxManagerFactory::getManager(
            Context::getContext()->shop->getAddress(),
            (int) $this->getObject()->id_tax_rules_group
        );
        return $tax_manager->getTaxCalculator()->getTotalRate();
    }

    protected function getDefaultSupplierReference()
    {
        if (!empty($this->block->supplier_reference)) {
            return (string) $this->block->supplier_reference;
        }
        if (!empty($this->block->ean)) {
            return (string) $this->block->ean;
        }
        if (!empty($this->block->reference)) {
            return (string) $this->block->reference;
        }
    }

    /**
     * Defined a custom str2url.
     */
    protected function str2url($str)
    {
        static $allow_accented_chars = null;
        static $has_mb_strtolower = null;

        if ($has_mb_strtolower === null) {
            $has_mb_strtolower = function_exists('mb_strtolower');
        }

        if (!is_string($str)) {
            return false;
        }

        if ($str == '') {
            return '';
        }

        if ($allow_accented_chars === null) {
            $allow_accented_chars = Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL');
        }

        $return_str = trim($str);

        if ($has_mb_strtolower) {
            $return_str = mb_strtolower($return_str, 'utf-8');
        }
        if (!$allow_accented_chars) {
            $return_str = Tools::replaceAccentedChars($return_str);
        }

        // Remove all non-whitelist chars.
        if ($allow_accented_chars) {
            $return_str = preg_replace('/[^a-zA-Z0-9\s\'\:\/\[\]\-\p{L}]/u', '', $return_str);
        } else {
            $return_str = preg_replace('/[^a-zA-Z0-9\s\'\:\/\[\]\-]/', '', $return_str);
        }

        $return_str = preg_replace('/[\s\'\:\/\[\]\-]+/', ' ', $return_str);
        $return_str = str_replace(array(' ', '/'), '-', $return_str);

        // If it was not possible to lowercase the string with mb_strtolower, we do it after the transformations.
        // This way we lose fewer special chars.
        if (!$has_mb_strtolower) {
            $return_str = Tools::strtolower($return_str);
        }

        return $return_str;
    }

    protected function afterExecuteEntities()
    {
        // remove missing images
        if (isset($this->block['remove-missing-images']) && $this->isTrue($this->block['remove-missing-images'])) {
            foreach ($this->getObject()->getImages(Context::getContext()->language->id) as $image) {
                if (!in_array($image['id_image'], $this->imageIds)) {
                    $imageObject = new Image((int) $image['id_image']);
                    $imageObject->delete();
                }
            }
        }

        // Remove missing category attachments
        if (isset($this->block['remove-missing-categories'])
            && $this->isTrue($this->block['remove-missing-categories'])
        ) {
            foreach ($this->getObject()->getCategories() as $categoryId) {
                if (!in_array($categoryId, $this->categoryIds)) {
                    $this->getObject()->deleteCategory($categoryId);
                }
            }

            if (!in_array($this->getObject()->id_category_default, $this->categoryIds)) {
                $this->getObject()->id_category_default = null;
            }
        }

        if (!$this->getObject()->id_category_default && !empty($this->categoryIds)) {
            $this->getObject()->id_category_default = current($this->categoryIds);
            $this->getObject()->save();
        }

        // Remove missing combinations
        if (isset($this->block['remove-missing-combinations'])
            && $this->isTrue($this->block['remove-missing-combinations'])
        ) {
            $query = 'SELECT `id_product_attribute`
                FROM `'._DB_PREFIX_.'product_attribute`
                WHERE `id_product` = '.(int) $this->getObject()->id;

            foreach (Db::getInstance()->executeS($query, true, false) as $result) {
                if (!in_array((int) $result['id_product_attribute'], $this->combinationIds)) {
                    $combination = new Combination((int) $result['id_product_attribute']);
                    $combination->delete();
                }
            }
        }

        if (!empty($this->combinationIds)) {
            $query = 'SELECT `id_product_attribute`
                FROM `'._DB_PREFIX_.'product_attribute`
                WHERE `id_product` = '.(int) $this->getObject()->id.'
                AND default_on = 1';
            if (!Db::getInstance()->getValue($query)) {
                $query = 'SELECT `id_product_attribute`
                    FROM `'._DB_PREFIX_.'product_attribute`
                    WHERE `id_product` = '.(int) $this->getObject()->id;
                if ($res = Db::getInstance()->getValue($query)) {
                    $this->getObject()->setDefaultAttribute((int) $res);
                }
            }
        }

        // Remove missing tags
        if (isset($this->block['remove-missing-tags'])
            && $this->isTrue($this->block['remove-missing-tags'])
        ) {
            $query = 'SELECT `id_tag`
                FROM `'._DB_PREFIX_.'product_tag`
                WHERE `id_product` = '.(int) $this->getObject()->id;

            foreach (Db::getInstance()->executeS($query, true, false) as $result) {
                if (!in_array((int) $result['id_tag'], $this->tagIds)) {
                    Db::getInstance()->execute(
                        'DELETE FROM `'._DB_PREFIX_.'product_tag`
                        WHERE id_product = '.(int) $this->getObject()->id.'
                        AND id_tag = '.(int) $result['id_tag']
                    );
                }
            }
        }

        // Remove missing accessories
        if (isset($this->block['remove-missing-accessories'])
            && $this->isTrue($this->block['remove-missing-accessories'])
        ) {
            $query = 'SELECT `id_product_2`
                FROM `'._DB_PREFIX_.'accessory`
                WHERE `id_product_1` = '.(int) $this->getObject()->id;

            foreach (Db::getInstance()->executeS($query, true, false) as $result) {
                if (!in_array((int) $result['id_product_2'], $this->accessoryIds)) {
                    Db::getInstance()->execute(
                        'DELETE FROM `'._DB_PREFIX_.'accessory`
                        WHERE id_product_1 = '.(int) $this->getObject()->id.'
                        AND id_product_2 = '.(int) $result['id_product_2']
                    );
                }
            }
        }


        // Set carriers
        if (isset($this->block['remove-missing-carriers'])
            && $this->isTrue($this->block['remove-missing-carriers'])
        ) {
            $this->getObject()->setCarriers($this->carrierIds);
        } elseif (!empty($this->carrierIds)) {
            $carrierAsChanged = false;
            $carriers = $this->getObject()->getCarriers();
            if (empty($carriers)) {
                $carrierAsChanged = true;
            } else {
                foreach ($carriers as $carrier) {
                    $carrierAsChanged = true;
                    if (!in_array((int) $carrier['id_carrier'], $this->carrierIds)) {
                        $this->carrierIds[] = (int) $carrier['id_carrier'];
                    }
                }
            }

            if ($carrierAsChanged) {
                $this->getObject()->setCarriers($this->carrierIds);
            }
        }
    }

    protected function afterSave()
    {
        if (isset($this->block['remove-missing-features']) && $this->isTrue($this->block['remove-missing-features'])) {
            $this->getObject()->setWsProductFeatures(array());
        }
    }
}
