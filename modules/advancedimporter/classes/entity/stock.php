<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class AIEntity_Stock extends AIEntity_Abstract
{
    protected $cacheProduct = null;
    protected $cacheCombination = null;

    public function execute()
    {
        $this->switchShop();

        try {
            if (empty($this->block->warehouse)) {
                $this->executeStock();
            } else {
                $this->executeAdvancedStock();
            }
        } catch (Exception $e) {
            $this->restoreShop();
            throw $e;
        }
        $this->restoreShop();
    }

    protected function executeStock()
    {
        if (isset($this->block['mode']) && $this->block['mode'] == 'delta') {
            StockAvailable::updateQuantity(
                $this->getProductId(),
                $this->getCombinationId(),
                (int) (string) $this->block->quantity,
                Context::getContext()->shop->id
            );
        } else {
            StockAvailable::setQuantity(
                $this->getProductId(),
                $this->getCombinationId(),
                (int) (string) $this->block->quantity,
                Context::getContext()->shop->id
            );
        }

        if (isset($this->block->sellable_out_of_stock)) {
            StockAvailable::setProductOutOfStock(
                $this->getProductId(),
                (int) (string) $this->block->sellable_out_of_stock,
                null,
                $this->getCombinationId()
            );
        }
    }

    protected function executeAdvancedStock()
    {
        if (!empty($this->block->warehouse['identifier'])) {
            $warehouseId = (int) $this->getWarehouseFromIdentifier(
                (string) $this->block->warehouse['identifier'],
                (string) $this->block->warehouse
            );
        } elseif (!empty($this->block->warehouse['supplier-reference'])) {
            $supplierReference = SupplierReference::getBySupplierReference(
                (string) $this->block->warehouse,
                (string) $this->block->warehouse['supplier-reference']
            );
            $warehouseId = $supplierReference->id_object;
        } else {
            $warehouseId = (int) (string) $this->block->warehouse;
        }
        $warehouse = new Warehouse((int) $warehouseId);

        if (!$warehouse->id) {
            throw new Exception('Warehouse is not valid.');
        }

        if (!isset($this->block->reason)) {
            $this->block->reason = 1;
        }

        $reason = (int) (string) $this->block->reason;
        if ($reason <= 0 || !StockMvtReason::exists($reason)) {
            throw new Exception('The reason is not valid.');
        }

        if (!empty($this->block->price) && !empty($this->block->currency)
            && ((int) (string) $this->block->currency) != $warehouse->id_currency
        ) {
            // First convert price to the default currency
            $price_converted_to_default_currency = Tools::convertPrice(
                (float) (string) $this->block->price,
                (int) (string) $this->block->currency,
                false
            );

            // Convert the new price from default currency to needed currency
            $this->block->price = Tools::convertPrice($price_converted_to_default_currency, $warehouse->id_currency, true);
        }

        StockAvailable::setProductDependsOnStock(
            $this->getProductId(),
            1,
            Context::getContext()->shop->id,
            $this->getCombinationId()
        );
        StockAvailable::setProductDependsOnStock(
            $this->getProductId(),
            1,
            Context::getContext()->shop->id,
            0
        );

        // add stock
        $stock_manager = StockManagerFactory::getManager();

        $employeeId = empty($this->block->employee) ? 1 : (int) (string) $this->block->employee;
        $employee = new Employee($employeeId);

        Db::getInstance()->execute(
            'UPDATE `'._DB_PREFIX_.'product_shop`
            SET `advanced_stock_management`= 1
            WHERE id_product='.(int) $this->getProductId()
        );

        Db::getInstance()->execute(
            'UPDATE `'._DB_PREFIX_.'product`
            SET `advanced_stock_management`= 1
            WHERE id_product='.(int) $this->getProductId()
        );

        $quantity = (int) (string) $this->block->quantity;

        if (!isset($this->block['mode']) || $this->block['mode'] != 'delta') {
            $stock_collection = self::getStockCollection(
                $this->getProductId(),
                $this->getCombinationId(),
                $warehouse->id
            );

            $stock_collection->getAll();
            if (count($stock_collection) <= 0) {
                $current_quantity = 0;
            } else {
                $stock = $stock_collection->current();
                if ((float) (bool) $this->block->usable) {
                    $current_quantity = $stock->physical_quantity;
                } else {
                    $current_quantity = $stock->usable_quantity;
                }
            }

            $quantity = $quantity - $current_quantity;

            if ($quantity == 0) {
                Log::sys(
                    $this->id_advancedimporter_block,
                    $this->id_advancedimporter_flow,
                    "Quantity of product {$this->block->product}({$this->block->combination}) already set"
                );

                return;
            }
        }

        $current_employee = Context::getContext()->employee;
        Context::getContext()->employee = $employee;

        if ($quantity >= 0) {
            if ($stock_manager->addProduct(
                $this->getProductId(),
                $this->getCombinationId(),
                $warehouse,
                $quantity,
                (int) (string) $reason,
                isset($this->block->price) ? (float) (int) $this->block->price : null,
                (float) (bool) $this->block->usable,
                null,
                $employee
            )) {
                // Create warehouse_product_location entry if we add stock to a new warehouse
                $id_wpl = (int) WarehouseProductLocation::getIdByProductAndWarehouse(
                    $this->getProductId(),
                    $this->getCombinationId(),
                    $warehouse->id
                );
                if (!$id_wpl) {
                    $wpl = new WarehouseProductLocation();
                    $wpl->id_product = $this->getProductId();
                    $wpl->id_product_attribute = $this->getCombinationId();
                    $wpl->id_warehouse = (int) $warehouse->id;
                    $wpl->location = (string) $this->block->location;
                    $wpl->save();
                } elseif (isset($this->block->location)) {
                    $wpl = new WarehouseProductLocation($id_wpl);
                    $wpl->location = (string) $this->block->location;
                    $wpl->save();
                }

                StockAvailable::synchronize($this->getProductId());
            } else {
                throw new Exception('An error occurred. No stock was added.');
            }
        } else {
            if ($stock_manager->removeProduct(
                $this->getProductId(),
                $this->getCombinationId(),
                $warehouse,
                -$quantity,
                (int) (string) $reason,
                (float) (bool) $this->block->usable,
                null,
                null,
                $employee
            )) {
                // Create warehouse_product_location entry if we add stock to a new warehouse
                $id_wpl = (int) WarehouseProductLocation::getIdByProductAndWarehouse(
                    $this->getProductId(),
                    $this->getCombinationId(),
                    $warehouse->id
                );
                if (!$id_wpl) {
                    $wpl = new WarehouseProductLocation();
                    $wpl->id_product = $this->getProductId();
                    $wpl->id_product_attribute = $this->getCombinationId();
                    $wpl->id_warehouse = (int) $warehouse->id;
                    $wpl->location = (string) $this->block->location;
                    $wpl->save();
                } elseif (isset($this->block->location)) {
                    $wpl = new WarehouseProductLocation($id_wpl);
                    $wpl->location = (string) $this->block->location;
                    $wpl->save();
                }

                StockAvailable::synchronize($this->getProductId());
            } else {
                throw new Exception('An error occurred. No stock was added.');
            }

            Context::getContext()->employee = $current_employee;
        }
    }

    protected function getProductId()
    {
        if (!is_null($this->cacheProduct)) {
            return $this->cacheProduct;
        }

        if (empty($this->block->product)) {
            if (empty($this->block->combination)) {
                throw new Exception('Product or combination must be set for the stock');
            }
            $combination = $this->getCombinationId();
            $combinationObject = new Combination((int) $combination);
            $this->cacheProduct = (int) $combinationObject->id_product;
        } else {
            if (!empty($this->block->product['identifier'])) {
                $this->cacheProduct = (int) $this->getProductFromIdentifier(
                    (string) $this->block->product['identifier'],
                    (string) $this->block->product
                );
            } elseif (!empty($this->block->product['supplier-reference'])) {
                $supplierReference = SupplierReference::getBySupplierReference(
                    (string) $this->block->product,
                    (string) $this->block->product['supplier-reference']
                );
                $this->cacheProduct = $supplierReference->id_object;
            } else {
                $this->cacheProduct = (int) (string) $this->block->product;
            }
        }

        return $this->cacheProduct;
    }

    protected function getProductFromIdentifier($identifier, $value)
    {
        $query = 'SELECT `id_product`
            FROM `'._DB_PREFIX_.'product`
            WHERE `'.pSql($identifier).'` = "'.pSql($value).'"';

        $id =  (int) Db::getInstance()->getValue($query);

        if (!$id) {
            throw new Exception('No product with '.$identifier.' « '.$value.' »');
        }

        return $id;
    }

    protected function getCombinationId()
    {
        if (!is_null($this->cacheCombination)) {
            return $this->cacheCombination;
        }

        if (empty($this->block->combination)) {
            $this->cacheCombination = 0;
        } else {
            if (!empty($this->block->combination['identifier'])) {
                $this->cacheCombination = (int) $this->getCombinationFromIdentifier(
                    (string) $this->block->combination['identifier'],
                    (string) $this->block->combination
                );
            } elseif (!empty($this->block->combination['supplier-reference'])) {
                $supplierReference = SupplierReference::getBySupplierReference(
                    (string) $this->block->combination,
                    (string) $this->block->combination['supplier-reference']
                );
                $this->cacheCombination = $supplierReference->id_object;
            } else {
                $this->cacheCombination = (int) (string) $this->block->combination;
            }
        }

        return $this->cacheCombination;
    }

    protected function getCombinationFromIdentifier($identifier, $value)
    {
        $query = 'SELECT `id_product_attribute`
            FROM `'._DB_PREFIX_.'prodict_attribute`
            WHERE `'.pSql($identifier).'` = "'.pSql($value).'"';

        $id =  (int) Db::getInstance()->getValue($query);

        if (!$id) {
            throw new Exception('No combination with '.$identifier.' « '.$value.' »');
        }

        return $id;
    }

    protected function switchShop()
    {
        $this->original_shop = Context::getContext()->shop->id;
        if (!isset($this->block['shop'])) {
            $this->setShop(1);
            return;
        }

        $this->setShop($this->block['shop']);
    }

    /**
     * For a given product, retrieves the stock collection.
     *
     * @param int $id_product
     * @param int $id_product_attribute
     * @param int $id_warehouse         Optional
     * @param int $price_te             Optional
     *
     * @return PrestaShopCollection Collection of Stock
     */
    protected static function getStockCollection(
        $id_product,
        $id_product_attribute,
        $id_warehouse = null,
        $price_te = null
    ) {
        $stocks = new PrestaShopCollection('Stock');
        $stocks->where('id_product', '=', $id_product);
        $stocks->where('id_product_attribute', '=', $id_product_attribute);
        if ($id_warehouse) {
            $stocks->where('id_warehouse', '=', $id_warehouse);
        }
        if ($price_te) {
            $stocks->where('price_te', '=', $price_te);
        }
        return $stocks;
    }
}
