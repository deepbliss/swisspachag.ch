<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class AIEntity_Product_Customfeatures extends AIEntity_Abstract
{
    public function execute()
    {
        $data = (string) $this->block->data;
        if (empty($data)) {
            return;
        }

        $explode1 = explode(' - ', $data);
        if (!is_array($explode1)) {
            return;
        }

        foreach ($explode1 as $sub) {
            $explode2 = explode(':', $sub);

            if (!is_array($explode2) || count($explode2) != 2) {
                return;
            }

            $feature = trim($explode2[0]);
            $value = trim($explode2[1]);

            $this->executeFeature($feature, $value);
        }

        // Nothing to do
    }

    protected function executeFeature($featureName, $value)
    {
        echo $featureName;
        echo ':';
        echo $value;
        echo '<br/>';

        try {
            $supplierReference = SupplierReference::getBySupplierReference(
                $featureName,
                'Feature'
            );

            $feature = $supplierReference->getInstance();
        } catch (Exception $e) {
            $feature = new Feature();
            $feature->name = array(1 => $featureName);
            $feature->save();

            $supplierReference = new SupplierReference();
            $supplierReference->supplier_reference = $featureName;
            $supplierReference->object_type = 'Feature';
            $supplierReference->id_object = (int) $feature->id;
            $supplierReference->save();
        }

        try {
            $supplierReference = SupplierReference::getBySupplierReference(
                $featureName.'-'.$value,
                'FeatureValue'
            );

            $featureValue = $supplierReference->getInstance();
        } catch (Exception $e) {
            $featureValue = new FeatureValue();
            $featureValue->id_feature = $feature->id;
            $featureValue->custom = 0;
            $featureValue->value = array(1 => $value);
            $featureValue->save();

            $supplierReference = new SupplierReference();
            $supplierReference->supplier_reference = $featureName.'-'.$value;
            $supplierReference->object_type = 'FeatureValue';
            $supplierReference->id_object = (int) $featureValue->id;
            $supplierReference->save();
        }

        Db::getInstance()->execute(
            'INSERT IGNORE INTO `'._DB_PREFIX_.'feature_product`
            SET
                id_feature = '.(int) $featureValue->id_feature.',
                id_feature_value = '.(int) $featureValue->id.',
                id_product = '.(int) $this->getParent()->getObject()->id
        );
    }
}
