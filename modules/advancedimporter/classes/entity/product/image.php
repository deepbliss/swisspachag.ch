<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class AIEntity_Product_Image extends AIEntity_ObjectAbstract
{
    protected $tmpfile;

    protected function getProduct()
    {
        return $this->getParent()->getObject();
    }

    protected function fileExists($path)
    {
        if (strpos($path, 'http')) {
            $ch = curl_init($path);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_exec($ch);
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            if ($code == 200) {
                return true;
            } else {
                return false;
            }
        } else {
            return file_exists($path);
        }
    }

    protected function beforeSetFields()
    {
        // Copy image
        $this->tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');

        $modifier = '';
        if (isset($this->block->url['modifier'])) {
            $modifier = (string) $this->block->url['modifier'];
        }
        $url = $this->modify((string) $this->block->url, $modifier, $this);
        if (strpos($url, 'http') !== 0) {
            $url = _PS_ROOT_DIR_.$url;
        }

        if (isset($this->block['ignore-not-found']) && $this->isTrue($this->block['ignore-not-found'])) {
            if (!$this->fileExists($url)) {
                $this->processLocked = true;
                return;
            }
        }

        if (!copy((string) $url, $this->tmpfile)) {
            throw new Exception("Cannot save image $url");
        }

        $this->block->id_product = $this->getProduct()->id;

        if (!isset($this->getProduct()->product_cover)) {
            $product_cover_data = Image::getCover((int) $this->getProduct()->id);
            if ($product_cover_data === false) {
                $this->getProduct()->product_cover = false;
            } else {
                $this->getProduct()->product_cover = (int) $product_cover_data['id_image'];
            }
        }

        if (isset($this->block->cover) && $this->isTrue($this->block->cover)) {
            if ($this->getProduct()->product_cover && $this->block->cover) {
                if (!$this->getObject()->id
                    || $this->getObject()->id !== $this->getProduct()->product_cover
                ) {
                    Image::deleteCover($this->getProduct()->id);
                }
            }
            $this->block->cover = (bool)$this->block->cover;
        } elseif (!$this->getProduct()->product_cover
            || $this->getProduct()->product_cover === $this->getObject()->id
        ) {
            $this->block->cover = true;
        } else {
            $this->block->cover = false;
        }

        if (!isset($this->block->position)
            && empty($this->getObject()->position)
        ) {
            $this->block->position = Image::getHighestPosition($this->getProduct()->id) + 1;
        }
    }

    protected function afterSave()
    {
        if ($this->processLocked) {
            return;
        }


        $path = $this->getObject()->getPathForCreation();
        ImageManager::resize($this->tmpfile, $path.'.jpg');
        $images_types = ImageType::getImagesTypes('products');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

        foreach ($images_types as $image_type) {
            ImageManager::resize(
                $this->tmpfile,
                $path.'-'.Tools::stripslashes($image_type['name']).'.jpg',
                $image_type['width'],
                $image_type['height']
            );
            if (in_array($image_type['id_image_type'], $watermark_types)) {
                Hook::exec(
                    'actionWatermark',
                    array(
                        'id_image' => $this->getObject()->id,
                        'id_product' => $this->getProduct()->id,
                    )
                );
            }
        }
        unlink($this->tmpfile);

        if ($this->block->cover == true) {
            $this->getProduct()->product_cover = $this->getObject()->id;
        }
    }

    protected function beforeEnd()
    {
        if ($this->processLocked) {
            return;
        }

        // Set image ids to parent
        $this->getParent()->imageIds[] = $this->getObject()->id;
    }

    protected function getDefaultSupplierReference()
    {
        if (!empty($this->block->url)) {
            return md5((string) $this->block->url).'-'.$this->getProduct()->id;
        }
    }
}
