<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class class HelperCustom */
class HelperCustom
{
    public static function roundPriceWt($values, $entity, string $attribute)
    {
        $ht = $values[0];

        $ttc = $ht * 1.2;
        $ttc = ceil($ttc);

        return Tools::ps_round($ttc / 1.2, 6);
    }

    public static function isEnable($values, $entity, string $attribute)
    {
        $qty = (int) $values[0];
        $outOnStock = StockAvailable::outOfStock((int) $entity->getObject()->id);

        if ($outOnStock == 2) {
            return $entity->getObject()->$attribute;
        }

        return $qty > 0;
    }

    public function translate($values, $entity, string $attribute)
    {
        return str_replace(
            array(
                'Pocket watch',
                'Wristwatch',
            ),
            array(
                'Montre gousset',
                'Montre',
            ),
            $values[0]
        );
    }
}
