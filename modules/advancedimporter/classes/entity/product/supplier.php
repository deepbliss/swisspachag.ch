<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/entity/supplier.php';

class AIEntity_Product_Supplier extends AIEntity_Supplier
{
    protected function afterSave()
    {
        $supplierId = $this->getObject()->id;
        $productId = $this->getParent()->getObject()->id;

        // Try to get ProductSupplier id
        $productSupplierId = ProductSupplier::getIdByProductAndSupplier(
            $productId,
            null,
            $supplierId
        );

        $productSupplier = new ProductSupplier($productSupplierId);

        if (isset($this->block->price)) {
            $this->setField('product_supplier_price_te', $this->block->price, $productSupplier);
            if ($this->block->price['tax'] == 'include') {
                if ($productSupplier->product_supplier_price_te) {
                    $productSupplier->product_supplier_price_te = Tools::ps_round(
                        $productSupplier->product_supplier_price_te / (1 + ($this->getParent()->getTaxPercentage() / 100)),
                        6
                    );
                }
            }
        }

        if (isset($this->block->reference)) {
            $this->setField('product_supplier_reference', $this->block->reference, $productSupplier);
        }

        $productSupplier->id_product = (int) $productId;
        $productSupplier->id_supplier = (int) $supplierId;
        $productSupplier->id_product_attribute = 0;
        $productSupplier->save();
    }
}
