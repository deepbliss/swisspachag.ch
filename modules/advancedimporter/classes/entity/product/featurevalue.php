<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/entity/featurevalue.php';

class AIEntity_Product_FeatureValue extends AIEntity_FeatureValue
{
    protected function beforeEnd()
    {
        Db::getInstance()->execute(
            'INSERT IGNORE INTO `'._DB_PREFIX_.'feature_product`
            SET
                id_feature = '.(int) $this->getObject()->id_feature.',
                id_feature_value = '.(int) $this->getObject()->id.',
                id_product = '.(int) $this->getParent()->getObject()->id
        );
    }
}
