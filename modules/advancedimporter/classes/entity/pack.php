<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/entity/product.php';

class AIEntity_Pack extends AIEntity_Product
{
    public $products = array();

    protected function afterExecuteEntities()
    {
        if (isset($this->block['remove-missing-products'])
            && $this->isTrue($this->block['remove-missing-products'])
        ) {
            Pack::deleteItems($this->getObject()->id);
        }

        $items = Pack::getItemTable($this->getObject()->id, Context::getContext()->language->id);

        foreach ($this->products as $product) {
            $found = false;
            foreach ($items as $item) {
                if ($item['id_product'] == $this->getObject()->id
                    && $item['id_product_attribute_item'] == $product->getCombination()
                ) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                Pack::addItem($this->getObject()->id, $product->getObject()->id, $product->getQty());
            }
        }
    }
}
