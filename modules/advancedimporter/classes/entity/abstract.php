<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

abstract class AIEntity_Abstract implements AIEntity_Interface
{
    protected $block;
    protected $id_advancedimporter_flow;
    protected $id_advancedimporter_block;
    protected $parent;
    public $needParentSave = true;

    public function __construct($block, $id_block, $id_flow, $parent = null)
    {
        $this->block = $block;
        $this->id_advancedimporter_block = $id_block;
        $this->id_advancedimporter_flow = $id_flow;
        $this->parent = $parent;
    }

    public function getType()
    {
        return str_replace(
            '_',
            '/',
            str_replace('AIEntity_', '', get_class($this))
        );
    }

    public function modify($value, $modifier, $attribute)
    {
        if (empty($modifier)) {
            return $value;
        }

        $params = array(
            $value,
        );

        if (strpos($modifier, '::') !== false) {
            list($class, $function) = explode('::', $modifier);

            if (!class_exists($class) && strpos($class, 'Helper') === 0) {
                $filename = _PS_MODULE_DIR_.'advancedimporter/classes/'
                    .Tools::strtolower(str_replace('Helper', 'helper/', $class)).'.php';
                require $filename;
            }

            $this->extractModifierParams($function, $params);

            if (strpos($class, 'Helper') === 0) {
                return call_user_func(array($class, $function), $params, $this, $attribute);
            } else {
                return call_user_func_array(array($class, $function), $params);
            }
        } else {
            $this->extractModifierParams($modifier, $params);

            return call_user_func_array($modifier, $params);
        }
    }

    public function extractModifierParams(&$function, &$params)
    {
        preg_match('/^([A-Z_0-9]+)(?:\((.*)\))?$/Usi', $function, $matches);

        $function = $matches[1];
        if (isset($matches[2])) {
            foreach (str_getcsv($matches[2], ',', "'") as $value) {
                $params[] = $value;
            }
        }
    }

    public function getBlock()
    {
        return $this->block;
    }

    public function getParent()
    {
        return $this->parent;
    }

    protected function isTrue($value)
    {
        $value = (string) $value;
        $lowerValue = Tools::strtolower((string) $value);
        if ($value == '1'
            || $lowerValue == 'on'
            || $lowerValue == 'yes'
            || $lowerValue == 'y'
            || $lowerValue == 'true'
            || $lowerValue == 'enable'
            || $lowerValue == 'active'
        ) {
            return true;
        }

        return false;
    }

    protected function switchShop()
    {
        $this->original_shop = Context::getContext()->shop->id;
        if (!isset($this->block['shop'])) {
            $this->setShop(0);
            return;
        }

        $this->setShop($this->block['shop']);
    }

    protected function restoreShop()
    {
        $this->setShop($this->original_shop);
    }

    public function setShop($shop)
    {
        // Fix - shop 0 introduce multiple bugs
        // The shops must be defined on multistore
        if ($shop === 0) {
            $shop = 1;
        }

        Context::getContext()->shop->id = (int) $shop;
        if ((int) $shop === 0) {
            Shop::setContext(Shop::CONTEXT_ALL);
        } else {
            Shop::setContext(Shop::CONTEXT_SHOP, (int) $shop);
        }

        // To fix usage of POST from native Category Class of the PrestaShop
        // Using _POST is not a good idea, but there are no other solutions in this case
        $_POST['checkBoxShopAsso_category'] = array(
            (int) $shop => array()
        );
    }
}
