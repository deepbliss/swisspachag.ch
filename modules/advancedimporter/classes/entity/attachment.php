<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class AIEntity_Attachment extends AIEntity_ObjectAbstract
{
    protected function beforeValidate()
    {
        if ($this->getObject()->file) {
            $uniqid = $this->getObject()->file;
        } else {
            do {
                $uniqid = sha1(microtime());
            } while (file_exists(_PS_DOWNLOAD_DIR_.$uniqid));
        }

        $url = str_replace(' ', '%20', trim((string) $this->block->url));

        if (!is_writable(_PS_DOWNLOAD_DIR_)) {
            throw new Exception('Download dir is not writable');
        }

        // 'file_exists' doesn't work on distant file, and getimagesize makes the import slower.
        // Just hide the warning, the processing will be the same.
        if (!copy($url, _PS_DOWNLOAD_DIR_.$uniqid)) {
            throw new Exception("Attachment '$url' could not be uploaded");
        }

        $this->getObject()->file = $uniqid;

        if (!$this->getObject()->mime) {
            $this->getObject()->mime = mime_content_type(_PS_DOWNLOAD_DIR_.$uniqid);
        }
    }
}
