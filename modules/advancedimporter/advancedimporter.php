<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class class */
if (!defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_.'advancedimporter/classes/cron.php';

class AdvancedImporter extends Module
{
    protected $tabs = array();

    public function __construct()
    {
        $this->name = 'advancedimporter';
        $this->tab = 'front_office_features';
        $this->version = '2.4.2';
        $this->author = 'MADEF IT';
        $this->author_address = '0xD6601e65eb52c429E223580Dd3998cFE5D5a4f85';
        $this->need_instance = 0;
        $this->module_key = 'e991c55da91cc98de9e9e27445befc72';

        $this->tabs = array(
            array(
                'name' => $this->l('Advanced Importer'),
                'class_name' => 'AdminAdvancedImporter',
                'ParentClassName' => 'DEFAULT',
            ),
            array(
                'name' => $this->l('Flows'),
                'class_name' => 'AdminAdvancedImporterFlow',
                'ParentClassName' => 'AdminAdvancedImporter',
            ),
            array(
                'name' => $this->l('Blocks'),
                'class_name' => 'AdminAdvancedImporterBlock',
                'ParentClassName' => 'AdminAdvancedImporter',
            ),
            array(
                'name' => $this->l('Supplier References'),
                'class_name' => 'AdminAdvancedImporterSupplierReferences',
                'ParentClassName' => 'AdminAdvancedImporter',
            ),
            array(
                'name' => $this->l('Entity Tracking'),
                'class_name' => 'AdminAdvancedImporterHistory',
                'ParentClassName' => 'AdminAdvancedImporter',
            ),
            array(
                'name' => $this->l('Logs'),
                'class_name' => 'AdminAdvancedImporterLog',
                'ParentClassName' => 'AdminAdvancedImporter',
            ),
            array(
                'name' => $this->l('Recurring tasks'),
                'class_name' => 'AdminAdvancedImporterCron',
                'ParentClassName' => 'AdminAdvancedImporter',
            ),
            array(
                'name' => $this->l('Templates'),
                'class_name' => 'AdminAdvancedImporterTemplate',
                'ParentClassName' => 'AdminAdvancedImporter',
            ),
            array(
                'name' => $this->l('Configuration'),
                'class_name' => 'AdminAdvancedImporterConfiguration',
                'ParentClassName' => 'AdminAdvancedImporter',
            ),
            array(
                'name' => $this->l('Upload a flow'),
                'class_name' => 'AdminAdvancedImporterUpload',
                'ParentClassName' => 'AdminAdvancedImporter',
                'visible' => false,
            ),
            array(
                'name' => $this->l('Create supplier references'),
                'class_name' => 'AdminAdvancedImporterCreateSupplierReference',
                'ParentClassName' => 'AdminAdvancedImporter',
                'visible' => false,
            ),
            array(
                'name' => $this->l('Template Assistant'),
                'class_name' => 'AdminAdvancedImporterTemplateAssistant',
                'ParentClassName' => 'AdminAdvancedImporter',
                'visible' => false,
            ),
        );

        parent::__construct();

        $this->displayName = $this->l('PrestaShop XML Importer');
        $this->description = $this->l('Import every thing you need.');
    }

    public function install()
    {
        Db::getInstance()->execute(
            'CREATE TABLE `'._DB_PREFIX_.'advancedimporter_block` (
              `id_advancedimporter_block` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `id_shop` int(10) unsigned NOT NULL,
              `created_at` datetime NOT NULL,
              `planned_at` datetime DEFAULT NULL,
              `treatment_start` datetime DEFAULT NULL,
              `treatment_end` datetime DEFAULT NULL,
              `channel` int(10) unsigned NOT NULL,
              `callback` varchar(255) DEFAULT NULL,
              `block` text,
              `result` int(11) DEFAULT "1",
              `memory` int(11) DEFAULT "0",
              `error` text,
              `id_advancedimporter_flow` int(11) DEFAULT NULL,
              `id_parent` int(10) unsigned NOT NULL,
              `root` varchar(15) DEFAULT NULL,
              PRIMARY KEY (`id_advancedimporter_block`),
              KEY `created_at` (`created_at`),
              KEY `treatment_start` (`treatment_start`),
              KEY `channel` (`channel`),
              KEY `planned_at` (`planned_at`),
              KEY `treatment_end` (`treatment_end`),
              KEY `id_shop` (`id_shop`),
              KEY `id_advancedimporter_flow` (`id_advancedimporter_flow`),
              KEY `id_parent` (`id_parent`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE `'._DB_PREFIX_.'advancedimporter_cron` (
              `id_advancedimporter_cron` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `description` varchar(50) NOT NULL,
              `id_shop` int(10) unsigned NOT NULL,
              `channel` int(10) unsigned NOT NULL,
              `callback` varchar(255) NOT NULL,
              `block` text,
              `crontime` varchar(20) NOT NULL,
              PRIMARY KEY (`id_advancedimporter_cron`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE `'._DB_PREFIX_.'advancedimporter_csv_template` (
              `id_advancedimporter_csv_template` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `template` text,
              `data` text,
              `filepath` varchar(255) DEFAULT NULL,
              `roottag` varchar(20) DEFAULT NULL,
              `delimiter` varchar(1) DEFAULT NULL,
              `enclosure` varchar(1) DEFAULT NULL,
              `escape` varchar(1) DEFAULT NULL,
              `encoding` varchar(11) DEFAULT NULL,
              `flow_type` varchar(32) DEFAULT NULL,
              `ignore_first_line` int(11) DEFAULT NULL,
              `advanced_mode` int(11) DEFAULT "0",
              `date_add` datetime NOT NULL,
              `date_upd` datetime NOT NULL,
              `nodes_serialized` text,
              `schema_serialized` text,
              `autodelete` varchar(20) DEFAULT NULL,
              PRIMARY KEY (`id_advancedimporter_csv_template`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE `'._DB_PREFIX_.'advancedimporter_flow` (
              `id_advancedimporter_flow` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `date_add` datetime NOT NULL,
              `date_upd` datetime NOT NULL,
              `filename` varchar(255) DEFAULT NULL,
              `path` varchar(255) DEFAULT NULL,
              `status` int(10) unsigned NOT NULL,
              `block_count` int(10) unsigned NOT NULL DEFAULT "0",
              `success_count` int(10) unsigned NOT NULL DEFAULT "0",
              `error_count` int(10) unsigned NOT NULL DEFAULT "0",
              `started_at` datetime DEFAULT NULL,
              `ended_at` datetime DEFAULT NULL,
              `type` varchar(4) NOT NULL,
              PRIMARY KEY (`id_advancedimporter_flow`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE `'._DB_PREFIX_.'advancedimporter_history` (
              `id_advancedimporter_history` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `flow_type` varchar(255) DEFAULT NULL,
              `id_advancedimporter_block` int(10) unsigned NOT NULL,
              `id_advancedimporter_flow` int(10) unsigned NOT NULL,
              `object_type` varchar(255) DEFAULT NULL,
              `object_id` int(10) unsigned NOT NULL,
              `object_supplier_reference` varchar(255) DEFAULT NULL,
              `date_add` datetime NOT NULL,
              PRIMARY KEY (`id_advancedimporter_history`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE `'._DB_PREFIX_.'advancedimporter_log` (
              `id_advancedimporter_log` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `id_advancedimporter_block` int(10) unsigned NOT NULL,
              `type` varchar(20) DEFAULT NULL,
              `message` text,
              `id_shop` int(10) unsigned NOT NULL,
              `date_add` datetime DEFAULT NULL,
              `id_advancedimporter_flow` int(10) unsigned NOT NULL,
              PRIMARY KEY (`id_advancedimporter_log`),
              KEY `type` (`type`),
              KEY `id_shop` (`id_shop`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE `'._DB_PREFIX_.'advancedimporter_priority` (
              `root` varchar(15) NOT NULL,
              `priority` int(11) DEFAULT NULL,
              PRIMARY KEY (`root`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE `'._DB_PREFIX_.'advancedimporter_supplierreference` (
              `id_advancedimporter_supplierreference` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `id_object` varchar(255) NOT NULL,
              `supplier_reference` varchar(255) DEFAULT NULL,
              `object_type` varchar(255) DEFAULT NULL,
              `date_add` datetime NOT NULL,
              `date_upd` datetime NOT NULL,
              PRIMARY KEY (`id_advancedimporter_supplierreference`),
              KEY `supplier_reference` (`supplier_reference`),
              KEY `object_type` (`object_type`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE `'._DB_PREFIX_.'advancedimporter_supplierreference_source` (
              `id_advancedimporter_supplierreference_source` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `supplier_reference` varchar(255) DEFAULT NULL,
              `supplier` varchar(255) DEFAULT NULL,
              `object_type` varchar(255) DEFAULT NULL,
              `to_delete` tinyint(4) DEFAULT "0",
              `date_add` datetime NOT NULL,
              `date_upd` datetime NOT NULL,
              PRIMARY KEY (`id_advancedimporter_supplierreference_source`),
              UNIQUE KEY `UC_SUPPLIERREFERENCE_SOURCE` (`supplier_reference`,`supplier`,`object_type`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE `'._DB_PREFIX_.'advancedimporter_template` (
              `id_advancedimporter_template` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `name` varchar(50) DEFAULT NULL,
              `extractor` varchar(50) DEFAULT NULL,
              `xslt` text,
              `delimiter` varchar(1) DEFAULT NULL,
              `enclosure` varchar(1) DEFAULT NULL,
              `escape` varchar(1) DEFAULT NULL,
              `encoding` varchar(10) DEFAULT NULL,
              `ignore_first_line` int(11) DEFAULT NULL,
              `item_root` varchar(25) DEFAULT NULL,
              `nodes_serialized` mediumtext,
              `schema_serialized` mediumtext,
              `date_add` datetime NOT NULL,
              `date_upd` datetime NOT NULL,
              PRIMARY KEY (`id_advancedimporter_template`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE `'._DB_PREFIX_.'advancedimporter_xslt` (
              `id_advancedimporter_xslt` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `xml` text,
              `xpath_query` varchar(255) DEFAULT NULL,
              `date_add` datetime NOT NULL,
              `date_upd` datetime NOT NULL,
              `use_tpl` tinyint(4) DEFAULT "0",
              `nodes_serialized` text,
              `schema_serialized` text,
              `item_root` varchar(25) DEFAULT NULL,
              PRIMARY KEY (`id_advancedimporter_xslt`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'
        );
        Configuration::updateGlobalValue('AI_USE_API', 0);
        Configuration::updateGlobalValue('AI_NB_CHANNEL', 1);
        Configuration::updateGlobalValue('AI_LOG_ENABLE', 1);
        Configuration::updateGlobalValue('AI_SYSLOG_ENABLE', 1);
        Configuration::updateGlobalValue('AI_NOTICELOG_ENABLE', 1);
        Configuration::updateGlobalValue('AI_ERRORLOG_ENABLE', 1);
        Configuration::updateGlobalValue('AI_SUCCESSLOG_ENABLE', 1);
        Configuration::updateGlobalValue('AI_AUTO_RELEASE_AFTER', 300);
        Configuration::updateGlobalValue('AI_ADD_CRON_TASK_SCALE', 5);
        Configuration::updateGlobalValue('AI_BLOCK_STACK_SIZE', 1);
        Configuration::updateGlobalValue('AI_ADD_CRON_TASK_EACH', 2);
        Configuration::updateGlobalValue('AI_ADVANCED_MODE', 0);
        Configuration::updateGlobalValue('AI_HISTORY_ENABLE', 1);
        Configuration::updateGlobalValue('AI_FORCE_EXECUTION_TIME', 1);
        Configuration::updateGlobalValue('AI_CRON_LIFETIME', 110);
        Configuration::updateGlobalValue('AI_TOKEN', Tools::substr(Tools::encrypt(uniqid()), 0, 20));
        Configuration::updateGlobalValue('AI_PRELOAD_SR_CACHE', 1);

        $res = parent::install();

        $cron = new Cron();
        $cron->description = $this->l('Clean old blocks');
        $cron->callback = 'Cleaner::exec';
        $cron->block = '{"ttl":3600}';
        $cron->crontime = '*/5 * * * *';
        $cron->channel = 1;
        $cron->save();

        $cron = new Cron();
        $cron->description = $this->l('Clean old flows');
        $cron->callback = 'Cleaner::flow';
        $cron->block = '{"ttl":604800}';
        $cron->crontime = '0 * * * *';
        $cron->channel = 1;
        $cron->save();

        $this->createAdminTabs();

        $res &= $this->registerHook('footer');
        $res &= $this->registerHook('displayAdminAdvancedImporterUploadView');
        $res &= $this->registerHook('actionAdminControllerSetMedia');
        $res &= $this->registerHook('moduleRoutes');

        return (bool) $res;
    }

    public function hookActionAdminControllerSetMedia()
    {
        $this->context->controller->addJS($this->_path.'views/js/admin.js');
        Media::addJsDef(array('baseDir' => __PS_BASE_URI__));
        Media::addJsDef(array('AI_TOKEN' => Configuration::getGlobalValue('AI_TOKEN')));
        Media::addJsDef(array('CONFIRM_DELETE' => $this->l('Are you sure?')));

        $this->context->controller->addCSS($this->_path.'views/css/admin.css', 'all');
    }

    public function getTab($controller, $module = 'advancedimporter')
    {
        if (class_exists('PrestaShopCollection')) {
            $collection = new PrestaShopCollection('Tab');
        } else {
            $collection = new Collection('Tab');
        }

        $collection->where('class_name', '=', $controller);
        if ($module) {
            $collection->where('module', '=', 'advancedimporter');
        }

        $tab = $collection->getFirst();
        if (!$tab) {
            $tab = new Tab();
        }
        $tab->class_name = $controller;
        $tab->module = $module;

        return $tab;
    }

    public function createAdminTabs()
    {
        if (version_compare(_PS_VERSION_, '1.7', '>')) {
            return;
        }

        $langs = Language::getLanguages();
        $id_lang = (int) Configuration::getGlobalValue('PS_LANG_DEFAULT');

        // Create tab publications
        $tab00 = $this->getTab('AdminAdvancedImporter', null);
        if (!$tab00->id) {
            $tab00 = $this->getTab('AdvancedImporter', null);
            $tab00->class_name = 'AdminAdvancedImporter';
        }

        $tab00->id_parent = 0;

        foreach ($langs as $l) {
            $tab00->name[$l['id_lang']] = $this->l('PrestaShop XML Importer');
        }

        $tab00->save();
        $tab_id = $tab00->id;

        // Create tab Flows
        $tab20 = $this->getTab('AdminAdvancedImporterFlow');
        $tab20->id_parent = $tab_id;
        $tab20->position = 20;
        foreach ($langs as $l) {
            $tab20->name[$l['id_lang']] = $this->l('Flows');
        }

        $tab20->save();

        // Create tab Block
        $tab30 = $this->getTab('AdminAdvancedImporterBlock');
        $tab30->id_parent = $tab_id;
        $tab30->position = 30;
        foreach ($langs as $l) {
            $tab30->name[$l['id_lang']] = $this->l('Blocks');
        }

        $tab30->save();

        // Create tab Logs
        $tab35 = $this->getTab('AdminAdvancedImporterSupplierReferences');
        $tab35->id_parent = $tab_id;
        $tab35->position = 35;
        foreach ($langs as $l) {
            $tab35->name[$l['id_lang']] = $this->l('Supplier References');
        }

        $tab35->save();

        // Create tab Logs
        $tab36 = $this->getTab('AdminAdvancedImporterHistory');
        $tab36->id_parent = $tab_id;
        $tab36->position = 36;
        foreach ($langs as $l) {
            $tab36->name[$l['id_lang']] = $this->l('Entity tracking');
        }

        $tab36->save();

        // Create tab Logs
        $tab40 = $this->getTab('AdminAdvancedImporterLog');
        $tab40->id_parent = $tab_id;
        $tab40->position = 40;
        foreach ($langs as $l) {
            $tab40->name[$l['id_lang']] = $this->l('Logs');
        }

        $tab40->save();

        // Create tab Cron
        $tab50 = $this->getTab('AdminAdvancedImporterCron');
        $tab50->id_parent = $tab_id;
        $tab50->position = 50;
        foreach ($langs as $l) {
            $tab50->name[$l['id_lang']] = $this->l('Recurring tasks');
        }

        $tab50->save();

        // Create tab Cron
        $tab70 = $this->getTab('AdminAdvancedImporterTemplate');
        $tab70->id_parent = $tab_id;
        $tab70->position = 70;
        foreach ($langs as $l) {
            $tab70->name[$l['id_lang']] = $this->l('Template');
        }

        $tab70->save();

        // Create tab Configuration
        $tab80 = $this->getTab('AdminAdvancedImporterConfiguration');
        $tab80->id_parent = $tab_id;
        $tab80->position = 80;
        foreach ($langs as $l) {
            $tab80->name[$l['id_lang']] = $this->l('Configuration');
        }

        $tab80->save();


        // Create tab Upload
        $tab90 = $this->getTab('AdminAdvancedImporterUpload');
        $tab90->id_parent = $tab_id;
        $tab90->active = false;
        $tab90->position = 90;
        foreach ($langs as $l) {
            $tab90->name[$l['id_lang']] = $this->l('Upload a flow');
        }

        $tab90->save();

        // Create tab Upload
        $tab91 = $this->getTab('AdminAdvancedImporterCreateSupplierReference');
        $tab91->id_parent = $tab_id;
        $tab91->active = false;
        $tab91->position = 91;
        foreach ($langs as $l) {
            $tab91->name[$l['id_lang']] = $this->l('Create supplier references');
        }

        $tab91->save();

        // Create tab Template assistant
        $tab100 = $this->getTab('AdminAdvancedImporterTemplateAssistant');
        $tab100->id_parent = $tab_id;
        $tab100->active = false;
        $tab100->position = 100;
        foreach ($langs as $l) {
            $tab100->name[$l['id_lang']] = $this->l('Template Assistant');
        }

        $tab100->save();

        $profiles = Profile::getProfiles($id_lang);

        if (count($profiles)) {
            foreach ($profiles as $p) {
                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab00->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab20->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab30->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab35->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab36->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab40->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab50->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab70->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab80->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab90->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab91->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO `'._DB_PREFIX_.'access`(`id_profile`,`id_tab`,`view`,`add`,`edit`,`delete`)
                     VALUES ('.(int) $p['id_profile'].', '.(int) $tab100->id.',1,1,1,1)'
                );

                Db::getInstance()->Execute(
                    'INSERT IGNORE INTO '._DB_PREFIX_.'module_access(`id_profile`, `id_module`, `configure`, `view`)
                     VALUES ('.(int) $p['id_profile'].','.(int) $this->id.',1,1)'
                );
            }
        }
    }

    public function uninstall()
    {
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_block`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_log`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_cron`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_flow`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_xslt`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_history`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_csv_template`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_template`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_externalreference`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_supplierreference`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_externalreference_supplier`');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'advancedimporter_supplierreference_source`');

        Db::getInstance()->Execute(
            'DELETE FROM '._DB_PREFIX_.'tab
            WHERE module = "advancedimporter"
            OR class_name = "AdvancedImporter"'
        );

        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            Db::getInstance()->Execute('DELETE FROM '._DB_PREFIX_.'module_access WHERE `id_module` = '.(int) $this->id);
        }

        return parent::uninstall();
    }

    public function getContent()
    {
        return Tools::redirectAdmin($this->context->link->getAdminLink('AdminAdvancedImporterConfiguration'));
    }


    public function hookDisplayAdminAdvancedImporterUploadView()
    {
        Context::getContext()->smarty->assign('moduleDir', _PS_MODULE_DIR_);
        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            return $this->display(__FILE__, 'views/templates/admin/upload.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/admin/upload-1.5.tpl');
        }
    }

    public function hookModuleRoutes()
    {
        return array(
            'module-advancedimporter-export' => array(
                'controller' => 'export',
                'rule' =>  'advancedimporter/export/{token}/{file}',
                'keywords' => array(
                    'module'  => array('regexp' => '[\w]+', 'param' => 'module'),
                    'controller' => array('regexp' => '[\w]+', 'param' => 'controller'),
                    'token'  => array('regexp' => '[\w\d]+', 'param' => 'token'),
                    'file'  => array('regexp' => '[\w\.]+', 'param' => 'file'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'advancedimporter',
                    'controller' => 'export',
                ),
            ),
        );
    }

    /*
     * Hack to translate class files
     */
    // $this->l('Add entry');
    // $this->l('External reference:');
    // $this->l('Attribute "%s"');
    // $this->l('Condition:');
    // $this->l('Value:');
    // $this->l('Attribute "%s":');
    // $this->l('Quantity:');
    // $this->l('Mode (set, delta):');
    // $this->l('Stock');
    // $this->l('Image url');
    // $this->l('Image url:');
    // $this->l('Features');
    // $this->l('Path of categories');
    // $this->l('Tax rule name');
    // $this->l('Tax rule name:');
    // $this->l('Price tax calculation');
    // $this->l('Prices include tax (ti ou te):');
}
