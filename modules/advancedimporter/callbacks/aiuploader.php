<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/template.php';

/** Class AIUploader */
class AIUploader
{
    public static $id_advancedimporter_block;
    public static $id_advancedimporter_flow;
    public static $id_shop;

    public static function ftpUploader($block)
    {
        if (empty($block->port)) {
            $block->port = 21;
        }
        if (empty($block->sourcedir)) {
            $block->sourcedir = '.';
        }
        if (empty($block->destinationdir)) {
            $block->destinationdir = _PS_MODULE_DIR_.'advancedimporter/flows/import/queue/';
        }
        if (!preg_match('/.*\/$/', $block->destinationdir)) {
            $block->destinationdir .= '/';
        }

        $conn_id = ftp_connect($block->host, $block->port);
        $login_result = ftp_login($conn_id, $block->user, $block->password);

        if (!$login_result) {
            throw new Exception("Cannot connect to '$block->host' with user '$block->user'");
        }

        // List files
        if (!($files = ftp_nlist($conn_id, $block->sourcedir))) {
            throw new Exception("Cannot read directory '$block->sourcedir' from ftp server '$block->host'");
        }

        // Download files
        foreach ($files as $file) {
            $pathinfo = pathinfo($file);
            $filename = $pathinfo['filename'].'.xml';

            $dirname = dirname($file);
            if ($dirname !== '.') {
                ftp_chdir($conn_id, $dirname);
                $file = basename($file);
            }

            if (!ftp_get(
                $conn_id,
                _PS_MODULE_DIR_.'advancedimporter/flows/import/temporary/'.$filename,
                $file,
                FTP_ASCII
            )) {
                throw new Exception("Cannot donwload the file '$file' from ftp server '$block->host'");
            }
            $block->temppath = _PS_MODULE_DIR_.'advancedimporter/flows/import/temporary/'.$filename;
            $block->destinationpath = $block->destinationdir.$filename;

            self::moveAndTranslateFile($block);
        }
    }

    public static function httpUploader($block)
    {
        try {
            ini_set('default_socket_timeout', 500);
        } catch (Exception $e) {
            Log::notice(
                self::$id_advancedimporter_block,
                self::$id_advancedimporter_flow,
                'Cannot define default_socket_timeout'
            );
        }
        if (!isset($block->sourcepath)) {
            throw new Exception('Unknow source path');
        }
        if (empty($block->destinationpath)) {
            $pathinfo = pathinfo($block->sourcepath);
            $block->destinationpath = $pathinfo['basename'];
            if (!preg_match('/\.xml$/', $block->destinationpath)) {
                $block->destinationpath = $block->destinationpath.'.xml';
            }
        }
        $date = new DateTime();
        $str_date = $date->format('Ymd-His');
        $block->temppath = _PS_MODULE_DIR_.'advancedimporter/flows/import/temporary/'
            .str_replace('%date%', $str_date, $block->destinationpath);
        $block->destinationpath = _PS_MODULE_DIR_.'advancedimporter/flows/import/queue/'
            .str_replace('%date%', $str_date, $block->destinationpath);

        /*if (!isset($block->tryeach))
            $block->tryeach = 60;
        if (file_exists(_PS_MODULE_DIR_.'advancedimporter/flows/import/queue/UPLOADED'))
            unlink(_PS_MODULE_DIR_.'advancedimporter/flows/import/queue/UPLOADED');
        elseif (file_exists(_PS_MODULE_DIR_.'advancedimporter/flows/import/queue/PROCESSING')) {
            Log::warn("Cannot upload XML {$block->sourcepath} because some flows are under process");
            $date = new DateTime();
            $date->add(new DateInterval('PT'.(int) $block->tryeach.'s'));
            $new_block = new Block();
            $new_block->planned_at = $date->format('Y-m-d H:i:s');
            $new_block->callback = 'AIUploader::::httpUploader';
            $new_block->block = Tools::jsonEncode($block);
            $new_block->id_shop = self::$id_shop;
            $new_block->channel = 1;
            $new_block->id_advancedimporter_flow = 0;
            $created_at = new DateTime();
            $new_block->created_at = $created_at->format('Y-m-d H:i:s');
            $new_block->save();

            return;
        }*/

        if (!self::fileExists((string) $block->sourcepath)) {
            Log::sys(
                self::$id_advancedimporter_block,
                self::$id_advancedimporter_flow,
                "Nothing to upload : {$block->sourcepath} do not exists"
            );

            return false;
        }
        copy($block->sourcepath, $block->temppath);
        self::moveAndTranslateFile($block);
    }


    protected static function moveAndTranslateFile($block)
    {
        $template = new AITemplate((int)$block->template);
        try {
            $xml = $template->loadFile($block->temppath);
        } catch (Exception $e) {
            throw new Exception(
                'The file cannot be uploaded, it seems to be an invalid XML. If it\'s a CSV you need to create a template.'
                .' Sytem error: '.$e->getMessage()
            );
        }

        if ($xml->getName() != 'advancedimporter') {
            throw new Exception(
                'The flow is an XML valid, but is not an XML supported by the module. Try to create a template first'
            );
        }

        $dom = dom_import_simplexml($xml)->ownerDocument;
        $dom->formatOutput = true;
        $res = file_put_contents(
            $block->destinationpath,
            $dom->saveXML()
        );

        if (!$res) {
            throw new Exception(
                sprintf(
                    'Not possible to upload file. Please check the right on the directory "%s"',
                    _PS_MODULE_DIR_.'advancedimporter/flows/import/queue/'
                )
            );
        }
        unlink($block->temppath);
    }

    public static function fileExists($file_path)
    {
        try {
            $file_headers = get_headers($file_path);
        } catch (Exception $e) {
            return true;
        }

        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            return false;
        } else {
            return true;
        }
    }

    public static function multipleHttpUploader($block)
    {
        $count = (int) $block->start;
        $limit = isset($block->end) ? (int) $block->end : 200;
        $sourcepath = (string) $block->sourcepath;
        $destinationpath = (string) $block->destinationpath;
        do {
            $block->sourcepath = str_replace('%count%', $count, $sourcepath);
            $block->destinationpath = str_replace('%count%', $count, $destinationpath);
            $count++;
            try {
                if (!self::httpUploader($block)) {
                    return;
                }
            } catch (Exception $e) {
                return;
            }
        } while ($count < $limit);
    }
}
