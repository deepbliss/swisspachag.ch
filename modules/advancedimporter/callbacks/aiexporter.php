<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/template.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/export.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/block.php';

/** Class AIUploader */
class AIExporter
{
    public static $id_advancedimporter_block;
    public static $id_advancedimporter_flow;
    public static $id_shop;

    public static function run($block)
    {
        $xml = new SimpleXmlElement('<advancedimporter />');

        if (!isset($block->page)) {
            $block->page = 1;
        }

        $exportClass = new AIExport(
            $xml,
            lcfirst($block->type),
            Tools::ucfirst($block->type)
        );
        $exportClass->setPage((int) $block->page);
        $exportClass->setPageSize((int) $block->pagesize);

        if (!empty($block->shop)) {
            $exportClass->setShop((int) $block->shop);
        }

        if ((int) $block->supplierreferenceonly) {
            $exportClass->supplierReferenceOnly();
        }

        $exportClass->getXml();

        if ($block->page == 1) {
            if (!file_exists(_PS_MODULE_DIR_.'advancedimporter/flows/export')) {
                mkdir(_PS_MODULE_DIR_.'advancedimporter/flows/export');
            }
            if (!file_exists(_PS_MODULE_DIR_.'advancedimporter/flows/export/processing')) {
                mkdir(_PS_MODULE_DIR_.'advancedimporter/flows/export/processing');
            }
            if (file_exists(_PS_MODULE_DIR_.'advancedimporter/flows/export/processing/'.$block->filetoken.'.'.$block->file)) {
                unlink(_PS_MODULE_DIR_.'advancedimporter/flows/export/processing/'.$block->filetoken.'.'.$block->file);
            }
            file_put_contents(
                _PS_MODULE_DIR_.'advancedimporter/flows/export/processing/'.$block->filetoken.'.'.$block->file,
                $xml->asXml()
            );
        } else {
            $content = Tools::file_get_contents(
                _PS_MODULE_DIR_.'advancedimporter/flows/export/processing/'.$block->filetoken.'.'.$block->file
            );
            $newXml = new SimpleXmlElement($content);
            $domNewXml = dom_import_simplexml($newXml);

            foreach ($xml as $node) {
                $domNode = dom_import_simplexml($node);
                $domNode = $domNewXml->ownerDocument->importNode($domNode, true);
                $domNewXml->appendChild($domNode);
            }

            file_put_contents(
                _PS_MODULE_DIR_.'advancedimporter/flows/export/processing/'.$block->filetoken.'.'.$block->file,
                $newXml->asXml()
            );
        }

        if ($xml->count() > 0) {
            $task = new Block();
            $block->page++;
            $task->block = Tools::jsonEncode($block);
            $task->channel = 1;
            $task->callback = 'AIExporter::run';
            $task->created_at = (new DateTime())->format('Y-m-d H:i:s');
            $task->id_advancedimporter_flow = self::$id_advancedimporter_flow;
            $task->save();
        } else {
            if (!file_exists(_PS_MODULE_DIR_.'advancedimporter/flows/export/export')) {
                mkdir(_PS_MODULE_DIR_.'advancedimporter/flows/export/export');
            }

            if (!empty($block->xslt)) {
                $template = new AITemplate();
                $template->xslt = $block->xslt;
                $content = Tools::file_get_contents(
                    _PS_MODULE_DIR_.'advancedimporter/flows/export/processing/'.$block->filetoken.'.'.$block->file
                );
                $xml = new SimpleXmlElement($content);
                file_put_contents(
                    _PS_MODULE_DIR_.'advancedimporter/flows/export/processing/'.$block->filetoken.'.'.$block->file,
                    $template->convert($xml, 'No file')->asXml()
                );
            }

            rename(
                _PS_MODULE_DIR_.'advancedimporter/flows/export/processing/'.$block->filetoken.'.'.$block->file,
                _PS_MODULE_DIR_.'advancedimporter/flows/export/export/'.$block->filetoken.'.'.$block->file
            );

            $params = array(
                'file' => $block->file,
                'token' => $block->filetoken,
            );

            $link = Context::getContext()->link;
            $url = $link->getModuleLink(
                'advancedimporter',
                'export',
                $params
            );

            Log::success(
                self::$id_advancedimporter_block,
                self::$id_advancedimporter_flow,
                'You can download the export file : '.$url
            );
        }
    }
}
