<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class class */
require_once dirname(__FILE__).'/../../config/config.inc.php';
require_once dirname(__FILE__).'/classes/block.php';
require_once dirname(__FILE__).'/classes/log.php';
require_once dirname(__FILE__).'/classes/cron.php';
require_once dirname(__FILE__).'/classes/importercollection.php';
require_once dirname(__FILE__).'/classes/objectmodel/SupplierReference.php';

$cli_mode = php_sapi_name() == 'cli';

if (Tools::getIsset('debug') || $cli_mode) {
    define('_AI_DEBUG_', true);
} else {
    define('_AI_DEBUG_', false);
}

if ($cli_mode) {
    define('_AI_LB_', "\n");
} else {
    define('_AI_LB_', '<br />');
}

if (!$cli_mode && Tools::getValue('token') != Configuration::getGlobalValue('AI_TOKEN')) {
    die("Bad token");
}

$blockStackSize = Configuration::getGlobalValue('AI_BLOCK_STACK_SIZE');

$identifier = str_pad(rand(1, 1000), 4, '0', STR_PAD_LEFT);

set_error_handler('noticeHandler', E_NOTICE);
set_error_handler('warningHandler', E_WARNING);

$id_shop = Context::getContext()->shop->id;

if (!isset(Context::getContext()->cart)) {
    Context::getContext()->cart = new Cart();
}

if ((int) Configuration::getGlobalValue('AI_FORCE_EXECUTION_TIME')) {
    $cron_lifetime = (int) Configuration::getGlobalValue('AI_CRON_LIFETIME');
    @ini_set('max_execution_time', $cron_lifetime * 1.5);
}

/* Limit max execution time to 5 seconds + 1 operation */
$max_execution_time = (int) @ini_get('max_execution_time');
$cron_lifetime = (int) Configuration::getGlobalValue('AI_CRON_LIFETIME');
if ($max_execution_time > $cron_lifetime || !$max_execution_time) {
    $max_execution_time = $cron_lifetime;
}

if (_AI_DEBUG_) {
    echo 'Real max execution time = '.(int) @ini_get('max_execution_time')._AI_LB_;
    echo '$max_execution_time = '.var_export($max_execution_time, true)._AI_LB_;
    echo '$cron_lifetime = '.var_export($cron_lifetime, true)._AI_LB_;
}

$max_memory = (int) Tools::getMemoryLimit();
if (!$max_memory || $max_memory < 0) {
    $max_memory = 32 * 1024 * 1024;
}

if (_AI_DEBUG_) {
    echo '$max_memory = '.var_export($max_memory, true)._AI_LB_;
    echo 'memory_get_peak_usage() = '.var_export(memory_get_peak_usage(), true)._AI_LB_;
}

$start_time = microtime(true);
$channel = rand(1, Configuration::getGlobalValue('AI_NB_CHANNEL'));

if ($cli_mode) {
    if (isset($argv[1])) {
        $max_execution_time = (int) $argv[1];
    }

    if (isset($argv[2])) {
        $max_memory = (int) $argv[2];
    }

    if (isset($argv[3])) {
        $channel = (int) $argv[3];
    }
}

$time = time();

$lock_file = 'lock/import.lock';
/* Auto release channel lock */
if (Configuration::getGlobalValue('AI_AUTO_RELEASE_AFTER') && file_exists($lock_file)) {
    if ($time - filemtime($lock_file) > Configuration::getGlobalValue('AI_AUTO_RELEASE_AFTER')) {
        if (file_exists($lock_file)) {
            unlink($lock_file);
        }
    }
}

$cannotReleaseLock = false;
if (file_exists($lock_file)) {
    $cannotReleaseLock = true;
}

if (!$cannotReleaseLock) {
    // Import files
    //$fp = fopen($lock_file, 'w+');
    //chmod($lock_file, 0777);
    if (_AI_DEBUG_) {
        echo 'Start importing flows'._AI_LB_;
    }
    foreach (ImporterCollection::getInstance() as $importer) {
        // Update lock file mtime
        touch($lock_file);
        chmod($lock_file, 0777);

        new $importer();
        $time_elapsed = microtime(true) - $start_time;
        if ($max_memory != -1
            && $max_memory < memory_get_peak_usage()
            || $max_execution_time != -1
            && $time_elapsed > $max_execution_time
        ) {
            if (_AI_DEBUG_) {
                echo 'Limit reached'._AI_LB_;
            }
            if (Tools::getIsset('ajax')) {
                echo 2;
            }

            if (file_exists($lock_file)) {
                unlink($lock_file);
            }

            if (Tools::getIsset('reload')) {
                echo '<script>window.setTimeout(function () { location.reload(); }, 2000);</script>';
            }

            exit;
        }
    }
    //fclose($fp);
}
if (_AI_DEBUG_) {
    echo 'End importing flows'._AI_LB_;
}
if (!$cannotReleaseLock) {
    if (file_exists($lock_file)) {
        unlink($lock_file);
    }
}

$lock_file = "lock/$channel.lock";
/* Auto release channel lock */
if (Configuration::getGlobalValue('AI_AUTO_RELEASE_AFTER') && file_exists($lock_file)) {
    if ($time - filemtime($lock_file) > Configuration::getGlobalValue('AI_AUTO_RELEASE_AFTER')) {
        if (file_exists($lock_file)) {
            unlink($lock_file);
        }
    }
}

$cannotReleaseLock = false;
if (file_exists($lock_file)) {
    $cannotReleaseLock = true;
}

if (!$cannotReleaseLock) {
    //$fp = fopen($lock_file, 'w+');
    //chmod($lock_file, 0777);

    // Execute blocks
    if (_AI_DEBUG_) {
        echo 'Lock created'._AI_LB_;
    }

    error_log("$identifier - EXEC START");

    $count = 1;
    do {
        // Update lock file mtime
        touch($lock_file);
        chmod($lock_file, 0777);

        $block = Block::getNextBlock($channel);

        ++$count;

        if (!$block) {
            if (_AI_DEBUG_) {
                echo 'Nothing to do'._AI_LB_;
            }

            if (Tools::getIsset('ajax')) {
                echo 3;
            }

            error_log("$identifier - NOTHING TO DO");
            break;
        }

        if (_AI_DEBUG_) {
            echo 'Start running block #'.$block->id._AI_LB_;
        }

        $block->run($identifier);

        if (_AI_DEBUG_) {
            echo 'End running block #'.$block->id._AI_LB_;
        }
        $time_elapsed = microtime(true) - $start_time;
        if ($count % $blockStackSize == 0) {
            Block::writeCache();
        }
    } while (($max_memory == -1 || $max_memory > memory_get_peak_usage())
        && ($max_execution_time == -1
        || $time_elapsed < $max_execution_time));

    if (Tools::getIsset('ajax')) {
        if ($block) {
            echo 1;
        }
    }

    Block::writeCache();
    SupplierReference::storeCache();

    error_log("$identifier - CHANNEL $channel LOCKED");
} else {
    if (_AI_DEBUG_) {
        echo 'Cannot release lock'._AI_LB_;
    }

    if (Tools::getIsset('ajax')) {
        echo 4;
    }

    error_log("$identifier - CHANNEL $channel LOCKED");
}

if (!$cannotReleaseLock) {
    if (file_exists($lock_file)) {
        unlink($lock_file);
    }
}

$time_elapsed = microtime(true) - $start_time;
error_log("$identifier - END - $time_elapsed");

if (Tools::getIsset('reload')) {
    echo '<script>window.setTimeout(function () { location.reload(); }, 2000);</script>';
}

function warningHandler($errno, $errstr, $errfile, $errline)
{
    if (strpos($errfile, dirname(__FILE__)) === false) {
        return false;
    }

    $channel = rand(1, Configuration::getGlobalValue('AI_NB_CHANNEL'));

    $lock_file = 'lock/import.lock';
    if (file_exists($lock_file)) {
        unlink($lock_file);
    }
    $lock_file = "lock/$channel.lock";
    if (file_exists($lock_file)) {
        unlink($lock_file);
    }
    throw new Exception("Warning: $errstr");
}

function noticeHandler($errno, $errstr, $errfile, $errline)
{
    if (strpos($errfile, dirname(__FILE__)) !== true) {
        return false;
    }

    $channel = rand(1, Configuration::getGlobalValue('AI_NB_CHANNEL'));

    $lock_file = 'lock/import.lock';
    if (file_exists($lock_file)) {
        unlink($lock_file);
    }
    $lock_file = "lock/$channel.lock";
    if (file_exists($lock_file)) {
        unlink($lock_file);
    }
    throw new Exception("Notice: $errstr");
}
