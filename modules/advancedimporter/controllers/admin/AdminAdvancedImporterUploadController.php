<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class class */
require_once _PS_MODULE_DIR_.'advancedimporter/classes/block.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/template.php';

class AdminAdvancedImporterUploadController extends ModuleAdminController
{
    protected $colorOnBackground = true;
    protected $color_on_background = true; /* Ne sert que si un jour PS réutilise la norme */

    public $tpl_view_vars = array();

    public function __construct()
    {
        $this->bootstrap = true;
        parent::__construct();

        if (Tools::getValue('submitAddconfiguration')) {
            $this->processForm();
        }
    }

    public function initContent()
    {
        $this->display = 'view';

        return parent::initContent();
    }

    public function initToolbarTitle()
    {
        $this->toolbar_title = array_unique($this->breadcrumbs);
    }

    public function renderView()
    {
        $this->tpl_view_vars['moduleDir'] = _PS_MODULE_DIR_;
        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            $this->tpl_view_vars['display_button'] = true;
        } else {
            $this->tpl_view_vars['display_button'] = false;
        }

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
            Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->fields_value['template'] = Tools::getValue('template');

        if (class_exists('PrestaShopCollection')) {
            $tmp_templates = new PrestaShopCollection('AITemplate');
        } else {
            $tmp_templates = new Collection('AITemplate');
        }
        $templates = array();
        $emptyTemplate = new AITemplate();
        $emptyTemplate->name = $this->l('None');
        $templates[] = $emptyTemplate;

        foreach ($tmp_templates as $template) {
            $templates[] = $template;
        }

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Import a flow'),
                    'icon' => 'icon-download',
                ),
                'input' => array(
                    array(
                        'type' => 'file',
                        'name' => 'filename',
                        'required' => true,
                        'label' => $this->l('File to upload:'),
                    ),
                    array(
                        'type' => 'select',
                        'name' => 'template',
                        'required' => true,
                        'label' => $this->l('Apply a template:'),
                        'options' => array(
                            'query' => $templates,
                            'id' => 'id',
                            'name' => 'name',
                        ),
                    ),
                ),
                'submit' => array(
                    'name' => 'submit_configuration',
                    'icon' => 'process-icon-import',
                    'title' => $this->l('Upload the flow')
                )
            )
        );

        $helper->languages = $this->context->controller->getLanguages();
        $helper->default_form_language = (int) $this->context->language->id;

        return $helper->generateForm(array($fields_form));
    }

    public function initToolbar()
    {
        $this->toolbar_btn['save'] = array(
            'href' => '#',
            'desc' => $this->l('Upload the flow'),
        );

        return false;
    }

    public function processForm()
    {
        if (!isset($_FILES['filename']) || !$_FILES['filename']['size']) {
            $this->errors[] = $this->l('Please select a file');
            return;
        }

        $file_to_move = $_FILES['filename']['tmp_name'];
        $filename = $_FILES['filename']['name'];
        $pathinfo = pathinfo($filename);
        $filename = $pathinfo['filename'].'.xml';

        $template = new AITemplate((int)Tools::getValue('template'));
        try {
            $xml = $template->loadFile($file_to_move);
        } catch (Exception $e) {
            $this->errors[] = $this->l('The file cannot be uploaded, it seems to be an invalid XML.');
            $this->errors[] = $this->l('If it\'s a CSV you need to create a template.');
            $this->errors[] = $this->l(' Sytem error: ').$e->getMessage();
            return;
        }

        if ($xml->getName() != 'advancedimporter') {
            $this->errors[] = $this->l('The flow is an XML valid, but is not an XML supported by the module.');
            $this->errors[] = $this->l('Try to create a template first');
            return;
        }

        $dom = dom_import_simplexml($xml)->ownerDocument;
        $dom->formatOutput = true;
        try {
            $res = file_put_contents(
                _PS_MODULE_DIR_.'advancedimporter/flows/import/queue/'.$filename,
                $dom->saveXML()
            );

            if (!$res) {
                $this->errors[] = sprintf(
                    $this->l('Not possible to upload file. Please check the right on the directory "%s"'),
                    _PS_MODULE_DIR_.'advancedimporter/flows/import/queue/'
                );

                return;
            }
        } catch (Exception $e) {
            $this->errors[] = $e->getMessage();
            return;
        }
        $this->confirmations[] = $this->l('Flow imported correctly');
    }
}
