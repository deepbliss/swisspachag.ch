<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class class */
require_once _PS_MODULE_DIR_.'advancedimporter/classes/objectmodel/SupplierReference.php';

class AdminAdvancedImporterCreateSupplierReferenceController extends ModuleAdminController
{
    protected $colorOnBackground = true;
    protected $color_on_background = true; /* Ne sert que si un jour PS réutilise la norme */

    public $tpl_view_vars = array();

    public function __construct()
    {
        $this->module = 'advancedimporter';
        $this->bootstrap = true;
        parent::__construct();

        if (Tools::getValue('submitAddconfiguration')) {
            $this->processForm();
        }
    }

    public function initContent()
    {
        $this->display = 'view';

        return parent::initContent();
    }

    public function initToolbarTitle()
    {
        $this->toolbar_title = array_unique($this->breadcrumbs);
    }

    public function renderView()
    {
        $this->tpl_view_vars['moduleDir'] = _PS_MODULE_DIR_;
        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            $this->tpl_view_vars['display_button'] = true;
        } else {
            $this->tpl_view_vars['display_button'] = false;
        }

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
            Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->identifier = $this->identifier;
        $helper->fields_value['entity'] = Tools::getValue('entity');
        $helper->fields_value['parentid'] = Tools::getValue('parentid');
        $helper->fields_value['parententity'] = Tools::getValue('parententity');
        $helper->fields_value['format'] = Tools::getValue('format', '{name}');
        $helper->fields_value['condition'] = Tools::getValue('condition');
        $helper->fields_value['testmode'] = Tools::getValue('testmode', true);

        $fields_form = array(
            'form' => array(
                'input' => array(
                    array(
                        'type' => 'text',
                        'name' => 'entity',
                        'required' => true,
                        'label' => $this->l('Entity class:'),
                        'desc' => $this->l('Class of the entity'),
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'parentid',
                        'required' => false,
                        'label' => $this->l('Parent ID attribute:'),
                        'desc' => $this->l('Set the parent id attribute. Ex: id_feature'),
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'parententity',
                        'required' => false,
                        'label' => $this->l('Parent entity:'),
                        'desc' => $this->l('Set the parent entity. Ex: Feature'),
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'format',
                        'required' => true,
                        'label' => $this->l('Format of the supplier reference:'),
                        'desc' => $this->l('Ex: {parent:name}-{value}'),
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'condition',
                        'required' => false,
                        'label' => $this->l('Condition:'),
                        'desc' => $this->l('Condition on the entity to be created.')
                        .' '.$this->l('Let empty if you want to import all entities')
                        .' '.$this->l('The format is attribute;operator;values')
                        .' '.$this->l('Seperate the conditions with pipe.')
                        .' '.$this->l('Ex: reference;like;TK%|id;in;1,2,5,6'),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Test mode:'),
                        'size' => 92,
                        'name' => 'testmode',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes'),
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No'),
                            ),
                        ),
                        'desc' => $this->l('If enable no suppliers reference will be created. But a list of reference created will be displayed.'),
                    ),
                ),
                'submit' => array(
                    'name' => 'submit_configuration',
                    'icon' => 'process-icon-save',
                    'title' => $this->l('Process')
                )
            )
        );

        $helper->languages = $this->context->controller->getLanguages();
        $helper->default_form_language = (int) $this->context->language->id;

        return $helper->generateForm(array($fields_form));
    }

    public function initToolbar()
    {
        $this->toolbar_btn['save'] = array(
            'href' => '#',
            'desc' => $this->l('Create the supplier references'),
        );

        return false;
    }

    public function processForm()
    {
        $testmode = Tools::getValue('testmode');

        if (!Tools::getValue('entity')) {
            $this->errors[] = $this->l('Please set an entity.');
            return;
        }

        if (!Tools::getValue('format')) {
            $this->errors[] = $this->l('Please set the format of the supplier reference.');
            return;
        }

        $class = Tools::getValue('entity');

        if (!class_exists($class)) {
            $this->errors[] = sprintf(
                $this->l('The entity « %s » do not exists.'),
                $class
            );
            return;
        }

        $attribute = Tools::getValue('parentid');
        if (!empty($attribute)) {
            if (!property_exists($class, $attribute)) {
                $this->errors[] = sprintf(
                    $this->l('The parent ID attribute « %s » does not exists.'),
                    $attribute
                );
                return;
            }
            $parentEntity = Tools::getValue('parententity');
            if (empty($parentEntity)) {
                $this->errors[] = $this->l('The parent entity is not set.');
                return;
            }
        }

        $cache = array();
        if (class_exists('PrestaShopCollection')) {
            $collection = new PrestaShopCollection($class, Context::getContext()->language->id);
        } else {
            $collection = new Collection($class, Context::getContext()->language->id);
        }

        $condition = Tools::getValue('condition');
        if (!empty($condition)) {
            $parts = explode('|', $condition);
            foreach ($parts as $part) {
                $subparts = explode(';', $part);
                if ($subparts[0] == 'id') {
                    $subparts[0] = $class::$definition['primary'];
                }

                switch ($subparts[1]) {
                    case '=':
                    case '!=':
                    case '<>':
                    case '>':
                    case '>':
                    case '<=':
                    case '>=':
                    case 'like':
                    case 'notlike':
                    case 'regexp':
                    case 'notregexp':
                        if (count($subparts) != 3) {
                            $this->errors[] = $this->l('Condition is not valid.');
                            return;
                        }
                        $collection->where($subparts[0], $subparts[1], $subparts[2]);
                        break;
                    case 'in':
                    case 'notin':
                        if (count($subparts) < 3) {
                            $this->errors[] = $this->l('Condition is not valid.');
                            return;
                        }
                        $values = array();
                        for ($i = 2; $i < count($subparts); $i++) {
                            $values[] = $subparts[$i];
                        }
                        $collection->where($subparts[0], $subparts[1], $values);
                        break;
                    default:
                        $this->errors[] = $this->l('Operator of the condition is not valid.');
                        return;
                }
            }
        }

        $count = 0;
        $message = array();
        foreach ($collection as $entity) {
            // If we use parent entity (attribute and parentEntity are set)
            // get the parentEntity
            if (!empty($attribute) && !empty($entity->$attribute)) {
                if (!isset($cache[$entity->$attribute])) {
                    $cache[$entity->$attribute] = new $parentEntity(
                        $entity->$attribute,
                        Context::getContext()->language->id
                    );
                }
                $parent = $cache[$entity->$attribute];
            }

            $reference = Tools::getValue('format');
            try {
                $errorMsg = $this->l('The attribute «·%s·» does not exists.');
                $reference = preg_replace_callback(
                    '/\{([\w\d_]+)\}/',
                    function ($matches) use ($entity, $errorMsg) {
                        $attribute = $matches[1];
                        if (!isset($entity->$attribute)) {
                            throw new Exception(
                                sprintf(
                                    $errorMsg,
                                    $attribute
                                )
                            );
                        }
                        return $entity->$attribute;
                    },
                    $reference
                );

                if (isset($parent)) {
                    $errorMsg = $this->l('The attribute «·%s·» does not exists.');
                    $reference = preg_replace_callback(
                        '/\{parent:([\w\d_]+)\}/',
                        function ($matches) use ($parent, $errorMsg) {
                            $attribute = $matches[1];
                            if (!isset($parent->$attribute)) {
                                throw new Exception(
                                    sprintf(
                                        $errorMsg,
                                        $attribute
                                    )
                                );
                            }
                            return $parent->$attribute;
                        },
                        $reference
                    );
                }

                if (empty($reference)) {
                    continue;
                }
            } catch (Exception $e) {
                $this->errors[] = $e->getMessage();
                return;
            }

            try {
                SupplierReference::getBySupplierReference($reference, get_class($entity));
            } catch (SupplierReferenceException $e) {
                if (!$testmode) {
                    $supplierReference = new SupplierReference();
                    $supplierReference->id_object = $entity->id;
                    $supplierReference->object_type = get_class($entity);
                    $supplierReference->supplier_reference = $reference;
                    $supplierReference->save();
                } else {
                    if ($count < 50) {
                        $message[] = '<br />'.sprintf(
                            $this->l(' - Supplier reference %s for entity #%d'),
                            $reference,
                            $entity->id
                        );
                    }
                }

                $count++;
            }
        }

        if (!$testmode) {
            $this->confirmations[] = sprintf($this->l('%d reference(s) created'), $count);
        } else {
            $this->confirmations[] = $this->l('Testing mode is enable. No references created.')
                .' '.
                sprintf($this->l('If you disable test mode, %d reference(s) were created:'), $count);
            foreach ($message as $msg) {
                $this->confirmations[] = $msg;
            }
        }
    }
}
