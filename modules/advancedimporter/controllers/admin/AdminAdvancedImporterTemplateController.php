<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/template.php';

class AdminAdvancedImporterTemplateController extends ModuleAdminController
{
    protected $colorOnBackground = true;
    protected $color_on_background = true; /* Ne sert que si un jour PS réutilise la norme */

    public function __construct()
    {
        $this->table = 'advancedimporter_template';
        $this->className = 'AITemplate';

        $this->module = 'advancedimporter';
        $this->multishop_context = Shop::CONTEXT_ALL;

        $this->_orderBy = 'id_advancedimporter_template';
        $this->_orderWay = 'DESC';

        $this->bootstrap = true;

        parent::__construct();

        $this->fields_list = array(
            'id_advancedimporter_template' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'width' => 30,
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'align' => 'left',
            ),
        );

        $this->addRowAction('add');
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete'),
                'confirm' => $this->l('Are you sure?'),
            ),
        );

        $this->informations[] = $this->l('Templates translate CSV or XML to the XML format supported by the module.');
    }

    protected function getAssistantUrl()
    {
        if (!($obj = $this->loadObject(true))) {
            return;
        }

        return $this->context->link->getAdminLink('AdminAdvancedImporterTemplateAssistant')
            .'&template='.(int)$obj->id;
    }

    public function renderForm()
    {
        if (!($obj = $this->loadObject(true))) {
            return;
        }

        if (!empty($obj->nodes)) {
            $this->informations[] = '<a href="'.$this->getAssistantUrl().'">'
                .$this->l('Edit with the assistant')
                .'</a>';
        }

        $this->fields_form['input'][] = array(
            'type' => 'text',
            'label' => $this->l('Name:'),
            'size' => 92,
            'id' => 'name',
            'name' => 'name',
            'required' => true,
        );

        $this->fields_form['input'][] = array(
            'type' => 'select',
            'label' => $this->l('Extractor:'),
            'id' => 'extractor',
            'name' => 'extractor',
            'options' => array(
                'query' => $this->getExtractors(),
                'id' => 'id',
                'name' => 'label',
            ),
            'required' => true,
        );

        $this->fields_form['input'][] = array(
            'type' => 'text',
            'label' => $this->l('Delimiter:'),
            'size' => 92,
            'id' => 'delimiter',
            'name' => 'delimiter',
            'desc' => $this->l('Generally "," or ";" in France'),
            'required' => true,
        );

        $this->fields_form['input'][] = array(
            'type' => 'text',
            'label' => $this->l('Enclosure:'),
            'size' => 92,
            'id' => 'enclosure',
            'name' => 'enclosure',
            'desc' => $this->l('Generally \'"\''),
            'required' => true,
        );

        $this->fields_form['input'][] = array(
            'type' => 'text',
            'label' => $this->l('Encoding:'),
            'size' => 92,
            'id' => 'encoding',
            'name' => 'encoding',
            'desc' => $this->l('Generally \'UTF-8\''),
            'required' => true,
        );

        $this->fields_form['input'][] = array(
            'type' => 'switch',
            'label' => $this->l('Ignore first line:'),
            'size' => 92,
            'id' => 'ignore_first_line',
            'name' => 'ignore_first_line',
            'is_bool' => true,
            'values' => array(
                array(
                    'id' => 'active_on',
                    'value' => 1,
                    'label' => $this->l('Yes'),
                ),
                array(
                    'id' => 'active_off',
                    'value' => 0,
                    'label' => $this->l('No'),
                ),
            ),
            'desc' => $this->l('Does the first line contain title of the columns?'),
            'required' => true,
        );

        $this->fields_form['input'][] = array(
            'type' => 'textarea',
            'label' => $this->l('XSLT:'),
            'rows' => 50,
            'cols' => 91,
            'id' => 'xslt',
            'name' => 'xslt',
            'desc' => $this->l('XSLT definition used to translate XML files into compatible XML files for import'),
            'required' => true,
        );

        $this->fields_form['submit'] = array(
                'title' => $this->l('Save'),
                'class' => 'button btn btn-default',
        );

        $this->tpl_form_vars = array('comment' => $obj);

        return parent::renderForm();
    }

    public function initToolbar()
    {
        $return = parent::initToolbar();
        unset($this->toolbar_btn['new']);

        $this->toolbar_btn['help'] = array(
            'href' => $this->context->link->getAdminLink('AdminAdvancedImporterTemplateAssistant'),
            'desc' => $this->l('Create template with the assistant'),
        );

        $this->toolbar_btn['new'] = array(
            'href' => self::$currentIndex.'&addadvancedimporter_template&token='.$this->token,
            'desc' => $this->l('Create a new template (advanced mode)'),
        );

        return $return;
    }

    public function getExtractors()
    {
        $extractors = array();
        foreach (glob(_PS_MODULE_DIR_.'advancedimporter/classes/extractor/*.php') as $file) {
            $path = pathinfo($file);
            $filename = $path['filename'];

            if ($filename === 'interface') {
                continue;
            }

            if ($filename === 'index') {
                continue;
            }

            $classname = 'AI'.Tools::ucfirst($filename).'Extractor';
            include_once $file;

            $name = $classname::getName();
            $extractors[] = array(
                'id' => $classname,
                'label' => $name ,
            );
        }

        return $extractors;
    }
}
