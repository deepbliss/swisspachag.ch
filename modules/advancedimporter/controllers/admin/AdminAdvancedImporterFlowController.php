<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class class */
require_once _PS_MODULE_DIR_.'advancedimporter/classes/flow.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/block.php';

class AdminAdvancedImporterFlowController extends ModuleAdminController
{
    protected $colorOnBackground = true;
    protected $color_on_background = true; /* Ne sert que si un jour PS réutilise la norme */

    public function __construct()
    {
        $this->table = 'advancedimporter_flow';
        $this->className = 'Flow';
        $this->list_no_link = true;

        $this->module = 'advancedimporter';
        $this->multishop_context = Shop::CONTEXT_ALL;

        $this->_orderBy = 'id_advancedimporter_flow';
        $this->_orderWay = 'DESC';

        $this->bootstrap = true;

        $this->addWaitingFlows();

        parent::__construct();

        $status_list = array(
            FLOW::STATUS_ERROR => $this->l('Error'),
            FLOW::STATUS_WAITING => $this->l('Waiting'),
            FLOW::STATUS_PROCESSING => $this->l('Processing the file'),
            FLOW::STATUS_IMPORTING => $this->l('Importing'),
            FLOW::STATUS_FINISHED => $this->l('Finished'),
        );
        $this->fields_list = array(
            'id_advancedimporter_flow' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'width' => 30,
            ),
            'date_add' => array(
                'title' => $this->l('Created at'),
                'align' => 'center',
                'width' => 120,
            ),
            'started_at' => array(
                'title' => $this->l('Started at'),
                'align' => 'center',
                'width' => 120,
            ),
            'ended_at' => array(
                'title' => $this->l('Ended at'),
                'align' => 'center',
                'width' => 120,
            ),
            'filename' => array(
                'title' => $this->l('filename'),
                'align' => 'left',
            ),
            'path' => array(
                'title' => $this->l('path'),
                'align' => 'left',
            ),
            'block_count' => array(
                'title' => $this->l('Number of blocks'),
                'align' => 'left',
                'width' => 60,
            ),
            'success_count' => array(
                'title' => $this->l('Number of success'),
                'align' => 'left',
                'width' => 60,
            ),
            'error_count' => array(
                'title' => $this->l('Number of errors'),
                'align' => 'left',
                'width' => 60,
            ),
            'status' => array(
                'title' => $this->l('Status'),
                'align' => 'center',
                'type' => 'select',
                'list' => $status_list,
                'filter_key' => 'status',
                'width' => 60,
            ),
        );

        $this->addRowAction('downloadreport');
        $this->addRowAction('download');
        $this->addRowAction('delete');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete'),
                'confirm' => $this->l('Are you sure?'),
            ),
        );

        $this->renderForm();
    }

    /**
     * AdminController::getList() override.
     *
     * @see AdminController::getList()
     */
    public function getList(
        $id_lang,
        $order_by = null,
        $order_way = null,
        $start = 0,
        $limit = null,
        $id_lang_shop = false
    ) {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);

        foreach ($this->_list as &$item) {
            switch ($item['status']) {
                case FLOW::STATUS_ERROR:
                    $item['status'] = $this->l('Error');
                    break;
                case FLOW::STATUS_WAITING:
                    $item['status'] = $this->l('Waiting');
                    break;
                case FLOW::STATUS_PROCESSING:
                    $item['status'] = $this->l('Processing the file');
                    break;
                case FLOW::STATUS_IMPORTING:
                    $item['status'] = $this->l('Importing');
                    break;
                case FLOW::STATUS_FINISHED:
                    $item['status'] = $this->l('Finished');
                    break;
            }
        }
    }

    /*
    public function postProcess()
    {
        switch (Tools::getValue('customaction')) {
            case 'delete':
                $this->deleteAll();
                break;
            default:
                break;
        }

        return parent::postProcess();
    }

    public function deleteAll()
    {
        Db::getInstance()->execute('truncate `'._DB_PREFIX_.$this->table.'`');
    }
     */

    public function initToolbar()
    {
        $return = parent::initToolbar();
        unset($this->toolbar_btn['new']);

        $this->toolbar_btn['new'] = array(
            'href' => $this->context->link->getAdminLink('AdminAdvancedImporterUpload'),
            'desc' => $this->l('Add a new flow'),
        );

        return $return;
    }

    /**
     * Custom action icon "download".
     */
    public function displayDownloadLink($token = null, $id = null)
    {
        if (!array_key_exists('download', self::$cache_lang)) {
            self::$cache_lang['download'] = $this->l('Download flow');
        }

        $this->context->smarty->assign(array(
            'module_dir' => __PS_BASE_URI__.'modules/advancedimporter/',
            'href' => self::$currentIndex.
                '&'.$this->identifier.'='.$id.
                '&download&token='.($token != null ? $token : $this->token),
            'action' => self::$cache_lang['download'],
            'first' => false,
            'classes' => '',
        ));

        return $this->context->smarty->fetch(
            _PS_MODULE_DIR_.'advancedimporter/views/templates/admin/list_action/download.tpl'
        );
    }

    /**
     * Custom action icon "download report".
     */
    public function displayDownloadReportLink($token = null, $id = null)
    {
        if (!array_key_exists('downloadreport', self::$cache_lang)) {
            self::$cache_lang['downloadreport'] = $this->l('Download report');
        }

        $this->context->smarty->assign(array(
            'module_dir' => __PS_BASE_URI__.'modules/advancedimporter/',
            'href' => self::$currentIndex.
                '&'.$this->identifier.'='.$id.
                '&downloadreport&token='.($token != null ? $token : $this->token),
            'action' => self::$cache_lang['downloadreport'],
            'first' => true,
            'classes' => '',
        ));

        return $this->context->smarty->fetch(
            _PS_MODULE_DIR_.'advancedimporter/views/templates/admin/list_action/download.tpl'
        );
    }

    public function renderForm()
    {
        if (Tools::getIsset('download')) {
            $flow = new Flow((int) Tools::getValue('id_advancedimporter_flow'));
            $filepath = _PS_MODULE_DIR_.'advancedimporter/flows/import/'.$flow->path;

            $size = filesize($filepath);
            header('Content-Type: application/force-download; name="'.$flow->filename.'"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.$size);
            header('Content-Disposition: attachment; filename="'.$flow->filename.'"');
            header('Expires: 0');
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            readfile($filepath);
            exit();
        } elseif (Tools::getIsset('downloadreport')) {
            $flow = new Flow((int) Tools::getValue('id_advancedimporter_flow'));

            header('Content-Type: application/force-download; name="report-'.basename($flow->filename, '.xml').'.csv"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Disposition: attachment; filename="report-'.basename($flow->filename, '.xml').'.csv"');
            header('Expires: 0');
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');

            // Get all blocks with error
            if (class_exists('PrestaShopCollection')) {
                $collection = new PrestaShopCollection('Block');
            } else {
                $collection = new Collection('Block');
            }
            $collection->sqlWhere('error IS NOT NULL');
            $collection->where('error', '<>', '');
            $collection->where('id_advancedimporter_flow', '=', $flow->id);

            $out = fopen('php://output', 'w');
            fputcsv(
                $out,
                array(
                    $this->l('ID'),
                    $this->l('Error'),
                    $this->l('Block'),
                )
            );
            foreach ($collection as $block) {
                fputcsv(
                    $out,
                    array(
                        $block->id,
                        $block->error,
                        $block->block,
                    )
                );
            }

            exit();
        }
    }

    public function addWaitingFlows()
    {
        foreach (array('xml', 'XML', 'csv', 'CSV') as $ext) {
            $array = glob(_PS_MODULE_DIR_.'advancedimporter/flows/import/queue/*.'.$ext);
            if ($array !== false) {
                foreach ($array as $file) {
                    Flow::getByPath($file);
                }
            }
        }
    }

    public function renderList()
    {
        // @TODO Add box to display ajax cron call
        return parent::renderList();
    }

    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        Media::addJsDef(array('baseDir' => __PS_BASE_URI__));
        Media::addJsDef(array('AI_TOKEN' => Configuration::getGlobalValue('AI_TOKEN')));
        Media::addJsDef(array('CONFIRM_DELETE' => $this->l('Are you sure?')));
        $this->addJS(_PS_MODULE_DIR_.$this->module->name.'/views/js/admin/cron.js');
    }
}
