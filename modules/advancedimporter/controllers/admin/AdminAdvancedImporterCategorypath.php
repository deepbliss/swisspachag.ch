<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/categorypath.php';

/** Class AdminAdvancedImporterCategorypathController */
class AdminAdvancedImporterCategorypathController extends ModuleAdminController
{
    protected $list_no_link = true;
    protected $colorOnBackground = true;
    protected $color_on_background = true; /* Ne sert que si un jour PS réutilise la norme */

    public function __construct()
    {
        $this->table = 'advancedimporter_categorypath';
        $this->className = 'AICategorypath';

        $this->module = 'advancedimporter';
        $this->multishop_context = Shop::CONTEXT_ALL;

        $this->_orderBy = 'id_advancedimporter_categorypath';
        $this->_orderWay = 'DESC';

        $this->bootstrap = true;

        $this->autocreateCategorypathTable();

        parent::__construct();

        $this->fields_list = array(
            'id_advancedimporter_categorypath' => array(
                'title' => $this->l('ID'),
                'align' => 'right',
                'width' => 30,
            ),
            'categorypath1' => array(
                'title' => $this->l('Initial category path'),
                'align' => 'left',
            ),
            'categorypath2' => array(
                'title' => $this->l('Final category path'),
                'align' => 'left',
            ),
        );

        $this->addRowAction('edit');

        $this->fields_form['submit'] = array(
                'title' => $this->l('Save'),
                'class' => 'button btn btn-default',
        );
    }

    public function renderForm()
    {
        if (!($this->loadObject(true))) {
            return;
        }

        $this->fields_form['input'][] = array(
            'type' => 'text',
            'label' => $this->l('Initial category path:'),
            'size' => 150,
            'id' => 'categorypath1',
            'name' => 'categorypath1',
        );

        $this->fields_form['input'][] = array(
            'type' => 'text',
            'label' => $this->l('Final category path:'),
            'size' => 150,
            'id' => 'categorypath2',
            'name' => 'categorypath2',
        );

        return parent::renderForm();
    }

    protected function autocreateCategorypathTable()
    {
        if ((int) Configuration::getGlobalValue('AI_CP_TABLE')) {
            return;
        }

        Configuration::updateGlobalValue('AI_CP_TABLE', 1);
        Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'advancedimporter_categorypath` (
                `id_advancedimporter_categorypath` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `categorypath1` TEXT DEFAULT NULL,
                `categorypath2` TEXT DEFAULT NULL,
                PRIMARY KEY (`id_advancedimporter_categorypath`)
            )'
        );
    }
}
