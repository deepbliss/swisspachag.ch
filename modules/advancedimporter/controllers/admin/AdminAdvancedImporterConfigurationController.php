<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/** Class class AdminAdvancedImporterConfigurationController */
class AdminAdvancedImporterConfigurationController extends ModuleAdminController
{
    public function __construct()
    {
        $this->multishop_context = Shop::CONTEXT_ALL;
        $this->bootstrap = true;

        parent::__construct();

        if (Tools::getValue('submitAddadvancedimporter_configuration')) {
            $this->processForm();
        }
    }

    protected function getValue($key)
    {
        if (Tools::getIsset($key)) {
            return Tools::getValue($key);
        }
        return Configuration::getGlobalValue($key);
    }

    public function renderList()
    {
        $smarty = Context::getContext()->smarty;
        $smarty->assign(array(
            'AI_ADVANCED_MODE' => $this->getValue('AI_ADVANCED_MODE'),
            'AI_FORCE_EXECUTION_TIME' => $this->getValue('AI_FORCE_EXECUTION_TIME'),
            'AI_USE_API' => $this->getValue('AI_USE_API'),
            'AI_KEY' => $this->getValue('AI_KEY'),
            'AI_NB_CHANNEL' => $this->getValue('AI_NB_CHANNEL'),
            'AI_LOG_ENABLE' => $this->getValue('AI_LOG_ENABLE'),
            'AI_HISTORY_ENABLE' => $this->getValue('AI_HISTORY_ENABLE'),
            'AI_SYSLOG_ENABLE' => $this->getValue('AI_SYSLOG_ENABLE'),
            'AI_NOTICELOG_ENABLE' => $this->getValue('AI_NOTICELOG_ENABLE'),
            'AI_ERRORLOG_ENABLE' => $this->getValue('AI_ERRORLOG_ENABLE'),
            'AI_SUCCESSLOG_ENABLE' => $this->getValue('AI_SUCCESSLOG_ENABLE'),
            'AI_AUTO_RELEASE_AFTER' => $this->getValue('AI_AUTO_RELEASE_AFTER'),
            'AI_ADD_CRON_TASK_SCALE' => $this->getValue('AI_ADD_CRON_TASK_SCALE'),
            'AI_BLOCK_STACK_SIZE' => $this->getValue('AI_BLOCK_STACK_SIZE'),
            'AI_ADD_CRON_TASK_EACH' => $this->getValue('AI_ADD_CRON_TASK_EACH'),
            'AI_CRON_LIFETIME' => $this->getValue('AI_CRON_LIFETIME'),
            'AI_PRELOAD_SR_CACHE' => $this->getValue('AI_PRELOAD_SR_CACHE'),
            'AI_USE_SR_CACHE' => $this->getValue('AI_USE_SR_CACHE'),
            'AI_TOKEN' => $this->getValue('AI_TOKEN'),
            'cronUrl' => '//'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__.'modules/advancedimporter/cron.php',
        ));

        $errorWriteImportFolder = !is_writable(_PS_MODULE_DIR_.'advancedimporter/flows/import/imported/');
        $errorWriteTemporaryFolder = !is_writable(_PS_MODULE_DIR_.'advancedimporter/flows/import/temporary/');
        $errorWriteErrorFolder = !is_writable(_PS_MODULE_DIR_.'advancedimporter/flows/import/error/');
        $errorWriteQueueFolder = !is_writable(_PS_MODULE_DIR_.'advancedimporter/flows/import/queue/');
        $errorWriteImportedFolder = !is_writable(_PS_MODULE_DIR_.'advancedimporter/flows/import/imported/');
        $errorWriteLockFolder = !is_writable(_PS_MODULE_DIR_.'advancedimporter/lock/');
        $errorWriteClassTmpEntityFolder = !is_writable(_PS_MODULE_DIR_.'advancedimporter/classes/tmp/entity/');
        $errorWriteClassTmpOMFolder = !is_writable(_PS_MODULE_DIR_.'advancedimporter/classes/tmp/objectmodel/');
        $errorWriteClassTmpFolder = !is_writable(_PS_MODULE_DIR_.'advancedimporter/classes/tmp/');
        $errorPhpVersion = !version_compare(phpversion(), '5.3', '>=');
        $warningLocalhost = $_SERVER['HTTP_HOST'] == '127.0.0.1'
            || $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['SERVER_ADDR'] == '127.0.0.1';

        $errorsMessage = '';
        if ($errorWriteImportFolder
            || $errorWriteTemporaryFolder
            || $errorWriteErrorFolder
            || $errorWriteQueueFolder
            || $errorWriteImportedFolder
            || $errorWriteClassTmpEntityFolder
            || $errorWriteClassTmpOMFolder
            || $errorWriteClassTmpFolder
            || $errorWriteLockFolder
            || $errorPhpVersion
        ) {
            $errors = array();
            if ($errorWriteImportFolder) {
                $errors[] = $this->l('Please check right for the folder "modules/advancedimporter/flows/import/imported". The module need to write in this folder!');
            }
            if ($errorWriteTemporaryFolder) {
                $errors[] = $this->l('Please check right for the folder "modules/advancedimporter/flows/import/temporary". The module need to write in this folder!');
            }
            if ($errorWriteErrorFolder) {
                $errors[] = $this->l('Please check right for the folder "modules/advancedimporter/flows/import/error". The module need to write in this folder!');
            }
            if ($errorWriteQueueFolder) {
                $errors[] = $this->l('Please check right for the folder "modules/advancedimporter/flows/import/queue". The module need to write in this folder!');
            }
            if ($errorWriteImportFolder) {
                $errors[] = $this->l('Please check right for the folder "modules/advancedimporter/flows/import/imported". The module need to write in this folder!');
            }
            if ($errorWriteLockFolder) {
                $errors[] = $this->l('Please check right for the folder "modules/advancedimporter/lock". The module need to write in this folder!');
            }
            if ($errorWriteClassTmpOMFolder) {
                $errors[] = $this->l('Please check right for the folder "modules/advancedimporter/classes/tmp/objectmodel". The module need to write in this folder!');
            }
            if ($errorWriteClassTmpEntityFolder) {
                $errors[] = $this->l('Please check right for the folder "modules/advancedimporter/classes/tmp/entity". The module need to write in this folder!');
            }
            if ($errorWriteClassTmpFolder) {
                $errors[] = $this->l('Please check right for the folder "modules/advancedimporter/classes/tmp". The module need to write in this folder!');
            }
            if ($errorPhpVersion) {
                $errors[] = $this->l('php version must be greater or equal that 5.3.0.')
                    .' '.$this->l('actual version: ').phpversion();
            }

            $errorsMessage = $this->module->displayError(implode('<br />', $errors));
        }

        $warningsMessage = '';
        if ($warningLocalhost) {
            $warningsMessage = $this->module->displayWarning(
                $this->l('It seems your PrestaShop is installed on a local network (not the production server). Smart cron cannot work properly.')
            );
        }

        $confirmationMessage = '';
        if (!empty($this->confirmation)) {
            $confirmationMessage = $this->module->displayConfirmation($this->confirmation);
        }

        return $errorsMessage
            .$warningsMessage
            .$confirmationMessage
            .$smarty->fetch(_PS_MODULE_DIR_.'advancedimporter/views/templates/admin/configuration.tpl');
    }

    public function processForm()
    {
        $smarty = Context::getContext()->smarty;

        $key = Tools::getValue('AI_KEY');
        if (!empty($key) && !Validate::isInt(Tools::getValue('AI_KEY'))) {
            $this->errors = $this->l('« Order reference » must be an integer.');
            return;
        }
        if (!Validate::isInt(Tools::getValue('AI_CRON_LIFETIME'))) {
            $this->errors = $this->l('« Cron lifetime » must be an integer.');
            return;
        }
        if (!Validate::isInt(Tools::getValue('AI_NB_CHANNEL'))) {
            $this->errors = $this->l('« Number of channel » must be an integer.');
            return;
        }
        if ((int) Tools::getValue('AI_NB_CHANNEL') <= 0) {
            $this->errors = $this->l('« Number of channel » must be greater than 0.');
            return;
        }
        if (!Validate::isInt(Tools::getValue('AI_AUTO_RELEASE_AFTER'))) {
            $this->errors = $this->l('« Release a channel after » must be an integer.');
            return;
        }
        if (!Validate::isInt(Tools::getValue('AI_ADD_CRON_TASK_EACH'))) {
            $this->errors = $this->l('« Add recurring task each » must be an integer.');
            return;
        }
        if (!Validate::isInt(Tools::getValue('AI_ADD_CRON_TASK_SCALE'))) {
            $this->errors = $this->l('« Add the next recurring task of » must be an integer.');
            return;
        }
        if (!Validate::isInt(Tools::getValue('AI_BLOCK_STACK_SIZE'))) {
            $this->errors = $this->l('« Block stack size » must be an integer.');
            return;
        }

        $protocol = 'http';
        if (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) {
            $protocol = 'https';
        }

        if ((int) Tools::getValue('AI_USE_API')) {
            /*
            $smarty->assign(
                'iframe',
                'https://register.prestashopxmlimporter.madef.fr/?orderid='
                .(int) Tools::getValue('AI_KEY').'&callbackurl='
                .urlencode(
                    $protocol.'://'.preg_replace(
                        '/^(.*)\/[^\/]+\/index.php$/',
                        '$1/modules/advancedimporter',
                        $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']
                    )
                )
                .'&token='.Configuration::getGlobalValue('AI_TOKEN')
            );
             */

            $result = Tools::file_get_contents(
                'http://register.prestashopxmlimporter.madef.fr/?orderid='
                .(int) Tools::getValue('AI_KEY').'&callbackurl='
                .urlencode(
                    $protocol.'://'.preg_replace(
                        '/^(.*)\/[^\/]+\/index.php$/',
                        '$1/modules/advancedimporter',
                        $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']
                    )
                )
                .'&token='.Configuration::getGlobalValue('AI_TOKEN')
            );

            if (!$result) {
                $this->warnings[] = $this->l('API service is down. Please contact the suport of the module.');
            }
        } else {
            /*
            $smarty->assign(
                'iframe',
                'https://unregister.prestashopxmlimporter.madef.fr/?callbackurl='
                .urlencode(
                    $protocol.'://'.preg_replace(
                        '/^(.*)\/[^\/]+\/index.php$/',
                        '$1/modules/advancedimporter',
                        $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']
                    )
                )
            );
             */

            $result = Tools::file_get_contents(
                'http://unregister.prestashopxmlimporter.madef.fr/?callbackurl='
                .urlencode(
                    $protocol.'://'.preg_replace(
                        '/^(.*)\/[^\/]+\/index.php$/',
                        '$1/modules/advancedimporter',
                        $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']
                    )
                )
            );

            if (!$result) {
                $this->warnings[] = $this->l('API service is down. Please contact the suport of the module.');
            }
        }

        Configuration::updateGlobalValue('AI_ADVANCED_MODE', (int) Tools::getValue('AI_ADVANCED_MODE'));
        Configuration::updateGlobalValue(
            'AI_FORCE_EXECUTION_TIME',
            (int) Tools::getValue('AI_FORCE_EXECUTION_TIME')
        );
        $key = (int) Tools::getValue('AI_KEY');
        if (!$key) {
            Configuration::updateGlobalValue('AI_KEY', '');
            Configuration::updateGlobalValue('AI_USE_API', 0);
        } else {
            Configuration::updateGlobalValue('AI_KEY', (int) Tools::getValue('AI_KEY'));
            Configuration::updateGlobalValue('AI_USE_API', (int) Tools::getValue('AI_USE_API'));
        }
        if ((int) Tools::getValue('AI_NB_CHANNEL') > 0) {
            Configuration::updateGlobalValue('AI_NB_CHANNEL', (int) Tools::getValue('AI_NB_CHANNEL'));
        }

        Configuration::updateGlobalValue('AI_LOG_ENABLE', (int) Tools::getValue('AI_LOG_ENABLE'));
        Configuration::updateGlobalValue('AI_HISTORY_ENABLE', (int) Tools::getValue('AI_HISTORY_ENABLE'));
        Configuration::updateGlobalValue('AI_SYSLOG_ENABLE', (int) Tools::getValue('AI_SYSLOG_ENABLE'));
        Configuration::updateGlobalValue('AI_NOTICELOG_ENABLE', (int) Tools::getValue('AI_NOTICELOG_ENABLE'));
        Configuration::updateGlobalValue('AI_ERRORLOG_ENABLE', (int) Tools::getValue('AI_ERRORLOG_ENABLE'));
        Configuration::updateGlobalValue('AI_SUCCESSLOG_ENABLE', (int) Tools::getValue('AI_SUCCESSLOG_ENABLE'));
        Configuration::updateGlobalValue('AI_AUTO_RELEASE_AFTER', (int) Tools::getValue('AI_AUTO_RELEASE_AFTER'));
        Configuration::updateGlobalValue('AI_ADD_CRON_TASK_SCALE', (int) Tools::getValue('AI_ADD_CRON_TASK_SCALE'));
        Configuration::updateGlobalValue('AI_BLOCK_STACK_SIZE', (int) Tools::getValue('AI_BLOCK_STACK_SIZE'));
        Configuration::updateGlobalValue('AI_ADD_CRON_TASK_EACH', (int) Tools::getValue('AI_ADD_CRON_TASK_EACH'));
        Configuration::updateGlobalValue('AI_CRON_LIFETIME', (int) Tools::getValue('AI_CRON_LIFETIME'));
        Configuration::updateGlobalValue('AI_PRELOAD_SR_CACHE', (int) Tools::getValue('AI_PRELOAD_SR_CACHE'));
        Configuration::updateGlobalValue('AI_USE_SR_CACHE', (int) Tools::getValue('AI_USE_SR_CACHE'));
        Configuration::updateGlobalValue('AI_TOKEN', Tools::getValue('AI_TOKEN'));

        $this->confirmation = $this->l('Configuration saved');
    }
}
