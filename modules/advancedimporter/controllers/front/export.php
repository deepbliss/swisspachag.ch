<?php
/**
 * 2013-2018 MADEF IT.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to contact@madef.fr so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    MADEF IT <contact@madef.fr>
 *  @copyright 2013-2018 MADEF IT
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

require_once _PS_MODULE_DIR_.'advancedimporter/classes/export.php';
require_once _PS_MODULE_DIR_.'advancedimporter/classes/template.php';

class AdvancedimporterExportModuleFrontController extends ModuleFrontController
{
    public function __construct()
    {
        $filepath = _PS_MODULE_DIR_.'advancedimporter/flows/export/export/'.Tools::getValue('token').'.'.Tools::getValue('file');
        if (!file_exists($filepath)) {
            Tools::redirect('index.php?controller=404');
            return;
        }

        $content = Tools::file_get_contents($filepath);

        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache'); // HTTP/1.0
        header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Content-type: text/xml');

        echo $content;
        exit;
    }
}
