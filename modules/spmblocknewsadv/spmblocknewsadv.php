<?php
/**
 * 2011 - 2018 StorePrestaModules SPM LLC.
 *
 * MODULE spmblocknewsadv
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.2.2
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class spmblocknewsadv extends Module{


    private $_data_translate_custom = array();

	private $_is15;
	private $_iso_lng;
	private $_is_friendly_url;
	private $_is16;
    private $_is_cloud;
    private $_translate;
    private $_token_cron;
    public $is_demo = 0;

    private $_id_instagram_twitter_pinterest_module = 41665;
    private $_id_vk_module = 0;

    private $_template_name;
    private $_template_path = 'views/templates/hooks/';


    public function __construct(){

		$this->name = 'spmblocknewsadv';
 	 	$this->version = '1.2.2';
 	 	$this->tab = 'content_management';
		$this->module_key = 'dca4fa8fb3c31915b367452edb214199';
		$this->author = 'SPM';
		


        $this->_token_cron = $this->getokencron();

        $this->bootstrap = true;
        $this->need_instance = 0;

        if (defined('_PS_HOST_MODE_'))
            $this->_is_cloud = 1;
        else
            $this->_is_cloud = 0;

        //$this->_is_cloud = 1;

        if($this->_is_cloud){
            $this->path_img_cloud = "modules/".$this->name."/upload/";
        } else {
            $this->path_img_cloud = "upload/".$this->name."/";

        }

        $this->_translate = array(
            'name'=>$this->name,
            'title'=>$this->l('Automatically posts Integration'),
            'title_pstwitterpost'=>$this->l('Twitter, Instagram, Pinterest Wall Posts (3 in 1)'),
            'title_psvkpost'=>$this->l('Vkontakte Wall Post'),

            'buy_module_pstwitterpost'=>$this->l('This feature requires you to have purchased, installed and correctly configured our Twitter, Instagram, Pinterest Wall Posts (3 in 1) module.')
                .' '
                .$this->l('You may purchase it on:')
                .' '
                .$this->_linkAHtml(array('title'=>$this->l('PrestaShop Addons'),'href'=>'http://addons.prestashop.com/product.php?id_product='.$this->_id_instagram_twitter_pinterest_module))

                .' '.$this->l('Once this is all set, you will see the configuration options instead of this red text.'),

            'buy_module_psvkpost'=>$this->l('This feature requires you to have purchased, installed and correctly configured our Vkontakte Wall Post module.')
                .' '
                .$this->_linkAHtml(array('title'=>$this->l('PrestaShop Addons'),'href'=>'http://addons.prestashop.com/product.php?id_product='.$this->_id_vk_module))

                .$this->l('Once this is all set, you will see the configuration options instead of this red text.'),

            'hint1'=>$this->l('This section lets you integrate with our')

                .' '.$this->_linkAHtml(array('title'=>$this->l('Twitter, Instagram, Pinterest Wall Posts (3 in 1) module'),'href'=>'http://addons.prestashop.com/product.php?id_product='.$this->_id_instagram_twitter_pinterest_module,'is_b'=>1))

                /*
                .$this->l('and')
                .' '.$this->_linkAHtml(array('title'=>$this->l('Vkontakte Wall Post module'),'href'=>'http://addons.prestashop.com/product.php?id_product='.$this->_id_vk_module,'is_b'=>1))
                */

                .$this->l(', and allows you posted news on your PrestaShop website to be automatically posted to your')
                .' '
                .$this->l(' Twitter, Instagram, Pinterest fan pages').'.'.' ',

            'hint2'=>'',

            'update_button'=>$this->l('Update settings'),
            'form_action'=>Tools::safeOutput($_SERVER['REQUEST_URI']),

            'enable_psvkpost'=> $this->l('Enable Vkontakte Wall Post'),

            'enable_pstwitterpost_main'=> $this->l('Enable Twitter, Instagram, Pinterest Wall Posts (3 in 1)'),

            'enable_pstwitterpost'=> $this->l('Enable Twitter Wall Posts'),
            'enable_pstwitterpost_i'=> $this->l('Enable Instagram Wall Posts'),
            'enable_pstwitterpost_p'=> $this->l('Enable Pinterest Wall Posts'),

            'template_text'=>$this->l('Post template text'),

        );




		$this->_is15 = 1;

		$this->_is16 = 1;

		parent::__construct();
		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('News Advanced + Google Rich Snippets');
		$this->description = $this->l('News Advanced + Google Rich Snippets');
		$this->confirmUninstall = $this->l('Are you sure you want to remove it ? Your News Advanced will no longer work. Be careful, all your configuration and your data will be lost');



        if(version_compare(_PS_VERSION_, '1.7.4', '>') && version_compare(_PS_VERSION_, '1.7.4.2', '<')) {
            $this->_template_name = '';
        } else {
            $this->_template_name = $this->_template_path;
        }


		$this->initContext();
		
		
		$this->_loadVariables();

        ## prestashop 1.7 ##
        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            require_once(_PS_MODULE_DIR_.$this->name.'/classes/ps17helpspmblocknewsadv.class.php');
            $ps17help = new ps17helpspmblocknewsadv();
            $ps17help->setMissedVariables();
        } else {
            $smarty = $this->context->smarty;
            $smarty->assign($this->name.'is17' , 0);
        }
        ## prestashop 1.7 ##
		
	}

    private function _linkAHtml($data){

        $title_a = $data['title'];
        $href_a = $data['href'];
        $is_b = isset($data['is_b'])?$data['is_b']:0;
        $onclick = isset($data['onclick'])?$data['onclick']:0;
        $disable_target_blank = isset($data['disable_target_blank'])?$data['disable_target_blank']:0;
        $br_after_count = isset($data['br_after_count'])?$data['br_after_count']:0;

        $_html = '';
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');

        return $_html;
    }

    public function getokencron(){

        return sha1(_COOKIE_KEY_ . $this->name);
    }

	private function initContext()
	{
	  $this->context = Context::getContext();
	  $this->context->currentindex = isset(AdminController::$currentIndex)?AdminController::$currentIndex:'index.php?controller=AdminModules';

	}

    public function getCloudImgPath(){
        return $this->path_img_cloud;
    }

	private function _loadVariables(){

		$is_friendly_url = $this->isURLRewriting();
		$this->_is_friendly_url = $is_friendly_url;
		$this->_iso_lng = $this->getLangISO();
	}

	public function isURLRewriting(){
    	$_is_rewriting_settings = 0;
    	if(Configuration::get($this->name.'rew_on') == 1){
			$_is_rewriting_settings = 1;
		}
		return $_is_rewriting_settings;
    }


    public function getLangISO(){
        $cookie = $this->context->cookie;

        $id_shop = Context::getContext()->shop->id;
        if($id_shop) {
            $all_laguages = Language::getLanguages(true,$id_shop);
        } else {
            $all_laguages = Language::getLanguages(true);
        }

        $id_lang = (int)$cookie->id_lang;

        if($this->isURLRewriting() && sizeof($all_laguages)>1)
            $iso_lang = Language::getIsoById((int)($id_lang))."/";
        else
            $iso_lang = '';

        return $iso_lang;

    }

	public function install(){

		if (!parent::install()) return false;

        if(Configuration::get('PS_REWRITING_SETTINGS')){
            Configuration::updateValue($this->name.'rew_on', 1);
        }

		#### posts api ###
		$languages = Language::getLanguages(false);
    	foreach ($languages as $language){
    		$i = $language['id_lang'];
    		$iso = Tools::strtoupper(Language::getIsoById($i));
    		Configuration::updateValue($this->name.'twdesc_'.$i, $this->l('New news has been added in our shop').' '.$iso);
            Configuration::updateValue($this->name.'idesc_'.$i, $this->l('New news has been added in our shop').' '.$iso);
            Configuration::updateValue($this->name.'pdesc_'.$i, $this->l('New news has been added in our shop').' '.$iso);
            Configuration::updateValue($this->name.'vkdesc_'.$i, $this->l('New news has been added in our shop').' '.$iso);
		}

		#### posts api ###

		// only for prestashop 1.3, 1.4, 1.5
		Configuration::updateValue($this->name.'switch_s', 0);
        // only for prestashop 1.3, 1.4, 1.5



        Configuration::updateValue($this->name.'block_display_img', 1);



		Configuration::updateValue($this->name.'leftnews', 1);


        Configuration::updateValue($this->name.'search_left_n', 1);
        Configuration::updateValue($this->name.'arch_left_n', 1);



        Configuration::updateValue($this->name.'footernews', 1);
        Configuration::updateValue($this->name.'arch_footer_n', 1);
        Configuration::updateValue($this->name.'search_footer_n', 1);
		Configuration::updateValue($this->name.'homenews', 1);


        /// custom hook
        Configuration::updateValue($this->name.'news_m', 1);
        Configuration::updateValue($this->name.'arch_m', 1);
        Configuration::updateValue($this->name.'search_m', 1);


        Configuration::updateValue($this->name.'is_like', 1);
        Configuration::updateValue($this->name.'is_unlike', 1);


		Configuration::updateValue($this->name.'n_block_img_w', 50);

		Configuration::updateValue($this->name.'lists_img_w', 100);


		Configuration::updateValue($this->name.'item_img_w', 500);
        Configuration::updateValue($this->name.'is_soc_but', 1);

        $medium_default = "medium"."_"."default";
        Configuration::updateValue($this->name . 'img_size_rp', $medium_default);
        Configuration::updateValue($this->name.'item_rp_tr', 75);

        Configuration::updateValue($this->name.'rp_img_width', 150);


		Configuration::updateValue($this->name.'b_display_date', 1);
		Configuration::updateValue($this->name.'l_display_date', 1);
		Configuration::updateValue($this->name.'i_display_date', 1);



		Configuration::updateValue($this->name.'number_ni', 3);
		Configuration::updateValue($this->name.'news_page', 5);

		Configuration::updateValue($this->name.'hnumber_ni', 5);
        Configuration::updateValue($this->name.'items_w_h', 150);
        Configuration::updateValue($this->name.'item_h_tr', 250);
        Configuration::updateValue($this->name.'h_display_date', 1);

        Configuration::updateValue($this->name.'is_comments', 1);
        Configuration::updateValue($this->name.'number_fc', 5);


        Configuration::updateValue($this->name.'news_h', 3);
        Configuration::updateValue($this->name.'news_bp_sl', 3);


        Configuration::updateValue($this->name.'bnews_slider', 1);
        Configuration::updateValue($this->name.'bnews_sl', 2);


        Configuration::updateValue($this->name.'rsson', 1);
		Configuration::updateValue($this->name.'number_rssitems', 10);


        Configuration::updateValue($this->name.'relpr_slider', 1);
        Configuration::updateValue($this->name.'npr_slider', 3);

        Configuration::updateValue($this->name.'relp_slider', 1);
        Configuration::updateValue($this->name.'np_slider', 3);




        $languages = Language::getLanguages(false);
    	foreach ($languages as $language){
    		$i = $language['id_lang'];

    		$rssname = Configuration::get('PS_SHOP_NAME');
    		Configuration::updateValue($this->name.'rssname_'.$i, $rssname);
			$rssdesc = Configuration::get('PS_SHOP_NAME');
			Configuration::updateValue($this->name.'rssdesc_'.$i, $rssdesc);
		}




        $this->createAdminTabs15();


		if (!$this->registerHook('leftColumn')
			|| !$this->registerHook('rightColumn')
			|| !$this->registerHook('Header')
			|| !$this->registerHook('home')
			|| !$this->registerHook('Footer')
			|| !$this->_installDatabaseTable()
            || !$this->createLikePostTable()

            OR !$this->registerHook('ModuleRoutes')

            OR !($this->_is_cloud? true : $this->_createFolderAndSetPermissions())
            OR !$this->registerHook('DisplayBackOfficeHeader')
            OR !$this->registerHook('newsSPM')
			 )
			return false;

		return true;
	}

    public function hookModuleRoutes()
    {
        return array(

            ## news ##
            'spmblocknewsadv-news-id' => array(
                'controller' =>	null,
                'rule' =>		'{controller}/{id}',
                'keywords' => array(
                    'id'		=>	array('regexp' => '[a-zA-Z0-9-_]+','param'=>'id'),
                    'controller'	=>	array('regexp' => 'news', 'param' => 'controller')
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'spmblocknewsadv'
                )
            ),



            'spmblocknewsadv-news' => array(
                'controller' =>	null,
                'rule' =>		'{controller}',
                'keywords' => array(
                    'controller'	=>	array('regexp' => 'news', 'param' => 'controller')
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'spmblocknewsadv'
                )
            ),



            ## news ##


        );
    }

    public function hookDisplayBackOfficeHeader()
    {



        ob_start();
        include(_PS_MODULE_DIR_.$this->name .'/views/templates/hooks/hookDisplayBackOfficeHeader.phtml');
        $css = ob_get_clean();




        return $css;
    }

	public function uninstall(){

        Configuration::deleteByName($this->name.'rew_on');

        #### posts api ###
        $languages = Language::getLanguages(false);
        foreach ($languages as $language){
            $i = $language['id_lang'];

            Configuration::deleteByName($this->name.'twdesc_'.$i);
            Configuration::deleteByName($this->name.'pdesc_'.$i);
            Configuration::deleteByName($this->name.'vkdesc_'.$i);
            Configuration::deleteByName($this->name.'vkdesc_'.$i);
        }

        #### posts api ###

        // only for prestashop 1.3, 1.4, 1.5
        Configuration::deleteByName($this->name.'switch_s');
        // only for prestashop 1.3, 1.4, 1.5


        Configuration::deleteByName($this->name.'block_display_img');


        Configuration::deleteByName($this->name.'leftnews');
        Configuration::deleteByName($this->name.'search_left_n');
        Configuration::deleteByName($this->name.'arch_left_n');

        Configuration::deleteByName($this->name.'rightnews');
        Configuration::deleteByName($this->name.'search_right_n');
        Configuration::deleteByName($this->name.'arch_right_n');

        Configuration::deleteByName($this->name.'footernews');
        Configuration::deleteByName($this->name.'arch_footer_n');
        Configuration::deleteByName($this->name.'search_footer_n');
        Configuration::deleteByName($this->name.'homenews');



        Configuration::deleteByName($this->name.'is_like');
        Configuration::deleteByName($this->name.'is_unlike');


        Configuration::deleteByName($this->name.'n_block_img_w');

        Configuration::deleteByName($this->name.'lists_img_w');


        Configuration::deleteByName($this->name.'item_img_w');
        Configuration::deleteByName($this->name.'is_soc_but');


        Configuration::deleteByName($this->name . 'img_size_rp');
        Configuration::deleteByName($this->name.'item_rp_tr');

        Configuration::deleteByName($this->name.'rp_img_width');


        Configuration::deleteByName($this->name.'b_display_date');
        Configuration::deleteByName($this->name.'l_display_date');
        Configuration::deleteByName($this->name.'i_display_date');



        Configuration::deleteByName($this->name.'number_ni');
        Configuration::deleteByName($this->name.'news_page');

        Configuration::deleteByName($this->name.'hnumber_ni');
        Configuration::deleteByName($this->name.'items_w_h');
        Configuration::deleteByName($this->name.'item_h_tr');
        Configuration::deleteByName($this->name.'h_display_date');

        Configuration::deleteByName($this->name.'is_comments');
        Configuration::deleteByName($this->name.'number_fc');




        Configuration::deleteByName($this->name.'rsson');
        Configuration::deleteByName($this->name.'number_rssitems');



        $languages = Language::getLanguages(false);
        foreach ($languages as $language){
            $i = $language['id_lang'];

            Configuration::deleteByName($this->name.'rssname_'.$i);
            Configuration::deleteByName($this->name.'rssdesc_'.$i);
        }

        $this->uninstallTab15();

		if (!parent::uninstall() || !$this->uninstallTable()) return false;

		return true;
	}

	private function uninstallTable() {
		Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmblocknewsadv');
		Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmblocknewsadv_data');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmblocknewsadv_news_like');

		return true;
	}



	private function _installDatabaseTable(){

        if(Module::isInstalled('blocknewsadv')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'blocknewsadv TO '._DB_PREFIX_.'spmblocknewsadv';
            if (!Db::getInstance()->Execute($sql))
                return false;

            $sql = 'RENAME TABLE '._DB_PREFIX_.'blocknewsadv_data TO '._DB_PREFIX_.'spmblocknewsadv_data';
            if (!Db::getInstance()->Execute($sql))
                return false;


        } else {

            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmblocknewsadv` (
							  `id` int(11) NOT NULL auto_increment,
							  `img` text,
							  `status` int(11) NOT NULL default \'1\',
							  `id_shop` varchar(1024) NOT NULL default \'0\',
							  `related_products` varchar(1024) NOT NULL default \'0\',
					  		  `related_posts` varchar(1024) NOT NULL default \'0\',
					  		  `is_comments` int(11) NOT NULL default \'1\',
							  `time_add` timestamp NOT NULL default CURRENT_TIMESTAMP,
							  PRIMARY KEY  (`id`)
							) ENGINE=' . (defined('_MYSQL_ENGINE_') ? _MYSQL_ENGINE_ : "MyISAM") . ' DEFAULT CHARSET=utf8;';
            if (!Db::getInstance()->Execute($sql))
                return false;

            $query = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmblocknewsadv_data` (
							  `id_item` int(11) NOT NULL,
							  `id_lang` int(11) NOT NULL,
							  `seo_description` text,
							  `seo_keywords` varchar(5000) default NULL,
							  `seo_url` varchar(5000) default NULL,
							  `title` varchar(5000) NOT NULL,
							  `content` text NOT NULL,

							  KEY `id_item` (`id_item`)
							) ENGINE=' . (defined('_MYSQL_ENGINE_') ? _MYSQL_ENGINE_ : "MyISAM") . ' DEFAULT CHARSET=utf8';
            if (!Db::getInstance()->Execute($query))
                return false;


        }
        return true;

	}

    public function createLikePostTable()
        {

            if(Module::isInstalled('blocknewsadv')){
                $sql = 'RENAME TABLE '._DB_PREFIX_.'blocknewsadv_news_like TO '._DB_PREFIX_.'spmblocknewsadv_news_like';
                if (!Db::getInstance()->Execute($sql))
                    return false;

            } else {
                $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmblocknewsadv_news_like` (
					  `id` int(11) NOT NULL auto_increment,
					  `news_id` int(11) NOT NULL,
					  `like` int(11) NOT NULL,
					  `ip` varchar(255) default NULL,
					  `time_add` timestamp NOT NULL default CURRENT_TIMESTAMP,
					  PRIMARY KEY  (`id`)
					) ENGINE=' . (defined('_MYSQL_ENGINE_') ? _MYSQL_ENGINE_ : "MyISAM") . ' DEFAULT CHARSET=utf8;';
                if (!Db::getInstance()->Execute($sql))
                    return false;

            }


        return true;
    }

	private function _createFolderAndSetPermissions(){

        $prev_cwd = getcwd();

        $module_dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "upload" . DIRECTORY_SEPARATOR;
        @chdir($module_dir);


        if(Module::isInstalled('blocknewsadv')){

            //folder items
            $module_dir_img_old = $module_dir . "blocknewsadv";
            $module_dir_img_new = $module_dir . "spmblocknewsadv";

            @rename($module_dir_img_old, $module_dir_img_new);


        } else {

            //folder items
            $module_dir_img = $module_dir . "spmblocknewsadv" . DIRECTORY_SEPARATOR;
            @mkdir($module_dir_img, 0777);


        }

        @chdir($prev_cwd);

		return true;
	}



    public function createAdminTabs15(){


        Tools::copy(dirname(__FILE__)."/views/img/AdminSpmblocknewsadv.gif",_PS_ROOT_DIR_."/img/t/AdminSpmblocknewsadv.gif");

        $langs = Language::getLanguages();


        $tab0 = new Tab();
        $tab0->class_name = "AdminSpmblocknewsadv";
        $tab0->module = $this->name;
        $tab0->id_parent = 0;
        foreach($langs as $l){
            $tab0->name[$l['id_lang']] = $this->l('News');
        }
        $tab0->save();
        $main_tab_id = $tab0->id;

        unset($tab0);

        $tab1 = new Tab();
        $tab1->class_name = "AdminSpmblocknewsadvnews";
        $tab1->module = $this->name;
        $tab1->id_parent = $main_tab_id;
        foreach($langs as $l){
            $tab1->name[$l['id_lang']] = $this->l('Moderate News');
        }
        $tab1->save();


        unset($tab1);


        $tab_ajax = new Tab();
        $tab_ajax->module = $this->name;
        $tab_ajax->active = 0;
        $tab_ajax->class_name = 'AdminSpmblocknewsadvajax';
        $tab_ajax->id_parent = (int)Tab::getIdFromClassName($this->name);
        foreach (Language::getLanguages(true) as $lang)
            $tab_ajax->name[$lang['id_lang']] = 'Spmblocknewsadvajax';
        $tab_ajax->add();

    }

    private function uninstallTab15(){


        $tab_id = Tab::getIdFromClassName("AdminSpmblocknewsadv");
        if($tab_id){
            $tab = new Tab($tab_id);
            $tab->delete();
        }

        $tab_id = Tab::getIdFromClassName("AdminSpmblocknewsadvNews");
        if($tab_id){
            $tab = new Tab($tab_id);
            $tab->delete();
        }


        $tab_id = Tab::getIdFromClassName("AdminSpmblocknewsadvajax");
        if($tab_id){
            $tab = new Tab($tab_id);
            $tab->delete();
        }

        @unlink(_PS_ROOT_DIR_."/img/t/AdminSpmblocknewsadv.gif");
    }

	public function hookHeader($params){
    	$smarty = $this->context->smarty;

        $this->context->controller->addCSS(($this->_path).'views/css/font-custom.min.css', 'all');
    	$this->context->controller->addCSS(($this->_path).'views/css/spmblocknewsadv.css', 'all');
        $this->context->controller->addJS($this->_path.'views/js/'.$this->name.'.js');




        ### owl carousel ###
        $news_h = Configuration::get($this->name . 'news_h');
        $bnews_slider = Configuration::get($this->name . 'bnews_slider');
        $relpr_slider = Configuration::get($this->name . 'relpr_slider');
        $relp_slider = Configuration::get($this->name . 'relp_slider');


        if ($news_h == 3 || $bnews_slider == 1 || $relpr_slider == 1 || $relp_slider == 1) {

            $this->context->controller->addJs(__PS_BASE_URI__ . 'modules/' . $this->name . '/views/js/owl.carousel.js');
            $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $this->name . '/views/css/owl.carousel.css');
            $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $this->name . '/views/css/owl.theme.default.css');
        }
        ### owl carousel ###


        if (version_compare(_PS_VERSION_, '1.7', '>'))
            $this->context->controller->addCSS(($this->_path) . 'views/css/'.$this->name.'17.css', 'all');



        $name_template = "head.tpl";
        $cache_id = $this->name.'|'.$name_template;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

            $smarty->assign($this->name.'is16', $this->_is16);
            $smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));

            $smarty->assign($this->name.'appid', Configuration::get($this->name.'appid'));
            $smarty->assign($this->name.'appadmin', Configuration::get($this->name.'appadmin'));


            $smarty->assign($this->name.'is15', $this->_is15);


            $item_id = Tools::getValue('id');
            $is_news_page = 0;
            if($item_id && stripos($_SERVER['REQUEST_URI'],"news")){
                $is_news_page = 1;


                include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmblocknewsadvfunctions.class.php');
                $obj_spmblocknewsadvfunctions = new spmblocknewsadvfunctions();

                $item_id = $obj_spmblocknewsadvfunctions->getTransformSEOURLtoID(array('id'=>$item_id));

                $_info_cat = $obj_spmblocknewsadvfunctions->getItem(array('id' => $item_id,'site'=>1));

                $name = isset($_info_cat['item'][0]['title'])?$_info_cat['item'][0]['title']:'';
                $img = isset($_info_cat['item'][0]['img_orig'])?$_info_cat['item'][0]['img_orig']:'';
                $news_seo_url = isset($_info_cat['item'][0]['seo_url'])?$_info_cat['item'][0]['seo_url']:'';
                $news_id = isset($_info_cat['item'][0]['id'])?$_info_cat['item'][0]['id']:'';

                $smarty->assign($this->name.'name', $name);
                $smarty->assign($this->name.'img', $img);

                $smarty->assign($this->name.'news_seo_url', $news_seo_url);
                $smarty->assign($this->name.'news_id', $news_id);
                $smarty->assign($this->name.'rew_on', Configuration::get($this->name.'rew_on'));
            }

            $this->setSEOUrls();

            $smarty->assign($this->name.'is_news', $is_news_page);


        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
    }


    public function setSEOUrls(){
        $smarty = $this->context->smarty;
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmblocknewsadvfunctions.class.php');
        $obj_spmblocknewsadvfunctions = new spmblocknewsadvfunctions();

        $data_url = $obj_spmblocknewsadvfunctions->getSEOURLs();
        $news_url = $data_url['news_url'];
        $news_item_url = $data_url['news_item_url'];
        $process_url = $data_url['process_url'];
        $rss_url = $data_url['rss_url'];

        $smarty->assign(
            array(
                $this->name.'news_url' => $news_url,
                $this->name.'news_item_url' => $news_item_url,
                $this->name.'process_url' => $process_url,
                $this->name.'rss_url' => $rss_url,
            )
        );

        $smarty->assign($this->name.'pic', $this->path_img_cloud);
        $smarty->assign($this->name.'is_ps15', $this->_is15);

        $smarty->assign($this->name.'is_like', Configuration::get($this->name.'is_like'));
        $smarty->assign($this->name.'is_unlike', Configuration::get($this->name.'is_unlike'));

        $smarty->assign($this->name.'rew_on', Configuration::get($this->name.'rew_on'));
        $smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));

        $smarty->assign($this->name.'b_display_date', Configuration::get($this->name.'b_display_date'));
        $smarty->assign($this->name.'block_display_img', Configuration::get($this->name.'block_display_img'));

        $smarty->assign($this->name.'bnews_slider', Configuration::get($this->name.'bnews_slider'));
        $smarty->assign($this->name.'bnews_sl', Configuration::get($this->name.'bnews_sl'));

        $smarty->assign($this->name.'relpr_slider', Configuration::get($this->name.'relpr_slider'));
        $smarty->assign($this->name.'npr_slider', Configuration::get($this->name.'npr_slider'));


        $smarty->assign($this->name.'relp_slider', Configuration::get($this->name.'relp_slider'));
        $smarty->assign($this->name.'np_slider', Configuration::get($this->name.'np_slider'));



    }

	public function hookLeftColumn($params)
	{

        $name_template = "left.tpl";
        $cache_id = $this->name.'|'.$name_template;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

            $smarty = $this->context->smarty;

            $smarty->assign($this->name.'is16', $this->_is16);
            include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmblocknewsadvfunctions.class.php');
            $obj_blocknewshelp = new spmblocknewsadvfunctions();
            $_data = $obj_blocknewshelp->getItemsBlock();
            $_data_arch = $obj_blocknewshelp->getArchives();

            $smarty->assign(array($this->name.'itemsblock' => $_data['items'],
                    $this->name.'arch' => $_data_arch['items'],
                )
            );

            $smarty->assign($this->name.'leftnews', Configuration::get($this->name.'leftnews'));
            $smarty->assign($this->name.'arch_left_n', Configuration::get($this->name.'arch_left_n'));
            $smarty->assign($this->name.'search_left_n', Configuration::get($this->name.'search_left_n'));


            $this->setSEOUrls();



        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));

	}

	public function hookRightColumn($params)
	{
        $name_template = "right.tpl";
        $cache_id = $this->name.'|'.$name_template;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {
            $smarty = $this->context->smarty;
            $smarty->assign($this->name.'is16', $this->_is16);
            include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmblocknewsadvfunctions.class.php');
            $obj_blocknewshelp = new spmblocknewsadvfunctions();
            $_data = $obj_blocknewshelp->getItemsBlock();
            $_data_arch = $obj_blocknewshelp->getArchives();

            $smarty->assign(array($this->name.'itemsblock' => $_data['items'],
                                 $this->name.'arch' => $_data_arch['items'],
                )
            );

            $smarty->assign($this->name.'rightnews', Configuration::get($this->name.'rightnews'));
            $smarty->assign($this->name.'arch_right_n', Configuration::get($this->name.'arch_right_n'));
            $smarty->assign($this->name.'search_right_n', Configuration::get($this->name.'search_right_n'));




            $this->setSEOUrls();

        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
	}

    public function hooknewsSPM($params)
    {

        $name_template = "newsSPM.tpl";
        $cache_id = $this->name.'|'.$name_template;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {
            $smarty = $this->context->smarty;
            $cookie = $this->context->cookie;
            $smarty->assign($this->name.'is16', $this->_is16);
            include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmblocknewsadvfunctions.class.php');
            $obj = new spmblocknewsadvfunctions();

            $_data_arch = $obj->getArchives();

            $smarty->assign(array(
                    $this->name.'arch' => $_data_arch['items'],
                )
            );



            $data = $obj->getHomeLastNews();
            $current_language = (int)$cookie->id_lang;
            // strip tags for content
            foreach($data['items'] as $_k => $_item){
                if($current_language == @$_item['data'][$_k]['id_lang']){
                    $data['items'][$_k]['data'][$current_language]['content'] = strip_tags($_item['data'][$_k]['content']);
                }
            }
            $smarty->assign(array($this->name.'items_home' => $data['items']));




            $smarty->assign($this->name.'item_h_tr', Configuration::get($this->name.'item_h_tr'));
            $smarty->assign($this->name.'h_display_date', Configuration::get($this->name.'h_display_date'));

            $smarty->assign($this->name.'news_m', Configuration::get($this->name.'news_m'));
            $smarty->assign($this->name.'arch_m', Configuration::get($this->name.'arch_m'));
            $smarty->assign($this->name.'search_m', Configuration::get($this->name.'search_m'));




            $this->setSEOUrls();


        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
    }

	public function hookHome($params){
		if(Configuration::get($this->name.'homenews') == 1){

            $name_template = "home.tpl";
            $cache_id = $this->name.'|'.$name_template;

            if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

                $smarty = $this->context->smarty;
                $cookie = $this->context->cookie;
                $smarty->assign($this->name.'is16', $this->_is16);
                include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmblocknewsadvfunctions.class.php');
                $obj = new spmblocknewsadvfunctions();

                $data = $obj->getHomeLastNews();
                $current_language = (int)$cookie->id_lang;
                // strip tags for content
                foreach($data['items'] as $_k => $_item){
                    if($current_language == @$_item['data'][$_k]['id_lang']){
                    $data['items'][$_k]['data'][$current_language]['content'] = strip_tags($_item['data'][$_k]['content']);
                    }
                }
                $smarty->assign(array($this->name.'items_home' => $data['items']));


                $smarty->assign($this->name.'item_h_tr', Configuration::get($this->name.'item_h_tr'));
                $smarty->assign($this->name.'h_display_date', Configuration::get($this->name.'h_display_date'));


                $smarty->assign($this->name.'news_h', Configuration::get($this->name.'news_h'));
                $smarty->assign($this->name.'news_bp_sl', Configuration::get($this->name.'news_bp_sl'));


                $this->setSEOUrls();


            }
            return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
		}

	}

	public function hookFooter($params)
	{
        $name_template = "footer.tpl";
        $cache_id = $this->name.'|'.$name_template;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

            $smarty = $this->context->smarty;

            $smarty->assign($this->name.'footernews', Configuration::get($this->name.'footernews'));

            $smarty->assign($this->name.'is16', $this->_is16);
            $smarty = $this->context->smarty;
            include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmblocknewsadvfunctions.class.php');
            $obj_blocknewshelp = new spmblocknewsadvfunctions();
            $_data = $obj_blocknewshelp->getItemsBlock();
            $_data_arch = $obj_blocknewshelp->getArchives();

            $smarty->assign(array($this->name.'itemsblock' => $_data['items'],
                                  $this->name.'arch' => $_data_arch['items'],
                                  )
                            );

            $smarty->assign($this->name.'leftnews', Configuration::get($this->name.'leftnews'));
            $smarty->assign($this->name.'arch_footer_n', Configuration::get($this->name.'arch_footer_n'));
            $smarty->assign($this->name.'search_footer_n', Configuration::get($this->name.'search_footer_n'));

            $this->setSEOUrls();



        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));

	}


    protected function addBackOfficeMedia()
    {
        $this->context->controller->addCSS($this->_path.'views/css/font-custom.min.css');
        //CSS files
        $this->context->controller->addCSS($this->_path.'views/css/menu16.css');

        // JS files
        $this->context->controller->addJs($this->_path.'views/js/menu16.js');


    }

	private $_html;

    public function getCurrentShopCustom(){
        $current_shop_id = Shop::getContextShopID();
        return $current_shop_id;

    }

	public function getContent()
    {
    	$currentIndex = $this->context->currentindex;
    	$cookie = $this->context->cookie;
    	include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmblocknewsadvfunctions.class.php');
    	$spmblocknewsadvfunctions_obj = new spmblocknewsadvfunctions();

    	$_html = '';
        $errors = array();


    	$this->addBackOfficeMedia();


    	#### posts api ###
    	$vkset = Tools::getValue("vkset");
        if (Tools::strlen($vkset)>0) {

            ob_start();
            $number_tab = 8;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();


        }
    	if (Tools::isSubmit('psvkpostsettings'))
        {
        	Configuration::updateValue($this->name.'vkpost_on', Tools::getValue('vkpost_on'));

        	$languages = Language::getLanguages(false);
        	foreach ($languages as $language){
    			$i = $language['id_lang'];
        		Configuration::updateValue($this->name.'vkdesc_'.$i, Tools::getValue('vkdesc_'.$i));
        	}

            // clear cache //
            $this->clearSmartyCacheItems();
            // clear cache //

        	$url = $currentIndex.'&conf=6&tab=AdminModules&vkset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }



    	$twset = Tools::getValue("twset");
        if (Tools::strlen($twset)>0) {

            ob_start();
            $number_tab = 8;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();

        }
    	if (Tools::isSubmit('pstwitterpostsettings'))
        {

            $array_wall_posts_items = array('tw','i','p');

            foreach($array_wall_posts_items as $wall_post_item) {
                Configuration::updateValue($this->name . $wall_post_item.'post_on', Tools::getValue($wall_post_item.'post_on'));


                $languages = Language::getLanguages(false);
                foreach ($languages as $language) {
                    $i = $language['id_lang'];
                    Configuration::updateValue($this->name . $wall_post_item.'desc_' . $i, Tools::getValue($wall_post_item.'desc_' . $i));
                }
            }

            // clear cache //
            $this->clearSmartyCacheItems();
            // clear cache //

            $url = $currentIndex.'&conf=6&tab=AdminModules&twset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
            Tools::redirectAdmin($url);
        }
    	#### posts api ###





    	// url rewriting settings
    	$urlrewriting_settingsset = Tools::getValue("urlrewriting_settingsset");
   	    if (Tools::strlen($urlrewriting_settingsset)>0) {
        	ob_start();
            $number_tab = 5;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }
    	if (Tools::isSubmit('submit_urlrewriting'))
        {
        	Configuration::updateValue($this->name.'rew_on', Tools::getValue('rew_on'));

            // clear cache //
            $this->clearSmartyCacheItems();
            // clear cache //

        	$url = $currentIndex.'&conf=6&tab=AdminModules&urlrewriting_settingsset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }
        // url rewriting settings


        // sitemap
    	$submitsitemap_settingsset = Tools::getValue("submitsitemap_settingsset");
   	    if (Tools::strlen($submitsitemap_settingsset)>0) {

            ob_start();
            $number_tab = 5;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();


        }
    	if (Tools::isSubmit('submitsitemap'))
        {
        	$spmblocknewsadvfunctions_obj->generateSitemap();

            // clear cache //
            $this->clearSmartyCacheItems();
            // clear cache //

        	$url = $currentIndex.'&conf=6&tab=AdminModules&submitsitemap_settingsset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }
        // sitemap



    	// news settings
   	    $news_settingsset = Tools::getValue("news_settingsset");
   	    if (Tools::strlen($news_settingsset)>0) {

            ob_start();
            $number_tab = 3;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('news_settings'))
        {

        	Configuration::updateValue($this->name.'switch_s', Tools::getValue('switch_s'));

            Configuration::updateValue($this->name.'block_display_img', Tools::getValue('block_display_img'));


        	Configuration::updateValue($this->name.'leftnews', Tools::getValue('leftnews'));
        	Configuration::updateValue($this->name.'rightnews', Tools::getValue('rightnews'));
        	Configuration::updateValue($this->name.'homenews', Tools::getValue('homenews'));
        	Configuration::updateValue($this->name.'footernews', Tools::getValue('footernews'));
            Configuration::updateValue($this->name.'news_m', Tools::getValue('news_m'));


            Configuration::updateValue($this->name.'arch_left_n', Tools::getValue('arch_left_n'));
            Configuration::updateValue($this->name.'arch_right_n', Tools::getValue('arch_right_n'));
            Configuration::updateValue($this->name.'arch_footer_n', Tools::getValue('arch_footer_n'));
            Configuration::updateValue($this->name.'arch_m', Tools::getValue('arch_m'));


            Configuration::updateValue($this->name.'search_footer_n', Tools::getValue('search_footer_n'));
            Configuration::updateValue($this->name.'search_left_n', Tools::getValue('search_left_n'));
            Configuration::updateValue($this->name.'search_right_n', Tools::getValue('search_right_n'));
            Configuration::updateValue($this->name.'search_m', Tools::getValue('search_m'));

            Configuration::updateValue($this->name.'is_like', Tools::getValue('is_like'));
            Configuration::updateValue($this->name.'is_unlike', Tools::getValue('is_unlike'));

            if(!ctype_digit(Tools::getValue('n_block_img_w')) || Tools::getValue('n_block_img_w') == NULL) {
                $errors[] = $this->l('Image width in the block "Last News"').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'n_block_img_w', Tools::getValue('n_block_img_w'));
            }


            if(!ctype_digit(Tools::getValue('lists_img_w')) || Tools::getValue('lists_img_w') == NULL) {
                $errors[] = $this->l('Image width in the lists items').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'lists_img_w', Tools::getValue('lists_img_w'));
            }


            Configuration::updateValue($this->name.'is_soc_but', Tools::getValue('is_soc_but'));

            Configuration::updateValue($this->name.'img_size_rp', Tools::getValue('img_size_rp'));


            if(!ctype_digit(Tools::getValue('item_rp_tr')) || Tools::getValue('item_rp_tr') == NULL) {
                $errors[] = $this->l('Truncate product description').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'item_rp_tr', Tools::getValue('item_rp_tr'));
            }




            if(!ctype_digit(Tools::getValue('rp_img_width')) || Tools::getValue('rp_img_width') == NULL) {
                $errors[] = $this->l('Image width in the related news block on the news page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'rp_img_width', Tools::getValue('rp_img_width'));
            }





            if(!ctype_digit(Tools::getValue('item_img_w')) || Tools::getValue('item_img_w') == NULL) {
                $errors[] = $this->l('Image width on the news page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'item_img_w', Tools::getValue('item_img_w'));
            }



    		Configuration::updateValue($this->name.'b_display_date', Tools::getValue('b_display_date'));
			Configuration::updateValue($this->name.'l_display_date', Tools::getValue('l_display_date'));
			Configuration::updateValue($this->name.'i_display_date', Tools::getValue('i_display_date'));


            if(!ctype_digit(Tools::getValue('number_ni')) || Tools::getValue('number_ni') == NULL) {
                $errors[] = $this->l('Number of items in the Block Last News').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'number_ni', Tools::getValue('number_ni'));
            }


            if(!ctype_digit(Tools::getValue('news_page')) || Tools::getValue('news_page') == NULL) {
                $errors[] = $this->l('News per Page in the list view').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'news_page', Tools::getValue('news_page'));
            }



            if(!ctype_digit(Tools::getValue('hnumber_ni')) || Tools::getValue('hnumber_ni') == NULL) {
                $errors[] = $this->l('Number of items in Block Last News on Home page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'hnumber_ni', Tools::getValue('hnumber_ni'));
            }


            if(!ctype_digit(Tools::getValue('items_w_h')) || Tools::getValue('items_w_h') == NULL) {
                $errors[] = $this->l('Image width in the block "Latest News" on home page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'items_w_h', Tools::getValue('items_w_h'));
            }

            if(!ctype_digit(Tools::getValue('item_h_tr')) || Tools::getValue('item_h_tr') == NULL) {
                $errors[] = $this->l('Truncate text in the block "Latest News" on home page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'item_h_tr', Tools::getValue('item_h_tr'));
            }


            Configuration::updateValue($this->name.'h_display_date', Tools::getValue('h_display_date'));


            Configuration::updateValue($this->name.'news_h', Tools::getValue('news_h'));

            if(!ctype_digit(Tools::getValue('news_bp_sl')) || Tools::getValue('news_bp_sl') == NULL) {
                $errors[] = $this->l('Displayed number of items in the slider on the home page in the block "Latest News"').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'news_bp_sl', Tools::getValue('news_bp_sl'));
            }



            Configuration::updateValue($this->name.'bnews_slider', Tools::getValue('bnews_slider'));

            if(!ctype_digit(Tools::getValue('bnews_sl')) || Tools::getValue('bnews_sl') == NULL) {
                $errors[] = $this->l('Displayed number of items in the slider in the block "Last News"').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'bnews_sl', Tools::getValue('bnews_sl'));
            }


            Configuration::updateValue($this->name.'is_comments', Tools::getValue('is_comments'));


            if(!ctype_digit(Tools::getValue('number_fc')) || Tools::getValue('number_fc') == NULL) {
                $errors[] = $this->l('Numbers of comments visible').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'number_fc', Tools::getValue('number_fc'));
            }



            Configuration::updateValue($this->name.'appid', Tools::getValue('appid'));
            Configuration::updateValue($this->name.'appadmin', Tools::getValue('appadmin'));


            Configuration::updateValue($this->name.'relpr_slider', Tools::getValue('relpr_slider'));


            if(!ctype_digit(Tools::getValue('npr_slider')) || Tools::getValue('npr_slider') == NULL) {
                $errors[] = $this->l('Number Products in the Related Products slider').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'npr_slider', Tools::getValue('npr_slider'));
            }



            Configuration::updateValue($this->name.'relp_slider', Tools::getValue('relp_slider'));


            if(!ctype_digit(Tools::getValue('np_slider')) || Tools::getValue('np_slider') == NULL) {
                $errors[] = $this->l('Number News in the Related News slider').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'np_slider', Tools::getValue('np_slider'));
            }


            Configuration::updateValue($this->name.'rsson', Tools::getValue('rsson'));

            if(!ctype_digit(Tools::getValue('number_rssitems')) || Tools::getValue('number_rssitems') == NULL) {
                $errors[] = $this->l('Number of items in RSS Feed').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'number_rssitems', Tools::getValue('number_rssitems'));
            }




        	$languages = Language::getLanguages(false);
        	foreach ($languages as $language){
    			$i = $language['id_lang'];
        		Configuration::updateValue($this->name.'rssname_'.$i, Tools::getValue('rssname_'.$i));
        		Configuration::updateValue($this->name.'rssdesc_'.$i, Tools::getValue('rssdesc_'.$i));
        	}

            if(count($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //

                $url = $currentIndex . '&conf=6&tab=AdminModules&news_settingsset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {

                ob_start();
                $number_tab = 3;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();

            }
        }
        // news settings




        $_html .= $this->_displayForm16(array('errors'=>$errors));

        return $_html;
    }

    private function _error($data){
        $text = $data['text'];
        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();
        return $_html;
    }

    private function _displayForm16($data = null){
        $_html = '';

        $errors = $data['errors'];
        if(count($errors)>0) {
            foreach ($errors as $error_text) {
                $_html .= $this->_error(array('text' => $error_text));
            }
        }

        /*ob_start();
        include_once(_PS_MODULE_DIR_.$this->name.'/views/templates/hooks/promo.phtml');
        $_html .= ob_get_clean();*/


        $this->_data_translate_custom['_displayForm16_welcome'] = $this->l('Welcome');
        $this->_data_translate_custom['_displayForm16_news_set'] = $this->l('News Settings');
        $this->_data_translate_custom['_displayForm16_seo_url_sitemap'] = $this->l('SEO URL Rewriting, Sitemap');
        $this->_data_translate_custom['_displayForm16_auto_post_int'] = $this->l('Automatically posts Integration');

        $this->_data_translate_custom['_displayForm16_help_doc'] = $this->l('Help / Documentation');
        $this->_data_translate_custom['_displayForm16_mitr_mod'] = $this->l('SPM Modules');


        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html_main = ob_get_clean();




        return $_html;
    }

    private function _newssettings16(){
        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('News Settings'),
                    'icon' => 'fa fa-newspaper-o fa-lg'
                ),
                'input' => array(



                    array(
                        'type' => 'checkbox_custom_blocks',
                        'label' => $this->l('Position Block Last News'),
                        'name' => 'pos_news',
                        'hint' => $this->l('Position Block Last News'),
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'leftnews',
                                    'name' => $this->l('Left'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'rightnews',
                                    'name' => $this->l('Right'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'homenews',
                                    'name' => $this->l('Home'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'footernews',
                                    'name' => $this->l('Footer'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'news_m',
                                    'name' => $this->l('Block Last News in CUSTOM HOOK (newsSPM)').
                                        ' '.$this->_linkAHtml(array('title'=>$this->l('How to configure CUSTOM HOOK?'),'href'=>'javascript:void(0)','onclick'=>'tabs_custom(6)','disable_target_blank'=>1)),
                                    'val' => 1
                                ),

                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),

                    ),


                    array(
                        'type' => 'checkbox_custom_blocks',
                        'label' => $this->l('Position Block News Archives'),
                        'name' => 'pos_news',
                        'hint' => $this->l('Position Block News Archives'),
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'arch_left_n',
                                    'name' => $this->l('Left'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'arch_right_n',
                                    'name' => $this->l('Right'),
                                    'val' => '1'
                                ),

                                array(
                                    'id' => 'arch_footer_n',
                                    'name' => $this->l('Footer'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'arch_m',
                                    'name' => $this->l('Block News Archives in CUSTOM HOOK (newsSPM)').
                                        ' '.$this->_linkAHtml(array('title'=>$this->l('How to configure CUSTOM HOOK?'),'href'=>'javascript:void(0)','onclick'=>'tabs_custom(6)','disable_target_blank'=>1)),
                                    'val' => 1
                                ),

                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),

                    ),


                    array(
                        'type' => 'checkbox_custom_blocks',
                        'label' => $this->l('Position Block Search News'),
                        'name' => 'pos_news',
                        'hint' => $this->l('Position Block Search News'),
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'search_left_n',
                                    'name' => $this->l('Left'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'search_right_n',
                                    'name' => $this->l('Right'),
                                    'val' => '1'
                                ),

                                array(
                                    'id' => 'search_footer_n',
                                    'name' => $this->l('Footer'),
                                    'val' => '1'
                                ),
                                array(
                                    'id' => 'search_m',
                                    'name' => $this->l('Block Search News in CUSTOM HOOK (newsSPM)').
                                        ' '.$this->_linkAHtml(array('title'=>$this->l('How to configure CUSTOM HOOK?'),'href'=>'javascript:void(0)','onclick'=>'tabs_custom(6)','disable_target_blank'=>1)),
                                    'val' => 1
                                ),

                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),

                    ),



                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable Like Functional'),
                        'name' => 'is_like',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable UnLike Functional'),
                        'name' => 'is_unlike',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),


                ),



            ),


        );

        $fields_form1 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('Block "Last News" settings'),
                    'icon' => 'fa fa-list-alt fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable Slider for block "Last News"'),
                        'name' => 'bnews_slider',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Displayed number of items in the slider in the block "Last News"'),
                        'name' => 'bnews_sl',
                        'id' => 'bnews_sl',
                        'lang' => FALSE,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Number of items in the Block Last News'),
                        'name' => 'number_ni',
                        'id' => 'number_ni',
                        'lang' => FALSE,
                    ),

                    array(
                        'type' => 'image_custom_px',
                        'label' => $this->l('Image width in the block "Last News"'),
                        'name' => 'n_block_img_w',
                        'value' => (int)Configuration::get($this->name.'n_block_img_w')
                    ),

                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Display date in the block "Last News"'),
                        'name' => 'b_display_date',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'b_display_date')
                        ),
                    ),

                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Display images in the block "Last News"'),
                        'name' => 'block_display_img',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'block_display_img')
                        ),
                    ),



                ),



            ),


        );


        $fields_form2 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('News in the list view settings'),
                    'icon' => 'fa fa-newspaper-o fa-lg'
                ),
                'input' => array(


                    array(
                        'type' => 'text',
                        'label' => $this->l('News per Page in the list view'),
                        'name' => 'news_page',
                        'id' => 'news_page',
                        'lang' => FALSE,
                    ),

                    array(
                        'type' => 'image_custom_px',
                        'label' => $this->l('Image width in the lists items'),
                        'name' => 'lists_img_w',
                        'value' => (int)Configuration::get($this->name.'lists_img_w')
                    ),

                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Display date in the lists items'),
                        'name' => 'l_display_date',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'l_display_date')
                        ),
                    ),



                ),



            ),


        );

        $fields_form3 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('News page settings'),
                    'icon' => 'fa fa-newspaper-o fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'image_custom_px',
                        'label' => $this->l('Image width on the news page'),
                        'name' => 'item_img_w',
                        'value' => (int)Configuration::get($this->name.'item_img_w')
                    ),

                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Display date on the news page'),
                        'name' => 'i_display_date',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'i_display_date')
                        ),
                    ),
                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Active Social share buttons'),
                        'name' => 'is_soc_but',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'is_soc_but')
                        ),
                    ),


                ),



            ),


        );


        $data_img_sizes = array();

        $available_types = ImageType::getImagesTypes('products');

        foreach ($available_types as $type){

            $id = $type['name'];
            $name = $type['name'].' ('.$type['width'].' x '.$type['height'].')';

            $data_item_size = array(
                'id' => $id,
                'name' => $name,
            );

            array_push($data_img_sizes,$data_item_size);


        }



        $fields_form4 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('Related products on the news page settings'),
                    'icon' => 'fa fa-book fa-lg'
                ),
                'input' => array(



                    array(
                        'type' => 'select',
                        'label' => $this->l('Image size for related products'),
                        'name' => 'img_size_rp',
                        'options' => array(
                            'query' => $data_img_sizes,
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),

                    array(
                        'type' => 'text_truncate',
                        'label' => $this->l('Truncate product description'),
                        'name' => 'item_rp_tr',
                        'id' => 'item_rp_tr',
                        'lang' => FALSE,
                        'value' => (int)Configuration::get($this->name.'item_rp_tr'),
                    ),


                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable Slider for Related Products on the news page'),
                        'name' => 'relpr_slider',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Number Products in the Related Products slider'),
                        'name' => 'npr_slider',
                        'id' => 'npr_slider',
                        'lang' => FALSE,

                    ),



                ),



            ),


        );

        $fields_form5 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('Related News on the news page settings'),
                    'icon' => 'fa fa-list-alt fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'image_custom_px',
                        'label' => $this->l('Image width in the related news block on the news page'),
                        'name' => 'rp_img_width',
                        'value' => (int)Configuration::get($this->name.'rp_img_width')
                    ),


                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable Slider for Related News on the news page'),
                        'name' => 'relp_slider',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Number News in the Related News slider'),
                        'name' => 'np_slider',
                        'id' => 'np_slider',
                        'lang' => FALSE,

                    ),

                ),

            ),


        );

        $admin_img = "";
        $admin_explode = explode(",", Configuration::get($this->name.'appadmin'));
        if(sizeof($admin_explode)>0) {
            foreach ($admin_explode as $admin_explode) {
                if(Tools::strlen($admin_explode)==0) continue;

                include(dirname(__FILE__).'/views/templates/hooks/fb_moderators.phtml');
            }
        }

        $fields_form6 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('Facebook Comments settings'),
                    'icon' => 'fa fa-facebook fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable Facebook comments'),
                        'name' => 'is_comments',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Numbers of comments visible'),
                        'name' => 'number_fc',
                        'id' => 'number_fc',
                        'lang' => FALSE,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Facebook Application Id'),
                        'name' => 'appid',
                        'id' => 'appid',
                        'lang' => FALSE,
                        'desc' => $this->l('To configure the Facebook Application Id read').
                            ' '.$this->_linkAHtml(array('title'=>'Installation_Guid.pdf','href'=>'../modules/'.$this->name.'/Installation_Guid.pdf','disable_target_blank'=>0)).' , '.

                        $this->l('which is located in the folder  with the module.'),
			         ),

                    array(
                        'type' => 'text_fcom',
                        'label' => $this->l('My App'),
                        'name' => 'fcom_text',
                        'id' => 'fcom_text',
                        'lang' => FALSE,
                        'f_appid' => Configuration::get($this->name.'appid'),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Moderators'),
                        'name' => 'appadmin',
                        'id' => 'appadmin',
                        'lang' => FALSE,
                        'desc' => $this->l('Id administrators, separated by commas').'. '.
                            $this->l('Add a facebook accounts ID for beeing comments moderators. ID Can be found on').
                            ' '.$this->_linkAHtml(array('title'=>'http://findmyfbid.com/','href'=>'http://findmyfbid.com/','disable_target_blank'=>0,'br_after_count'=>2)).
                            $admin_img,
                    ),

                    array(
                        'type' => 'text_fcom_set',
                        'label' => $this->l('Moderate Facebook comments'),
                        'name' => 'fcom_text',
                        'id' => 'fcom_text',
                        'lang' => FALSE,
                        'f_appid' => Configuration::get($this->name.'appid'),

                    ),
                ),

            ),


        );

        $fields_form7 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('News on the home page settings'),
                    'icon' => 'fa fa-newspaper-o fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'image_custom_px',
                        'label' => $this->l('Image width in the block "Latest News" on home page'),
                        'name' => 'items_w_h',
                        'value' => (int)Configuration::get($this->name.'items_w_h')
                    ),

                    array(
                        'type' => 'text_truncate',
                        'label' => $this->l('Truncate text in the block "Latest News" on home page'),
                        'name' => 'item_h_tr',
                        'id' => 'item_h_tr',
                        'lang' => FALSE,
                        'value' => (int)Configuration::get($this->name.'item_h_tr'),
                    ),

                    array(
                        'type' => 'select',
                        'label' => $this->l('"Latest News" on the Home Page'),
                        'name' => 'news_h',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => 1,
                                    'name' => $this->l('Horizontal view News on the home page')),

                                array(
                                    'id' => 2,
                                    'name' => $this->l('News on home page in Blocks'),
                                ),

                                array(
                                    'id' => 3,
                                    'name' => $this->l('Slider for News on the home page'),
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),

                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Displayed number of items in the slider on the home page in the block "Latest News"'),
                        'name' => 'news_bp_sl',
                        'id' => 'news_bp_sl',
                        'lang' => FALSE,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Number of items in Block Last News on Home page'),
                        'name' => 'hnumber_ni',
                        'id' => 'hnumber_ni',
                        'lang' => FALSE,
                    ),

                    array(
                        'type' => 'checkbox_custom',
                        'label' => $this->l('Display date in Block Last News on Home page'),
                        'name' => 'h_display_date',
                        'values' => array(
                            'value' => (int)Configuration::get($this->name.'h_display_date')
                        ),
                    ),

                  ),



            ),


        );

        $fields_form8 = array(
            'form'=> array(

                'legend' => array(
                    'title' => $this->l('RSS Feed'),
                    'icon' => 'fa fa-rss fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable RSS Feed'),
                        'name' => 'rsson',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Title of your RSS Feed'),
                        'name' => 'rssname',
                        'id' => 'rssname',
                        'lang' => TRUE,
                        //'required' => TRUE,
                        'size' => 50,
                        //'maxlength' => 50,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Description of your RSS Feed'),
                        'name' => 'rssdesc',
                        'id' => 'rssdesc',
                        'lang' => TRUE,
                        //'required' => TRUE,
                        'size' => 50,
                        //'maxlength' => 50,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $this->l('Number of items in RSS Feed'),
                        'name' => 'number_rssitems',
                        'class' => ' fixed-width-sm',

                    ),


                ),



            ),


        );

        $fields_form9 = array(
            'form' => array(


                'submit' => array(
                    'title' => $this->l('Update Settings'),
                )
            ),
        );




        $helper = new HelperForm();



        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'news_settings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValuesNewsSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );


        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'_js.phtml');
        $_html = ob_get_clean();



        return  $_html.$helper->generateForm(array($fields_form,$fields_form1,$fields_form2,$fields_form3,$fields_form4,$fields_form5,$fields_form6,$fields_form7,$fields_form8,$fields_form9));
    }

    public function getConfigFieldsValuesNewsSettings(){

        $languages = Language::getLanguages(false);
        $fields_rssname = array();
        $fields_rssdesc = array();

        foreach ($languages as $lang)
        {
            $fields_rssname[$lang['id_lang']] =  Configuration::get($this->name.'rssname_'.$lang['id_lang']);

            $fields_rssdesc[$lang['id_lang']] =  Configuration::get($this->name.'rssdesc_'.$lang['id_lang']);
        }


        $data_config = array(
            'switch_s' => (int)Configuration::get($this->name.'switch_s'),

            'leftnews' => Configuration::get($this->name.'leftnews'),
            'rightnews' => Configuration::get($this->name.'rightnews'),
            'homenews' => Configuration::get($this->name.'homenews'),
            'footernews' => Configuration::get($this->name.'footernews'),
            'news_m' => Configuration::get($this->name.'news_m'),

            'arch_left_n' => Configuration::get($this->name.'arch_left_n'),
            'arch_right_n' => Configuration::get($this->name.'arch_right_n'),
            'arch_footer_n' => Configuration::get($this->name.'arch_footer_n'),
            'arch_m' => Configuration::get($this->name.'arch_m'),

            'search_left_n' => Configuration::get($this->name.'search_left_n'),
            'search_right_n' => Configuration::get($this->name.'search_right_n'),
            'search_footer_n' => Configuration::get($this->name.'search_footer_n'),
            'search_m' => Configuration::get($this->name.'search_m'),

            'is_like' => (int)Configuration::get($this->name.'is_like'),
            'is_unlike' => (int)Configuration::get($this->name.'is_unlike'),

            'number_ni' => (int)Configuration::get($this->name.'number_ni'),

            'news_page' => (int)Configuration::get($this->name.'news_page'),

            'hnumber_ni' => (int)Configuration::get($this->name.'hnumber_ni'),

            'news_h' => (int)Configuration::get($this->name.'news_h'),
            'news_bp_sl' => (int)Configuration::get($this->name.'news_bp_sl'),

            'bnews_slider' => (int)Configuration::get($this->name.'bnews_slider'),
            'bnews_sl' => (int)Configuration::get($this->name.'bnews_sl'),


            'rsson' => Configuration::get($this->name.'rsson'),
            'rssname' => $fields_rssname,
            'rssdesc' => $fields_rssdesc,
            'number_rssitems' => (int)Configuration::get($this->name.'number_rssitems'),

            'img_size_rp' => Configuration::get($this->name.'img_size_rp'),

            'is_comments' => Configuration::get($this->name.'is_comments'),
            'number_fc' => Configuration::get($this->name.'number_fc'),
            'appid' => Configuration::get($this->name.'appid'),
            'appadmin' => Configuration::get($this->name.'appadmin'),


            'relpr_slider' => Configuration::get($this->name.'relpr_slider'),
            'npr_slider' => Configuration::get($this->name.'npr_slider'),

            'relp_slider' => Configuration::get($this->name.'relp_slider'),
            'np_slider' => Configuration::get($this->name.'np_slider'),

        );

        return $data_config;

    }

    private function _sitemap(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmblocknewsadvfunctions.class.php');
        $obj_spmblocknewsadvfunctions = new spmblocknewsadvfunctions();

        $data_url = $obj_spmblocknewsadvfunctions->getSEOURLs();
        $newssitemap_url = $data_url['newssitemap_url'];



        $this->_data_translate_custom['_sitemap_sitemap'] = $this->l('Sitemap');
        $this->_data_translate_custom['_sitemap_cron_url'] = $this->l('Your CRON URL to call');
        $this->_data_translate_custom['_sitemap_regenerate_g_sitemap'] = $this->l('Regenerate Google sitemap');
        $this->_data_translate_custom['_sitemap_to_declare'] = $this->l('To declare news sitemap xml, add this line at the end of your robots.txt file');


        $is_rewrite = '&';
        if (Configuration::get('PS_REWRITING_SETTINGS')) {
            $is_rewrite = '?';
        }

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();



        $_html .= $this->_cronhelp(
            array(
                'url'=>$newssitemap_url.$is_rewrite.'token='.$this->_token_cron
                 )
        );

        return $_html;
    }


    private function _cronhelp($data = null){
        $url_cron = isset($data['url'])?$data['url']:'';

        $this->_data_translate_custom['_cronhelp_cron_help'] = $this->l('CRON HELP');
        $this->_data_translate_custom['_cronhelp_conf_2_possibl'] = $this->l('You can configure publish products to your wall through cron. You have 2 possibilities:');
        $this->_data_translate_custom['_cronhelp_enter_url_in_browser'] = $this->l('You can enter the following url in your browser: ');
        $this->_data_translate_custom['_cronhelp_set_cron_task'] = $this->l('You can set a cron\'s task (a recursive task that fulfills the publish products to your wall)');
        $this->_data_translate_custom['_cronhelp_every_hour'] = $this->l('The task run every hour');
        $this->_data_translate_custom['_cronhelp_hot_to_conf'] = $this->l('How to configure a cron task ?');
        $this->_data_translate_custom['_cronhelp_on_your_serv'] = $this->l('On your server, the interface allows you to configure cron\'s tasks');
        $this->_data_translate_custom['_cronhelp_about_cron'] = $this->l('About CRON');



        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();

        return $_html;
    }

    private function _urlrewrite(){
        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $this->l('URL Rewriting'),
                    'icon' => 'fa fa-link fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Enable or Disable URL rewriting:'),
                        'name' => 'rew_on',
                        'desc' => $this->l('Enable only if your server allows URL rewriting (recommended).'),

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Yes')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('No')
                            )
                        ),
                    ),

                ),



            ),


        );

        $fields_form1 = array(
            'form' => array(


                'submit' => array(
                    'title' => $this->l('Update Settings'),
                )
            ),
        );




        $helper = new HelperForm();



        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit_urlrewriting';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValuesUrlrewriteSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );




        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesUrlrewriteSettings(){

        $data_config = array(
            'rew_on' => (int)Configuration::get($this->name.'rew_on'),


        );

        return $data_config;

    }

    public function psvkform16($data_in){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/postshelp.class.php');
        $postshelp = new postshelp();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'translate'=>$data_in,
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),

            'update_title'=>$this->l('Update Settings'),
        );
        return $postshelp->psvkform16($data);
    }

    public function pstwitterform16($data_in){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/postshelp.class.php');
        $postshelp = new postshelp();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'translate'=>$data_in,
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),

            'update_title'=>$this->l('Update Settings'),
        );
        return $postshelp->pstwitterform16($data);
    }

    private function _autoposts16(){
        #### posts api ###
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/postshelp.class.php');
        $postshelp = new postshelp();

        return $postshelp->postsSettings(array('translate'=>$this->_translate));
        #### posts api ###
    }






 private function _help_documentation(){


     $this->_data_translate_custom['_help_documentation_help_doc'] = $this->l('Help / Documentation');
     $this->_data_translate_custom['_help_documentation_mod_doc'] = $this->l('MODULE DOCUMENTATION ');
     $this->_data_translate_custom['_help_documentation_gsnip_tool'] = $this->l('GOOGLE RICH SNIPPETS TEST TOOL ');
     $this->_data_translate_custom['_help_documentation_snip_based'] = $this->l('Snippets based on the ');
     $this->_data_translate_custom['_help_documentation_rich_snip_art'] = $this->l('Rich Snippets for Articles');
     $this->_data_translate_custom['_help_documentation_more_info'] = $this->l('More info');

     ob_start();
     include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
     $_html = ob_get_clean();

     $_html .= $this->_customhookhelp();

     return $_html;
    }



    private function _customhookhelp(){

        $this->_data_translate_custom['_customhookhelp_faq'] = $this->l('Frequently Asked Questions');
        $this->_data_translate_custom['_customhookhelp_c_hook_help'] = $this->l('CUSTOM HOOK HELP:');
        $this->_data_translate_custom['_customhookhelp_how_i_can_show'] = $this->l('How I can show Block Last News, Block News Archives, Block Search News on a single page (CMS or other places for example) ?');
        $this->_data_translate_custom['_customhookhelp_just_need_add'] = $this->l('You just need to add a line of code to the tpl file of the page where you want to add the Block Last News, Block News Archives, Block Search News');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();

        return $_html;
    }


 private function _welcome(){

     $this->_data_translate_custom['_welcome_wel'] = $this->l('Welcome');
     $this->_data_translate_custom['_welcome_thank_for_purch'] = $this->l('Welcome and thank you for purchasing the module.');
     $this->_data_translate_custom['_welcome_to_conf'] = $this->l('To configure module please read');
     $this->_data_translate_custom['_welcome_help_doc'] = $this->l('Help / Documentation');


     ob_start();
     include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
     $_html = ob_get_clean();

     return $_html;


    }





	public function translateText(){
		return array('seo_text'=> $this->l('News'),
					 'page'=>$this->l('Page'));
	}






	public function tools_redirect($url){

	 	Tools::redirect($url);


	}



    public function translateItems(){
        return array('page'=>$this->l('Page'),
                    'message_like' => $this->l('You have already voted for this news!'),
        );
    }

    public function getIdLang(){
        $cookie = $this->context->cookie;
        $id_lang = (int)($cookie->id_lang);
        return $id_lang;
    }

    public function clearSmartyCacheItems(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/cachespmblocknewsadv.class.php');
        $cache = new cachespmblocknewsadv();


        $cache->clearSmartyCacheModule();
    }
    

}