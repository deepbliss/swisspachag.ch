/**
 * SPM
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    SPM
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SPM
 * @license   SPM
 */

function spmblocknewsadv_list(id,action,value,type_action,token_custom){

    var text_action = '';
    var prefixid = '';
    if(type_action == 'news'){
        text_action = 'news';
    } else if(type_action == 'comment'){
        text_action = 'comment';
        prefixid = 'c';
    }


    if(action == 'active') {
        $('#activeitem'+prefixid + id).html('<img src="../img/admin/../../modules/spmblocknewsadv/views/img/loader.gif" />');
    }

    $.post(ajax_link_spmblocknewsadv,
        { id:id,
            action_custom:action,
            value: value,
            type_action: type_action,
            token:token_custom,
            ajax : true,
            controller : 'AdminSpmblocknewsadvajax',
            action : 'Spmblocknewsadvajax'
        },
        function (data) {
            if (data.status == 'success') {


                var data = data.params.content;



                if(action == 'active'){

                    $('#activeitem'+prefixid+id).html('');
                    if(value == 0){
                        var img_ok = 'ok';
                        var action_value = 1;
                    } else {
                        var img_ok = 'no_ok';
                        var action_value = 0;
                    }
                    var html = '<span class="label-tooltip" data-original-title="Click here to activate or deactivate '+text_action+' on your site" data-toggle="tooltip">'+
                            '<a href="javascript:void(0)" onclick="spmblocknewsadv_list('+id+',\'active\', '+action_value+',\''+type_action+'\',\''+token_custom+'\');" style="text-decoration:none">'+
                        '<img src="../img/admin/../../modules/spmblocknewsadv/views/img/'+img_ok+'.gif" />'+
                        '</a>'+
                    '</span>';
                    $('#activeitem'+prefixid+id).html(html);


                }

            } else {
                alert(data.message);

            }
        }, 'json');
}




function delete_img(item_id,token_custom){
    if(confirm("Are you sure you want to remove this item?"))
    {
        $('#post_images_list').css('opacity',0.5);
        $.post(ajax_link_spmblocknewsadv, {
                action_custom:'deleteimg',
                item_id : item_id,
                token:token_custom,
                ajax : true,
                controller : 'AdminSpmblocknewsadvajax',
                action : 'Spmblocknewsadvajax'
            },
            function (data) {
                if (data.status == 'success') {
                    $('#post_images_list').css('opacity',1);
                    $('#post_images_list').html('');

                } else {
                    $('#post_images_list').css('opacity',1);
                    alert(data.message);
                }

            }, 'json');
    }

}







function initAccessoriesAutocomplete(){
    $('document').ready( function() {
        $('#product_autocomplete_input')
            .autocomplete('ajax_products_list.php',{
                minChars: 1,
                autoFill: true,
                max:20,
                matchContains: true,
                mustMatch:true,
                scroll:false,
                cacheLength:0,
                formatItem: function(item) {
                    return item[1]+' - '+item[0];
                }
            }).result(addAccessory);

        $('#product_autocomplete_input').setOptions({
            extraParams: {
                excludeIds : getAccessoriesIds()
            }
        });
    });
}

function getAccessoriesIds()
{
    if ($('#inputAccessories').val() === undefined) return '';
    if ($('#inputAccessories').val() == '') return ',';
    ids = $('#inputAccessories').val().replace(/\-/g,',');
    //.replace(/\,$/,'')
    //ids = ids.replace(/\,$/,'');
    return ids;
}

function addAccessory(event, data, formatted)
{
    if (data == null)
        return false;
    //var productId = data[1];
    productId = data[data.length - 1];
    var productName = data[0];

    var $divAccessories = $('#divAccessories');
    var $inputAccessories = $('#inputAccessories');
    var $nameAccessories = $('#nameAccessories');

    /* delete product from select + add product line to the div, input_name, input_ids elements */
    $divAccessories.html($divAccessories.html() + productName + ' <span class="delAccessory" name="' + productId + '" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />');
    $nameAccessories.val($nameAccessories.val() + productName + '¤');
    $inputAccessories.val($inputAccessories.val() + productId + '-');
    $('#product_autocomplete_input').val('');
    $('#product_autocomplete_input').setOptions({
        extraParams: {excludeIds : getAccessoriesIds()}
    });
}

function delAccessory(id)
{
    var div = getE('divAccessories');
    var input = getE('inputAccessories');
    var name = getE('nameAccessories');

    // Cut hidden fields in array
    var inputCut = input.value.split('-');
    var nameCut = name.value.split('¤');

    if (inputCut.length != nameCut.length)
        return jAlert('Bad size');

    // Reset all hidden fields
    input.value = '';
    name.value = '';
    div.innerHTML = '';
    for (i in inputCut)
    {
        // If empty, error, next
        if (!inputCut[i] || !nameCut[i])
            continue ;

        // Add to hidden fields no selected products OR add to select field selected product
        if (inputCut[i] != id)
        {
            input.value += inputCut[i] + '-';
            name.value += nameCut[i] + '¤';
            div.innerHTML += nameCut[i] + ' <span class="delAccessory" name="' + inputCut[i] + '" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />';
        }
        else
            $('#selectAccessories').append('<option selected="selected" value="' + inputCut[i] + '-' + nameCut[i] + '">' + inputCut[i] + ' - ' + nameCut[i] + '</option>');
    }

    $('#product_autocomplete_input').setOptions({
        extraParams: {excludeIds : getAccessoriesIds()}
    });
}




