/**
 * SPM
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    SPM
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SPM
 * @license   SPM
 */


function spmblocknewsadv_like_news(id,like){

    $('.block-item-like-'+id).css('opacity',0.5);

    $.post(process_url_spmblocknewsadv, {
            action:'like',
            id : id,
            like : like
        },
        function (data) {
            if (data.status == 'success') {

                $('.block-item-like-'+id).css('opacity',1);

                var count = data.params.count;
                if(like == 1){
                    $('.block-item-like-'+id).html('');
                    $('.block-item-like-'+id).append('<i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">'+count+'</span>)');

                    if($('.block-item-like1-'+id)){
                        $('.block-item-like1-'+id).html('');
                        $('.block-item-like1-'+id).append('<i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">'+count+'</span>)');
                    }
                } else {
                    $('.block-item-unlike-'+id).html('');
                    $('.block-item-unlike-'+id).append('<i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">'+count+'</span>)');

                    if($('.block-item-unlike1-'+id)){
                        $('.block-item-unlike1-'+id).html('');
                        $('.block-item-unlike1-'+id).append('<i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">'+count+'</span>)');
                    }
                }


            } else {
                $('.block-item-like-'+id).css('opacity',1);
                alert(data.message);
            }

        }, 'json');
}

    function show_arch(id,column){
        for(i=0;i<100;i++){
            $('#arch'+i+column).hide(200);
        }
        $('#arch'+id+column).show(200);

    }

document.addEventListener("DOMContentLoaded", function(event) {

if ($('.owl_news_home_latest_news_type_carousel ul').length > 0) {

    if ($('.owl_news_home_latest_news_type_carousel ul').length > 0) {

        if (typeof $('.owl_news_home_latest_news_type_carousel ul').owlCarousel === 'function') {

            $('.owl_news_home_latest_news_type_carousel ul').owlCarousel({
                items: spmblocknewsadv_number_home_latest_news_slider,

                loop: true,
                responsive: true,
                nav: true,
                navRewind: false,
                margin: 20,
                dots: true,

                lazyLoad: true,
                lazyFollow: true,
                lazyEffect: "fade",
            });

        }
    }

}


    if ($('.owl_news_latest_news_type_carousel ul').length > 0) {

        if ($('.owl_news_latest_news_type_carousel ul').length > 0) {

            if (typeof $('.owl_news_latest_news_type_carousel ul').owlCarousel === 'function') {

                $('.owl_news_latest_news_type_carousel ul').owlCarousel({
                    items: 1,

                    loop: true,
                    responsive: true,
                    nav: true,
                    navRewind: false,
                    margin: 20,
                    dots: true,
                    navText: [,],

                    lazyLoad: true,
                    lazyFollow: true,
                    lazyEffect: "fade",
                });
            }
        }

    }



    if ($('.owl_news_related_products_type_carousel ul').length > 0) {


        if (typeof $('.owl_news_related_products_type_carousel ul').owlCarousel === 'function') {

            $('.owl_news_related_products_type_carousel ul').owlCarousel({
                items: spmblocknewsadv_number_product_related_slider,
                loop: true,
                responsive: true,
                nav: true,
                navRewind: false,
                margin: 10,
                dots: true,

                lazyLoad: true,
                lazyFollow: true,
                lazyEffect: "fade",
            });
        }
    }


    if ($('.owl_news_related_news_type_carousel ul').length > 0) {
        if (typeof $('.owl_news_related_news_type_carousel ul').owlCarousel === 'function') {
            $('.owl_news_related_news_type_carousel ul').owlCarousel({
                items: spmblocknewsadv_number_news_slider,
                loop: true,
                responsive: true,
                nav: true,
                navRewind: false,
                margin: 10,
                dots: true,

                lazyLoad: true,
                lazyFollow: true,
                lazyEffect: "fade",
            });
        }

    }


});