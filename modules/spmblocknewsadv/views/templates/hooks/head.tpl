{*
/**
 * SPM
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    SPM
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SPM
 * @license   SPM
 */
 *}
 
{if $spmblocknewsadvis_news != 0}
        <meta property="fb:app_id" content="{$spmblocknewsadvappid|escape:'htmlall':'UTF-8'}" />
        <meta property="fb:admins" content="{$spmblocknewsadvappadmin|escape:'htmlall':'UTF-8'}"/>
       <meta property="og:title" content="{$spmblocknewsadvname|escape:'htmlall':'UTF-8'}"/>
       {if strlen($spmblocknewsadvimg) >0}
	   <meta property="og:image" content="{$base_dir_ssl|escape:'htmlall':'UTF-8'}upload/spmblocknewsadv/{$spmblocknewsadvimg|escape:'htmlall':'UTF-8'}"/>
	   {/if}
	   <meta property="og:type" content="product"/>
       <meta property="og:url" content="{if $spmblocknewsadvrew_on == 1}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$spmblocknewsadvnews_seo_url|escape:'htmlall':'UTF-8'}{else}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$spmblocknewsadvnews_id|escape:'htmlall':'UTF-8'}{/if}"/>
{/if}


{if $spmblocknewsadvrsson == 1}
<link rel="alternate" type="application/rss+xml" href="{$spmblocknewsadvrss_url|escape:'htmlall':'UTF-8'}" />
{/if}


{literal}
<script type="text/javascript">
    var process_url_spmblocknewsadv = '{/literal}{$spmblocknewsadvprocess_url nofilter}{literal}';

</script>
{/literal}

