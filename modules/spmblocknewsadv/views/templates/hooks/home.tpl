{*
/**
 * SPM
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    SPM
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SPM
 * @license   SPM
 */
 *}

<div class="clear"></div>
<div {if $spmblocknewsadvis_ps15 != 0 && $spmblocknewsadvis17 == 0}id="left_column"{/if}>

    <div id="spmblocknewsadvblock_block_left"

         class="block
         {if $spmblocknewsadvis17 == 1}block-categories{/if}
         {if $spmblocknewsadvis16 == 1}blockmanufacturer16{else}blockmanufacturer{/if}
         spmblocknewsadv-block
         {if $spmblocknewsadvis_ps15 == 0}margin-top-10{/if}
         {if $spmblocknewsadvnews_h == 3}owl_news_home_latest_news_type_carousel{/if}
         "
            >
        <h4 class="title_block {if $spmblocknewsadvis17 == 1}text-uppercase h6{/if} {if $spmblocknewsadvrsson == 0}rss-home-block{/if}">

            <div {if $spmblocknewsadvrsson == 1}class="float-left"{/if}>
                <a href="{$spmblocknewsadvnews_url|escape:'htmlall':'UTF-8'}" title="{l s='Latest News' mod='spmblocknewsadv'}"
                        >{l s='Latest News' mod='spmblocknewsadv'}</a>

            </div>
            {if $spmblocknewsadvrsson == 1}
                <div class="float-left margin-left-10">
                    <a href="{$spmblocknewsadvrss_url|escape:'htmlall':'UTF-8'}" title="{l s='RSS Feed' mod='spmblocknewsadv'}" target="_blank">
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmblocknewsadv/views/img/feed.png" alt="{l s='RSS Feed' mod='spmblocknewsadv'}" />
                    </a>
                </div>
            {/if}

            <div class="clear"></div>
        </h4>
        <div class="block_content block-items-data">
            {if count($spmblocknewsadvitems_home)>0}

                    {if $spmblocknewsadvnews_h == 1}
                        <div class="items-articles-block">
                    {else}
                        <ul {if $spmblocknewsadvnews_h == 3}class="owl-carousel owl-theme"{/if}>
                    {/if}



                        {foreach from=$spmblocknewsadvitems_home item=items name=myLoop}
                            {foreach from=$items.data item=item name=myLoop}


                        {if $spmblocknewsadvnews_h == 1}

                        <div class="current-item-block">

                            {if $spmblocknewsadvblock_display_img == 1}
                                {if strlen($item.img)>0}
                                    <div class="block-side">
                                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$spmblocknewsadvpic|escape:'htmlall':'UTF-8'}{$item.img|escape:'htmlall':'UTF-8'}"
                                             title="{$item.title|escape:'htmlall':'UTF-8'}" alt="{$item.title|escape:'htmlall':'UTF-8'}"  />
                                    </div>
                                {/if}
                            {/if}

                            <div class="block-content">
                                <a class="item-article" title="{$item.title|escape:'htmlall':'UTF-8'}"
                                   href="{if $spmblocknewsadvrew_on == 1}
                                            {$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.seo_url|escape:'htmlall':'UTF-8'}
                                         {else}
                                            {$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.id|escape:'htmlall':'UTF-8'}
                                         {/if}
                                        "
                                        >{$item.title|escape:'htmlall':'UTF-8'}</a>
                                        <div class="item-text-content">{$item.content|strip_tags|substr:0:$spmblocknewsadvitem_h_tr|escape:'quotes':'UTF-8'}{if strlen($item.content)>$spmblocknewsadvitem_h_tr}...{/if}</div>

                                <div class="clr"></div>

                                {if $spmblocknewsadvh_display_date == 1}
                                    <span class="float-left block-item-date"><i class="fa fa-clock-o fa-lg"></i>&nbsp;{$item.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}</span>
                                {/if}

                                <span class="float-right comment block-item-like">
                                    {if $spmblocknewsadvis_like == 1}
                                    {if $item.is_liked_news}
                                        <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)
                                    {else}
                                        <span class="block-item-like-{$item.id|escape:'htmlall':'UTF-8'}">
                                        <a onclick="spmblocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},1)"
                                           href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                        </span>
                                    {/if}
                                    {/if}
                                    &nbsp;
                                    {if $spmblocknewsadvis_unlike == 1}
                                    {if $item.is_unliked_news}
                                        <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)
                                    {else}
                                    <span class="block-item-unlike-{$item.id|escape:'htmlall':'UTF-8'}">
                                    <a onclick="spmblocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},0)"
                                       href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)</a>
                                    </span>
                                    {/if}
                                    {/if}
                                </span>


                                <div class="clr"></div>
                            </div>
                        </div>

                     {elseif $spmblocknewsadvnews_h == 2}


                                    <li class="vertical-blocks-news {if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if}">

                                        <table width="100%">

                                        {if $spmblocknewsadvblock_display_img == 1}
                                            {if strlen($item.img)>0}
                                                <tr>
                                                    <td align="center" class="text-align-center">
                                                        <a href="{if $spmblocknewsadvrew_on == 1}
                                                                    {$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.seo_url|escape:'htmlall':'UTF-8'}
                                                                 {else}
                                                                    {$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.id|escape:'htmlall':'UTF-8'}
                                                                 {/if}"
                                                           title="{$item.title|escape:'htmlall':'UTF-8'}">

                                                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$spmblocknewsadvpic|escape:'htmlall':'UTF-8'}{$item.img|escape:'htmlall':'UTF-8'}"
                                                                     title="{$item.title|escape:'htmlall':'UTF-8'}"
                                                                     alt="{$item.title|escape:'htmlall':'UTF-8'}" />
                                                            </a>

                                                    </td>
                                                </tr>
                                            {/if}
                                         {/if}
                                            <tr>
                                                <td class="v-b-title {if $spmblocknewsadvblock_display_img == 1 && strlen($item.img)==0}v-b-bottom{/if}">

                                                        <a href="{if $spmblocknewsadvrew_on == 1}
                                                                    {$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.seo_url|escape:'htmlall':'UTF-8'}
                                                                 {else}
                                                                    {$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.id|escape:'htmlall':'UTF-8'}
                                                                 {/if}"
                                                            title="{$item.title|escape:'htmlall':'UTF-8'}">
                                                            <b>
                                                                {$item.title|escape:'htmlall':'UTF-8'}
                                                            </b>
                                                        </a>


                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="v-footer">
                                                    {if $spmblocknewsadvh_display_date == 1}
                                                        <span class="float-left block-item-date"><i class="fa fa-clock-o fa-lg"></i>&nbsp;{$item.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}</span>
                                                    {/if}

                                                    <span class="float-right comment block-news-like">


                                                        {if $spmblocknewsadvis_like == 1}
                                                            {if $item.is_liked_news}
                                                                <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)
                                                        {else}
                                                            <span class="block-item-like-{$item.id|escape:'htmlall':'UTF-8'}">
                                                            <a onclick="spmblocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},1)"
                                                               href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                                            </span>
                                                                                    {/if}
                                                                                {/if}
                                                                                &nbsp;
                                                                                {if $spmblocknewsadvis_unlike == 1}
                                                                                    {if $item.is_unliked_news}
                                                                                        <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)
                                                        {else}
                                                        <span class="block-item-unlike-{$item.id|escape:'htmlall':'UTF-8'}">
                                                        <a onclick="spmblocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},0)"
                                                           href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)</a>
                                                        </span>
                                                            {/if}
                                                        {/if}

                                                </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </li>

                    {elseif $spmblocknewsadvnews_h == 3}


                    <div class="current-item-block">

                            {if $spmblocknewsadvblock_display_img == 1}
                                   {if strlen($item.img)>0}

                                    <div class="text-align-center">
                                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$spmblocknewsadvpic|escape:'htmlall':'UTF-8'}{$item.img|escape:'htmlall':'UTF-8'}"
                                             title="{$item.title|escape:'htmlall':'UTF-8'}" alt="{$item.title|escape:'htmlall':'UTF-8'}" />
                                    </div>
                                {/if}
                            {/if}
                            <br/>
                            <div class="block-content text-align-center">
                                <a class="item-article" title="{$item.title|escape:'htmlall':'UTF-8'}"
                                   href="{if $spmblocknewsadvrew_on == 1}
                                                                    {$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.seo_url|escape:'htmlall':'UTF-8'}
                                                                 {else}
                                                                    {$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$item.id|escape:'htmlall':'UTF-8'}
                                                                 {/if}"
                                        >{$item.title|escape:'htmlall':'UTF-8'}</a>

                                    <br/><br/>
                                        <div>{$item.content|strip_tags|substr:0:$spmblocknewsadvitem_h_tr|escape:'htmlall':'UTF-8'}{if strlen($item.content)>$spmblocknewsadvitem_h_tr}...{/if}</div>


                                <div class="clr"></div>



                                <div class="clr"></div>


                                {if $spmblocknewsadvh_display_date == 1}
                                    <span class="float-left block-item-date"><i class="fa fa-clock-o fa-lg"></i>&nbsp;{$item.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}</span>
                                {/if}

                                <span class="float-right comment block-news-like">


                                 {if $spmblocknewsadvis_like == 1}
                                                            {if $item.is_liked_news}
                                                                <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)
                                                        {else}
                                                            <span class="block-item-like-{$item.id|escape:'htmlall':'UTF-8'}">
                                                            <a onclick="spmblocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},1)"
                                                               href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                                            </span>
                                                                                    {/if}
                                                                                {/if}
                                                                                &nbsp;
                                                                                {if $spmblocknewsadvis_unlike == 1}
                                                                                    {if $item.is_unliked_news}
                                                                                        <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)
                                                        {else}
                                                        <span class="block-item-unlike-{$item.id|escape:'htmlall':'UTF-8'}">
                                                        <a onclick="spmblocknewsadv_like_news({$item.id|escape:'htmlall':'UTF-8'},0)"
                                                           href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number">{$item.count_unlike|escape:'htmlall':'UTF-8'}</span>)</a>
                                                        </span>
                                                            {/if}
                                                        {/if}
                                 </span>


                                <div class="clr"></div>
                            </div>
                        </div>

                    {/if}


                {/foreach}
            {/foreach}



            {if $spmblocknewsadvnews_h == 1}
                </div>
            {else}
                </ul>
            {/if}



            <div class="clear"></div>
            {else}
            <div class="block-no-items">
                {l s='News not found.' mod='spmblocknewsadv'}
            </div>
            {/if}
        </div>
    </div>
</div>


{if $spmblocknewsadvnews_h == 3}
{literal}
    <script type="text/javascript">
        var spmblocknewsadv_number_home_latest_news_slider = {/literal}{$spmblocknewsadvnews_bp_sl|escape:'htmlall':'UTF-8'}{literal};
    </script>
{/literal}
{/if}


