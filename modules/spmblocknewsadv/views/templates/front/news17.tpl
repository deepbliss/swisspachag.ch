{*
/**
 * SPM
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    SPM
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SPM
 * @license   SPM
 */
 *}

 {extends file='layouts/layout-left-column.tpl'}

{block name='content_wrapper'}
  <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">

    {include file="modules/spmblocknewsadv/views/templates/front/news.tpl"}
</div>
{/block}
