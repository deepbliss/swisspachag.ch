{*
/**
 * SPM
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    SPM
 * @category content_management
 * @package spmblocknewsadv
 * @copyright Copyright SPM
 * @license   SPM
 */
 *}

{extends file='layouts/layout-left-column.tpl'}

{block name='content_wrapper'}

  <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">

    {if $spmblocknewsadvis17 == 1}
        <nav data-depth="2" class="breadcrumb hidden-sm-down">
            <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                    <a itemprop="item" href="{$spmblocknewsadvnews_url|escape:'htmlall':'UTF-8'}">
                        <span itemprop="name">{l s='News' mod='spmblocknewsadv'}</span>
                    </a>
                    <meta itemprop="position" content="1">
                </li>
                {foreach from=$posts item=post name=myLoop}
                {/foreach}
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                    <a itemprop="item" href="{if $spmblocknewsadvrew_on == 1}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.seo_url|escape:'htmlall':'UTF-8'}{else}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}{/if}">
                        <span itemprop="name">{$meta_title|escape:'htmlall':'UTF-8'}</span>
                    </a>
                    <meta itemprop="position" content="2">
                </li>
            </ol>
        </nav>

    {/if}

    {capture name=path}
        <a href="{$spmblocknewsadvnews_url|escape:'htmlall':'UTF-8'}">
            {l s='News' mod='spmblocknewsadv'}
        </a>
        <span class="navigation-pipe">></span>
        {$meta_title|escape:'htmlall':'UTF-8'}
    {/capture}




    {if count($posts) > 0}


        <div class="news-block-item">


        {foreach from=$posts item=post name=myLoop}
            <div class="item-page" itemscope itemtype="http{if $spmblocknewsadvis_ssl == 1}s{/if}://schema.org/Article">

                <meta itemscope itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage"
                      itemid="{if $spmblocknewsadvrew_on == 1}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.seo_url|escape:'htmlall':'UTF-8'}{else}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}{/if}"

                        />

                <meta itemprop="datePublished" content="{$post.time_add_rss|escape:'htmlall':'UTF-8'}"/>
                <meta itemprop="dateModified" content="{$post.time_add_rss|escape:'htmlall':'UTF-8'}"/>
                <meta itemprop="headline" content="{$post.title|escape:'htmlall':'UTF-8'}"/>
                <meta itemprop="alternativeHeadline" content="{$post.title|escape:'htmlall':'UTF-8'}"/>

        <span itemprop="author" itemscope itemtype="https://schema.org/Person">
             <meta itemprop="name" content="admin"/>
        </span>


        <span itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
            <span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                <meta itemprop="url" content="{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/logo.jpg">
                <meta itemprop="width" content="600">
                <meta itemprop="height" content="60">
            </span>
            <meta itemprop="name" content="{$spmblocknewsadvsnip_publisher|escape:'htmlall':'UTF-8'}">
        </span>

                <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                    {if strlen($post.img)>0}
                        <meta itemprop="url" content="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$spmblocknewsadvpic|escape:'htmlall':'UTF-8'}{$post.img|escape:'htmlall':'UTF-8'}">
                        <meta itemprop="width" content="{$spmblocknewsadvsnip_width|escape:'htmlall':'UTF-8'}">
                        <meta itemprop="height" content="{$spmblocknewsadvsnip_height|escape:'htmlall':'UTF-8'}">

                    {else}
                        <meta itemprop="url" content="{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/logo.jpg"/>
                        <meta itemprop="width" content="600">
                        <meta itemprop="height" content="60">
                    {/if}
                </div>

                <meta itemprop="description" content="{$post.content|strip_tags|substr:0:140|escape:'htmlall':'UTF-8'}"/>

               


                <div class="top-item">

                    <h1>{$meta_title|escape:'html':'UTF-8'}  {if $spmblocknewsadvi_display_date == 1}
                        <p class="float-right date-time">
                            <time datetime="{$post.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}" pubdate="pubdate"
                                    >{$post.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}</time>
                        </p>
                    {/if}</h1>
                     {if strlen($post.img)>0}
                     <div class="detail-img">
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$spmblocknewsadvpic|escape:'htmlall':'UTF-8'}{$post.img|escape:'htmlall':'UTF-8'}"
                             alt="{$post.title|escape:'htmlall':'UTF-8'}" class="img-responsive" />   </div>
                {/if}
                    <div class="body-post">
                    {$post.content nofilter}
                </div>

                  
                    <div class="clear"></div>
                  

                    <p class="float-right comment">


                        {if $spmblocknewsadvis_like == 1}
                            {if $post.is_liked_news}
                                <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_like|escape:'htmlall':'UTF-8'}</span>)
                            {else}
                                <span class="block-item-like-{$post.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="spmblocknewsadv_like_news({$post.id|escape:'htmlall':'UTF-8'},1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                            {/if}
                        {/if}
                        &nbsp;
                        {if $spmblocknewsadvis_unlike == 1}
                            {if $post.is_unliked_news}
                                <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_unlike|escape:'htmlall':'UTF-8'}</span>)
                            {else}
                                <span class="block-item-unlike-{$post.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="spmblocknewsadv_like_news({$post.id|escape:'htmlall':'UTF-8'},0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number">{$post.count_unlike|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                            {/if}
                        {/if}




                    </p>
                    <div class="clear"></div>



                </div>





              




                {if $spmblocknewsadvis_soc_but == 1}
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <div class="fb-like" data-href="http{if $spmblocknewsadvis_ssl == 1}s{/if}://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}"
                                     data-send="false" data-layout="button_count" data-width="50" data-show-faces="true"></div>
                            </td>
                            <td>
                                <a href="http{if $spmblocknewsadvis_ssl == 1}s{/if}://twitter.com/share" class="twitter-share-button"
                                   data-url="http{if $spmblocknewsadvis_ssl == 1}s{/if}://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}"
                                   data-counturl="http{if $spmblocknewsadvis_ssl == 1}s{/if}://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}"
                                   data-via="#"
                                   data-count="horizontal">Tweet</a>
                            </td>
                            <td>
                                <!-- Place this render call where appropriate -->
                                {literal}
                                    <script type="text/javascript">
                                        (function() {
                                            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                            po.src = 'https://apis.google.com/js/plusone.js';
                                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                                        })();
                                    </script>
                                {/literal}

                                <div class="g-plusone" data-size="medium" href="http{if $spmblocknewsadvis_ssl == 1}s{/if}://$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}" data-count="true"></div>

                            </td>
                            <td>
                                {literal}
                                <script type="IN/Share" data-url="http{if $spmblocknewsadvis_ssl == 1}s{/if}://{/literal}{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}{literal}"
                                        data-counter="right"></script>
                                {/literal}
                            </td>
                            <td>
                                <a href="http{if $spmblocknewsadvis_ssl == 1}s{/if}://pinterest.com/pin/create/button/?url=http{if $spmblocknewsadvis_ssl == 1}s{/if}://{$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}{$smarty.server.REQUEST_URI|escape:'htmlall':'UTF-8'}&media={if strlen($post.img)>0}{$base_dir_ssl|escape:'htmlall':'UTF-8'}upload/spmblocknewsadv/{$post.img|escape:'htmlall':'UTF-8'}{else}{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/logo.jpg{/if}&description={$meta_title|escape:'htmlall':'UTF-8'}"
                                   class="pin-it-button" count-layout="horizontal">
                                    <img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
                            </td>
                        </tr>
                    </table>

                {if $spmblocknewsadvis_comments == 0}
                {if $post.is_comments == 1}
                    <div id="fb-root"></div>
                {literal}

                    <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/{/literal}{$spmblocknewsadvlng_iso|escape:'htmlall':'UTF-8'}{literal}/all.js#xfbml=1&appid={/literal}{$spmblocknewsadvappid|escape:'htmlall':'UTF-8'}{literal}";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                {/literal}
                {/if}
                {/if}

                {literal}

                    <script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>
                    <script src="//platform.linkedin.com/in.js" async type="text/javascript"></script>
                    <script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>
                {/literal}

                {/if}


            </div>
        {/foreach}




        {if count($related_products)>0}
            <div class="rel-products-block {if $spmblocknewsadvrelpr_slider == 1}owl_news_related_products_type_carousel{/if}">
                <h4 class="related-products-title"><i class="fa fa-book fa-lg"></i>&nbsp;{l s='Related Products' mod='spmblocknewsadv'}</h4>


                <ul {if $spmblocknewsadvrelpr_slider == 1}class="owl-carousel owl-theme"{/if}>
                    {foreach from=$related_products item=product name=myLoop}
                        <li class="clearfix {if $spmblocknewsadvrelpr_slider == 0}no-slider{/if}">
                            {if $product.picture}
                                <a title="{$product.title|escape:'htmlall':'UTF-8'}" href="{$product.product_url|escape:'htmlall':'UTF-8'}" class="products-block-image">
                                    <img alt="{$product.title|escape:'htmlall':'UTF-8'}" src="{$product.picture|escape:'htmlall':'UTF-8'}"
                                         {if $spmblocknewsadvrelpr_slider == 1}class="img-responsive"{/if}/>
                                </a>
                            {/if}
                            <div class="clear"></div>
                            <div class="product-content">
                                <h5>
                                    <a title="{$product.title|escape:'htmlall':'UTF-8'}" href="{$product.product_url|escape:'htmlall':'UTF-8'}"
                                       class="product-name">
                                        {$product.title|escape:'htmlall':'UTF-8'}
                                    </a>
                                </h5>
                                <p class="product-description">{$product.description|strip_tags|substr:0:$spmblocknewsadvitem_rp_tr|escape:'htmlall':'UTF-8'}{if strlen($product.description)>$spmblocknewsadvitem_rp_tr}...{/if}</p>
                            </div>
                        </li>
                    {/foreach}
                </ul>

                <div class="clear"></div>
            </div>
        {/if}





        {if count($related_posts)>0}
            <div class="rel-posts-block {if $spmblocknewsadvrelp_slider == 1}owl_news_related_news_type_carousel{/if}">

                <h4 class="related-posts-title"><i class="fa fa-newspaper-o fa-lg"></i>&nbsp;{l s='Related News' mod='spmblocknewsadv'}</h4>

                <div class="other-posts">


                    <ul class="{if $spmblocknewsadvrelp_slider == 1}owl-carousel owl-theme{else}row-custom{/if}">
                        {foreach from=$related_posts item=relpost name=myLoop}
                            {if $smarty.foreach.myLoop.index%3 == 0}<div class="clear"></div>{/if}
                            <li {if $spmblocknewsadvrelp_slider == 0}class="col-sm-4-custom"{/if}>
                                {if strlen($relpost.img)>0}
                                    <div class="block-top">
                                        <a title="{$relpost.title|escape:'htmlall':'UTF-8'}"
                                           href="{if $spmblocknewsadvrew_on == 1}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$relpost.url|escape:'htmlall':'UTF-8'}{else}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$relpost.id|escape:'htmlall':'UTF-8'}{/if}"
                                                >
                                            <img alt="{$relpost.title|escape:'htmlall':'UTF-8'}" {if $spmblocknewsadvrelp_slider == 1}class="img-responsive"{/if}
                                                 src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}{$spmblocknewsadvpic|escape:'htmlall':'UTF-8'}{$relpost.img|escape:'htmlall':'UTF-8'}">
                                        </a>
                                    </div>
                                {/if}


                                <div class="block-content"><h4 class="block-heading">
                                        <a title="{$relpost.title|escape:'htmlall':'UTF-8'}"
                                           href="{if $spmblocknewsadvrew_on == 1}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$relpost.url|escape:'htmlall':'UTF-8'}{else}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$relpost.id|escape:'htmlall':'UTF-8'}{/if}"

                                                >{$relpost.title|escape:'htmlall':'UTF-8'}</a></h4></div>
                                <div class="block-footer">
                                    <p class="float-left">
                                        <time pubdate="pubdate" datetime="{$relpost.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}"
                                                ><i class="fa fa-clock-o fa-lg"></i>&nbsp;&nbsp;{$relpost.time_add|date_format:"%d/%m/%Y"|escape:'htmlall':'UTF-8'}
                                        </time>
                                    </p>
                                    <p class="float-right comment">


                                        {if $spmblocknewsadvis_like == 1}
                                            {if $relpost.is_liked_news}
                                                <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number">{$relpost.count_like|escape:'htmlall':'UTF-8'}</span>)
                                            {else}
                                                <span class="block-item-like-{$relpost.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="spmblocknewsadv_like_news({$relpost.id|escape:'htmlall':'UTF-8'},1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number">{$relpost.count_like|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                                            {/if}
                                        {/if}
                                        &nbsp;
                                        {if $spmblocknewsadvis_unlike == 1}
                                            {if $relpost.is_unliked_news}
                                                <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number">{$relpost.count_unlike|escape:'htmlall':'UTF-8'}</span>)
                                            {else}
                                                <span class="block-item-unlike-{$relpost.id|escape:'htmlall':'UTF-8'}">
                                <a onclick="spmblocknewsadv_like_news({$relpost.id|escape:'htmlall':'UTF-8'},0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number">{$relpost.count_unlike|escape:'htmlall':'UTF-8'}</span>)</a>
                                </span>
                                            {/if}
                                        {/if}






                                    </p>

                                    <div class="clear"></div>
                                </div>
                            </li>
                        {/foreach}

                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>


            </div>
        {/if}

        {if $spmblocknewsadvis_comments == 1}
            {if $post.is_comments == 0}
                <div class="blocnewsadv-noitems">
                    {l s='Comments are сlosed for this post' mod='spmblocknewsadv'}
                </div>
            {else}
                <div class="fcomment-title"><i class="fa fa-facebook fa-lg"></i> {l s='Facebook comments' mod='spmblocknewsadv'}</div>

                <div class="fcomment-content">
                    <div id="fb-root"></div>
                    {literal}

                    <script type="text/javascript">
                        (function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/{/literal}{$spmblocknewsadvlng_iso|escape:'htmlall':'UTF-8'}{literal}/all.js#xfbml=1&appid={/literal}{$spmblocknewsadvappid|escape:'htmlall':'UTF-8'}{literal}";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                        <style>.fb_iframe_widget_fluid_desktop, .fb_iframe_widget_fluid_desktop span, .fb_iframe_widget_fluid_desktop iframe{width:100%!important}</style>
                    {/literal}

                    <div class="fb-comments" data-href="{if $spmblocknewsadvrew_on == 1}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.seo_url|escape:'htmlall':'UTF-8'}{else}{$spmblocknewsadvnews_item_url|escape:'htmlall':'UTF-8'}{$post.id|escape:'htmlall':'UTF-8'}{/if}"
                         data-numposts="{$spmblocknewsadvnumber_fc|escape:'htmlall':'UTF-8'}" data-width="100%" data-mobile="false"></div>


                </div>

            {/if}
        {/if}


    {else}
        <div class="blocnewsadv-noitems">
            {l s='There are not news yet' mod='spmblocknewsadv'}
        </div>
    {/if}

    </div>




{literal}
    <script type="text/javascript">

        {/literal}{if $spmblocknewsadvrelpr_slider == 1}{literal}
        var spmblocknewsadv_number_product_related_slider = {/literal}{$spmblocknewsadvnpr_slider|escape:'htmlall':'UTF-8'}{literal};
        {/literal}{/if}{literal}

        {/literal}{if $spmblocknewsadvrelp_slider == 1}{literal}
        var spmblocknewsadv_number_news_slider = {/literal}{$spmblocknewsadvnp_slider|escape:'htmlall':'UTF-8'}{literal};
        {/literal}{/if}{literal}

    </script>
{/literal}


    </div>
{/block}
