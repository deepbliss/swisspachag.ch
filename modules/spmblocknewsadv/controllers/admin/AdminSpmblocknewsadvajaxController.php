<?php
/**
 * 2011 - 2018 StorePrestaModules SPM LLC.
 *
 * MODULE spmblocknewsadv
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.2.2
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class AdminSpmblocknewsadvajaxController extends ModuleAdminController
{



    public function ajaxProcessSpmblocknewsadvAjax()
    {

        $HTTP_X_REQUESTED_WITH = isset($_SERVER['HTTP_X_REQUESTED_WITH'])?$_SERVER['HTTP_X_REQUESTED_WITH']:'';
        if($HTTP_X_REQUESTED_WITH != 'XMLHttpRequest') {
            exit;
        }

        $module_name = 'spmblocknewsadv';


        ob_start();
        $status = 'success';
        $message = '';



        if(Tools::getValue('token') == Tools::getAdminTokenLite('AdminSpmblocknewsadvajax')) {


            $action = Tools::getValue('action_custom');
            include_once(_PS_MODULE_DIR_ . $module_name . '/classes/spmblocknewsadvfunctions.class.php');
            $spmblocknewsadvfunctions_obj = new spmblocknewsadvfunctions();



            switch ($action){


                case 'active':
                    $id = (int)Tools::getValue('id');
                    $value = (int)Tools::getValue('value');
                    if($value == 0){
                        $value = 1;
                    } else {
                        $value = 0;
                    }
                    $type_action = Tools::getValue('type_action');

                    switch($type_action){
                        case 'comment':
                            $spmblocknewsadvfunctions_obj->updateItemComments(array('id'=>$id,'status'=>$value));
                            break;
                        case 'news':
                            $spmblocknewsadvfunctions_obj->updateItemStatus(array('id'=>$id,'status'=>$value));
                            break;
                    }




                break;

                case 'deleteimg':
                    include_once(_PS_MODULE_DIR_ . $module_name.'/spmblocknewsadv.php');
                    $obj_spmblocknewsadv = new spmblocknewsadv();

                    if($obj_spmblocknewsadv->is_demo){
                        $status = 'error';
                        $message = 'Feature disabled on the demo mode!';
                    } else {
                        $item_id = Tools::getValue('item_id');
                        $spmblocknewsadvfunctions_obj->deleteImg(array('id' => $item_id));
                    }
                break;


                default:
                    $status = 'error';
                    $message = 'Unknown parameters!';
                    break;
            }

        } else {
            $status = 'error';
            $message = 'Invalid token.';
        }

        $response = new stdClass();
        $content = ob_get_clean();
        $response->status = $status;
        $response->message = $message;
        $response->params = array('content' => $content);


        echo json_encode($response);
        exit;

    }
}
?>