<?php
/**
 * 2011 - 2018 StorePrestaModules SPM LLC.
 *
 * MODULE spmblocknewsadv
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.2.2
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

ob_start();
require_once(_PS_MODULE_DIR_ . 'spmblocknewsadv/classes/SpmblocknewsadvnewsItems.php');

class AdminSpmblocknewsadvnewsController extends ModuleAdminController{

    private $_name_controller = 'AdminSpmblocknewsadvnews';
    private $_name_module = 'spmblocknewsadv';
    private $_data_table = 'spmblocknewsadv_data';
    private  $_id_lang;
    private  $_id_shop;
    private  $_iso_code;

    public function __construct()

	{

            $this->bootstrap = true;
            $this->context = Context::getContext();
            $this->table = 'spmblocknewsadv';


            $this->identifier = 'id';
            $this->className = 'SpmblocknewsadvnewsItems';


            $this->lang = false;

            $this->_orderBy = 'id';
            $this->_orderWay = 'DESC';


            $this->allow_export = false;

            $this->list_no_link = true;

            $id_lang =  $this->context->cookie->id_lang;
            $this->_id_lang = $id_lang;

            $id_shop =  $this->context->shop->id;
            $this->_id_shop = $id_shop;

            $iso_code = Language::getIsoById($id_lang);
            $this->_iso_code = $iso_code;


            $this->_select .= 'a.id, a.img, c.title, a.time_add , c.seo_url, c.id_lang, '.(int)$id_shop.' as id_shop, a.status ';
            $this->_join .= '  JOIN `' . _DB_PREFIX_ . $this->_data_table.'` c ON (c.id_item = a.id and c.id_lang = '.(int)$id_lang.')';


            $this->_select .= ', (SELECT group_concat(sh.`name` SEPARATOR \', \')
                    FROM `'._DB_PREFIX_.'shop` sh
                    WHERE sh.`active` = 1 AND sh.deleted = 0 AND sh.`id_shop`
                    IN(SELECT
                          SUBSTRING_INDEX(SUBSTRING_INDEX(pt_in.id_shop, \',\', sh_in.id_shop), \',\', -1) name
                        FROM
                          '._DB_PREFIX_.'shop as sh_in INNER JOIN '._DB_PREFIX_.$this->table.' pt_in
                          ON CHAR_LENGTH(pt_in.id_shop)
                             -CHAR_LENGTH(REPLACE(pt_in.id_shop, \',\', \'\'))>=sh_in.id_shop-1
                        WHERE pt_in.id =  a.id
                        ORDER BY
                          id, sh_in.id_shop)
                    ) as shop_name';

            $this->_select .= ', (SELECT group_concat(l.`iso_code` SEPARATOR \', \')
	            FROM `'._DB_PREFIX_.'lang` l
	            JOIN
	            `'._DB_PREFIX_.'lang_shop` ls
	            ON(l.id_lang = ls.id_lang)
	            WHERE l.`active` = 1 AND ls.id_shop = '.(int)$id_shop.' AND l.`id_lang`
	            IN( select pt_d.id_lang FROM `'._DB_PREFIX_.$this->_data_table.'` pt_d WHERE pt_d.id_item = a.id)) as language';


           $this->_select .= ', (select count(*) as count from `'._DB_PREFIX_.'spmblocknewsadv_news_like` pclike
				    WHERE pclike.news_id = a.id) as count_likes ';



            $this->addRowAction('edit');
            $this->addRowAction('delete');
            //$this->addRowAction('view');
            //$this->addRowAction('&nbsp;');


            if(Configuration::get($this->_name_module.'rew_on')){
              $is_rewrite = 1;
            } else {
               $is_rewrite = 0;
            }

            // is cloud ?? //
            if(defined('_PS_HOST_MODE_')){
                $logo_img_path = '../modules/'.$this->_name_module.'/upload/';
            } else {
                $logo_img_path = '../upload/'.$this->_name_module.'/';
            }
            // is cloud ?? //

        $is16= 0;
        if(version_compare(_PS_VERSION_, '1.6', '>')) {
            $is16= 1;
        }

            $this->fields_list = array(
                'id' => array(
                    'title' => $this->l('ID'),
                    'align' => 'center',
                    'search' => true,
                    'orderby' => true,

                ),

                'img' => array(
                    'title' => $this->l('Image'),
                    'width' => 'auto',
                    'search' => false,
                    'align' => 'center',
                    'logo_img_path' => $logo_img_path,
                    'type_custom' => 'img',
                    'orderby' => true,

                ),

                'title' => array(
                    'title' => $this->l('Title'),
                    'width' => 'auto',
                    'orderby' => true,
                    'type_custom' => 'title_item',
                    'is_rewrite' => $is_rewrite,
                    'iso_code' => $this->_iso_code,
                    'base_dir_ssl' => _PS_BASE_URL_SSL_.__PS_BASE_URI__,
                    'is16'=>$is16,

                ),



                'count_likes' => array(
                    'title' => $this->l('Count likes'),
                    'width' => 'auto',
                    'search' => false,
                    'align' => 'center',

                ),


                'shop_name' => array(
                    'title' => $this->l('Shop'),
                    'width' => 'auto',
                    'search' => false

                ),

                'language' => array(
                    'title' => $this->l('Language'),
                    'width' => 'auto',
                    'search' => false

                ),

                'time_add' => array(
                    'title' => $this->l('Date add'),
                    'width' => 'auto',
                    'search' => false,

                ),

                'is_comments' => array(
                    'title' => $this->l('Enable Facebook Comments'),
                    'width' => 'auto',
                    'align' => 'center',
                    'type' => 'bool',
                    'orderby' => FALSE,
                    'type_custom' => 'is_comments',
                    'token_custom'=>Tools::getAdminTokenLite('AdminSpmblocknewsadvajax'),
                    'ajax_link' => $this->context->link->getAdminLink('AdminSpmblocknewsadvajax'),
                ),

                'status' => array(
                    'title' => $this->l('Status'),
                    'width' => 40,
                    'align' => 'center',
                    'type' => 'bool',
                    'orderby' => FALSE,
                    'type_custom' => 'is_active',
                    'token_custom'=>Tools::getAdminTokenLite('AdminSpmblocknewsadvajax'),
                    'ajax_link' => $this->context->link->getAdminLink('AdminSpmblocknewsadvajax'),
                ),



            );

            $this->bulk_actions = array(
                'delete' => array(
                    'text' => $this->l('Delete selected'),
                    'icon' => 'icon-trash',
                    'confirm' => $this->l('Delete selected items?')
                )
            );



		parent::__construct();
		
	}




    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {
        $list = parent::getList($id_lang, $order_by, $order_way, $start, $limit, $id_lang_shop);
        $this->_listsql = false;
        return $list;
    }

    public function initPageHeaderToolbar()
    {
        if (empty($this->display)) {
            $this->page_header_toolbar_btn['add_item'] = array(
                'href' => self::$currentIndex.'&add'.$this->_name_module.'&token='.$this->token,
                'desc' => $this->l('Add new news', null, null, false),
                'icon' => 'process-icon-new'
            );
        }

        parent::initPageHeaderToolbar();
    }

    public function initToolbar() {

        parent::initToolbar();

    }



    public function postProcess()
    {


        require_once(_PS_MODULE_DIR_ . '' . $this->_name_module . '/classes/spmblocknewsadvfunctions.class.php');
        $spmblocknewsadvfunctions_obj = new spmblocknewsadvfunctions();


        if (Tools::isSubmit('add_item')) {
            ## add item ##
            $seo_url = Tools::getValue("seo_url");
            $languages = Language::getLanguages(false);
            $data_title_content_lang = array();
            $data_validation = array();

            $ids_related_products = Tools::getValue("inputAccessories");

            $ids_related_posts = Tools::getValue("ids_related_posts");


            $time_add = Tools::getValue("time_add");
            $cat_shop_association = Tools::getValue("cat_shop_association");


            foreach ($languages as $language){
                $id_lang = $language['id_lang'];
                $item_title = Tools::getValue("item_title_".$id_lang);
                $item_seokeywords = Tools::getValue("item_seokeywords_".$id_lang);
                $item_seodescription = Tools::getValue("item_seodescription_".$id_lang);
                $item_content = Tools::getValue("content_".$id_lang);

                if(Tools::strlen($item_title)>0 || Tools::strlen($item_content)>0)
                {
                    $data_title_content_lang[$id_lang] = array('title' => $item_title,
                        'seokeywords' => $item_seokeywords,
                        'seodescription' => $item_seodescription,
                        'content' => $item_content,
                        'seo_url'=>$seo_url
                    );
                    $data_validation[$id_lang] = array('content' => $item_content);
                }
            }


            $item_status = Tools::getValue("item_status");
            $item_iscomments = Tools::getValue("item_iscomments");
            $item_images = Tools::getValue("post_images");

            $data = array('data_title_content_lang'=>$data_title_content_lang,
                            'item_status' => $item_status,
                            'iscomments' => $item_iscomments,
                            'post_images' => $item_images,
                            'cat_shop_association' => $cat_shop_association,
                            'related_products'=>$ids_related_products,
                            'ids_related_posts'=>$ids_related_posts,
                            'time_add' => $time_add
            );







            if(sizeof($data_title_content_lang)==0)
                $this->errors[] = $this->l('Please fill the Title');
            if(sizeof($data_validation)==0)
                $this->errors[] = $this->l('Please fill the Content');
            if(!($cat_shop_association))
                $this->errors[] = $this->l('Please select the Shop');
            if(!$time_add)
                $this->errors[] = $this->l('Please select Date Add');


            if (empty($this->errors)) {



                $data_result = $spmblocknewsadvfunctions_obj->saveItem($data);

                $data_result_error = $data_result['error'];
                $data_result_error_text = $data_result['error_text'];

                if($data_result_error){
                    $this->errors[] = $data_result_error_text;
                    $this->display = 'add';
                    return FALSE;
                } else {


                    Tools::redirectAdmin(self::$currentIndex . '&conf=3&token=' . Tools::getAdminTokenLite($this->_name_controller));
                }
            } else {
                $this->display = 'add';
                return FALSE;
             }
            ## add item ##

        } elseif(Tools::isSubmit('update_item')) {
                $id = Tools::getValue('id');


                 //echo "<pre>"; var_dump($_FILES);exit;


                $seo_url = Tools::getValue("seo_url");
                $languages = Language::getLanguages(false);
                $data_title_content_lang = array();
                $data_validation = array();

                $ids_related_products = Tools::getValue("inputAccessories");

                $ids_related_posts = Tools::getValue("ids_related_posts");


                $time_add = Tools::getValue("time_add");
                $cat_shop_association = Tools::getValue("cat_shop_association");


                foreach ($languages as $language){
                    $id_lang = $language['id_lang'];
                    $item_title = Tools::getValue("item_title_".$id_lang);
                    $item_seokeywords = Tools::getValue("item_seokeywords_".$id_lang);
                    $item_seodescription = Tools::getValue("item_seodescription_".$id_lang);
                    $item_content = Tools::getValue("content_".$id_lang);

                    if(Tools::strlen($item_title)>0 || Tools::strlen($item_content)>0)
                    {
                        $data_title_content_lang[$id_lang] = array('title' => $item_title,
                                                                    'seokeywords' => $item_seokeywords,
                                                                    'seodescription' => $item_seodescription,
                                                                    'content' => $item_content,
                                                                    'seo_url'=>$seo_url
                        );
                        $data_validation[$id_lang] = array('content' => $item_content);
                    }
                }


                $item_status = Tools::getValue("item_status");
                $item_iscomments = Tools::getValue("item_iscomments");
                $item_images = Tools::getValue("post_images");

                $data = array('data_title_content_lang'=>$data_title_content_lang,
                            'item_status' => $item_status,
                            'iscomments' => $item_iscomments,
                            'id' => $id,
                            'post_images' => $item_images,
                            'cat_shop_association' => $cat_shop_association,
                            'related_products'=>$ids_related_products,
                            'ids_related_posts'=>$ids_related_posts,
                            'time_add' => $time_add
                );



                if(sizeof($data_title_content_lang)==0)
                    $this->errors[] = $this->l('Please fill the Title');
                if(sizeof($data_validation)==0)
                    $this->errors[] = $this->l('Please fill the Content');
                if(!($cat_shop_association))
                    $this->errors[] = $this->l('Please select the Shop');
                if(!$time_add)
                    $this->errors[] = $this->l('Please select Date Add');



             if (empty($this->errors)) {

                 $data_result = $spmblocknewsadvfunctions_obj->updateItem($data);

                 $data_result_error = $data_result['error'];
                 $data_result_error_text = $data_result['error_text'];

                 if($data_result_error){
                     $this->errors[] = $data_result_error_text;
                     $this->display = 'add';
                     return FALSE;
                 } else {


                     Tools::redirectAdmin(self::$currentIndex . '&conf=4&token=' . Tools::getAdminTokenLite($this->_name_controller));
                 }
            }else{

                $this->display = 'add';
                return FALSE;
            }

            ## update item ##
            } elseif (Tools::isSubmit('submitBulkdelete' . $this->_name_module)) {
                ### delete more than one  items ###
                if ($this->tabAccess['delete'] === '1' || $this->tabAccess['delete'] === true) {
                    if (Tools::getValue($this->list_id . 'Box')) {


                        $object = new $this->className();

                        if ($object->deleteSelection(Tools::getValue($this->list_id . 'Box'))) {
                            Tools::redirectAdmin(self::$currentIndex . '&conf=2' . '&token=' . $this->token);
                        }
                        $this->errors[] = $this->l('An error occurred while deleting this selection.');
                    } else {
                        $this->errors[] = $this->l('You must select at least one element to delete.');
                    }
                } else {
                    $this->errors[] = $this->l('You do not have permission to delete this.');
                }
                ### delete more than one  items ###
            } elseif (Tools::isSubmit('delete' . $this->_name_module)) {
                ## delete item ##

                $id = Tools::getValue('id');

                $spmblocknewsadvfunctions_obj->deleteItem(array('id' => $id));

                Tools::redirectAdmin(self::$currentIndex . '&conf=1&token=' . Tools::getAdminTokenLite($this->_name_controller));
                ## delete item ##
            } else {
               return parent::postProcess(true);
            }




    }


    public function setMedia($isNewTheme=null)
    {

        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            parent::setMedia($isNewTheme);
        } else {
            parent::setMedia();
        }

        $this->context->controller->addCSS(__PS_BASE_URI__.'js/jquery/plugins/autocomplete/jquery.autocomplete.css');
        $this->context->controller->addJs(__PS_BASE_URI__.'js/jquery/plugins/autocomplete/jquery.autocomplete.js');

        $this->context->controller->addJs(__PS_BASE_URI__.'modules/'.$this->_name_module.'/views/js/admin.js');

        $this->context->controller->addCSS(__PS_BASE_URI__.'modules/'.$this->_name_module.'/views/css/font-custom.min.css');

        $this->addJqueryUi(array('ui.core','ui.widget','ui.datepicker'));

    }


    public function renderForm()
    {
        if (!($this->loadObject(true)))
            return;

        if (Validate::isLoadedObject($this->object)) {
            $this->display = 'update';
        } else {
            $this->display = 'add';
        }


        $id = (int)Tools::getValue('id');

        require_once(_PS_MODULE_DIR_ . '' . $this->_name_module . '/classes/spmblocknewsadvfunctions.class.php');
        $spmblocknewsadvfunctions_obj = new spmblocknewsadvfunctions();



        require_once(_PS_MODULE_DIR_ . '' . $this->_name_module . '/spmblocknewsadv.php');
        $spmblocknewsadv = new spmblocknewsadv();
        $is_demo = $spmblocknewsadv->is_demo;
        if($is_demo){

            ob_start();
            include(dirname(__FILE__).'/../../views/templates/hooks/feature_disabled_on_the_demo.phtml');
            $is_demo = ob_get_clean();



        } else {
            $is_demo = '';
        }




        if($id) {

            $_data = $spmblocknewsadvfunctions_obj->getItem(array('id'=>$id));

            $id_shop = isset($_data['item'][0]['id_shop']) ? explode(",",$_data['item'][0]['id_shop']) : array();
            $time_add = isset($_data['item'][0]['time_add']) ? $_data['item'][0]['time_add'] :date("Y-m-d H:i:s") ;
            $logo_img = isset($_data['item'][0]['img']) ? $_data['item'][0]['img'] :'' ;

            $selected_data = $_data['item'][0]['related_posts'];
            $selected_data = explode(",",$selected_data);


            // is cloud ?? //
            if(defined('_PS_HOST_MODE_')){
                $logo_img_path = '../modules/'.$this->_name_module.'/upload/'.$logo_img;
            } else {
                $logo_img_path = '../upload/'.$this->_name_module.'/'.$logo_img;
            }
            // is cloud ?? //

            $related_products_selected = isset($_data['item'][0]['related_products']) ?$_data['item'][0]['related_products']: 0;


        } else {

            $id_shop = array();
            $time_add = date("Y-m-d H:i:s");
            $logo_img = '';
            $logo_img_path = '';
            $related_products_selected = 0;
            $selected_data = array();

        }

        $related_news  = $spmblocknewsadvfunctions_obj->getRelatedPosts(array('admin'=>1,'id'=>$id));

        $related_products = (($related_products_selected!=0) ? $spmblocknewsadvfunctions_obj->getProducts($related_products_selected) : array());


        if($id){
            $title_item_form = $this->l('Edit news:');
        } else{
            $title_item_form = $this->l('Add new news:');
        }



        $this->fields_form = array(
            'tinymce' => TRUE,
            'legend' => array(
                'title' => $title_item_form,
                'icon' => 'fa fa-newspaper-o fa-lg'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Title'),
                    'name' => 'item_title',
                    'id' => 'item_title',
                    'lang' => true,
                    'required' => TRUE,
                    'size' => 5000,
                    'maxlength' => 5000,
                ),



                array(
                    'type' => 'textarea',
                    'label' => $this->l('SEO Keywords'),
                    'name' => 'item_seokeywords',
                    'id' => 'item_seokeywords',
                    'required' => FALSE,
                    'autoload_rte' => FALSE,
                    'lang' => TRUE,
                    'rows' => 8,
                    'cols' => 40,

                ),

                array(
                    'type' => 'textarea',
                    'label' => $this->l('SEO Description'),
                    'name' => 'item_seodescription',
                    'id' => 'item_seodescription',
                    'required' => FALSE,
                    'autoload_rte' => FALSE,
                    'lang' => TRUE,
                    'rows' => 8,
                    'cols' => 40,

                ),

                array(
                    'type' => 'textarea',
                    'label' => $this->l('Content'),
                    'name' => 'content',
                    'id' => 'content',
                    'required' => TRUE,
                    'autoload_rte' => TRUE,
                    'lang' => TRUE,
                    'rows' => 8,
                    'cols' => 40,

                ),

                array(
                    'type' => 'item_image_custom',
                    'label' => $this->l('Logo Image'),
                    'name' => 'news_image',
                    'logo_img'=>$logo_img,
                    'logo_img_path' => $logo_img_path,
                    'id_item' => $id,
                    'required' => FALSE,
                    'desc' => $this->l('Allow formats *.jpg; *.jpeg; *.png; *.gif.'),
                    'is_demo' => $is_demo,
                    'max_upload_info' => ini_get('upload_max_filesize'),
                    'token_custom'=>Tools::getAdminTokenLite('AdminSpmblocknewsadvajax'),
                    'ajax_link' => $this->context->link->getAdminLink('AdminSpmblocknewsadvajax'),
                ),

                array(
                    'type' => 'related_items',
                    'label' => $this->l('Related news'),
                    'name' => 'ids_related_posts',
                    'values'=>$related_news['related_posts'],
                    'selected_data'=>$selected_data,
                    'required' => false,
                    'name_field_custom'=>'ids_related_posts',
                ),

                array(
                    'type' => 'related_products',
                    'label' => $this->l('Related Products'),
                    'name' => 'related_products',
                    'values'=>$related_products,
                    'selected_data'=>$related_products_selected,
                    'required' => false,
                    'desc' => $this->l('Begin typing the first letters of the product name, then select the product from the drop-down list'),
                ),


                array(
                    'type' => 'cms_pages',
                    'label' => $this->l('Shop association'),
                    'name' => 'cat_shop_association',
                    'values'=>Shop::getShops(),
                    'selected_data'=>$id_shop,
                    'required' => TRUE,
                ),

                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable Facebook Comments'),
                    'name' => 'item_iscomments',
                    'required' => FALSE,
                    'class' => 't',
                    'is_bool' => TRUE,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),

                array(
                    'type' => 'item_date',
                    'label' => $this->l('Date Add'),
                    'name' => 'date_on',
                    'time_add' => $time_add,
                    'required' => TRUE,
                ),

                array(
                    'type' => 'switch',
                    'label' => $this->l('Status'),
                    'name' => 'item_status',
                    'required' => FALSE,
                    'class' => 't',
                    'is_bool' => TRUE,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),


            ),


        );


        if(Configuration::get($this->_name_module.'rew_on') == 1){
            $this->array_push_pos($this->fields_form['input'],1,
                array(
                    'type' => 'text',
                    'label' => $this->l('Identifier (SEO URL)'),
                    'name' => 'seo_url',
                    'id' => 'seo_url',
                    'lang' => false,
                    'required' => FALSE,
                    'size' => 5000,
                    'maxlength' => 5000,
                    'desc' => $this->l('You can leave the field blank - then Identifier (SEO URL) is generated automatically!').' (eg: http://domain.com/news/identifier)',



                )
            );
        }





        $this->fields_form['submit'] = array(
            'title' => ($id)?$this->l('Update'):$this->l('Save'),
        );




        if($id) {

            $this->tpl_form_vars = array(
               'fields_value' => $this->getConfigFieldsValuesForm(array('id'=>$id)),
            );

            $this->submit_action = 'update_item';
        } else {
            $this->submit_action = 'add_item';

        }



        return parent::renderForm();
    }


    

    public function getConfigFieldsValuesForm($data_in){



        $id = (int)Tools::getValue('id');
        if($id) {
            $id = $data_in['id'];

            require_once(_PS_MODULE_DIR_ . '' . $this->_name_module . '/classes/spmblocknewsadvfunctions.class.php');
            $spmblocknewsadvfunctions_obj = new spmblocknewsadvfunctions();

           $_data = $spmblocknewsadvfunctions_obj->getItem(array('id'=>$id));

            //var_dump($_data);exit;

            $languages = Language::getLanguages(false);
            $fields_title = array();
            $fields_seo_keywords = array();
            $fields_seo_description = array();
            $fields_content = array();

            foreach ($languages as $lang)
            {
                $fields_title[$lang['id_lang']] = isset($_data['item']['data'][$lang['id_lang']]['title'])?$_data['item']['data'][$lang['id_lang']]['title']:'';

                $fields_seo_keywords[$lang['id_lang']] = isset($_data['item']['data'][$lang['id_lang']]['seo_keywords'])?$_data['item']['data'][$lang['id_lang']]['seo_keywords']:'';

                $fields_seo_description[$lang['id_lang']] = isset($_data['item']['data'][$lang['id_lang']]['seo_description'])?$_data['item']['data'][$lang['id_lang']]['seo_description']:'';

                $fields_content[$lang['id_lang']] = isset($_data['item']['data'][$lang['id_lang']]['content'])?$_data['item']['data'][$lang['id_lang']]['content']:'';
            }

            $seo_url = isset($_data['item']['data'][$this->_id_lang]['seo_url'])?$_data['item']['data'][$this->_id_lang]['seo_url']:"";
            $status = isset($_data['item'][0]['status'])?$_data['item'][0]['status']:0;
            $is_comments = isset($_data['item'][0]['is_comments'])?$_data['item'][0]['is_comments']:0;


            $config_array = array(
                'item_title' => $fields_title,
                'content'=>$fields_content,
                'seo_url' => $seo_url,
                'item_seokeywords' => $fields_seo_keywords,
                'item_seodescription' => $fields_seo_description,
                'item_iscomments' => $is_comments,
                'item_status' => $status,
            );
        } else {
            $config_array = array();
        }
        return $config_array;
    }

    private function array_push_pos(&$array,$pos=0,$value,$key='')
    {
        if (!is_array($array)) {return false;}
        else
        {
            if (Tools::strlen($key) == 0) {$key = $pos;}
            $c = count($array);
            $one = array_slice($array,0,$pos);
            $two = array_slice($array,$pos,$c);
            $one[$key] = $value;
            $array = array_merge($one,$two);
            return;
        }
    }

    public function l($string , $class = NULL, $addslashes = false, $htmlentities = true){
        if(version_compare(_PS_VERSION_, '1.7', '<')) {
            return parent::l($string);
        } else {
            //$class = array();
            //return Context::getContext()->getTranslator()->trans($string, $class, $addslashes, $htmlentities);
            return Translate::getModuleTranslation($this->_name_module, $string, $this->_name_module);
        }
    }
}





?>

