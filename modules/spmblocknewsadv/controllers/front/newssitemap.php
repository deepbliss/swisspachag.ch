<?php
/**
 * 2011 - 2018 StorePrestaModules SPM LLC.
 *
 * MODULE spmblocknewsadv
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.2.2
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmblocknewsadvNewssitemapModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {


        $name = "spmblocknewsadv";
        $token = Tools::getValue('token');

        include_once(_PS_MODULE_DIR_.$name.'/'.$name.'.php');
        $obj_main = new $name();

        $_token = $obj_main->getokencron();



        if($_token == $token){

            include_once(_PS_MODULE_DIR_.$name.'/classes/spmblocknewsadvfunctions.class.php');
            $spmblocknewsadvfunctions_obj = new spmblocknewsadvfunctions();

            $spmblocknewsadvfunctions_obj->generateSitemap();


        } else {
            echo 'Error: Access denien! Invalid token!';
        }


        exit;
    }
}
?>