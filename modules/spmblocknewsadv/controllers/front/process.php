<?php
/**
 * 2011 - 2018 StorePrestaModules SPM LLC.
 *
 * MODULE spmblocknewsadv
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.2.2
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmblocknewsadvProcessModuleFrontController extends ModuleFrontController
{

    public function postProcess()
    {

        $HTTP_X_REQUESTED_WITH = isset($_SERVER['HTTP_X_REQUESTED_WITH'])?$_SERVER['HTTP_X_REQUESTED_WITH']:'';
        if($HTTP_X_REQUESTED_WITH != 'XMLHttpRequest') {
            exit;
        }


        ob_start();
        $status = 'success';
        $message = '';

        $name_module = 'spmblocknewsadv';


        include_once(_PS_MODULE_DIR_ . $name_module.'/classes/spmblocknewsadvfunctions.class.php');
        $obj_blocknewshelp = new spmblocknewsadvfunctions();

        $action = Tools::getValue('action');

        switch ($action){

            case 'like':
                $ip = $_SERVER['REMOTE_ADDR'];
                $like = (int)Tools::getValue('like');
                $id = (int)Tools::getValue('id');

                $data = $obj_blocknewshelp->like(array('id'=>$id,'like'=>$like,'ip'=>$ip));
                $status = $data['error'];
                $message = $data['message'];
                $count = $data['count'];
            break;


            default:
                $status = 'error';
                $message = 'Unknown parameters!';
                break;
        }


        $response = new stdClass();
        $content = ob_get_clean();
        $response->status = $status;
        $response->message = $message;
        if($action == 'like')
            $response->params = array('content' => $content, 'count'=>$count);
        else
            $response->params = array('content' => $content);




        echo json_encode($response);

    }



}
?>