<?php
/**
 * 2011 - 2018 StorePrestaModules SPM LLC.
 *
 * MODULE spmblocknewsadv
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.2.2
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmblocknewsadvRssModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {


        $_name = "spmblocknewsadv";

        include_once(_PS_MODULE_DIR_.$_name.'/classes/spmblocknewsadvfunctions.class.php');
        $obj_blocknewshelp = new spmblocknewsadvfunctions();



        $cookie = Context::getContext()->cookie;

        $id_lang = (int)$cookie->id_lang;
        $data_language = $obj_blocknewshelp->getfacebooklib($id_lang);
        $rss_title =  Configuration::get($_name.'rssname_'.$id_lang);;
        $rss_description =  Configuration::get($_name.'rssdesc_'.$id_lang);;

        if (Configuration::get('PS_SSL_ENABLED') == 1)
            $url = "https://";
        else
            $url = "http://";

        $site = $_SERVER['HTTP_HOST'];

        // Lets build the page
        //$rootURL = $url.$site."/feeds/";
        $latestBuild = date("r");

        // Lets define the the type of doc we're creating.
        $createXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";


        if(Configuration::get($_name.'rsson') == 1){

            $_http_host = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;

            $createXML .= "<rss version=\"0.92\">\n";
            $createXML .= "<channel>
	<title>".$rss_title."</title>
	<link>$url.$site</link>
	<description>".$rss_description."</description>
	<lastBuildDate>$latestBuild</lastBuildDate>
	<docs>http://backend.userland.com/rss092</docs>
	<language>".$data_language['rss_language_iso']."</language>
	<image>
			<title><![CDATA[".$rss_title."]]></title>
			<url>"._PS_BASE_URL_.__PS_BASE_URI__."img/logo.jpg</url>
			<link>$url$site</link>
	</image>
";

            $data_rss_items = $obj_blocknewshelp->getItemsForRSS();
            foreach($data_rss_items['items'] as $_item)
            {
                if(Configuration::get($_name.'rew_on') == 1)
                    $page =  $_item['seo_url'];
                else
                    $page = $_item['id'];


                $description = $_item['seo_description'];
                $title = $_item['title'];
                $pubdate = $_item['pubdate'];
                if(Tools::strlen($_item['img'])>0) {
                    if (defined('_PS_HOST_MODE_'))
                        $img = $_http_host."modules/".$_name."/upload/".$_item['img'];
                    else
                        $img = $_http_host."upload/".$_name."/".$_item['img'];
                }else {
                    $img = '';
                }

                $createXML .= $obj_blocknewshelp->createRSSFile($title,$description,$page,$pubdate,$img);
            }
            $createXML .= "</channel>\n </rss>";
// Finish it up
        }

        echo $createXML;
        exit;
    }

}