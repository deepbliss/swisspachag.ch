<?php
/**
 * 2011 - 2018 StorePrestaModules SPM LLC.
 *
 * MODULE spmblocknewsadv
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.2.2
 * @link      http://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

function upgrade_module_1_2_2($module)
{


    $name_module = "spmblocknewsadv";


    #### posts api ###
    $languages = Language::getLanguages(false);
    foreach ($languages as $language){
        $i = $language['id_lang'];
        $iso = Tools::strtoupper(Language::getIsoById($i));
        Configuration::updateValue($name_module.'twdesc_'.$i, 'New news has been added in our shop '.$iso);
        Configuration::updateValue($name_module.'idesc_'.$i, 'New news has been added in our shop '.$iso);
        Configuration::updateValue($name_module.'pdesc_'.$i, 'New news has been added in our shop '.$iso);
        Configuration::updateValue($name_module.'vkdesc_'.$i, 'New news has been added in our shop '.$iso);
    }

    #### posts api ###
    

    Configuration::updateValue($name_module.'news_h', 3);
    Configuration::updateValue($name_module.'news_bp_sl', 3);


    Configuration::updateValue($name_module.'bnews_slider', 1);
    Configuration::updateValue($name_module.'bnews_sl', 2);

    Configuration::updateValue($name_module.'relpr_slider', 1);
    Configuration::updateValue($name_module.'npr_slider', 3);

    Configuration::updateValue($name_module.'relp_slider', 1);
    Configuration::updateValue($name_module.'np_slider', 3);


    ## regenerate tabs ##



    $tab_id = Tab::getIdFromClassName("AdminSpmblocknewsadv");
    if($tab_id){
        $tab = new Tab($tab_id);
        $tab->delete();
    }

    $tab_id = Tab::getIdFromClassName("AdminSpmblocknewsadvNews");
    if($tab_id){
        $tab = new Tab($tab_id);
        $tab->delete();
    }


    $module->createAdminTabs15();
    ## regenerate tabs ##




    /// clear smarty cache ///

    Tools::clearSmartyCache();

    /// clear smarty cache ///

    return true;

}