<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class Editorial extends Module
{
	public function __construct()
	{
		$this->name = 'editorial';
		$this->tab = 'front_office_features';
		$this->version = '2.0';
		$this->author = 'PrestaShop';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('Home text editor');
		$this->description = $this->l('A text-edit module for your homepage.');
		$path = dirname(__FILE__);
		if (strpos(__FILE__, 'Module.php') !== false)
			$path .= '/../modules/'.$this->name;
		include_once $path.'/EditorialClass.php';
	}

	public function install()
	{
		if (!parent::install() || !$this->registerHook('displayHome') || !$this->registerHook('displayHeader'))
			return false;

		$res = Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'editorial` (
			`id_editorial` int(10) unsigned NOT NULL auto_increment,
			`id_shop` int(10) unsigned NOT NULL ,
			PRIMARY KEY (`id_editorial`))
			ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8');

		if ($res)
			$res &= Db::getInstance()->execute('
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'editorial_lang` (
				`id_editorial` int(10) unsigned NOT NULL,
				`id_lang` int(10) unsigned NOT NULL,
				`body_title_one` varchar(255) NOT NULL,
				`body_title_two` varchar(255) NOT NULL,
				`body_title_three` varchar(255) NOT NULL,
				`body_paragraph_one` text NOT NULL,
				`body_paragraph_two` text NOT NULL,
				`body_paragraph_three` text NOT NULL,
				`body_link_one` text NOT NULL,
				`body_link_two` text NOT NULL,
				`body_link_three` text NOT NULL,
				`body_link_one_d` text NOT NULL,
				`body_link_two_d` text NOT NULL,
				`body_link_three_d` text NOT NULL,
				PRIMARY KEY (`id_editorial`, `id_lang`))
				ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8');


		if ($res)
			foreach
			(Shop::getShops(false) as $shop)
				$res &= $this->createExampleEditorial($shop['id_shop']);

			if (!$res)
				$res &= $this->uninstall();

			return $res;
	}

	private function createExampleEditorial($id_shop)
	{
		$editorial = new EditorialClass();
		$editorial->id_shop = (int)$id_shop;
		foreach (Language::getLanguages(false) as $lang)
		{
			$editorial->body_title_one[$lang['id_lang']] = 'Lorem ipsum dolor sit amet';
			$editorial->body_paragraph_one[$lang['id_lang']] = '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>';
            $editorial->body_link_one[$lang['id_lang']] = 'google.pl';
		}
		return $editorial->add();
	}

	public function uninstall()
	{
		$res = Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'editorial`');
		$res &= Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'editorial_lang`');

		if (!$res || !parent::uninstall())
			return false;

		return true;
	}

	private function initForm()
	{
		$languages = Language::getLanguages(false);
		foreach ($languages as $k => $language)
			$languages[$k]['is_default'] = (int)($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT'));

		$helper = new HelperForm();
		$helper->module = $this;
		$helper->name_controller = 'editorial';
		$helper->identifier = $this->identifier;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->languages = $languages;
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
		$helper->allow_employee_form_lang = true;
		$helper->toolbar_scroll = true;
		$helper->toolbar_btn = $this->initToolbar();
		$helper->title = $this->displayName;
		$helper->submit_action = 'submitUpdateEditorial';
		
		$this->fields_form[0]['form'] = array(
			'tinymce' => true,
			'legend' => array(
				'title' => $this->displayName,
				'image' => $this->_path.'logo.gif'
			),
			'submit' => array(
				'name' => 'submitUpdateEditorial',
				'title' => $this->l('Save '),
				'class' => 'button'
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Main title no 1'),
					'name' => 'body_title_one',
					'size' => 64
				),
				array(
					'type' => 'textarea',
					'label' => $this->l('Text no 1'),
					'name' => 'body_paragraph_one',
					'autoload_rte' => true,
					'hint' => $this->l('For example... explain your mission, highlight a new product, or describe a recent event.'),
					'cols' => 60,
					'rows' => 30
				),
                array(
                    'type' => 'text',
                    'label' => $this->l('Link URL box 1'),
                    'name' => 'body_link_one',
                    'size' => 64
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Link label box 1'),
                    'name' => 'body_link_one_d',
                    'size' => 64
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Main title box 2'),
                    'name' => 'body_title_two',
                    'size' => 64
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('text box 2'),
                    'name' => 'body_paragraph_two',
                    'autoload_rte' => true,
                    'hint' => $this->l('For example... explain your mission, highlight a new product, or describe a recent event.'),
                    'cols' => 60,
                    'rows' => 30
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Link URL box 2'),
                    'name' => 'body_link_two',
                    'size' => 64
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Link label box 2'),
                    'name' => 'body_link_two_d',
                    'size' => 64
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Main title box 3'),
                    'name' => 'body_title_three',
                    'size' => 64
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('text box 3'),
                    'name' => 'body_paragraph_three',
                    'autoload_rte' => true,
                    'hint' => $this->l('For example... explain your mission, highlight a new product, or describe a recent event.'),
                    'cols' => 60,
                    'rows' => 30
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Link URL box 3'),
                    'name' => 'body_link_three',
                    'size' => 64
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Link label box 3'),
                    'name' => 'body_link_three_d',
                    'size' => 64
                ),
            )
		);		
		return $helper;
	}

	private function initToolbar()
	{
		$this->toolbar_btn['save'] = array(
			'href' => '#',
			'desc' => $this->l('Save')
		);
		
		return $this->toolbar_btn;
	}

	public function getContent()
	{
		$this->_html = '';
		$this->postProcess();
		
		$helper = $this->initForm();
		
		$id_shop = (int)$this->context->shop->id;
		$editorial = EditorialClass::getByIdShop($id_shop);

        if (!$editorial)
            return;
        $editorial = new EditorialClass((int)$editorial->id, $this->context->language->id);

        $helper->fields_value = array (
            'body_title_one' => $editorial->body_title_one,
            'body_title_two' => $editorial->body_title_two,
            'body_title_three' => $editorial->body_title_three,
            'body_paragraph_one' => $editorial->body_paragraph_one,
            'body_paragraph_two' => $editorial->body_paragraph_two,
            'body_paragraph_three' => $editorial->body_paragraph_three,
            'body_link_one' => $editorial->body_link_one,
            'body_link_two' => $editorial->body_link_two,
            'body_link_three' => $editorial->body_link_three,
            'body_link_one_d' => $editorial->body_link_one_d,
            'body_link_two_d' => $editorial->body_link_two_d,
            'body_link_three_d' => $editorial->body_link_three_d,
        );
		
		$this->_html .= $helper->generateForm($this->fields_form);

		return $this->_html;
	}

	public function postProcess()
	{
		$errors = '';
		$id_shop = (int)$this->context->shop->id;

		if (Tools::isSubmit('submitUpdateEditorial'))
		{
			$id_shop = (int)$this->context->shop->id;
			$editorial = EditorialClass::getByIdShop($id_shop);
			$editorial->copyFromPost();
			$editorial->update();
			$this->_clearCache('editorial.tpl');
		}
	}

	public function hookDisplayHome($params)
	{
		if (!$this->isCached('editorial.tpl', $this->getCacheId()))
		{
			$id_shop = (int)$this->context->shop->id;
			$editorial = EditorialClass::getByIdShop($id_shop);
			if (!$editorial)
				return;			
			$editorial = new EditorialClass((int)$editorial->id, $this->context->language->id);
			if (!$editorial)
				return;
			$this->smarty->assign(array(
					'editorial' => $editorial,
					'default_lang' => (int)$this->context->language->id,
					'image_width' => Configuration::get('EDITORIAL_IMAGE_WIDTH'),
					'image_height' => Configuration::get('EDITORIAL_IMAGE_HEIGHT'),
					'id_lang' => $this->context->language->id,
					'homepage_logo' => !Configuration::get('EDITORIAL_IMAGE_DISABLE') && file_exists('modules/editorial/homepage_logo_'.(int)$id_shop.'.jpg'),
					'image_path' => $this->_path.'homepage_logo_'.(int)$id_shop.'.jpg'
				));
		}
		return $this->display(__FILE__, 'editorial.tpl', $this->getCacheId());
	}

	public function hookDisplayHeader()
	{
		$this->context->controller->addCSS(($this->_path).'editorial.css', 'all');
	}
}
