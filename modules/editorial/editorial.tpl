{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Module Editorial -->
{*<div id="editorial_block_center" class="editorial_block">*}
	{*{if $editorial->body_home_logo_link}<a href="{$editorial->body_home_logo_link|escape:'htmlall':'UTF-8'}" title="{$editorial->body_title|escape:'htmlall':'UTF-8'|stripslashes}">{/if}*}
	{*{if $homepage_logo}<img src="{$link->getMediaLink($image_path)}" alt="{$editorial->body_title|escape:'htmlall':'UTF-8'|stripslashes}" {if $image_width}width="{$image_width}"{/if} {if $image_height}height="{$image_height}" {/if}/>{/if}*}
	{*{if $editorial->body_home_logo_link}</a>{/if}*}
	{*{if $editorial->body_logo_subheading}<p id="editorial_image_legend">{$editorial->body_logo_subheading|stripslashes}</p>{/if}*}
	{*{if $editorial->body_title}<h1>{$editorial->body_title|stripslashes}</h1>{/if}*}
	{*{if $editorial->body_subheading}<h2>{$editorial->body_subheading|stripslashes}</h2>{/if}*}
	{*{if $editorial->body_paragraph}<div class="rte">{$editorial->body_paragraph|stripslashes}</div>{/if}*}
{*</div>*}

<div id="boxesWrapper">
    {if 2==3}
    <div class="box">
        <h2 class="box-header">RESTPOSTEN<span></span></h2>
        <div class="content">
            <img class="outsider" src="{$img_dir}/theme/stamp.png" />
            <p>Schnäppchen und Restposten zu Schnäppchenpreisen!</p>
            <p>Hier finden Sie Spezialangebote bis zu 20 % günstiger.
            </p>
        </div>
        <a href="http://swisspackag.ch/index.php?id_category=159&controller=category&id_lang=2" class="readMore">zum Restposten</a>
        <!--<span class="stamp"></span>-->
    </div>
    <div class="box">
        <h2 class="box-header">KLEBEBÄNDER<span></span></h2>
        <div class="content">
            <p>Klebebänder in diversen Variationen!</p>
            <img src="{$img_dir}/theme/ebene4.jpg" width=210px height=77px >
            <p>Klebebänder braun, transparent, bedruckt, Abdeckband, Hand-Abroll-geräte u.v.m.</p>
        </div>
        <a href="http://swisspackag.ch/index.php?id_category=52&controller=category&id_lang=2" class="readMore">jetzt bestellen</a>
    </div>
    <div class="box">
        <h2 class="box-header">KUNSTSTOFFBEUTEL<span></span></h2>
        <div class="content">
            <p>Verschiedene Kunststoffbeutel!</p>
            <img src="{$img_dir}/theme/ebene6.jpg" width=210px height=77px >
            <p>Druckverschlussbeutel, ESD-Beutel Flachbeutel, Schrumpfhauben, Abdeckfolien u.v.m.</p>
        </div>
        <a href="http://swisspackag.ch/index.php?id_category=33&controller=category&id_lang=2" class="readMore">jetzt bestellen</a>
    </div>
	{/if}


    {if $editorial->body_title_one}
    <div class="box">
        <h2 class="box-header">{$editorial->body_title_one}<span></span></h2>
        <div class="content">{$editorial->body_paragraph_one}</div>
        <a href="{$editorial->body_link_one}" class="readMore">{$editorial->body_link_one_d}</a>
    </div>
    {/if}
    {if $editorial->body_title_two}
    <div class="box">
        <h2 class="box-header">{$editorial->body_title_two}<span></span></h2>
        <div class="content">{$editorial->body_paragraph_two}</div>
        <a href="{$editorial->body_link_two}" class="readMore">{$editorial->body_link_two_d}</a>
    </div>
    {/if}
    {if $editorial->body_title_three}
    <div class="box">
        <h2 class="box-header">{$editorial->body_title_three}<span></span></h2>
        <div class="content">{$editorial->body_paragraph_three}</div>
        <a href="{$editorial->body_link_three}" class="readMore">{$editorial->body_link_three_d}</a>
    </div>
    {/if}
	
    <div class="box box-blue">
        <h2 class="box-header">ONLINE SUCHE<span></span></h2>
		<form id="searchbox_bottom" action="http://swisspackag.ch/index.php?controller=search" method="get">
        <div class="content">
            <p>Hier finden Sie Ihre Produkte auf den ersten Blick. Geben Sie Ihren Suchbegriff ein:
            </p>
			<p>
			<label for="search_query_top"><!-- image on background --></label>
			<input type="hidden" value="search" name="controller">
			<input type="hidden" value="position" name="orderby">
			<input type="hidden" value="desc" name="orderway">
			<input type="text" name="search_query" id="search_query_bottom" class="search_query ac_input search" value="" autocomplete="off">
            <input type="submit" class="button readMore" value="Suchen" name="submit_search" style="float: right; margin-top: -26px; width: 58px; margin-left: 189px;">
			</p>
            <p>Suchen Sie nach Verpackungsmaterialien aller Art.</p>
        </div>
		</form>
    </div>
    <div class="clearFix"></div>
</div>
<!-- /Module Editorial -->
