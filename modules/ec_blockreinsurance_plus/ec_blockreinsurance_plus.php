<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL Ether Création
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL Ether Création is strictly forbidden.
* In order to obtain a license, please contact us: contact@ethercreation.com
* ...........................................................................
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe Ether Création
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
* ...........................................................................
* @package ec_blockreinsurance_plus
* @copyright Copyright (c) 2010-2013 S.A.R.L Ether Création (http://www.ethercreation.com)
* @author Arthur R.
* @license Commercial license
*/

if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

include_once _PS_MODULE_DIR_.'ec_blockreinsurance_plus/class/ec_reinsuranceClass.php';

class Ec_Blockreinsurance_Plus extends Module
{
    public function __construct()
    {
        $this->name = 'ec_blockreinsurance_plus';
        $this->tab = 'front_office_features';

        $this->version = '1.1.0';
        $this->author = 'Ether Création';
        $this->module_key = 'b915277556e7d9f53ca85eb681f01953';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Customizable and responsive customer reassurance block');
        $this->description = $this->l('Adds a customizable and responsive information block aimed at offering helpful information to reassure customers that your store is trustworthy.');
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        return parent::install() &&
            $this->installDB() &&
            Configuration::updateValue('BLOCKREINSURANCE_NBBLOCKS', 5) &&
            $this->registerHook('footer') && $this->installFixtures() && $this->registerHook('header');
    }
    
    public function installDB()
    {
        $return = true;
        $return &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ec_reinsurance` (
                `id_reinsurance` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_shop` int(10) unsigned NOT NULL ,
                `file_name` VARCHAR(100) NOT NULL,
                PRIMARY KEY (`id_reinsurance`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');
        
        $return &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ec_reinsurance_lang` (
                `id_reinsurance` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `id_lang` int(10) unsigned NOT NULL ,
                `title` varchar(200) NOT NULL,
                `text` text NOT NULL,
                PRIMARY KEY (`id_reinsurance`, `id_lang`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');
        
        return $return;
    }

    public function uninstall()
    {
        return Configuration::deleteByName('BLOCKREINSURANCE_NBBLOCKS') &&
            $this->uninstallDB() &&
            parent::uninstall();
    }

    public function uninstallDB()
    {
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ec_reinsurance`') && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ec_reinsurance_lang`');
    }

    public function addToDB()
    {
        if (Tools::isSubmit('nbblocks')) {
            for ($i = 1; $i <= (int)Tools::getValue('nbblocks'); $i++) {
                $filename = explode('.', $_FILES['info'.$i.'_file']['name']);
                if (isset($_FILES['info'.$i.'_file']) && isset($_FILES['info'.$i.'_file']['tmp_name']) && !empty($_FILES['info'.$i.'_file']['tmp_name'])) {
                    if (ImageManager::validateUpload($_FILES['info'.$i.'_file'])) {
                        return false;
                    } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['info'.$i.'_file']['tmp_name'], $tmpName)) {
                        return false;
                    } elseif (!ImageManager::resize($tmpName, dirname(__FILE__).'/views/img/'.$filename[0].'.jpg')) {
                        return false;
                    }
                    unlink($tmpName);
                }
                Db::getInstance()->execute(
                    'INSERT INTO `'._DB_PREFIX_.'ec_reinsurance` (`filename`,`text`)
                                            VALUES ("'.((isset($filename[0]) && $filename[0] != '') ? pSQL($filename[0]) : '').
                    '", "'.((Tools::isSubmit('info'.$i.'_text') && Tools::getValue('info'.$i.'_text') != '') ? pSQL(Tools::getValue('info'.$i.'_text')) : '').'")'
                );
            }
            return true;
        } else {
            return false;
        }
    }

    public function removeFromDB()
    {
        $dir = opendir(dirname(__FILE__).'/views/img');
        while (false !== ($file = readdir($dir))) {
            $path = dirname(__FILE__).'/views/img/'.$file;
            if ($file != '..' && $file != '.' && !is_dir($file)) {
                unlink($path);
            }
        }
        closedir($dir);

        return Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'ec_reinsurance`');
    }

    public function getContent()
    {
        $html = '';
        $id_reinsurance = (int)Tools::getValue('id_reinsurance');

        if (Tools::isSubmit('saveblockreinsurance')) {
            if ($id_reinsurance = Tools::getValue('id_reinsurance')) {
                $reinsurance = new ec_reinsuranceClass((int)$id_reinsurance);
            } else {
                $reinsurance = new ec_reinsuranceClass();
            }
            $reinsurance->copyFromPost();
            $reinsurance->id_shop = $this->context->shop->id;
            if ($reinsurance->validateFields(false) && $reinsurance->validateFieldsLang(false)) {
                $reinsurance->save();
                if (isset($_FILES['image']) && isset($_FILES['image']['tmp_name']) && !empty($_FILES['image']['tmp_name'])) {
                    if (ImageManager::validateUpload($_FILES['image'])) {
                        return false;
                    } elseif (!($tmpName = tempnam(_PS_TMP_IMG_DIR_, 'PS')) || !move_uploaded_file($_FILES['image']['tmp_name'], $tmpName)) {
                        return false;
                    } elseif (!ImageManager::resize($tmpName, dirname(__FILE__).'/views/img/reinsurance-'.(int)$reinsurance->id.'-'.(int)$reinsurance->id_shop.'.jpg')) {
                        return false;
                    }
                    unlink($tmpName);
                    $reinsurance->file_name = 'reinsurance-'.(int)$reinsurance->id.'-'.(int)$reinsurance->id_shop.'.jpg';
                    $reinsurance->save();
                }
                $this->_clearCache('views/templates/front/ec_blockreinsurance_plus.tpl');
            } else {
                $html .= $this->displayError($this->l('An error occurred with a text field while attempting to save.'));
            }
        }
        
        if (Tools::isSubmit('updateec_blockreinsurance_plus') || Tools::isSubmit('addblockreinsurance')) {
            $helper = $this->initForm();
            foreach (Language::getLanguages(false) as $lang) {
                if ($id_reinsurance) {
                    $reinsurance = new ec_reinsuranceClass((int)$id_reinsurance);
                    $helper->fields_value['title'][(int)$lang['id_lang']] = $reinsurance->title[(int)$lang['id_lang']];
                    $helper->fields_value['text'][(int)$lang['id_lang']] = $reinsurance->text[(int)$lang['id_lang']];
                } else {
                    $helper->fields_value['title'][(int)$lang['id_lang']] = Tools::getValue('title_'.(int)$lang['id_lang'], '');
                    $helper->fields_value['text'][(int)$lang['id_lang']] = Tools::getValue('text_'.(int)$lang['id_lang'], '');
                }
            }
            if ($id_reinsurance = Tools::getValue('id_reinsurance')) {
                $this->fields_form[0]['form']['input'][] = array('type' => 'hidden', 'name' => 'id_reinsurance');
                $helper->fields_value['id_reinsurance'] = (int)$id_reinsurance;
            }
            return $html.$helper->generateForm($this->fields_form);
        } elseif (Tools::isSubmit('deleteec_blockreinsurance_plus')) {
            $reinsurance = new ec_reinsuranceClass((int)$id_reinsurance);
            if (file_exists(dirname(__FILE__).'/views/img/'.$reinsurance->file_name)) {
                unlink(dirname(__FILE__).'/views/img/'.$reinsurance->file_name);
            }
            $reinsurance->delete();
            $this->_clearCache('views/templates/front/ec_blockreinsurance_plus.tpl');
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        } else {
            $helper = $this->initList();
            return $html.$helper->generateList($this->getListContent((int)$this->context->language->id), $this->fields_list);
        }

        if (Tools::isSubmit('submitModule')) {
            Configuration::updateValue('BLOCKREINSURANCE_NBBLOCKS', ((Tools::isSubmit('nbblocks') && Tools::getValue('nbblocks') != '') ? (int)Tools::getValue('nbblocks') : ''));
            if ($this->removeFromDB() && $this->addToDB()) {
                $this->_clearCache('views/templates/front/ec_blockreinsurance_plus.tpl');
                $html .= $this->displayConfirmation($this->l('The block configuration has been updated.'));
            } else {
                $html .= $this->displayError($this->l('An error occurred while attempting to save.'));
            }
        }
    }

    protected function getListContent($id_lang)
    {
        return Db::getInstance()->executeS('
            SELECT r.`id_reinsurance`, r.`id_shop`, r.`file_name`, rl.`text`, rl.`title`
            FROM `'._DB_PREFIX_.'ec_reinsurance` r
            LEFT JOIN `'._DB_PREFIX_.'ec_reinsurance_lang` rl ON (r.`id_reinsurance` = rl.`id_reinsurance`)
            WHERE `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang());
    }

    protected function initForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        
        $image = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
            SELECT `file_name`
            FROM `'._DB_PREFIX_.'ec_reinsurance`
            WHERE id_reinsurance = '.(int)Tools::getValue('id_reinsurance'));
        $url_img = '';
        if ($image != '') {
            $url_img = '<img style="background-color:#333333;max-width: 100px;" src="'.__PS_BASE_URI__.'modules/ec_blockreinsurance_plus/views/img/'.$image.'" alt="" class="img-thumbnail" />';
        }
        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('New reassurance block'),
            ),
            'input' => array(
                array(
                    'type' => 'file',
                    'label' => $this->l('Image'),
                    'name' => 'image',
                    'display_image' => true,
                    'image' => $url_img,
                    'value' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Title'),
                    'lang' => true,
                    'name' => 'title'
                ),
                array(
                    'type' => 'textarea',
                    'autoload_rte' => true,
                    'label' => $this->l('Text'),
                    'lang' => true,
                    'name' => 'text'
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
            )
        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'blockreinsurance';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang) {
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        }
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'saveblockreinsurance';
        $helper->toolbar_btn = array(
            'save' =>
            array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&saveblockreinsurance&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' =>
            array(
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );
        return $helper;
    }

    protected function initList()
    {
        $this->fields_list = array(
            'id_reinsurance' => array(
                'title' => $this->l('ID'),
                'width' => 120,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'width' => 140,
                'type' => 'text',
                'search' => false,
                'orderby' => false
            ),
        );

        if (Shop::isFeatureActive()) {
            $this->fields_list['id_shop'] = array('title' => $this->l('ID Shop'), 'align' => 'center', 'width' => 25, 'type' => 'int');
        }
        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_reinsurance';
        $helper->actions = array('edit', 'delete');
        $helper->show_toolbar = true;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] = array(
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addblockreinsurance&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new')
        );

        $helper->title = $this->displayName;
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper;
    }

    public function hookFooter($params)
    {
        
        if (!$this->isCached('views/templates/front/ec_blockreinsurance_plus.tpl', $this->getCacheId())) {
            $infos = $this->getListContent($this->context->language->id);
            $version = '1.6';
            if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
                $version = '1.7';
            }
            $this->context->smarty->assign(array('infos' => $infos, 'nbblocks' => count($infos), 'version' => $version));
        }
        return $this->display(__FILE__, 'views/templates/front/ec_blockreinsurance_plus.tpl', $this->getCacheId());
    }
    
    public function hookHeader($params)
    {
        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $this->context->controller->addCSS($this->_path.'/views/css/style17.css');
        } else {
            $this->context->controller->addCSS($this->_path.'/views/css/style.css');
        }
    }

    public function installFixtures()
    {
        $return = true;
        $tab_texts = array();
        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            $tab_texts = array(
            array('title' => $this->l('Money back guarantee'), 'text' => $this->l('Money back guarantee'), 'file_name' => 'reinsurance-1-117.jpg'),
            array('title' => $this->l('In-store exchange'), 'text' => $this->l('In-store exchange'), 'file_name' => 'reinsurance-2-117.jpg'),
            array('title' => $this->l('Free Shipping'), 'text' => $this->l('Free Shipping'), 'file_name' => 'reinsurance-3-117.jpg'),
            array('title' => $this->l('100% secure payment processing'), 'text' => $this->l('100% secure payment processing'), 'file_name' => 'reinsurance-4-117.jpg')
            );
        } else {
            $tab_texts = array(
            array('title' => $this->l('Money back guarantee'), 'text' => $this->l('Money back guarantee'), 'file_name' => 'reinsurance-1-1.jpg'),
            array('title' => $this->l('In-store exchange'), 'text' => $this->l('In-store exchange'), 'file_name' => 'reinsurance-2-1.jpg'),
            array('title' => $this->l('Free Shipping'), 'text' => $this->l('Free Shipping'), 'file_name' => 'reinsurance-3-1.jpg'),
            array('title' => $this->l('100% secure payment processing'), 'text' => $this->l('100% secure payment processing'), 'file_name' => 'reinsurance-4-1.jpg')
            );
        }
        
        
        foreach ($tab_texts as $tab) {
            $reinsurance = new ec_reinsuranceClass();
            foreach (Language::getLanguages(false) as $lang) {
                $reinsurance->text[$lang['id_lang']] = $tab['text'];
                $reinsurance->title[$lang['id_lang']] = $tab['title'];
            }
                
            $reinsurance->file_name = $tab['file_name'];
            $reinsurance->id_shop = $this->context->shop->id;
            $return &= $reinsurance->save();
        }
        return $return;
    }
}
