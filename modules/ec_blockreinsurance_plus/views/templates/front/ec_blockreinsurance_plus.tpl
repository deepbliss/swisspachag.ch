{*
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL Ether Création
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL Ether Création is strictly forbidden.
* In order to obtain a license, please contact us: contact@ethercreation.com
* ...........................................................................
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe Ether Création
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
* ...........................................................................
*  @package ec_blockreinsurance_plus
*  @copyright Copyright (c) 2010-2013 S.A.R.L Ether Création (http://www.ethercreation.com)
*  @author Arthur R.
*  @license Commercial license
*}
{if $infos|@count > 0}
<!-- MODULE Block reinsurance -->
<div id="ec_reinsurance_block" class="clearfix">
	<ul>	
		{foreach from=$infos item=info}
			<li class="col-md-{if $nbblocks == 1}12{elseif $nbblocks == 2}6{elseif $nbblocks == 3}4{elseif $nbblocks == 4}3{elseif $nbblocks == 5}2{elseif $nbblocks == 6}2{else}1{/if} col-xs-6">{if $info.file_name != ""}<img src="{$link->getMediaLink("`$module_dir`views/img/`$info.file_name|escape:'htmlall':'UTF-8'`")}" alt="{$info.title|escape:html:'UTF-8'}" />{/if} <div>{if $version=='1.7'}{$info.text|escape:'htmlall':'UTF-8' nofilter}{else}{html_entity_decode($info.text|escape:'htmlall':'UTF-8')}{/if}</div></li>                        
		{/foreach}
	</ul>
</div>
<!-- /MODULE Block reinsurance -->
{/if}