<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_fa8ad9cf9ba018ff4c7ee43762b4b75a'] = 'Bloc d\'informations personnalisable et responsive';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_a30f4afa6d107ed8e6f523ed7c340fd2'] = 'Ajoutez un bloc d\'informations personnalisable et responsive visant à offrir des informations utiles pour rassurer les clients que votre magasin est digne de confiance.';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_4eab507e80c0679e6d5c46889b16e83e'] = 'Une erreur est survenue en tentant de sauver un champ texte.';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_0366c7b2ea1bb228cd44aec7e3e26a3e'] = 'La configuration de bloc a été mis à jour.';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_d52eaeff31af37a4a7e0550008aff5df'] = 'Une erreur est survenue en tentant de sauver.';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_8363eee01f4da190a4cef9d26a36750c'] = 'Nouveau bloc rassurance';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_be53a0541a6d36f6ecb879fa2c584b08'] = 'Image';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_b78a3223503896721cca1303f776159b'] = 'Titre';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_9dffbf69ffba8bc38bc4e01abf4b1675'] = 'Texte';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_630f6dc397fe74e52d5189e2c80f282b'] = 'Retour à la liste';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_23498d91b6017e5d7f4ddde70daba286'] = 'ID boutique';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_ef61fb324d729c341ea8ab9901e23566'] = 'Ajouter';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_b550d86cd48bd8983cc135a8e2a8662f'] = 'Satisfait ou remboursé ';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_31010280ee21bb0ca58ff9bb609a8a32'] = 'Echange en magasin ';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_b00b85425e74ed2c85dc3119b78ff2c3'] = 'Livraison Gratuite';
$_MODULE['<{ec_blockreinsurance_plus}prestashop>ec_blockreinsurance_plus_e838d6362d16346783a6412fd570a9c2'] = 'Paiement sécurisé ';
