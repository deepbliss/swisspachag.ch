<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL Ether Création
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL Ether Création is strictly forbidden.
* In order to obtain a license, please contact us: contact@ethercreation.com
* ...........................................................................
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe Ether Création
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
* ...........................................................................
* @package ec_blockreinsurance_plus
* @copyright Copyright (c) 2010-2013 S.A.R.L Ether Création (http://www.ethercreation.com)
* @author Arthur R.
* @license Commercial license
*/

class ec_reinsuranceClass extends ObjectModel
{
    /** @var integer reinsurance id*/
    public $id;

    /** @var integer reinsurance id shop*/
    public $id_shop;

    /** @var string reinsurance file name icon*/
    public $file_name;

    /** @var string reinsurance title*/
    public $title;

    /** @var html reinsurance text*/
    public $text;


    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'ec_reinsurance',
        'primary' => 'id_reinsurance',
        'multilang' => true,
        'fields' => array(
            'id_shop' =>                array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            'file_name' =>                array('type' => self::TYPE_STRING, 'validate' => 'isFileName'),
            /** Lang fields */
            'title' =>            array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml',  'size' => 255),//'required' => true,
            'text' =>                    array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 4000),
        )
    );

    public function copyFromPost()
    {
        /* Classical fields */
        foreach ($_POST as $key => $value) {
            if (array_key_exists($key, $this) && $key != 'id_'.$this->table) {
                $this->{$key} = $value;
            }
        }
        /* Multilingual fields */
        if (count($this->fieldsValidateLang)) {
            $languages = Language::getLanguages(false);
            foreach ($languages as $language) {
                foreach ($this->fieldsValidateLang as $field => $validation) {
                    if (Tools::isSubmit($field.'_'.(int)($language['id_lang']))) {
                        $this->{$field}[(int)($language['id_lang'])] = Tools::getValue($field.'_'.(int)($language['id_lang']));
                    }
                }
            }
        }
        
    }
}
