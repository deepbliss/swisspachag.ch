{**
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    FME Modules
*  @copyright © 2018 FME Modules
*  @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<hr>
<p class="help-block">{l s='You can use the following variables in your template.' mod='staticblock'}</p>
<p class="clearfix"><code class="col-lg-2">{literal}{id_static_block}{/literal}</code><span class="col-lg-10"> => <b>{l s='Your static block id.' mod='staticblock'}</b></span></p>
<p class="clearfix"><code class="col-lg-2">{literal}{block_title}{/literal}</code><span class="col-lg-10"> => <b>{l s='Your static block title.' mod='staticblock'}</b></span></p>
<p class="clearfix"><code class="col-lg-2">{literal}{content}{/literal}</code><span class="col-lg-10"> => <b>{l s='Your static block content.' mod='staticblock'}</b></span></p>
<p class="clearfix"><code class="col-lg-2">{literal}{id_lang}{/literal}</code><span class="col-lg-10"> => <b>{l s='Current active language.' mod='staticblock'}</b></span></p>
<p class="clearfix"><code class="col-lg-2">{literal}{iso_lang}{/literal}</code><span class="col-lg-10"> => <b>{l s='Current active language iso code.' mod='staticblock'}</b></span></p>
<p class="clearfix"><code class="col-lg-2">{literal}{shop_url}{/literal}</code><span class="col-lg-10"> => <b>{l s='Your shop URL.' mod='staticblock'}</b></span></p>
<p class="clearfix"><code class="col-lg-2">{literal}{shop_name}{/literal}</code><span class="col-lg-10"> => <b>{l s='Your shop name.' mod='staticblock'}</b></span></p>
<p class="clearfix"><code class="col-lg-2">{literal}{shop_email}{/literal}</code><span class="col-lg-10"> => <b>{l s='Your shop email.' mod='staticblock'}</b></span></p>
