<?php
/* Smarty version 3.1.33, created on 2018-12-18 13:25:25
  from 'D:\xampp\htdocs\project\swisspackag.ch\admin230rsywmq\themes\default\template\controllers\cms_content\content.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c18e7351c1a53_54845070',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '18dbf7851bc38789c599b2c5d3ed1bf3fb19ab5a' => 
    array (
      0 => 'D:\\xampp\\htdocs\\project\\swisspackag.ch\\admin230rsywmq\\themes\\default\\template\\controllers\\cms_content\\content.tpl',
      1 => 1545028955,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c18e7351c1a53_54845070 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['cms_breadcrumb']->value)) {?>
	<ul class="breadcrumb cat_bar">
		<?php echo $_smarty_tpl->tpl_vars['cms_breadcrumb']->value;?>

	</ul>
<?php }?>

<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

<?php if (isset($_smarty_tpl->tpl_vars['url_prev']->value)) {?>
	<?php echo '<script'; ?>
 type="text/javascript">
	$(document).ready(function () {
		var re = /url_preview=(.*)/;
		var url = re.exec(window.location.href);
		if (typeof url !== 'undefined' && url !== null && typeof url[1] !== 'undefined' && url[1] === "1")
			window.open("<?php echo $_smarty_tpl->tpl_vars['url_prev']->value;?>
", "_blank");
	});
	<?php echo '</script'; ?>
>
<?php }
}
}
