<?php
/* Smarty version 3.1.33, created on 2018-12-19 05:53:24
  from 'D:\xampp\htdocs\project\swisspackag.ch\modules\htmlbox\html.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c19cec4115c66_72540348',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2079bed6be59079c1e03e8f44ee0c14c855fc854' => 
    array (
      0 => 'D:\\xampp\\htdocs\\project\\swisspackag.ch\\modules\\htmlbox\\html.tpl',
      1 => 1545137848,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c19cec4115c66_72540348 (Smarty_Internal_Template $_smarty_tpl) {
if (Configuration::get('htmlbox_logged') == 1 || Configuration::get('htmlbox_unlogged') == 1) {?>
    <?php if (Configuration::get('htmlbox_logged') == 1) {?>
        <?php if ($_smarty_tpl->tpl_vars['logged']->value == true) {?>
            <?php $_smarty_tpl->_assignInScope('hidefreeblock', '0');?>
        <?php } else { ?>
            <?php $_smarty_tpl->_assignInScope('hidefreeblock', '1');?>
        <?php }?>
    <?php } elseif (Configuration::get('htmlbox_unlogged') == 1) {?>
        <?php if ($_smarty_tpl->tpl_vars['logged']->value == true) {?>
            <?php $_smarty_tpl->_assignInScope('hidefreeblock', '1');?>
        <?php } else { ?>
            <?php $_smarty_tpl->_assignInScope('hidefreeblock', '0');?>
        <?php }?>
    <?php }
} else { ?>
    <?php $_smarty_tpl->_assignInScope('hidefreeblock', '0');
}?>



<?php if ($_smarty_tpl->tpl_vars['hidefreeblock']->value == 0) {?>
    <?php if ($_smarty_tpl->tpl_vars['htmlbox_ssl']->value == 1) {?>
        <?php if ($_smarty_tpl->tpl_vars['is_https_htmlbox']->value == 1) {?>
            <?php if ($_smarty_tpl->tpl_vars['page_name']->value != 'index') {?>
                <?php if ($_smarty_tpl->tpl_vars['htmlbox_home']->value == 1) {?>
                                    <?php } else { ?>
                    <?php echo $_smarty_tpl->tpl_vars['htmlboxbody']->value;?>

                <?php }?>
            <?php } else { ?>
                <?php echo $_smarty_tpl->tpl_vars['htmlboxbody']->value;?>

            <?php }?>
        <?php }?>
    <?php } else { ?>
        <?php if ($_smarty_tpl->tpl_vars['page_name']->value != 'index') {?>
            <?php if ($_smarty_tpl->tpl_vars['htmlbox_home']->value == 1) {?>
                            <?php } else { ?>
                <?php echo $_smarty_tpl->tpl_vars['htmlboxbody']->value;?>

            <?php }?>
        <?php } else { ?>
            <?php echo $_smarty_tpl->tpl_vars['htmlboxbody']->value;?>

        <?php }?>
    <?php }
}
}
}
