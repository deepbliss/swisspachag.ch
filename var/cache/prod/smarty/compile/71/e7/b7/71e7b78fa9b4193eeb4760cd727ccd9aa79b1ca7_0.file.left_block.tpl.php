<?php
/* Smarty version 3.1.33, created on 2018-12-19 08:14:46
  from 'D:\xampp\htdocs\project\swisspackag.ch\modules\ets_pres2pres\views\templates\hook\left_block.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c19efe63cbfb3_09866923',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '71e7b78fa9b4193eeb4760cd727ccd9aa79b1ca7' => 
    array (
      0 => 'D:\\xampp\\htdocs\\project\\swisspackag.ch\\modules\\ets_pres2pres\\views\\templates\\hook\\left_block.tpl',
      1 => 1545196462,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c19efe63cbfb3_09866923 (Smarty_Internal_Template $_smarty_tpl) {
?><ul>
    <li<?php if ($_smarty_tpl->tpl_vars['controller']->value == 'AdminPresToPresImport') {?> class="active"<?php }?>><a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminPresToPresImport',true),'html','UTF-8' ));?>
"><i class="icon icon-cloud-upload"> </i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Migration','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</a></li>
    <li<?php if ($_smarty_tpl->tpl_vars['controller']->value == 'AdminPresToPresHistory') {?> class="active"<?php }?>><a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminPresToPresHistory',true),'html','UTF-8' ));?>
"><i class="icon icon-history"> </i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'History','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</a></li>
    <li<?php if ($_smarty_tpl->tpl_vars['controller']->value == 'AdminPresToPresClean') {?> class="active"<?php }?>><a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminPresToPresClean',true),'html','UTF-8' ));?>
"><i class="icon icon-eraser"> </i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Clean-up','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</a></li>
    <li<?php if ($_smarty_tpl->tpl_vars['controller']->value == 'AdminPresToPresHelp') {?> class="active"<?php }?>><a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminPresToPresHelp',true),'html','UTF-8' ));?>
"><i class="icon icon-question-circle"> </i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Help','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</a></li>
</ul><?php }
}
