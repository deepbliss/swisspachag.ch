<?php
/* Smarty version 3.1.33, created on 2018-12-18 07:45:40
  from 'D:\xampp\htdocs\project\swisspackag.ch\modules\blocknewsadv\home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c18979455b5d1_38876505',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e7c760a80c13e0bd66b57c3e71a7b4fe6b3ebd12' => 
    array (
      0 => 'D:\\xampp\\htdocs\\project\\swisspackag.ch\\modules\\blocknewsadv\\home.tpl',
      1 => 1545115475,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c18979455b5d1_38876505 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="block-last-blocknewsadvs block blockmanufacturer" style="margin-top:10px">
		<h4>
			
			<div class="blocknewsadv-float-left">
				<?php if ($_smarty_tpl->tpl_vars['blocknewsadvurlrewrite_on']->value == 1) {?>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir']->value, ENT_QUOTES, 'UTF-8');?>
news/">
				<?php } else { ?>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir']->value, ENT_QUOTES, 'UTF-8');?>
modules/blocknewsadv/news.php">
				<?php }?>
					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Last News','mod'=>'blocknewsadv'),$_smarty_tpl ) );?>

				</a>
			</div>
			<div class="blocknewsadv-float-right">
			<?php if ($_smarty_tpl->tpl_vars['blocknewsadvrsson']->value == 1) {?>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8');?>
rss.php" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'RSS Feed','mod'=>'blocknewsadv'),$_smarty_tpl ) );?>
">
					<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8');?>
i/feed.png" alt="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'RSS Feed','mod'=>'blocknewsadv'),$_smarty_tpl ) );?>
" />
				</a>
			<?php }?>
			</div>
			<div class="blocknewsadv-clear"></div>
		</h4>
		<div class="block_content">
			<?php if (count($_smarty_tpl->tpl_vars['blocknewsadvitems_home']->value) > 0) {?>
			<ul class="items-last-blocknewsadvs">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['blocknewsadvitems_home']->value, 'items', false, NULL, 'myLoop', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['items']->value) {
?>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value['data'], 'item', false, NULL, 'myLoop', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>
	    		<li>
	    			
	    			<table width="100%">
	    				<tr>
	    				<?php if (strlen($_smarty_tpl->tpl_vars['item']->value['img']) > 0) {?>
	    						<td align="center" style="width:20%">
	    							<?php if ($_smarty_tpl->tpl_vars['blocknewsadvurlrewrite_on']->value == 1) {?>
	    								<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir']->value, ENT_QUOTES, 'UTF-8');?>
news/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['seo_url'], ENT_QUOTES, 'UTF-8');?>
/" 
			    		  				   title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['title'],"htmlall","UTF-8" )), ENT_QUOTES, 'UTF-8');?>
">
	    							<?php } else { ?>
			    						<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir']->value, ENT_QUOTES, 'UTF-8');?>
modules/blocknewsadv/news-item.php?id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id'], ENT_QUOTES, 'UTF-8');?>
" 
			    		  				   title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['title'],"htmlall","UTF-8" )), ENT_QUOTES, 'UTF-8');?>
">
			    		  			<?php }?>
		    						<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir']->value, ENT_QUOTES, 'UTF-8');?>
upload/blocknewsadv/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['img'], ENT_QUOTES, 'UTF-8');?>
" 
		    								  title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['title'],"htmlall","UTF-8" )), ENT_QUOTES, 'UTF-8');?>
" style="height:45px" />
		    						</a>
		    					</td>
	    					
	    				<?php }?>
	    					<td class="blocknewsadv-title-and-content">
	    						<table width="100%">
	    							<tr>
	    								<td>
	    									<?php if ($_smarty_tpl->tpl_vars['blocknewsadvurlrewrite_on']->value == 1) {?>
			    								<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir']->value, ENT_QUOTES, 'UTF-8');?>
news/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['seo_url'], ENT_QUOTES, 'UTF-8');?>
/" 
					    		  				   title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['title'],"htmlall","UTF-8" )), ENT_QUOTES, 'UTF-8');?>
">
			    							<?php } else { ?>
					    						<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir']->value, ENT_QUOTES, 'UTF-8');?>
modules/blocknewsadv/news-item.php?id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id'], ENT_QUOTES, 'UTF-8');?>
" 
					    		  				   title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['title'],"htmlall","UTF-8" )), ENT_QUOTES, 'UTF-8');?>
">
					    		  			<?php }?>
	    										<b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'], ENT_QUOTES, 'UTF-8');?>
</b>
	    									</a>
	    								</td>
	    							</tr>
	    							
	    							<tr>
				    					<td>
				    						<?php if ($_smarty_tpl->tpl_vars['blocknewsadvurlrewrite_on']->value == 1) {?>
				    							<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir']->value, ENT_QUOTES, 'UTF-8');?>
news/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['seo_url'], ENT_QUOTES, 'UTF-8');?>
/" 
						    		  			   title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( substr(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['item']->value['content']),0,150),"htmlall","UTF-8" )), ENT_QUOTES, 'UTF-8');
if (strlen($_smarty_tpl->tpl_vars['item']->value['content']) > 150) {?>...<?php }?>"
				    		   	   			   	   style="font-size:11px">
				    		   	   			
				    						<?php } else { ?>
					    						<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['base_dir']->value, ENT_QUOTES, 'UTF-8');?>
modules/blocknewsadv/news-item.php?id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['id'], ENT_QUOTES, 'UTF-8');?>
" 
							    		  		   title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( substr(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['item']->value['content']),0,150),"htmlall","UTF-8" )), ENT_QUOTES, 'UTF-8');
if (strlen($_smarty_tpl->tpl_vars['item']->value['content']) > 150) {?>...<?php }?>"
					    		   	   			   style="font-size:11px">
					    		   	   		<?php }?>
				    		   	   				<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( substr(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['item']->value['content']),0,150),"htmlall","UTF-8" )), ENT_QUOTES, 'UTF-8');
if (strlen($_smarty_tpl->tpl_vars['item']->value['content']) > 150) {?>...<?php }?>
				    						</a>
				    					</td>
	    							</tr>
	    				
	    						</table>
	    					</td>
	    				</tr>
	    				</table>
	    		   	
	    		</li>
	    		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	    	<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	    	</ul>
	    	<?php } else { ?>
	    		<div class="blocknewsadv-block-noitems">
					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'News not found.','mod'=>'blocknewsadv'),$_smarty_tpl ) );?>

				</div>
	    	<?php }?>
	    </div>
</div>


<?php }
}
