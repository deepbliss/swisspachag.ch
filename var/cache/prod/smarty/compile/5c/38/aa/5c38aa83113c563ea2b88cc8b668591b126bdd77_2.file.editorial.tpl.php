<?php
/* Smarty version 3.1.33, created on 2018-12-18 09:42:19
  from 'D:\xampp\htdocs\project\swisspackag.ch\modules\editorial\editorial.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c18b2eb26ef42_16352869',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5c38aa83113c563ea2b88cc8b668591b126bdd77' => 
    array (
      0 => 'D:\\xampp\\htdocs\\project\\swisspackag.ch\\modules\\editorial\\editorial.tpl',
      1 => 1545121419,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c18b2eb26ef42_16352869 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- Module Editorial -->
							
<div id="boxesWrapper">
    <?php if (2 == 3) {?>
    <div class="box">
        <h2 class="box-header">RESTPOSTEN<span></span></h2>
        <div class="content">
            <img class="outsider" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['img_dir']->value, ENT_QUOTES, 'UTF-8');?>
/theme/stamp.png" />
            <p>Schnäppchen und Restposten zu Schnäppchenpreisen!</p>
            <p>Hier finden Sie Spezialangebote bis zu 20 % günstiger.
            </p>
        </div>
        <a href="http://swisspackag.ch/index.php?id_category=159&controller=category&id_lang=2" class="readMore">zum Restposten</a>
        <!--<span class="stamp"></span>-->
    </div>
    <div class="box">
        <h2 class="box-header">KLEBEBÄNDER<span></span></h2>
        <div class="content">
            <p>Klebebänder in diversen Variationen!</p>
            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['img_dir']->value, ENT_QUOTES, 'UTF-8');?>
/theme/ebene4.jpg" width=210px height=77px >
            <p>Klebebänder braun, transparent, bedruckt, Abdeckband, Hand-Abroll-geräte u.v.m.</p>
        </div>
        <a href="http://swisspackag.ch/index.php?id_category=52&controller=category&id_lang=2" class="readMore">jetzt bestellen</a>
    </div>
    <div class="box">
        <h2 class="box-header">KUNSTSTOFFBEUTEL<span></span></h2>
        <div class="content">
            <p>Verschiedene Kunststoffbeutel!</p>
            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['img_dir']->value, ENT_QUOTES, 'UTF-8');?>
/theme/ebene6.jpg" width=210px height=77px >
            <p>Druckverschlussbeutel, ESD-Beutel Flachbeutel, Schrumpfhauben, Abdeckfolien u.v.m.</p>
        </div>
        <a href="http://swisspackag.ch/index.php?id_category=33&controller=category&id_lang=2" class="readMore">jetzt bestellen</a>
    </div>
	<?php }?>


    <?php if ($_smarty_tpl->tpl_vars['editorial']->value->body_title_one) {?>
    <div class="box">
        <h2 class="box-header"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_title_one, ENT_QUOTES, 'UTF-8');?>
<span></span></h2>
        <div class="content"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_paragraph_one, ENT_QUOTES, 'UTF-8');?>
</div>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_link_one, ENT_QUOTES, 'UTF-8');?>
" class="readMore"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_link_one_d, ENT_QUOTES, 'UTF-8');?>
</a>
    </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['editorial']->value->body_title_two) {?>
    <div class="box">
        <h2 class="box-header"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_title_two, ENT_QUOTES, 'UTF-8');?>
<span></span></h2>
        <div class="content"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_paragraph_two, ENT_QUOTES, 'UTF-8');?>
</div>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_link_two, ENT_QUOTES, 'UTF-8');?>
" class="readMore"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_link_two_d, ENT_QUOTES, 'UTF-8');?>
</a>
    </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['editorial']->value->body_title_three) {?>
    <div class="box">
        <h2 class="box-header"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_title_three, ENT_QUOTES, 'UTF-8');?>
<span></span></h2>
        <div class="content"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_paragraph_three, ENT_QUOTES, 'UTF-8');?>
</div>
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_link_three, ENT_QUOTES, 'UTF-8');?>
" class="readMore"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['editorial']->value->body_link_three_d, ENT_QUOTES, 'UTF-8');?>
</a>
    </div>
    <?php }?>
	
    <div class="box box-blue">
        <h2 class="box-header">ONLINE SUCHE<span></span></h2>
		<form id="searchbox_bottom" action="http://swisspackag.ch/index.php?controller=search" method="get">
        <div class="content">
            <p>Hier finden Sie Ihre Produkte auf den ersten Blick. Geben Sie Ihren Suchbegriff ein:
            </p>
			<p>
			<label for="search_query_top"><!-- image on background --></label>
			<input type="hidden" value="search" name="controller">
			<input type="hidden" value="position" name="orderby">
			<input type="hidden" value="desc" name="orderway">
			<input type="text" name="search_query" id="search_query_bottom" class="search_query ac_input search" value="" autocomplete="off">
            <input type="submit" class="button readMore" value="Suchen" name="submit_search" style="float: right; margin-top: -26px; width: 58px; margin-left: 189px;">
			</p>
            <p>Suchen Sie nach Verpackungsmaterialien aller Art.</p>
        </div>
		</form>
    </div>
    <div class="clearFix"></div>
</div>
<!-- /Module Editorial -->
<?php }
}
