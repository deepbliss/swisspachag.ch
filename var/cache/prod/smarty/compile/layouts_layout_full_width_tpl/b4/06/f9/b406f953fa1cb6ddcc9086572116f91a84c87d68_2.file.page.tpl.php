<?php
/* Smarty version 3.1.33, created on 2018-12-19 11:39:37
  from '/var/www/html/swisspackag.ch/themes/classic/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c1a1fe9895c84_50912813',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b406f953fa1cb6ddcc9086572116f91a84c87d68' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/page.tpl',
      1 => 1545070356,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c1a1fe9895c84_50912813 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10143076445c1a1fe9890aa2_96078356', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_11171905015c1a1fe9891729_95172519 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_14423443345c1a1fe9891066_29470163 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11171905015c1a1fe9891729_95172519', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_7264824345c1a1fe9893670_14735540 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_1358044835c1a1fe9893e25_48217280 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_2551686535c1a1fe9893103_65125790 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7264824345c1a1fe9893670_14735540', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1358044835c1a1fe9893e25_48217280', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_6066121565c1a1fe9894f81_58495896 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_21073687795c1a1fe9894a89_52844871 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6066121565c1a1fe9894f81_58495896', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_10143076445c1a1fe9890aa2_96078356 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_10143076445c1a1fe9890aa2_96078356',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_14423443345c1a1fe9891066_29470163',
  ),
  'page_title' => 
  array (
    0 => 'Block_11171905015c1a1fe9891729_95172519',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_2551686535c1a1fe9893103_65125790',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_7264824345c1a1fe9893670_14735540',
  ),
  'page_content' => 
  array (
    0 => 'Block_1358044835c1a1fe9893e25_48217280',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_21073687795c1a1fe9894a89_52844871',
  ),
  'page_footer' => 
  array (
    0 => 'Block_6066121565c1a1fe9894f81_58495896',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14423443345c1a1fe9891066_29470163', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2551686535c1a1fe9893103_65125790', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21073687795c1a1fe9894a89_52844871', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
