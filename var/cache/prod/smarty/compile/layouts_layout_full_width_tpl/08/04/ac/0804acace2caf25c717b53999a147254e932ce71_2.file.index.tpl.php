<?php
/* Smarty version 3.1.33, created on 2018-12-19 11:39:37
  from '/var/www/html/swisspackag.ch/themes/classic/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c1a1fe988c578_90085846',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0804acace2caf25c717b53999a147254e932ce71' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/index.tpl',
      1 => 1545095484,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c1a1fe988c578_90085846 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14563092945c1a1fe98898a0_53247397', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_19687707485c1a1fe988a060_62470137 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_20530702585c1a1fe988af76_92967803 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_14211547495c1a1fe988aa25_41398659 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20530702585c1a1fe988af76_92967803', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_14563092945c1a1fe98898a0_53247397 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_14563092945c1a1fe98898a0_53247397',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_19687707485c1a1fe988a060_62470137',
  ),
  'page_content' => 
  array (
    0 => 'Block_14211547495c1a1fe988aa25_41398659',
  ),
  'hook_home' => 
  array (
    0 => 'Block_20530702585c1a1fe988af76_92967803',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19687707485c1a1fe988a060_62470137', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14211547495c1a1fe988aa25_41398659', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
