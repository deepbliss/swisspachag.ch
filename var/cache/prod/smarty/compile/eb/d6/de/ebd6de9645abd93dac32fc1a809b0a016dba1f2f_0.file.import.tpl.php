<?php
/* Smarty version 3.1.33, created on 2018-12-19 11:36:14
  from '/var/www/html/swisspackag.ch/modules/ets_pres2pres/views/templates/hook/import.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5c1a1f1e9a3495_99317511',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ebd6de9645abd93dac32fc1a809b0a016dba1f2f' => 
    array (
      0 => '/var/www/html/swisspackag.ch/modules/ets_pres2pres/views/templates/hook/import.tpl',
      1 => 1545237862,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5c1a1f1e9a3495_99317511 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
var ETS_DT_MODULE_URL_AJAX ='<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['ETS_DT_MODULE_URL_AJAX']->value,'html','UTF-8' ));?>
';
<?php echo '</script'; ?>
>
<div class="dtm-left-block">
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'pres2presLeftBlok'),$_smarty_tpl ) );?>

</div>
<div class="dtm-right-block">
    <?php if (isset($_smarty_tpl->tpl_vars['errors']->value) && $_smarty_tpl->tpl_vars['errors']->value) {?>
    <div class="bootstrap">
        <div class="module_error alert alert-danger">
            <button class="close" data-dismiss="alert" type="button">×</button>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['errors']->value, 'error');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['error']->value) {
?>
                <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['error']->value,'html','UTF-8' ));?>
<br />
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
    </div>
    <?php }?>
    <form id="module_form" action="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminPresToPresImport'),'html','UTF-8' ));?>
" class="defaultForm form-horizontal" novalidate="" enctype="multipart/form-data" method="post" >
        <input type="hidden" value="<?php echo intval($_smarty_tpl->tpl_vars['step']->value);?>
" name="step"/>
        <div id="fieldset_0" class="panel">
            <div class="panel-heading"><i class="icon-import"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Migration','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</div>
            <ul class="tab_step_data">
                <li class="data_number_step<?php if ($_smarty_tpl->tpl_vars['step']->value == 1) {?> active<?php }?>" data-step="1"><span>1</span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Source','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</li>
                 <li class="data_number_step<?php if ($_smarty_tpl->tpl_vars['step']->value == 2) {?> active<?php }?>" data-step="2"><span>2</span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Data to migrate','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</li>
                <li class="data_number_step<?php if ($_smarty_tpl->tpl_vars['step']->value == 3) {?> active<?php }?>" data-step="3"><span>3</span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Migration options','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</li>
                <li class="data_number_step<?php if ($_smarty_tpl->tpl_vars['step']->value == 4) {?> active<?php }?>" data-step="4"><span>4</span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Review migration','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</li>
                <li class="data_number_step<?php if ($_smarty_tpl->tpl_vars['step']->value == 5) {?> active<?php }?>" data-step="5"><span>5</span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Process migration','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</li>
            </ul>
            <div class="form-wrapper data_import_data">
                <div class="ybc-form-group connector_error">
                    <div class="alert alert-warning error">
                        Sorry! Direct migration can’t be done because there was problems with server to server connection between the target website and the source website. This problem happens because one of the servers (or both servers) is not responding or timed out. <br />
                        Below are suggested solutions for the problem:
                        <ul class="suggested_solutions">
                            <li>
                           	    <p>Update server settings, make sure both servers are configured with <strong>UNLIMITED</strong> (or least 2 minutes) for <a target="_blank" href="http://php.net/manual/en/info.configuration.php#ini.max-execution-time">max_execution_timeand</a> they can execute an <strong>UNLIMITED</strong> number of continuous HTTP requests. Disable any firewall programs or server settings that stop the servers (or give500 Internal Server Error) when a large number of HTTP requests are sent to them during the migration.</p> 
                            </li>
                            <li>
                                <p>Try to download entire data of the source website using the <strong>Prestashop Connector module</strong>, then import the data into the target website with <strong>“Source Type”</strong> is set to <strong>“Upload data file from computer”</strong> </p>
                            </li>
                            <li>
                                <p>Try to copy your websites to a better server or local computer (give maximum server resource usability for the websites) then start the migration again. </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ybc-form-group import_error">
                    <div class="alert alert-warning error">
                        Sorry! The server is not responding or timed out while importing data into the website database. This problem happens because the server is too <strong>LIMITED</strong> in resource usability.<br />
                        Below are suggested solutions for the problem:
                        <ul class="suggested_solutions">
                            <li>
                                <p>Update server settings, make sure both servers are configured with <strong>UNLIMITED</strong> (or least 2 minutes) for <a target="_blank" href="http://php.net/manual/en/info.configuration.php#ini.max-execution-time">max_execution_timeand</a> they can execute an <strong>UNLIMITED</strong> number of continuous HTTP requests. Disable any firewall programs or server settings that stop the servers (or give 500 Internal Server Error) when a large number of HTTP requests are sent to them during the migration. </p>
                            </li>
                            <li>
                                <p>Try to copy the target website to a better server or even  alocal computer (give maximum server resource usability for the website) then start the migration again. </p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ybc-form-group ybc-blog-tab-step1 <?php if ($_smarty_tpl->tpl_vars['step']->value == 1) {?> active<?php }?>">
                    <?php if (isset($_smarty_tpl->tpl_vars['form_step1']->value)) {?>
                        <?php echo $_smarty_tpl->tpl_vars['form_step1']->value;?>

                    <?php } else { ?>
                        <div class="form-group">
                            <label class="control-label col-lg-4" for="source_type"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Source type','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</label>
                            <div class="col-lg-8">
                                <select id="source_type" name="source_type">
                                    <option value="url_site"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Use connector URL','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</option>
                                    <option value="upload_file"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Upload data file from computer','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</option>
                                    <option value="link"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Upload data file from URL','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group source upload">
                            <label class="control-label col-lg-4" for="file_import"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Select data file','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
<span class="required">*</span></label>
                            <div class="col-lg-7">
                                <div class="data_upload_button_wrap">
                                    <input type="file" name="file_import" id="file_import"/>
                                    <div class="data_upload_button">
                                        <input type="text" name="data_upload_button" id="data_upload_button_input" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No file selected.','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
" />
                                        <div class="data_upload_button_right">
                                            <i class="icon icon-file"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Choose file','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group source link">
                            <label class="control-label col-lg-4" for="link_file"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enter data file URL','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
<span class="required">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" name="link_file" id="link_file" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'URL to .zip file','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
" />
                            </div>
                        </div>
                        <div class="form-group source url_site">
                            <label class="control-label col-lg-4" for="link_site"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Connector URL','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
<span class="required">*</span></label>
                            <div class="col-lg-8">
                                <input style="width: 100%;" type="text" name="link_site" id="link_site" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Eg: http://sourcewebsite.abc/modules/ets_pres2presconnector/connector.php','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
" />
                            </div>
                        </div>
                        <div class="form-group source url_site">
                            <label class="control-label col-lg-4" for="secure_access_tocken"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Secure access token','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
<span class="required">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" name="secure_access_tocken" id="secure_access_tocken" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Secure access token','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
" />
                            </div>
                        </div>
                        <input type="hidden" id="link_site_connector" value="" name="link_site_connector" />
                        <p class="link_download_plugin alert alert-info"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Before getting started with the migration, please download','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
&nbsp;<a href="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['mod_dr_pres2pres']->value,'html','UTF-8' ));?>
plugins/ets_pres2presconnector.zip" target="_blank"><b><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Prestashop Connector','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</b></a>&nbsp;<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>' module  and install the module on source Prestashop website. This Connector module gives you "Connector URL" and "Secure access token" (or data file) that is required above','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</p>
                    <?php }?> 
                </div>
                <div class="ybc-form-group ybc-blog-tab-step2 <?php if ($_smarty_tpl->tpl_vars['step']->value == 2) {?> active<?php }?>">
                    <?php if ($_smarty_tpl->tpl_vars['step']->value >= 2) {?>
                        <?php echo $_smarty_tpl->tpl_vars['form_step2']->value;?>

                    <?php }?>
                </div>
                <div class="ybc-form-group ybc-blog-tab-step3 <?php if ($_smarty_tpl->tpl_vars['step']->value == 3) {?> active<?php }?>">
                    <?php if ($_smarty_tpl->tpl_vars['step']->value >= 3) {?>
                        <?php echo $_smarty_tpl->tpl_vars['form_step3']->value;?>

                    <?php }?>
                </div>
                <div class="ybc-form-group ybc-blog-tab-step4<?php if ($_smarty_tpl->tpl_vars['step']->value == 4) {?> active<?php }?>">
                    <?php if ($_smarty_tpl->tpl_vars['step']->value >= 4) {?>
                        <?php echo $_smarty_tpl->tpl_vars['form_step4']->value;?>

                    <?php }?>
                </div>
                <div class="ybc-form-group ybc-blog-tab-step5<?php if ($_smarty_tpl->tpl_vars['step']->value == 5) {?> active<?php }?>">
                    <p><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'The import has been successfully processed.','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>
</p>
                </div>
                <div class="popup_uploading">
                    <div class="popup_uploading_table">
                        <div class="popup_uploading_tablecell">
                            <div class="popup_uploading_content">
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Loading data, please wait...!','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>

                                <div class="upload-wapper-all">
                                    <div class="upload-wapper-percent" style="width:0%;"></div><samp class="percentage_export">1%</samp>
                                    <div class="noTrespassingOuterBarG">
                                    	<div class="noTrespassingAnimationG">
                                    		<div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                                            <div class="noTrespassingBarLineG"></div>
                                            <div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                                            <div class="noTrespassingBarLineG"></div>
                                            <div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                                            <div class="noTrespassingBarLineG"></div>
                                            <div class="noTrespassingBarLineG"></div>
                                    		<div class="noTrespassingBarLineG"></div>
                    		                  <div class="noTrespassingBarLineG"></div>
                                    	</div>
                                    </div>
                                </div>
                                <samp class="percentage_export_table"></samp>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="popup_importing">
                </div>
            </div>
            
            <div class="panel-footer">
                <button class="btn btn-default pull-right" name="submitImport" value="1" type="submit" >
                    <i class="process-icon-next"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Next','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>

                </button>
                <button class="btn btn-default pull-left" name="submitBack" value="1" type="submit"<?php if ($_smarty_tpl->tpl_vars['step']->value == 1) {?> disabled="disabled"<?php }?>>
                    <i class="process-icon-back"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Back','mod'=>'ets_pres2pres'),$_smarty_tpl ) );?>

                </button>
                <div class="clearfix"> </div>
            </div>
        </div>
    </form>
</div>
<div class="dtm-clearfix"></div><?php }
}
