<?php

/* __string_template__658e6bf14c5a0caf7182350daf17f039ee02305563f29eb191c75f8d333489d3 */
class __TwigTemplate_1b9fb3a1d68900b7f2f5a2ec6a335427ca9288a27356220a43afcf4e471ac600 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'extra_stylesheets' => array($this, 'block_extra_stylesheets'),
            'content_header' => array($this, 'block_content_header'),
            'content' => array($this, 'block_content'),
            'content_footer' => array($this, 'block_content_footer'),
            'sidebar_right' => array($this, 'block_sidebar_right'),
            'javascripts' => array($this, 'block_javascripts'),
            'extra_javascripts' => array($this, 'block_extra_javascripts'),
            'translate_javascripts' => array($this, 'block_translate_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "__string_template__658e6bf14c5a0caf7182350daf17f039ee02305563f29eb191c75f8d333489d3"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "__string_template__658e6bf14c5a0caf7182350daf17f039ee02305563f29eb191c75f8d333489d3"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"de\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/swisspackag.ch/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/swisspackag.ch/img/app_icon.png\" />

<title>Installierte Module verwalten • Swisspack AG</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminModulesManage';
    var iso_user = 'de';
    var lang_is_rtl = '1';
    var full_language_code = 'de';
    var full_cldr_language_code = 'de-DE';
    var country_iso_code = 'CH';
    var _PS_VERSION_ = '1.7.4.4';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Eine neue Bestellung ist in Ihrem Shop eingegangen.';
    var order_number_msg = 'Bestell-Nr. ';
    var total_msg = 'Gesamt: ';
    var from_msg = 'von ';
    var see_order_msg = 'Diese Bestellung anzeigen';
    var new_customer_msg = 'Neue Kundenregistrierung im Shop.';
    var customer_name_msg = 'Kunde ';
    var new_msg = 'Ihr Shop hat eine neue Nachricht erhalten.';
    var see_msg = 'Nachricht lesen';
    var token = '34ae94254c1c60c88edfbe048b9711b1';
    var token_admin_orders = 'c8e1ac546cb5e9b266df20c3461f7690';
    var token_admin_customers = '1fba735d105445be8d8084ee92e1f22c';
    var token_admin_customer_threads = 'fc3cbcd5cccaedbf40043cb1767e5e19';
    var currentIndex = 'index.php?controller=AdminModulesManage';
    var employee_token = 'c5667a5df420aea0ae699d1b5ab862bc';
    var choose_language_translate = 'Wählen Sie eine Sprache';
    var default_language = '2';
    var admin_modules_link = '/swisspackag.ch/admin60238sgog/index.php/module/catalog/recommended?route=admin_module_catalog_post&_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0';
    var tab_modules_list = '';
    var update_success_msg = 'Aktualisierung durchgeführt!';
    var errorLogin = 'PrestaShop konnte sich nicht bei Addons anmelden. Überprüfen Sie bitte Ihre Zugangsdaten und Ihre Internetverbindung.';
    var search_product_msg = 'Artikel suchen';
  </script>

      <link href=\"/swisspackag.ch/modules/gamification/views/css/gamification_rtl.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/admin60238sgog/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/modules/ets_pres2pres/views/css/admin-icon.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/modules/ets_pres2pres/views/css/ps1.7.4.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/modules/invoicepayment/views/css/invoicepayment_editor.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/admin60238sgog/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/modules/advancedimporter/views/css/admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var AI_TOKEN = \"eb8a584e497de1354c41\";
var CONFIRM_DELETE = \"Sind Sie sicher?\";
var baseAdminDir = \"\\/swisspackag.ch\\/admin60238sgog\\/\";
var baseDir = \"\\/swisspackag.ch\\/\";
var currency = {\"iso_code\":\"CHF\",\"sign\":\"CHF\",\"name\":\"Schweizer Franken\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/jquery/jquery-1.11.0.min.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/jquery/jquery-migrate-1.2.1.min.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/modules/gamification/views/js/gamification_bt.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/modules/invoicepayment/views/js/invoicepayment_editor.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/admin60238sgog/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/admin.js?v=1.7.4.4\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/tools.js?v=1.7.4.4\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/admin60238sgog/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/admin60238sgog/themes/default/js/vendor/nv.d3.min.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/modules/advancedimporter/views/js/admin.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/rtl.js\"></script>

  <script>
\t\t\t\tvar ids_ps_advice = new Array();
\t\t\t\tvar admin_gamification_ajax_url = 'http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminGamification&token=ab2372d78e943e878316dc74ebf0a71f';
\t\t\t\tvar current_id_tab = 45;
\t\t\t</script><style type=\"text/css\">
\t\t.icon-AdminSpmblocknewsadv:before {
\t\t\tcontent: url(\"http://45.56.70.30/swisspackag.ch/modules/spmblocknewsadv/views/img/AdminSpmblocknewsadv.gif\");
\t\t}
\t\t</style>
\t\t

";
        // line 96
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>
<body class=\"lang-de lang-rtl adminmodulesmanage\">


<header id=\"header\">
  <nav id=\"header_infos\" class=\"main-header\">

    <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

        
        <i class=\"material-icons js-mobile-menu\">menu</i>
    <a id=\"header_logo\" class=\"logo float-left\" href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminDashboard&amp;token=a60173a8b528fe0fa2e6cac10511636e\"></a>
    <span id=\"shop_version\">1.7.4.4</span>

    <div class=\"component\" id=\"quick-access-container\">
      <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Schnellzugriff
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminOrders&amp;token=c8e1ac546cb5e9b266df20c3461f7690\"
                 data-item=\"Bestellungen\"
      >Bestellungen</a>
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php/module/manage?token=52d4027f0428270c2c0c8c1fd853b944\"
                 data-item=\"Installierte Module\"
      >Installierte Module</a>
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=134eaf1ee5657428650e3e2c47797fe7\"
                 data-item=\"Katalogauswertung\"
      >Katalogauswertung</a>
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCategories&amp;addcategory&amp;token=3af0b28a546b714056d81f9d29da6792\"
                 data-item=\"Neue Kategorie\"
      >Neue Kategorie</a>
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php/product/new?token=52d4027f0428270c2c0c8c1fd853b944\"
                 data-item=\"Neuer Artikel\"
      >Neuer Artikel</a>
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=a4b38ef1e57d20ebe3a7dee906b0b2f2\"
                 data-item=\"Neuer Ermäßigungsgutschein\"
      >Neuer Ermäßigungsgutschein</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"163\"
        data-icon=\"icon-AdminModulesSf\"
        data-method=\"add\"
        data-url=\"index.php/module/manage?-NLArZCZsQJXYfJ3iLSDCqP8CS0\"
        data-post-link=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminQuickAccesses&token=2af1366de1e7e292d56466020b0affbe\"
        data-prompt-text=\"Bitte dieses Kürzel angeben:\"
        data-link=\"Installierte Module - Liste\"
      >
        <i class=\"material-icons\">add_circle</i>
        Zu Schnellzugang hinzufügen
      </a>
        <a class=\"dropdown-item\" href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminQuickAccesses&token=2af1366de1e7e292d56466020b0affbe\">
      <i class=\"material-icons\">settings</i>
      Schnellzugänge verwalten
    </a>
  </div>
</div>
    </div>
    <div class=\"component\" id=\"header-search-container\">
      <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/swisspackag.ch/admin60238sgog/index.php?controller=AdminSearch&amp;token=fe7f625a9137f6b75ad9942cd922ba77\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Suche (z.B. Bestell-Nr., Kundenname ...)\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        Überall
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"Überall\" href=\"#\" data-value=\"0\" data-placeholder=\"Wonach suchen Sie?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> Überall</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Katalog\" href=\"#\" data-value=\"1\" data-placeholder=\"Artikelname, Bestandseinheit, Artikel-Nr. ...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Katalog</a>
        <a class=\"dropdown-item\" data-item=\"Kunden nach Name\" href=\"#\" data-value=\"2\" data-placeholder=\"E-Mail , Name...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Kunden nach Name</a>
        <a class=\"dropdown-item\" data-item=\"Kunden nach IP-Adresse\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Kunden nach IP-Adresse</a>
        <a class=\"dropdown-item\" data-item=\"Bestellungen\" href=\"#\" data-value=\"3\" data-placeholder=\"Bestell-Nr.\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Bestellungen</a>
        <a class=\"dropdown-item\" data-item=\"Rechnungen\" href=\"#\" data-value=\"4\" data-placeholder=\"Rechnungsnummer\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i></i> Rechnungen</a>
        <a class=\"dropdown-item\" data-item=\"Warenkörbe\" href=\"#\" data-value=\"5\" data-placeholder=\"Warenkorb-ID\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Warenkörbe</a>
        <a class=\"dropdown-item\" data-item=\"Module\" href=\"#\" data-value=\"7\" data-placeholder=\"Modul-Name\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Module</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">SUCHE</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
    </div>

          <div class=\"component hide-mobile-sm\" id=\"header-debug-mode-container\">
        <a class=\"link shop-state\"
           id=\"debug-mode\"
           data-toggle=\"pstooltip\"
           data-placement=\"bottom\"
           data-html=\"true\"
           title=\"<p class='text-left'><strong>Ihr Shop befindet sich im Debug-Modus.</strong></p><p class='text-left'>Alle PHP-Fehler und -Nachrichten werden angezeigt. Wenn Sie dies nicht mehr benötigen, wählen Sie die Option <strong>Abschalten</strong>.</p>\"
           href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/performance?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\"
        >
          <i class=\"material-icons\">bug_report</i>
          <span>Debug-Modus</span>
        </a>
      </div>
            <div class=\"component\" id=\"header-shop-list-container\">
        <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://45.56.70.30/swisspackag.ch/\" target= \"_blank\">
      <i class=\"material-icons\">visibility</i>
      Meinen Shop anzeigen
    </a>
  </div>
    </div>
          <div class=\"component header-right-component\" id=\"header-notifications-container\">
        <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Bestellungen<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Kunden<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Nachrichten<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Haben Sie in der letzten Zeit Ihre Konversionsrate überprüft?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Haben Sie in der letzten Zeit Werbe-Mails versandt?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell keine neuen Nachrichten <br>
              Mehr Zeit für Anderes!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      von <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registriert <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
      </div>
        <div class=\"component\" id=\"header-employee-container\">
      <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"text-center employee_avatar\">
      <img class=\"avatar rounded-circle\" src=\"http://profile.prestashop.com/admin%40gmail.com.jpg\" />
      <span>Swisspack User</span>
    </div>
    <a class=\"dropdown-item employee-link profile-link\" href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminEmployees&amp;token=c5667a5df420aea0ae699d1b5ab862bc&amp;id_employee=1&amp;updateemployee\">
      <i class=\"material-icons\">settings_applications</i>
      Ihr Profil
    </a>
    <a class=\"dropdown-item employee-link\" id=\"header_logout\" href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminLogin&amp;token=b1a6204c71eb49c2f06dc9f5d6a5674e&amp;logout\">
      <i class=\"material-icons\">power_settings_new</i>
      <span>Abmelden</span>
    </a>
  </div>
</div>
    </div>

      </nav>
  </header>

<nav class=\"nav-bar d-none d-md-block\">
  <span class=\"menu-collapse\">
    <i class=\"material-icons\">chevron_left</i>
    <i class=\"material-icons\">chevron_left</i>
  </span>

  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminDashboard&amp;token=a60173a8b528fe0fa2e6cac10511636e\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Übersicht</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Verkauf</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminOrders&amp;token=c8e1ac546cb5e9b266df20c3461f7690\" class=\"link\">
                    <i class=\"material-icons mi-shopping_basket\">shopping_basket</i>
                    <span>
                    Bestellungen
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminOrders&amp;token=c8e1ac546cb5e9b266df20c3461f7690\" class=\"link\"> Bestellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminInvoices&amp;token=8f7497e06be7ad0e4845e998755dcdf5\" class=\"link\"> Rechnungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminSlip&amp;token=24036e1783381823d42dd0cb42edd88e\" class=\"link\"> Rückvergütungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminDeliverySlip&amp;token=cc45e7a53a608b99dba10e0a232308ad\" class=\"link\"> Lieferscheine
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCarts&amp;token=35524d5e06e36653d668a3889ce0140e\" class=\"link\"> Warenkörbe
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/swisspackag.ch/admin60238sgog/index.php/product/catalog?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\">
                    <i class=\"material-icons mi-store\">store</i>
                    <span>
                    Katalog
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/product/catalog?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Artikel
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCategories&amp;token=3af0b28a546b714056d81f9d29da6792\" class=\"link\"> Kategorien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminTracking&amp;token=f5a32a6d1b08bdb418933a1f74cba70d\" class=\"link\"> Kontrollübersicht
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAttributesGroups&amp;token=a50ddc9fecf8c8f8aabe710134d04f66\" class=\"link\"> Varianten &amp; Eigenschaften
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminManufacturers&amp;token=768a0696780b65f5c3acf576d02f1d01\" class=\"link\"> Marken &amp; Lieferanten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAttachments&amp;token=8f218166f6b13c7d0b6e1b81deea192a\" class=\"link\"> Dateien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCartRules&amp;token=a4b38ef1e57d20ebe3a7dee906b0b2f2\" class=\"link\"> Rabatt
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/stock/?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCustomers&amp;token=1fba735d105445be8d8084ee92e1f22c\" class=\"link\">
                    <i class=\"material-icons mi-account_circle\">account_circle</i>
                    <span>
                    Kunden
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCustomers&amp;token=1fba735d105445be8d8084ee92e1f22c\" class=\"link\"> Kunden
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAddresses&amp;token=6871583bb2859f577e57d0ead555dacd\" class=\"link\"> Adressen
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCustomerThreads&amp;token=fc3cbcd5cccaedbf40043cb1767e5e19\" class=\"link\">
                    <i class=\"material-icons mi-chat\">chat</i>
                    <span>
                    Kundenservice
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCustomerThreads&amp;token=fc3cbcd5cccaedbf40043cb1767e5e19\" class=\"link\"> Kundenservice
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminOrderMessage&amp;token=27ebc426bc98d22d430007e53a5e0dc5\" class=\"link\"> Bestellnachrichten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminReturn&amp;token=3bfc7b30b08c49ed785adb2cbe4e6e75\" class=\"link\"> Warenrücksendungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminStats&amp;token=134eaf1ee5657428650e3e2c47797fe7\" class=\"link\">
                    <i class=\"material-icons mi-assessment\">assessment</i>
                    <span>
                    Statistiken
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Optimierung</span>
          </li>

                          
                
                                                
                                                    
                <li class=\"link-levelone has_submenu -active open ul-open\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/manage?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Module
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_up
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/manage?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Module und Dienste
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\" id=\"subtab-AdminAddonsCatalog\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/addons-store?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Versanddienst
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"50\" id=\"subtab-AdminParentThemes\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminThemes&amp;token=b51d4a85eb9e3a9a0881f0f67e28383e\" class=\"link\">
                    <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                    <span>
                    Design
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-50\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"121\" id=\"subtab-AdminThemesParent\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminThemes&amp;token=b51d4a85eb9e3a9a0881f0f67e28383e\" class=\"link\"> Template und Logo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"52\" id=\"subtab-AdminThemesCatalog\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminThemesCatalog&amp;token=557500b9347a507342705ea9656d8015\" class=\"link\"> Templates
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"53\" id=\"subtab-AdminCmsContent\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCmsContent&amp;token=454d1ecaf6c63c8c980da47065b89a8c\" class=\"link\"> Seiten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminModulesPositions&amp;token=6151bf7017c58872113e00c3029aaba3\" class=\"link\"> Positionen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminImages\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminImages&amp;token=aae8ee4cde96a5ecf7d81e927e1d9d50\" class=\"link\"> Bilder
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"120\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminLinkWidget&amp;token=df6bee1fe63017d801c4d86a30efc22d\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"56\" id=\"subtab-AdminParentShipping\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCarriers&amp;token=e3e03d887096ae09d2e12cea1a8689e8\" class=\"link\">
                    <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                    <span>
                    Versand
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-56\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminCarriers\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCarriers&amp;token=e3e03d887096ae09d2e12cea1a8689e8\" class=\"link\"> Versanddienste
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"58\" id=\"subtab-AdminShipping\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminShipping&amp;token=ead645a9c37533e1737965490995edea\" class=\"link\"> Voreinstellungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"59\" id=\"subtab-AdminParentPayment\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPayment&amp;token=e440c2e7b81706676175a4b797a79de7\" class=\"link\">
                    <i class=\"material-icons mi-payment\">payment</i>
                    <span>
                    Zahlungsart
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-59\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"60\" id=\"subtab-AdminPayment\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPayment&amp;token=e440c2e7b81706676175a4b797a79de7\" class=\"link\"> Zahlungsarten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"61\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPaymentPreferences&amp;token=46ee3ed408a80ed459e917b51e1ea336\" class=\"link\"> Voreinstellungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"62\" id=\"subtab-AdminInternational\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminLocalization&amp;token=1f1b2f80dd82c684d275c9672d9d9c61\" class=\"link\">
                    <i class=\"material-icons mi-language\">language</i>
                    <span>
                    international
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-62\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"63\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminLocalization&amp;token=1f1b2f80dd82c684d275c9672d9d9c61\" class=\"link\"> Lokalisierung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"68\" id=\"subtab-AdminParentCountries\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminZones&amp;token=f5184709cc5c04f6d608f9f30f3c671c\" class=\"link\"> Länder &amp; Gebiete
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"72\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminTaxes&amp;token=c4a43efc6ef8360b9669514889f01da3\" class=\"link\"> Steuersätze
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"75\" id=\"subtab-AdminTranslations\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminTranslations&amp;token=48bf0e8d498b01165bc2641eeb1c5f59\" class=\"link\"> Übersetzungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"76\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Einstellungen</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"77\" id=\"subtab-ShopParameters\">
                  <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/shop/preferences?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\">
                    <i class=\"material-icons mi-settings\">settings</i>
                    <span>
                    Shop-Einstellungen
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-77\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"78\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/shop/preferences?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Allgemein
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminOrderPreferences&amp;token=ab9264e408ab3970d681728892cdcd1c\" class=\"link\"> Bestellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"84\" id=\"subtab-AdminPPreferences\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/shop/product_preferences?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Artikel
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/shop/customer_preferences?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Benutzerdefinierte Einstellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"89\" id=\"subtab-AdminParentStores\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminContacts&amp;token=d6144e8c5a761f440480e490462c32f8\" class=\"link\"> Kontakt
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"92\" id=\"subtab-AdminParentMeta\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminMeta&amp;token=327925f8a93ae8d7639de383c207233d\" class=\"link\"> Traffic &amp; SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminSearchConf&amp;token=072fa2d0a7387532d294f89aedac0a33\" class=\"link\"> Suche
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"125\" id=\"subtab-AdminGamification\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminGamification&amp;token=ab2372d78e943e878316dc74ebf0a71f\" class=\"link\"> Merchant Expertise
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"99\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/system_information?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\">
                    <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                    <span>
                    Erweiterte Einstellungen
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-99\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\" id=\"subtab-AdminInformation\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/system_information?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Informationen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\" id=\"subtab-AdminPerformance\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/performance?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Leistung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"102\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/administration?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Verwaltung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"103\" id=\"subtab-AdminEmails\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminEmails&amp;token=7e0d1bb4e35721a6f5edb17229161fe6\" class=\"link\"> E-Mail
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"104\" id=\"subtab-AdminImport\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/import?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Importieren
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"105\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminEmployees&amp;token=c5667a5df420aea0ae699d1b5ab862bc\" class=\"link\"> Benutzerrechte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminRequestSql&amp;token=12b793795d4b58c95e68a5dc6eef5a44\" class=\"link\"> Datenbank
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"112\" id=\"subtab-AdminLogs\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminLogs&amp;token=fa24a8608d177d9255259825e7b2d042\" class=\"link\"> Log-Dateien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"113\" id=\"subtab-AdminWebservice\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminWebservice&amp;token=6260ca7ba4ad9f7c5e140364f2551153\" class=\"link\"> Webservice
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"139\" id=\"tab-DEFAULT_MTR\">
              <span class=\"title\">Mehr</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"117\" id=\"subtab-DEFAULT\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=DEFAULT&amp;token=8d233f3813024ce5e3f11536259ceb11\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Mehr
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"141\" id=\"subtab-AdminAdvancedImporter_MTR\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporter&amp;token=8eb9fd92101690e96c37b2dc62457a86\" class=\"link\">
                    <i class=\"material-icons mi-\"></i>
                    <span>
                    Advanced Importer
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-141\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"140\" id=\"subtab-AdminAdvancedImporter\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporter&amp;token=8eb9fd92101690e96c37b2dc62457a86\" class=\"link\"> Advanced Importer
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"142\" id=\"subtab-AdminAdvancedImporterFlow\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterFlow&amp;token=73dc69198a3ce96ffe6c313103509ec0\" class=\"link\"> Flows
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"143\" id=\"subtab-AdminAdvancedImporterBlock\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterBlock&amp;token=33ac3174f074eb17803110a08cf6fa2c\" class=\"link\"> Blocks
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"144\" id=\"subtab-AdminAdvancedImporterSupplierReferences\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterSupplierReferences&amp;token=8f0cae11d8b33d3440245a8408941ce2\" class=\"link\"> Supplier References
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"145\" id=\"subtab-AdminAdvancedImporterHistory\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterHistory&amp;token=417309635f9176ea3d855a64dabf2041\" class=\"link\"> Entity Tracking
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"146\" id=\"subtab-AdminAdvancedImporterLog\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterLog&amp;token=ef5c1d503935160dd0d45e3501328b94\" class=\"link\"> Log-Dateien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"147\" id=\"subtab-AdminAdvancedImporterCron\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterCron&amp;token=19d68889bd1b23033502b4ced8f42566\" class=\"link\"> Recurring tasks
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"148\" id=\"subtab-AdminAdvancedImporterTemplate\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterTemplate&amp;token=f55ab498409d04cd0f74784698e5bd55\" class=\"link\"> Templates
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"149\" id=\"subtab-AdminAdvancedImporterConfiguration\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterConfiguration&amp;token=d4b81ae0c8a7102ff1aa81f82f2c5e9e\" class=\"link\"> Einstellungen
                              </a>
                            </li>

                                                                                                                                                                                                                              </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"128\" id=\"tab-AdminPresToPres\">
              <span class=\"title\">Prestashop Migrator</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"129\" id=\"subtab-AdminPresToPresImport\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPresToPresImport&amp;token=84232b0fc8c11f285fc803d55522d643\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-cloud-upload\">icon icon-cloud-upload</i>
                    <span>
                    Migration
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"130\" id=\"subtab-AdminPresToPresHistory\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPresToPresHistory&amp;token=93d13456b68b1697597cc4014f1b84b2\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-history\">icon icon-history</i>
                    <span>
                    History
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"131\" id=\"subtab-AdminPresToPresClean\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPresToPresClean&amp;token=f507010a27952bac41de27e7af0fbfcc\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-eraser\">icon icon-eraser</i>
                    <span>
                    Clean-up
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"132\" id=\"subtab-AdminPresToPresHelp\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPresToPresHelp&amp;token=ecda92f9f3a6c1fb9967fea29289c76c\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-question-circle\">icon icon-question-circle</i>
                    <span>
                    Hilfe
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"133\" id=\"tab-AdminSpmblocknewsadv\">
              <span class=\"title\">Nachrichten</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"134\" id=\"subtab-AdminSpmblocknewsadvnews\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminSpmblocknewsadvnews&amp;token=8e25df5558db128e6b8486f0c057727e\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Moderate Nachrichten
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
            </ul>
  
</nav>

<div id=\"main-div\">

  
    
<div class=\"header-toolbar\">
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Module und Dienste</li>
          
                      <li class=\"breadcrumb-item active\">
              <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/manage?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" aria-current=\"page\">Installierte Module</a>
            </li>
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
          <h1 class=\"title\">
            Installierte Module verwalten          </h1>
      

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
            
                                                          <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-add_module\"
                  href=\"#\"                  title=\"Modul hochladen\"                  data-toggle=\"pstooltip\"
                  data-placement=\"bottom\"                >
                  <i class=\"material-icons\">cloud_upload</i>
                  Modul hochladen
                </a>
                                                                        <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-addons_connect\"
                  href=\"#\"                  title=\"Mit dem Addons Marktplatz verbinden\"                  data-toggle=\"pstooltip\"
                  data-placement=\"bottom\"                >
                  <i class=\"material-icons\">vpn_key</i>
                  Mit dem Addons Marktplatz verbinden
                </a>
                                                  
                              <a class=\"btn btn-outline-secondary btn-help btn-sidebar\" href=\"#\"
                   title=\"Hilfe\"
                   data-toggle=\"sidebar\"
                   data-target=\"#right-sidebar\"
                   data-url=\"/swisspackag.ch/admin60238sgog/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop.com%252Fde%252Fdoc%252FAdminModules%253Fversion%253D1.7.4.4%2526country%253Dde/Hilfe?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\"
                   id=\"product_form_open_help\"
                >
                  Hilfe
                </a>
                                    </div>
        </div>
      
    </div>
  </div>

  
      <div class=\"page-head-tabs\" id=\"head_tabs\">
      <ul class=\"nav nav-pills\">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <li class=\"nav-item\">
                    <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/manage?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" id=\"subtab-AdminModulesManage\" class=\"nav-link tab active current\" data-submenu=\"45\">Installierte Module</a>
                  </li>
                                                                <li class=\"nav-item\">
                    <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/catalog?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" id=\"subtab-AdminModulesCatalog\" class=\"nav-link tab \" data-submenu=\"46\">Auswahl</a>
                  </li>
                                                                <li class=\"nav-item\">
                    <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/notifications?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" id=\"subtab-AdminModulesNotifications\" class=\"nav-link tab \" data-submenu=\"47\">Nachrichten</a>
                  </li>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </ul>
    </div>
    
</div>
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=admin%40gmail.com&amp;firstname=Swisspack&amp;lastname=User&amp;website=http%3A%2F%2F45.56.70.30%2Fswisspackag.ch%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/swisspackag.ch/admin60238sgog/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle gekauften Zusatzmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=admin%40gmail.com&amp;firstname=Swisspack&amp;lastname=User&amp;website=http%3A%2F%2F45.56.70.30%2Fswisspackag.ch%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

    <div class=\"content-div  with-tabs\">

      

      
                        
      <div class=\"row \">
        <div class=\"col-sm-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>


  ";
        // line 1375
        $this->displayBlock('content_header', $context, $blocks);
        // line 1376
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 1377
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 1378
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 1379
        echo "
          
        </div>
      </div>

    </div>

  
</div>

<div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>Oh nein!</h1>
  <p class=\"mt-3\">
    Für diese Seite gibt es derzeit keine Mobilversion.
  </p>
  <p class=\"mt-2\">
    Da die Seite noch nicht für mobile Geräte angepasst wurde, können Sie sie nur am PC aufrufen.
  </p>
  <p class=\"mt-2\">
    Vielen Dank!
  </p>
  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminDashboard&amp;token=a60173a8b528fe0fa2e6cac10511636e\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Zurück
  </a>
</div>
<div class=\"mobile-layer\"></div>

  <div id=\"footer\" class=\"bootstrap\">
    
</div>


  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=admin%40gmail.com&amp;firstname=Swisspack&amp;lastname=User&amp;website=http%3A%2F%2F45.56.70.30%2Fswisspackag.ch%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/swisspackag.ch/admin60238sgog/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle gekauften Zusatzmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=admin%40gmail.com&amp;firstname=Swisspack&amp;lastname=User&amp;website=http%3A%2F%2F45.56.70.30%2Fswisspackag.ch%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

  </div>

";
        // line 1488
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 96
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_extra_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "extra_stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "extra_stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1375
    public function block_content_header($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_header"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1376
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1377
    public function block_content_footer($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_footer"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1378
    public function block_sidebar_right($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar_right"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar_right"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1488
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_extra_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "extra_javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "extra_javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_translate_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "translate_javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "translate_javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "__string_template__658e6bf14c5a0caf7182350daf17f039ee02305563f29eb191c75f8d333489d3";
    }

    public function getDebugInfo()
    {
        return array (  1651 => 1488,  1634 => 1378,  1617 => 1377,  1600 => 1376,  1583 => 1375,  1550 => 96,  1536 => 1488,  1425 => 1379,  1422 => 1378,  1419 => 1377,  1416 => 1376,  1414 => 1375,  131 => 96,  34 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"de\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.75, maximum-scale=0.75, user-scalable=0\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/swisspackag.ch/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/swisspackag.ch/img/app_icon.png\" />

<title>Installierte Module verwalten • Swisspack AG</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminModulesManage';
    var iso_user = 'de';
    var lang_is_rtl = '1';
    var full_language_code = 'de';
    var full_cldr_language_code = 'de-DE';
    var country_iso_code = 'CH';
    var _PS_VERSION_ = '1.7.4.4';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Eine neue Bestellung ist in Ihrem Shop eingegangen.';
    var order_number_msg = 'Bestell-Nr. ';
    var total_msg = 'Gesamt: ';
    var from_msg = 'von ';
    var see_order_msg = 'Diese Bestellung anzeigen';
    var new_customer_msg = 'Neue Kundenregistrierung im Shop.';
    var customer_name_msg = 'Kunde ';
    var new_msg = 'Ihr Shop hat eine neue Nachricht erhalten.';
    var see_msg = 'Nachricht lesen';
    var token = '34ae94254c1c60c88edfbe048b9711b1';
    var token_admin_orders = 'c8e1ac546cb5e9b266df20c3461f7690';
    var token_admin_customers = '1fba735d105445be8d8084ee92e1f22c';
    var token_admin_customer_threads = 'fc3cbcd5cccaedbf40043cb1767e5e19';
    var currentIndex = 'index.php?controller=AdminModulesManage';
    var employee_token = 'c5667a5df420aea0ae699d1b5ab862bc';
    var choose_language_translate = 'Wählen Sie eine Sprache';
    var default_language = '2';
    var admin_modules_link = '/swisspackag.ch/admin60238sgog/index.php/module/catalog/recommended?route=admin_module_catalog_post&_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0';
    var tab_modules_list = '';
    var update_success_msg = 'Aktualisierung durchgeführt!';
    var errorLogin = 'PrestaShop konnte sich nicht bei Addons anmelden. Überprüfen Sie bitte Ihre Zugangsdaten und Ihre Internetverbindung.';
    var search_product_msg = 'Artikel suchen';
  </script>

      <link href=\"/swisspackag.ch/modules/gamification/views/css/gamification_rtl.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/admin60238sgog/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/modules/ets_pres2pres/views/css/admin-icon.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/modules/ets_pres2pres/views/css/ps1.7.4.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/modules/invoicepayment/views/css/invoicepayment_editor.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/admin60238sgog/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/swisspackag.ch/modules/advancedimporter/views/css/admin.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var AI_TOKEN = \"eb8a584e497de1354c41\";
var CONFIRM_DELETE = \"Sind Sie sicher?\";
var baseAdminDir = \"\\/swisspackag.ch\\/admin60238sgog\\/\";
var baseDir = \"\\/swisspackag.ch\\/\";
var currency = {\"iso_code\":\"CHF\",\"sign\":\"CHF\",\"name\":\"Schweizer Franken\",\"format\":\"#,##0.00\\u00a0\\u00a4\"};
var host_mode = false;
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/jquery/jquery-1.11.0.min.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/jquery/jquery-migrate-1.2.1.min.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/modules/gamification/views/js/gamification_bt.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/modules/invoicepayment/views/js/invoicepayment_editor.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/admin60238sgog/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/admin.js?v=1.7.4.4\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/cldr.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/tools.js?v=1.7.4.4\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/admin60238sgog/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/admin60238sgog/themes/default/js/vendor/nv.d3.min.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/modules/advancedimporter/views/js/admin.js\"></script>
<script type=\"text/javascript\" src=\"/swisspackag.ch/js/rtl.js\"></script>

  <script>
\t\t\t\tvar ids_ps_advice = new Array();
\t\t\t\tvar admin_gamification_ajax_url = 'http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminGamification&token=ab2372d78e943e878316dc74ebf0a71f';
\t\t\t\tvar current_id_tab = 45;
\t\t\t</script><style type=\"text/css\">
\t\t.icon-AdminSpmblocknewsadv:before {
\t\t\tcontent: url(\"http://45.56.70.30/swisspackag.ch/modules/spmblocknewsadv/views/img/AdminSpmblocknewsadv.gif\");
\t\t}
\t\t</style>
\t\t

{% block stylesheets %}{% endblock %}{% block extra_stylesheets %}{% endblock %}</head>
<body class=\"lang-de lang-rtl adminmodulesmanage\">


<header id=\"header\">
  <nav id=\"header_infos\" class=\"main-header\">

    <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

        
        <i class=\"material-icons js-mobile-menu\">menu</i>
    <a id=\"header_logo\" class=\"logo float-left\" href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminDashboard&amp;token=a60173a8b528fe0fa2e6cac10511636e\"></a>
    <span id=\"shop_version\">1.7.4.4</span>

    <div class=\"component\" id=\"quick-access-container\">
      <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Schnellzugriff
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminOrders&amp;token=c8e1ac546cb5e9b266df20c3461f7690\"
                 data-item=\"Bestellungen\"
      >Bestellungen</a>
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php/module/manage?token=52d4027f0428270c2c0c8c1fd853b944\"
                 data-item=\"Installierte Module\"
      >Installierte Module</a>
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=134eaf1ee5657428650e3e2c47797fe7\"
                 data-item=\"Katalogauswertung\"
      >Katalogauswertung</a>
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCategories&amp;addcategory&amp;token=3af0b28a546b714056d81f9d29da6792\"
                 data-item=\"Neue Kategorie\"
      >Neue Kategorie</a>
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php/product/new?token=52d4027f0428270c2c0c8c1fd853b944\"
                 data-item=\"Neuer Artikel\"
      >Neuer Artikel</a>
          <a class=\"dropdown-item\"
         href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=a4b38ef1e57d20ebe3a7dee906b0b2f2\"
                 data-item=\"Neuer Ermäßigungsgutschein\"
      >Neuer Ermäßigungsgutschein</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"163\"
        data-icon=\"icon-AdminModulesSf\"
        data-method=\"add\"
        data-url=\"index.php/module/manage?-NLArZCZsQJXYfJ3iLSDCqP8CS0\"
        data-post-link=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminQuickAccesses&token=2af1366de1e7e292d56466020b0affbe\"
        data-prompt-text=\"Bitte dieses Kürzel angeben:\"
        data-link=\"Installierte Module - Liste\"
      >
        <i class=\"material-icons\">add_circle</i>
        Zu Schnellzugang hinzufügen
      </a>
        <a class=\"dropdown-item\" href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminQuickAccesses&token=2af1366de1e7e292d56466020b0affbe\">
      <i class=\"material-icons\">settings</i>
      Schnellzugänge verwalten
    </a>
  </div>
</div>
    </div>
    <div class=\"component\" id=\"header-search-container\">
      <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/swisspackag.ch/admin60238sgog/index.php?controller=AdminSearch&amp;token=fe7f625a9137f6b75ad9942cd922ba77\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Suche (z.B. Bestell-Nr., Kundenname ...)\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        Überall
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"Überall\" href=\"#\" data-value=\"0\" data-placeholder=\"Wonach suchen Sie?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> Überall</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Katalog\" href=\"#\" data-value=\"1\" data-placeholder=\"Artikelname, Bestandseinheit, Artikel-Nr. ...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Katalog</a>
        <a class=\"dropdown-item\" data-item=\"Kunden nach Name\" href=\"#\" data-value=\"2\" data-placeholder=\"E-Mail , Name...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Kunden nach Name</a>
        <a class=\"dropdown-item\" data-item=\"Kunden nach IP-Adresse\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Kunden nach IP-Adresse</a>
        <a class=\"dropdown-item\" data-item=\"Bestellungen\" href=\"#\" data-value=\"3\" data-placeholder=\"Bestell-Nr.\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Bestellungen</a>
        <a class=\"dropdown-item\" data-item=\"Rechnungen\" href=\"#\" data-value=\"4\" data-placeholder=\"Rechnungsnummer\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i></i> Rechnungen</a>
        <a class=\"dropdown-item\" data-item=\"Warenkörbe\" href=\"#\" data-value=\"5\" data-placeholder=\"Warenkorb-ID\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Warenkörbe</a>
        <a class=\"dropdown-item\" data-item=\"Module\" href=\"#\" data-value=\"7\" data-placeholder=\"Modul-Name\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Module</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">SUCHE</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
    </div>

          <div class=\"component hide-mobile-sm\" id=\"header-debug-mode-container\">
        <a class=\"link shop-state\"
           id=\"debug-mode\"
           data-toggle=\"pstooltip\"
           data-placement=\"bottom\"
           data-html=\"true\"
           title=\"<p class='text-left'><strong>Ihr Shop befindet sich im Debug-Modus.</strong></p><p class='text-left'>Alle PHP-Fehler und -Nachrichten werden angezeigt. Wenn Sie dies nicht mehr benötigen, wählen Sie die Option <strong>Abschalten</strong>.</p>\"
           href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/performance?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\"
        >
          <i class=\"material-icons\">bug_report</i>
          <span>Debug-Modus</span>
        </a>
      </div>
            <div class=\"component\" id=\"header-shop-list-container\">
        <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://45.56.70.30/swisspackag.ch/\" target= \"_blank\">
      <i class=\"material-icons\">visibility</i>
      Meinen Shop anzeigen
    </a>
  </div>
    </div>
          <div class=\"component header-right-component\" id=\"header-notifications-container\">
        <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Bestellungen<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Kunden<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Nachrichten<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Haben Sie in der letzten Zeit Ihre Konversionsrate überprüft?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell kein neuer Kunde!<br>
              Haben Sie in der letzten Zeit Werbe-Mails versandt?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aktuell keine neuen Nachrichten <br>
              Mehr Zeit für Anderes!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      von <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registriert <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
      </div>
        <div class=\"component\" id=\"header-employee-container\">
      <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"text-center employee_avatar\">
      <img class=\"avatar rounded-circle\" src=\"http://profile.prestashop.com/admin%40gmail.com.jpg\" />
      <span>Swisspack User</span>
    </div>
    <a class=\"dropdown-item employee-link profile-link\" href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminEmployees&amp;token=c5667a5df420aea0ae699d1b5ab862bc&amp;id_employee=1&amp;updateemployee\">
      <i class=\"material-icons\">settings_applications</i>
      Ihr Profil
    </a>
    <a class=\"dropdown-item employee-link\" id=\"header_logout\" href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminLogin&amp;token=b1a6204c71eb49c2f06dc9f5d6a5674e&amp;logout\">
      <i class=\"material-icons\">power_settings_new</i>
      <span>Abmelden</span>
    </a>
  </div>
</div>
    </div>

      </nav>
  </header>

<nav class=\"nav-bar d-none d-md-block\">
  <span class=\"menu-collapse\">
    <i class=\"material-icons\">chevron_left</i>
    <i class=\"material-icons\">chevron_left</i>
  </span>

  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminDashboard&amp;token=a60173a8b528fe0fa2e6cac10511636e\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Übersicht</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Verkauf</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminOrders&amp;token=c8e1ac546cb5e9b266df20c3461f7690\" class=\"link\">
                    <i class=\"material-icons mi-shopping_basket\">shopping_basket</i>
                    <span>
                    Bestellungen
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminOrders&amp;token=c8e1ac546cb5e9b266df20c3461f7690\" class=\"link\"> Bestellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminInvoices&amp;token=8f7497e06be7ad0e4845e998755dcdf5\" class=\"link\"> Rechnungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminSlip&amp;token=24036e1783381823d42dd0cb42edd88e\" class=\"link\"> Rückvergütungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminDeliverySlip&amp;token=cc45e7a53a608b99dba10e0a232308ad\" class=\"link\"> Lieferscheine
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCarts&amp;token=35524d5e06e36653d668a3889ce0140e\" class=\"link\"> Warenkörbe
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/swisspackag.ch/admin60238sgog/index.php/product/catalog?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\">
                    <i class=\"material-icons mi-store\">store</i>
                    <span>
                    Katalog
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/product/catalog?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Artikel
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCategories&amp;token=3af0b28a546b714056d81f9d29da6792\" class=\"link\"> Kategorien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminTracking&amp;token=f5a32a6d1b08bdb418933a1f74cba70d\" class=\"link\"> Kontrollübersicht
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAttributesGroups&amp;token=a50ddc9fecf8c8f8aabe710134d04f66\" class=\"link\"> Varianten &amp; Eigenschaften
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminManufacturers&amp;token=768a0696780b65f5c3acf576d02f1d01\" class=\"link\"> Marken &amp; Lieferanten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAttachments&amp;token=8f218166f6b13c7d0b6e1b81deea192a\" class=\"link\"> Dateien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCartRules&amp;token=a4b38ef1e57d20ebe3a7dee906b0b2f2\" class=\"link\"> Rabatt
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/stock/?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCustomers&amp;token=1fba735d105445be8d8084ee92e1f22c\" class=\"link\">
                    <i class=\"material-icons mi-account_circle\">account_circle</i>
                    <span>
                    Kunden
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCustomers&amp;token=1fba735d105445be8d8084ee92e1f22c\" class=\"link\"> Kunden
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAddresses&amp;token=6871583bb2859f577e57d0ead555dacd\" class=\"link\"> Adressen
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCustomerThreads&amp;token=fc3cbcd5cccaedbf40043cb1767e5e19\" class=\"link\">
                    <i class=\"material-icons mi-chat\">chat</i>
                    <span>
                    Kundenservice
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCustomerThreads&amp;token=fc3cbcd5cccaedbf40043cb1767e5e19\" class=\"link\"> Kundenservice
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminOrderMessage&amp;token=27ebc426bc98d22d430007e53a5e0dc5\" class=\"link\"> Bestellnachrichten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminReturn&amp;token=3bfc7b30b08c49ed785adb2cbe4e6e75\" class=\"link\"> Warenrücksendungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminStats&amp;token=134eaf1ee5657428650e3e2c47797fe7\" class=\"link\">
                    <i class=\"material-icons mi-assessment\">assessment</i>
                    <span>
                    Statistiken
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Optimierung</span>
          </li>

                          
                
                                                
                                                    
                <li class=\"link-levelone has_submenu -active open ul-open\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/manage?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Module
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_up
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/manage?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Module und Dienste
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"49\" id=\"subtab-AdminAddonsCatalog\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/addons-store?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Versanddienst
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"50\" id=\"subtab-AdminParentThemes\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminThemes&amp;token=b51d4a85eb9e3a9a0881f0f67e28383e\" class=\"link\">
                    <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                    <span>
                    Design
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-50\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"121\" id=\"subtab-AdminThemesParent\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminThemes&amp;token=b51d4a85eb9e3a9a0881f0f67e28383e\" class=\"link\"> Template und Logo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"52\" id=\"subtab-AdminThemesCatalog\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminThemesCatalog&amp;token=557500b9347a507342705ea9656d8015\" class=\"link\"> Templates
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"53\" id=\"subtab-AdminCmsContent\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCmsContent&amp;token=454d1ecaf6c63c8c980da47065b89a8c\" class=\"link\"> Seiten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"54\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminModulesPositions&amp;token=6151bf7017c58872113e00c3029aaba3\" class=\"link\"> Positionen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminImages\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminImages&amp;token=aae8ee4cde96a5ecf7d81e927e1d9d50\" class=\"link\"> Bilder
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"120\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminLinkWidget&amp;token=df6bee1fe63017d801c4d86a30efc22d\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"56\" id=\"subtab-AdminParentShipping\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCarriers&amp;token=e3e03d887096ae09d2e12cea1a8689e8\" class=\"link\">
                    <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                    <span>
                    Versand
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-56\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminCarriers\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminCarriers&amp;token=e3e03d887096ae09d2e12cea1a8689e8\" class=\"link\"> Versanddienste
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"58\" id=\"subtab-AdminShipping\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminShipping&amp;token=ead645a9c37533e1737965490995edea\" class=\"link\"> Voreinstellungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"59\" id=\"subtab-AdminParentPayment\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPayment&amp;token=e440c2e7b81706676175a4b797a79de7\" class=\"link\">
                    <i class=\"material-icons mi-payment\">payment</i>
                    <span>
                    Zahlungsart
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-59\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"60\" id=\"subtab-AdminPayment\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPayment&amp;token=e440c2e7b81706676175a4b797a79de7\" class=\"link\"> Zahlungsarten
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"61\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPaymentPreferences&amp;token=46ee3ed408a80ed459e917b51e1ea336\" class=\"link\"> Voreinstellungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"62\" id=\"subtab-AdminInternational\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminLocalization&amp;token=1f1b2f80dd82c684d275c9672d9d9c61\" class=\"link\">
                    <i class=\"material-icons mi-language\">language</i>
                    <span>
                    international
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-62\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"63\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminLocalization&amp;token=1f1b2f80dd82c684d275c9672d9d9c61\" class=\"link\"> Lokalisierung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"68\" id=\"subtab-AdminParentCountries\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminZones&amp;token=f5184709cc5c04f6d608f9f30f3c671c\" class=\"link\"> Länder &amp; Gebiete
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"72\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminTaxes&amp;token=c4a43efc6ef8360b9669514889f01da3\" class=\"link\"> Steuersätze
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"75\" id=\"subtab-AdminTranslations\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminTranslations&amp;token=48bf0e8d498b01165bc2641eeb1c5f59\" class=\"link\"> Übersetzungen
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"76\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Einstellungen</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"77\" id=\"subtab-ShopParameters\">
                  <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/shop/preferences?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\">
                    <i class=\"material-icons mi-settings\">settings</i>
                    <span>
                    Shop-Einstellungen
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-77\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"78\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/shop/preferences?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Allgemein
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"81\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminOrderPreferences&amp;token=ab9264e408ab3970d681728892cdcd1c\" class=\"link\"> Bestellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"84\" id=\"subtab-AdminPPreferences\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/shop/product_preferences?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Artikel
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/shop/customer_preferences?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Benutzerdefinierte Einstellungen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"89\" id=\"subtab-AdminParentStores\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminContacts&amp;token=d6144e8c5a761f440480e490462c32f8\" class=\"link\"> Kontakt
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"92\" id=\"subtab-AdminParentMeta\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminMeta&amp;token=327925f8a93ae8d7639de383c207233d\" class=\"link\"> Traffic &amp; SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminSearchConf&amp;token=072fa2d0a7387532d294f89aedac0a33\" class=\"link\"> Suche
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"125\" id=\"subtab-AdminGamification\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminGamification&amp;token=ab2372d78e943e878316dc74ebf0a71f\" class=\"link\"> Merchant Expertise
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"99\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/system_information?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\">
                    <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                    <span>
                    Erweiterte Einstellungen
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-99\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\" id=\"subtab-AdminInformation\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/system_information?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Informationen
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"101\" id=\"subtab-AdminPerformance\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/performance?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Leistung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"102\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/administration?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Verwaltung
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"103\" id=\"subtab-AdminEmails\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminEmails&amp;token=7e0d1bb4e35721a6f5edb17229161fe6\" class=\"link\"> E-Mail
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"104\" id=\"subtab-AdminImport\">
                              <a href=\"/swisspackag.ch/admin60238sgog/index.php/configure/advanced/import?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" class=\"link\"> Importieren
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"105\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminEmployees&amp;token=c5667a5df420aea0ae699d1b5ab862bc\" class=\"link\"> Benutzerrechte
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminRequestSql&amp;token=12b793795d4b58c95e68a5dc6eef5a44\" class=\"link\"> Datenbank
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"112\" id=\"subtab-AdminLogs\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminLogs&amp;token=fa24a8608d177d9255259825e7b2d042\" class=\"link\"> Log-Dateien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"113\" id=\"subtab-AdminWebservice\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminWebservice&amp;token=6260ca7ba4ad9f7c5e140364f2551153\" class=\"link\"> Webservice
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"139\" id=\"tab-DEFAULT_MTR\">
              <span class=\"title\">Mehr</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"117\" id=\"subtab-DEFAULT\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=DEFAULT&amp;token=8d233f3813024ce5e3f11536259ceb11\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Mehr
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"141\" id=\"subtab-AdminAdvancedImporter_MTR\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporter&amp;token=8eb9fd92101690e96c37b2dc62457a86\" class=\"link\">
                    <i class=\"material-icons mi-\"></i>
                    <span>
                    Advanced Importer
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-141\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"140\" id=\"subtab-AdminAdvancedImporter\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporter&amp;token=8eb9fd92101690e96c37b2dc62457a86\" class=\"link\"> Advanced Importer
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"142\" id=\"subtab-AdminAdvancedImporterFlow\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterFlow&amp;token=73dc69198a3ce96ffe6c313103509ec0\" class=\"link\"> Flows
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"143\" id=\"subtab-AdminAdvancedImporterBlock\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterBlock&amp;token=33ac3174f074eb17803110a08cf6fa2c\" class=\"link\"> Blocks
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"144\" id=\"subtab-AdminAdvancedImporterSupplierReferences\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterSupplierReferences&amp;token=8f0cae11d8b33d3440245a8408941ce2\" class=\"link\"> Supplier References
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"145\" id=\"subtab-AdminAdvancedImporterHistory\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterHistory&amp;token=417309635f9176ea3d855a64dabf2041\" class=\"link\"> Entity Tracking
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"146\" id=\"subtab-AdminAdvancedImporterLog\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterLog&amp;token=ef5c1d503935160dd0d45e3501328b94\" class=\"link\"> Log-Dateien
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"147\" id=\"subtab-AdminAdvancedImporterCron\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterCron&amp;token=19d68889bd1b23033502b4ced8f42566\" class=\"link\"> Recurring tasks
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"148\" id=\"subtab-AdminAdvancedImporterTemplate\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterTemplate&amp;token=f55ab498409d04cd0f74784698e5bd55\" class=\"link\"> Templates
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"149\" id=\"subtab-AdminAdvancedImporterConfiguration\">
                              <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminAdvancedImporterConfiguration&amp;token=d4b81ae0c8a7102ff1aa81f82f2c5e9e\" class=\"link\"> Einstellungen
                              </a>
                            </li>

                                                                                                                                                                                                                              </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"128\" id=\"tab-AdminPresToPres\">
              <span class=\"title\">Prestashop Migrator</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"129\" id=\"subtab-AdminPresToPresImport\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPresToPresImport&amp;token=84232b0fc8c11f285fc803d55522d643\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-cloud-upload\">icon icon-cloud-upload</i>
                    <span>
                    Migration
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"130\" id=\"subtab-AdminPresToPresHistory\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPresToPresHistory&amp;token=93d13456b68b1697597cc4014f1b84b2\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-history\">icon icon-history</i>
                    <span>
                    History
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"131\" id=\"subtab-AdminPresToPresClean\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPresToPresClean&amp;token=f507010a27952bac41de27e7af0fbfcc\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-eraser\">icon icon-eraser</i>
                    <span>
                    Clean-up
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"132\" id=\"subtab-AdminPresToPresHelp\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminPresToPresHelp&amp;token=ecda92f9f3a6c1fb9967fea29289c76c\" class=\"link\">
                    <i class=\"material-icons mi-icon icon-question-circle\">icon icon-question-circle</i>
                    <span>
                    Hilfe
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"133\" id=\"tab-AdminSpmblocknewsadv\">
              <span class=\"title\">Nachrichten</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"134\" id=\"subtab-AdminSpmblocknewsadvnews\">
                  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminSpmblocknewsadvnews&amp;token=8e25df5558db128e6b8486f0c057727e\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Moderate Nachrichten
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                          
        
            </ul>
  
</nav>

<div id=\"main-div\">

  
    
<div class=\"header-toolbar\">
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Module und Dienste</li>
          
                      <li class=\"breadcrumb-item active\">
              <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/manage?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" aria-current=\"page\">Installierte Module</a>
            </li>
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
          <h1 class=\"title\">
            Installierte Module verwalten          </h1>
      

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
            
                                                          <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-add_module\"
                  href=\"#\"                  title=\"Modul hochladen\"                  data-toggle=\"pstooltip\"
                  data-placement=\"bottom\"                >
                  <i class=\"material-icons\">cloud_upload</i>
                  Modul hochladen
                </a>
                                                                        <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-addons_connect\"
                  href=\"#\"                  title=\"Mit dem Addons Marktplatz verbinden\"                  data-toggle=\"pstooltip\"
                  data-placement=\"bottom\"                >
                  <i class=\"material-icons\">vpn_key</i>
                  Mit dem Addons Marktplatz verbinden
                </a>
                                                  
                              <a class=\"btn btn-outline-secondary btn-help btn-sidebar\" href=\"#\"
                   title=\"Hilfe\"
                   data-toggle=\"sidebar\"
                   data-target=\"#right-sidebar\"
                   data-url=\"/swisspackag.ch/admin60238sgog/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop.com%252Fde%252Fdoc%252FAdminModules%253Fversion%253D1.7.4.4%2526country%253Dde/Hilfe?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\"
                   id=\"product_form_open_help\"
                >
                  Hilfe
                </a>
                                    </div>
        </div>
      
    </div>
  </div>

  
      <div class=\"page-head-tabs\" id=\"head_tabs\">
      <ul class=\"nav nav-pills\">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <li class=\"nav-item\">
                    <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/manage?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" id=\"subtab-AdminModulesManage\" class=\"nav-link tab active current\" data-submenu=\"45\">Installierte Module</a>
                  </li>
                                                                <li class=\"nav-item\">
                    <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/catalog?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" id=\"subtab-AdminModulesCatalog\" class=\"nav-link tab \" data-submenu=\"46\">Auswahl</a>
                  </li>
                                                                <li class=\"nav-item\">
                    <a href=\"/swisspackag.ch/admin60238sgog/index.php/module/notifications?_token=yM4YpwGjL5kOiDf6-NLArZCZsQJXYfJ3iLSDCqP8CS0\" id=\"subtab-AdminModulesNotifications\" class=\"nav-link tab \" data-submenu=\"47\">Nachrichten</a>
                  </li>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </ul>
    </div>
    
</div>
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=admin%40gmail.com&amp;firstname=Swisspack&amp;lastname=User&amp;website=http%3A%2F%2F45.56.70.30%2Fswisspackag.ch%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/swisspackag.ch/admin60238sgog/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle gekauften Zusatzmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=admin%40gmail.com&amp;firstname=Swisspack&amp;lastname=User&amp;website=http%3A%2F%2F45.56.70.30%2Fswisspackag.ch%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

    <div class=\"content-div  with-tabs\">

      

      
                        
      <div class=\"row \">
        <div class=\"col-sm-12\">
          <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>


  {% block content_header %}{% endblock %}
                 {% block content %}{% endblock %}
                 {% block content_footer %}{% endblock %}
                 {% block sidebar_right %}{% endblock %}

          
        </div>
      </div>

    </div>

  
</div>

<div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>Oh nein!</h1>
  <p class=\"mt-3\">
    Für diese Seite gibt es derzeit keine Mobilversion.
  </p>
  <p class=\"mt-2\">
    Da die Seite noch nicht für mobile Geräte angepasst wurde, können Sie sie nur am PC aufrufen.
  </p>
  <p class=\"mt-2\">
    Vielen Dank!
  </p>
  <a href=\"http://45.56.70.30/swisspackag.ch/admin60238sgog/index.php?controller=AdminDashboard&amp;token=a60173a8b528fe0fa2e6cac10511636e\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Zurück
  </a>
</div>
<div class=\"mobile-layer\"></div>

  <div id=\"footer\" class=\"bootstrap\">
    
</div>


  <div class=\"bootstrap\">
    <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-DE&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/de/login?email=admin%40gmail.com&amp;firstname=Swisspack&amp;lastname=User&amp;website=http%3A%2F%2F45.56.70.30%2Fswisspackag.ch%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/swisspackag.ch/admin60238sgog/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Verbinden Sie Ihren Shop mit Prestashops Marktplatz, um automatisch alle gekauften Zusatzmodule zu importieren.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Sie haben noch keinen Account?</h4>
\t\t\t\t\t\t<p class='text-justify'>Entdecken Sie die Vielfalt der PrestaShop Addons! Stöbern Sie im offiziellen PrestaShop Martkplatz mit aktuell über 3 500 innovativen Templates und modularen Erweiterungen - ob es sich nun um Optimierung der Wechselkurse, Erhöhung der Zugriffsrate, Maßnahmen zur Kundenbindung oder Rentabilitätssteigerung handelt.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Wechseln Sie zu PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/de/forgot-your-password\">Ich habe mein Passwort vergessen</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/de/login?email=admin%40gmail.com&amp;firstname=Swisspack&amp;lastname=User&amp;website=http%3A%2F%2F45.56.70.30%2Fswisspackag.ch%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-DE&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tErstellen Sie ein Konto
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Anmelden
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

  </div>

{% block javascripts %}{% endblock %}{% block extra_javascripts %}{% endblock %}{% block translate_javascripts %}{% endblock %}</body>
</html>", "__string_template__658e6bf14c5a0caf7182350daf17f039ee02305563f29eb191c75f8d333489d3", "");
    }
}
