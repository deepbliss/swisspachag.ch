<?php
/* Smarty version 3.1.33, created on 2019-04-18 15:55:18
  from '/var/www/html/swisspackag.ch/themes/classic/templates/customer/_partials/address-form.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb881c6f1e603_05542916',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '48f6a2a4343315fcb2fa46ce865b16c288980b7e' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/customer/_partials/address-form.tpl',
      1 => 1549287214,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/form-errors.tpl' => 1,
  ),
),false)) {
function content_5cb881c6f1e603_05542916 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14626585455cb881c6f171b3_91711395', "address_form");
?>

<?php }
/* {block "address_form_url"} */
class Block_2039913035cb881c6f17ff6_80552369 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    
        <h3><?php if (isset($_smarty_tpl->tpl_vars['id_address']->value)) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'IHRE ADRESSE'),$_smarty_tpl ) );
} else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'New address'),$_smarty_tpl ) );
}?></h3>            
        <form
          method="POST"
          action="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'address','params'=>array('id_address'=>$_smarty_tpl->tpl_vars['id_address']->value)),$_smarty_tpl ) );?>
"
          data-id-address="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_address']->value, ENT_QUOTES, 'UTF-8');?>
"
          data-refresh-url="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'address','params'=>array('ajax'=>1,'action'=>'addressForm')),$_smarty_tpl ) );?>
"
         class="register-section">
        <?php
}
}
/* {/block "address_form_url"} */
/* {block 'form_field'} */
class Block_20672145455cb881c6f1c015_64300975 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['form_field'][0], array( array('field'=>$_smarty_tpl->tpl_vars['field']->value),$_smarty_tpl ) );?>

                  <?php
}
}
/* {/block 'form_field'} */
/* {block 'form_fields'} */
class Block_11099738535cb881c6f1b509_60391693 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['formFields']->value, 'field');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value) {
?>
                  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20672145455cb881c6f1c015_64300975', 'form_field', $this->tplIndex);
?>

                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
              <?php
}
}
/* {/block 'form_fields'} */
/* {block "address_form_fields"} */
class Block_15514158475cb881c6f1b0e9_78898013 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <section class="form-fields register-block">
              <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11099738535cb881c6f1b509_60391693', 'form_fields', $this->tplIndex);
?>

            </section>
          <?php
}
}
/* {/block "address_form_fields"} */
/* {block 'form_buttons'} */
class Block_20136339615cb881c6f1d675_04085485 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

              <button class="btn btn-primary float-xs-right" type="submit" class="form-control-submit">
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Save','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

              </button>
            <?php
}
}
/* {/block 'form_buttons'} */
/* {block "address_form_footer"} */
class Block_3038183535cb881c6f1d281_26461925 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <footer class="form-footer clearfix">
            <input type="hidden" name="submitAddress" value="1">
            <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20136339615cb881c6f1d675_04085485', 'form_buttons', $this->tplIndex);
?>

          </footer>
         <?php
}
}
/* {/block "address_form_footer"} */
/* {block "address_form"} */
class Block_14626585455cb881c6f171b3_91711395 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'address_form' => 
  array (
    0 => 'Block_14626585455cb881c6f171b3_91711395',
  ),
  'address_form_url' => 
  array (
    0 => 'Block_2039913035cb881c6f17ff6_80552369',
  ),
  'address_form_fields' => 
  array (
    0 => 'Block_15514158475cb881c6f1b0e9_78898013',
  ),
  'form_fields' => 
  array (
    0 => 'Block_11099738535cb881c6f1b509_60391693',
  ),
  'form_field' => 
  array (
    0 => 'Block_20672145455cb881c6f1c015_64300975',
  ),
  'address_form_footer' => 
  array (
    0 => 'Block_3038183535cb881c6f1d281_26461925',
  ),
  'form_buttons' => 
  array (
    0 => 'Block_20136339615cb881c6f1d675_04085485',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div class="js-address-form">
    <?php $_smarty_tpl->_subTemplateRender('file:_partials/form-errors.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('errors'=>$_smarty_tpl->tpl_vars['errors']->value['']), 0, false);
?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2039913035cb881c6f17ff6_80552369', "address_form_url", $this->tplIndex);
?>
       
          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15514158475cb881c6f1b0e9_78898013', "address_form_fields", $this->tplIndex);
?>


          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3038183535cb881c6f1d281_26461925', "address_form_footer", $this->tplIndex);
?>
   
        
        </form> 
 
 
 </div>
<?php
}
}
/* {/block "address_form"} */
}
