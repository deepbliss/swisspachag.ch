<?php
/* Smarty version 3.1.33, created on 2019-05-01 03:20:40
  from '/var/www/html/swisspackag.ch/modules/staticblock/views/templates/hook/staticblock.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc8f46851eea3_04645461',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '14a578fb746bf4967ade92b62b00bcd08b333dde' => 
    array (
      0 => '/var/www/html/swisspackag.ch/modules/staticblock/views/templates/hook/staticblock.tpl',
      1 => 1545400041,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc8f46851eea3_04645461 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/html/swisspackag.ch/vendor/smarty/smarty/libs/plugins/modifier.regex_replace.php','function'=>'smarty_modifier_regex_replace',),));
if (isset($_smarty_tpl->tpl_vars['style_sheet']->value) && !empty($_smarty_tpl->tpl_vars['style_sheet']->value)) {?>
<style type="text/css">{$style_sheet|escape:'htmlall':'UTF-8'}</style><?php }?>

<?php if (isset($_smarty_tpl->tpl_vars['static_block']->value) && $_smarty_tpl->tpl_vars['static_block']->value) {?>
	<div id="static_content_wrapper">
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['static_block']->value, 'block');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['block']->value) {
?>
			<?php if (isset($_smarty_tpl->tpl_vars['block']->value['id_static_block_template']) && $_smarty_tpl->tpl_vars['block']->value['id_static_block_template'] && isset($_smarty_tpl->tpl_vars['block']->value['template']) && $_smarty_tpl->tpl_vars['block']->value['template']) {?>
				<?php echo smarty_modifier_regex_replace($_smarty_tpl->tpl_vars['block']->value['template'],"/[\r\t\n]/"," ");?>
			<?php } else { ?>
				<div <?php if ($_smarty_tpl->tpl_vars['block']->value['custom_css'] == 1) {?> id="home_content_<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['block']->value['id_static_block'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" <?php } else { ?>id="home_content"<?php }?>>
				<?php if ($_smarty_tpl->tpl_vars['block']->value['title_active'] == 1) {?>
					<h4 <?php if ($_smarty_tpl->tpl_vars['block']->value['custom_css'] == 1) {?> id="mytitle_<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['block']->value['id_static_block'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"<?php } else { ?> id="mytitle"<?php }?> class="block" >
						<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['block']->value['block_title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

					</h4>
				<?php }?>
				<?php echo smarty_modifier_regex_replace($_smarty_tpl->tpl_vars['block']->value['content'],"/[\r\t\n]/"," ");?>
				</div>
			<?php }?>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</div>
<?php }
}
}
