<?php
/* Smarty version 3.1.33, created on 2019-04-18 13:38:09
  from '/var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/admin/spmblocknewsadvnews/helpers/list/list_content.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb861a1ec1811_96346122',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c729c3c71cb75c4169713af0a116776486838b8d' => 
    array (
      0 => '/var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/admin/spmblocknewsadvnews/helpers/list/list_content.tpl',
      1 => 1545651729,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cb861a1ec1811_96346122 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3297429795cb861a1ea8452_47548564', "td_content");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "helpers/list/list_content.tpl");
}
/* {block "td_content"} */
class Block_3297429795cb861a1ea8452_47548564 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'td_content' => 
  array (
    0 => 'Block_3297429795cb861a1ea8452_47548564',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php if (isset($_smarty_tpl->tpl_vars['params']->value['type_custom']) && $_smarty_tpl->tpl_vars['params']->value['type_custom'] == 'title_item') {?>
            <?php if (isset($_smarty_tpl->tpl_vars['tr']->value[$_smarty_tpl->tpl_vars['key']->value])) {?>
                <span class="label-tooltip" data-original-title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Click here to see news on your site','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
" data-toggle="tooltip">

                    <a href="
                    <?php if ($_smarty_tpl->tpl_vars['params']->value['is_rewrite'] == 0) {?>
                        <?php ob_start();
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value['id_lang'],'htmlall','UTF-8' ));
$_prefixVariable1 = ob_get_clean();
ob_start();
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value['id_shop'],'htmlall','UTF-8' ));
$_prefixVariable2 = ob_get_clean();
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('spmblocknewsadv','news',array(),true,$_prefixVariable1,$_prefixVariable2),'htmlall','UTF-8' ));
if ($_smarty_tpl->tpl_vars['params']->value['is16'] == 1) {?>&<?php } else { ?>?<?php }?>id=<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value['id'],'htmlall','UTF-8' ));?>

                    <?php } else { ?>
                        <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['params']->value['base_dir_ssl'],'htmlall','UTF-8' ));
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['params']->value['iso_code'],'htmlall','UTF-8' ));?>
/news/<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value['seo_url'],'htmlall','UTF-8' ));?>

                    <?php }?>
                    "
                       style="text-decoration:underline" target="_blank">
                        <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value[$_smarty_tpl->tpl_vars['key']->value],'htmlall','UTF-8' ));?>

                    </a>
                </span>
            <?php }?>

        <?php } elseif (isset($_smarty_tpl->tpl_vars['params']->value['type_custom']) && $_smarty_tpl->tpl_vars['params']->value['type_custom'] == 'is_active') {?>

        
            <?php echo '<script'; ?>
 type="text/javascript">

                var ajax_link_spmblocknewsadv = '<?php echo $_smarty_tpl->tpl_vars['params']->value['ajax_link'];?>
';

            <?php echo '</script'; ?>
>
        

            <span id="activeitem<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value['id'],'htmlall','UTF-8' ));?>
">
                    <span class="label-tooltip" data-original-title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Click here to activate or deactivate news on your site','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
" data-toggle="tooltip">
                    <a href="javascript:void(0)" onclick="spmblocknewsadv_list(<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value['id'],'htmlall','UTF-8' ));?>
,'active',<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value[$_smarty_tpl->tpl_vars['key']->value],'htmlall','UTF-8' ));?>
,'news','<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['params']->value['token_custom'],'htmlall','UTF-8' ));?>
');" style="text-decoration:none">
                        <img src="../img/admin/../../modules/spmblocknewsadv/views/img/<?php if ($_smarty_tpl->tpl_vars['tr']->value[$_smarty_tpl->tpl_vars['key']->value] == 1) {?>ok.gif<?php } else { ?>no_ok.gif<?php }?>"  />
                    </a>
                </span>
            </span>


        <?php } elseif (isset($_smarty_tpl->tpl_vars['params']->value['type_custom']) && $_smarty_tpl->tpl_vars['params']->value['type_custom'] == 'is_comments') {?>

        
            <?php echo '<script'; ?>
 type="text/javascript">

                var ajax_link_spmblocknewsadv = '<?php echo $_smarty_tpl->tpl_vars['params']->value['ajax_link'];?>
';

            <?php echo '</script'; ?>
>
        


            <span id="activeitemc<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value['id'],'htmlall','UTF-8' ));?>
">
                    <span class="label-tooltip" data-original-title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Click here to activate or deactivate comments for news','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
" data-toggle="tooltip">
                    <a href="javascript:void(0)" onclick="spmblocknewsadv_list(<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value['id'],'htmlall','UTF-8' ));?>
,'active',<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value[$_smarty_tpl->tpl_vars['key']->value],'htmlall','UTF-8' ));?>
,'comment','<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['params']->value['token_custom'],'htmlall','UTF-8' ));?>
');" style="text-decoration:none">
                        <img src="../img/admin/../../modules/spmblocknewsadv/views/img/<?php if ($_smarty_tpl->tpl_vars['tr']->value[$_smarty_tpl->tpl_vars['key']->value] == 1) {?>ok.gif<?php } else { ?>no_ok.gif<?php }?>"  />
                    </a>
                </span>
            </span>

        <?php } elseif (isset($_smarty_tpl->tpl_vars['params']->value['type_custom']) && $_smarty_tpl->tpl_vars['params']->value['type_custom'] == 'img') {?>
            <?php if (strlen($_smarty_tpl->tpl_vars['tr']->value[$_smarty_tpl->tpl_vars['key']->value]) > 0) {?>
            <img src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['params']->value['logo_img_path'],'htmlall','UTF-8' ));
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['tr']->value[$_smarty_tpl->tpl_vars['key']->value],'htmlall','UTF-8' ));?>
" class="img-thumbnail" style="width: 80px"/>
            <?php } else { ?>
                ---
            <?php }?>


        <?php } else { ?>
            <?php 
$_smarty_tpl->inheritance->callParent($_smarty_tpl, $this, '{$smarty.block.parent}');
?>

        <?php }?>


    <?php
}
}
/* {/block "td_content"} */
}
