<?php
/* Smarty version 3.1.33, created on 2019-04-18 13:36:47
  from '/var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/admin/spmblocknewsadvnews/helpers/form/form.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb8614f785d51_05471879',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '821eb7cf4b4189ab28dd5ad729ee6cc54900cf43' => 
    array (
      0 => '/var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/admin/spmblocknewsadvnews/helpers/form/form.tpl',
      1 => 1555587254,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cb8614f785d51_05471879 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9823161755cb8614f7539d8_24762335', "field");
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, "helpers/form/form.tpl");
}
/* {block "field"} */
class Block_9823161755cb8614f7539d8_24762335 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'field' => 
  array (
    0 => 'Block_9823161755cb8614f7539d8_24762335',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	<?php if ($_smarty_tpl->tpl_vars['input']->value['type'] == 'cms_pages') {?>

    <div class="col-lg-9">
        <div class="panel col-lg-7">


            <table width="50%" cellspacing="0" cellpadding="0" class="table">
                <thead>
                <tr>
                    <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Shop','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</th>
                </tr>
                </thead>
                <tbody>
                <?php $_smarty_tpl->_assignInScope('i', 0);?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['input']->value['values'], '_shop');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_shop']->value) {
?>
                    <tr>
                        <td>

                            <img src="../modules/spmblocknewsadv/views/img/lv2_<?php if (count($_smarty_tpl->tpl_vars['input']->value['values'])-1 == $_smarty_tpl->tpl_vars['i']->value) {?>f<?php } else { ?>b<?php }?>.png" alt="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['_shop']->value['name'],'htmlall','UTF-8' ));?>
" style="vertical-align:middle;">
                            <label class="child">
                                <input type="checkbox" class="input_shop" <?php if (in_array($_smarty_tpl->tpl_vars['_shop']->value['id_shop'],$_smarty_tpl->tpl_vars['input']->value['selected_data'])) {?>checked="checked"<?php }?> value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['_shop']->value['id_shop'],'htmlall','UTF-8' ));?>
" name="cat_shop_association[]">
                                <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['_shop']->value['name'],'htmlall','UTF-8' ));?>

                            </label>
                        </td>
                    </tr>
                    <?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value++);?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </tbody>
            </table>



        </div>
        <?php if (isset($_smarty_tpl->tpl_vars['input']->value['desc']) && !empty($_smarty_tpl->tpl_vars['input']->value['desc'])) {?>
            <p class="help-block">
                <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['desc'],'htmlall','UTF-8' ));?>

            </p>
        <?php }?>
    </div>

    <?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'related_products') {?>

        <div class="col-lg-9">




            <div id="divAccessories">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['input']->value['values'], 'accessory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['accessory']->value) {
?>
                    <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['accessory']->value['name'],'htmlall','UTF-8' ));
if (isset($_smarty_tpl->tpl_vars['accessory']->value['reference'])) {
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['accessory']->value['reference'],'htmlall','UTF-8' ));
}?>
                     <span class="delAccessory" name="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['accessory']->value['id_product'],'htmlall','UTF-8' ));?>
"
                           style="cursor:pointer;"><img src="../img/admin/delete.gif" class="middle" alt="Delete" /></span><br />
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
              </div>

               <input type="hidden" name="inputAccessories" id="inputAccessories"
                      value="<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['input']->value['values'], 'accessory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['accessory']->value) {
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['accessory']->value['id_product'],'htmlall','UTF-8' ));?>
-<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>" />
                <input type="hidden" name="nameAccessories" id="nameAccessories"
                       value="<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['input']->value['values'], 'accessory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['accessory']->value) {
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['accessory']->value['name'],'htmlall','UTF-8' ));?>
¤<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>" />

             <div id="ajax_choose_product">
                <input type="text" value="" id="product_autocomplete_input" />
             </div>



             
            <?php echo '<script'; ?>
 type="text/javascript">
                $('document').ready( function() {
                if($('#divAccessories').length){
                    initAccessoriesAutocomplete();
                    $('#divAccessories').delegate('.delAccessory', 'click', function(){ delAccessory($(this).attr('name')); });
                }
                });
            <?php echo '</script'; ?>
>
            



            <?php if (isset($_smarty_tpl->tpl_vars['input']->value['desc']) && !empty($_smarty_tpl->tpl_vars['input']->value['desc'])) {?>
                <p class="help-block">
                    <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['desc'],'htmlall','UTF-8' ));?>

                </p>
            <?php }?>
        </div>

    <?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'related_items') {?>

        <div class="col-lg-9">
            <div class="panel col-lg-9" style="height:200px; overflow-x:hidden; overflow-y:scroll;">


                <table width="50%" cellspacing="0" cellpadding="0" class="table">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'ID','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</th>
                        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Title','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</th>
                        <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Language','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $_smarty_tpl->_assignInScope('i', 0);?>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['input']->value['values'], '_item');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_item']->value) {
?>
                        <tr>
                            <td>
                                                                <input type="checkbox" class="input_shop" <?php if (in_array($_smarty_tpl->tpl_vars['_item']->value['id'],$_smarty_tpl->tpl_vars['input']->value['selected_data'])) {?>checked="checked"<?php }?>
                                       value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['_item']->value['id'],'htmlall','UTF-8' ));?>
" name="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['name_field_custom'],'htmlall','UTF-8' ));?>
[]">
                            </td>
                            <td>


                                    <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['_item']->value['id'],'htmlall','UTF-8' ));?>

                            </td>
                            <td>

                                <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['_item']->value['title'],'htmlall','UTF-8' ));?>

                            </td>
                            <td>

                                <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['_item']->value['iso_lang'],'htmlall','UTF-8' ));?>

                            </td>
                        </tr>
                        <?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value++);?>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </tbody>
                </table>


            </div>
            <?php if (isset($_smarty_tpl->tpl_vars['input']->value['desc']) && !empty($_smarty_tpl->tpl_vars['input']->value['desc'])) {?>
                <p class="help-block">
                    <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['desc'],'htmlall','UTF-8' ));?>

                </p>
            <?php }?>
        </div>

    <?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'item_image_custom') {?>

        <div class="col-lg-9">

            <div class="form-group">
                <div class="col-lg-6" >
                    <input id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['name'],'htmlall','UTF-8' ));?>
" type="file" name="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['name'],'htmlall','UTF-8' ));?>
" class="hide" />
                    <div class="dummyfile input-group">
                        <span class="input-group-addon"><i class="icon-file"></i></span>
                        <input id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['name'],'htmlall','UTF-8' ));?>
-name" type="text" class="disabled" name="filename" readonly />
							<span class="input-group-btn">
								<button id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['name'],'htmlall','UTF-8' ));?>
-selectbutton" type="button" name="submitAddAttachments" class="btn btn-default">
                                    <i class="icon-folder-open"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Choose a file','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>

                                </button>
							</span>
                    </div>

                    
                        <?php echo '<script'; ?>
 type="text/javascript">
                            $(document).ready(function(){
                                $('#<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['name'],'htmlall','UTF-8' ));?>
-selectbutton').click(function(e){
                                    $('#<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['name'],'htmlall','UTF-8' ));?>
').trigger('click');
                                });
                                $('#<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['name'],'htmlall','UTF-8' ));?>
').change(function(e){
                                    var val = $(this).val();
                                    var file = val.split(/[\/]/);
                                    $('#<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['name'],'htmlall','UTF-8' ));?>
-name').val(file[file.length-1]);
                                });
                            });
                        <?php echo '</script'; ?>
>
                    




                </div>



            </div>
            <?php if (isset($_smarty_tpl->tpl_vars['input']->value['desc']) && !empty($_smarty_tpl->tpl_vars['input']->value['desc'])) {?>
                <p class="help-block">
                    <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['desc'],'htmlall','UTF-8' ));?>

                    <br/>
                    <span style="color:black:font-size:13px"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Max file size in php.ini','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
: <b style="color:green"><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['max_upload_info'],'htmlall','UTF-8' ));?>
</b></span>
                </p>
            <?php }?>
            <?php if (isset($_smarty_tpl->tpl_vars['input']->value['is_demo']) && !empty($_smarty_tpl->tpl_vars['input']->value['is_demo'])) {?>
                <?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['is_demo'],'quotes','UTF-8' ));?>

            <?php }?>

            <?php if (isset($_smarty_tpl->tpl_vars['input']->value['logo_img']) && $_smarty_tpl->tpl_vars['input']->value['logo_img'] != '') {?>
            <div class="form-group" id="post_images_list">
                    <input type="radio" name="post_images" checked="" style="display: none">
                    <div id="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['name'],'htmlall','UTF-8' ));?>
-images-thumbnails" class="col-lg-12">
                        <img src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['logo_img_path'],'htmlall','UTF-8' ));?>
" class="img-thumbnail" style="width: 200px"/>

                    </div>

                    <a class="delete_product_image btn btn-default" href="javascript:void(0)"
                       onclick = "delete_img(<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['id_item'],'htmlall','UTF-8' ));?>
,'<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['token_custom'],'htmlall','UTF-8' ));?>
');"
                            style="margin-top: 10px">
                        <i class="icon-trash"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Delete this image','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>

                    </a>


                    
                    <?php echo '<script'; ?>
 type="text/javascript">

                        var ajax_link_spmblocknewsadv = '<?php echo $_smarty_tpl->tpl_vars['input']->value['ajax_link'];?>
';

                    <?php echo '</script'; ?>
>
                    

            </div>
            <?php }?>


        </div>
    <?php } elseif ($_smarty_tpl->tpl_vars['input']->value['type'] == 'item_date') {?>

        <div class="row">
            <div class="input-group col-lg-4">
                <input id="<?php if (isset($_smarty_tpl->tpl_vars['input']->value['id'])) {
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['id'],'htmlall','UTF-8' ));
} else {
echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['name'],'htmlall','UTF-8' ));
}?>"
                       type="text" data-hex="true"
                       <?php if (isset($_smarty_tpl->tpl_vars['input']->value['class'])) {?>class="<?php echo $_smarty_tpl->tpl_vars['input']->value['class'];?>
"
                       <?php } else { ?>class="item_datepicker"<?php }?> name="time_add" value="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['input']->value['time_add'],'html','UTF-8' ));?>
" />
                <span class="input-group-addon"><i class="icon-calendar-empty"></i></span>
            </div>
        </div>

    

        <?php echo '<script'; ?>
 type="text/javascript">
            $('document').ready( function() {

                var dateObj = new Date();
                var hours = dateObj.getHours();
                var mins = dateObj.getMinutes();
                var secs = dateObj.getSeconds();
                if (hours < 10) { hours = "0" + hours; }
                if (mins < 10) { mins = "0" + mins; }
                if (secs < 10) { secs = "0" + secs; }
                var time = " "+hours+":"+mins+":"+secs;

                if ($(".item_datepicker").length > 0)
                    $(".item_datepicker").datepicker({prevText: '',nextText: '',dateFormat: 'yy-mm-dd'+time});

            });
        <?php echo '</script'; ?>
>
    

	<?php } else { ?>
		<?php 
$_smarty_tpl->inheritance->callParent($_smarty_tpl, $this, '{$smarty.block.parent}');
?>

	<?php }
}
}
/* {/block "field"} */
}
