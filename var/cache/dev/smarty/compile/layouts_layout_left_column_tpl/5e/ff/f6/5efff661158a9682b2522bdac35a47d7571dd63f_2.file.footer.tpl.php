<?php
/* Smarty version 3.1.33, created on 2019-04-24 13:48:01
  from '/var/www/html/swisspackag.ch/themes/classic/templates/_partials/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc04cf17d2b80_16919071',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5efff661158a9682b2522bdac35a47d7571dd63f' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/_partials/footer.tpl',
      1 => 1545816612,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc04cf17d2b80_16919071 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="footer-container">
  <div class="container">
    
    <div class="row footer-border">
      <div class="col-md-12">
        <div class="block-custom-html-content block-left">
               <b>Swisspack AG</b> | Industriestrasse 53 | CH-9443 Widnau | Tel. +41 71 722 85 85 | Fax +41 71 722 85 84
        </div>
          <div class="block-custom-html-content block-right">
              <a href="<?php echo htmlspecialchars(__PS_BASE_URI__, ENT_QUOTES, 'UTF-8');?>
content/12-kontakt">Öffnungszeiten</a>
        </div>
       
      </div>
    </div>
  </div>
</div>
<?php }
}
