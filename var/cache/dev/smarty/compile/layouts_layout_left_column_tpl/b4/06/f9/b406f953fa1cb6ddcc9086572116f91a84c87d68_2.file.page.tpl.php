<?php
/* Smarty version 3.1.33, created on 2019-04-24 13:48:01
  from '/var/www/html/swisspackag.ch/themes/classic/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc04cf161fd76_31760173',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b406f953fa1cb6ddcc9086572116f91a84c87d68' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/page.tpl',
      1 => 1545070356,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc04cf161fd76_31760173 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18890374825cc04cf161ad84_96748736', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_3024085055cc04cf161b946_23878509 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_10387139695cc04cf161b315_53177215 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3024085055cc04cf161b946_23878509', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_15362373825cc04cf161d8d9_41730238 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_20966812765cc04cf161dff7_87764139 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_13206267515cc04cf161d238_89042280 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15362373825cc04cf161d8d9_41730238', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20966812765cc04cf161dff7_87764139', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_8830711675cc04cf161f1d7_32821123 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_10296926895cc04cf161ec78_41252401 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8830711675cc04cf161f1d7_32821123', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_18890374825cc04cf161ad84_96748736 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_18890374825cc04cf161ad84_96748736',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_10387139695cc04cf161b315_53177215',
  ),
  'page_title' => 
  array (
    0 => 'Block_3024085055cc04cf161b946_23878509',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_13206267515cc04cf161d238_89042280',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_15362373825cc04cf161d8d9_41730238',
  ),
  'page_content' => 
  array (
    0 => 'Block_20966812765cc04cf161dff7_87764139',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_10296926895cc04cf161ec78_41252401',
  ),
  'page_footer' => 
  array (
    0 => 'Block_8830711675cc04cf161f1d7_32821123',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10387139695cc04cf161b315_53177215', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13206267515cc04cf161d238_89042280', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10296926895cc04cf161ec78_41252401', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
