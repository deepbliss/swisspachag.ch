<?php
/* Smarty version 3.1.33, created on 2019-04-24 13:47:49
  from '/var/www/html/swisspackag.ch/themes/classic/templates/catalog/listing/product-list.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc04ce50c5746_50395252',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ddbced6ce722846208c5279f53deb53ea82d5f51' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/catalog/listing/product-list.tpl',
      1 => 1555410724,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/products-top.tpl' => 1,
    'file:catalog/_partials/products.tpl' => 1,
    'file:catalog/_partials/products-bottom.tpl' => 1,
    'file:errors/not-found.tpl' => 1,
  ),
),false)) {
function content_5cc04ce50c5746_50395252 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

           
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12189528095cc04ce50bed32_60223350', 'content');
?>


 
<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'product_list_header'} */
class Block_7065616885cc04ce50bf216_21932673 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <h2 class="h2"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['listing']->value['label'], ENT_QUOTES, 'UTF-8');?>
</h2>
    <?php
}
}
/* {/block 'product_list_header'} */
/* {block 'product_list_top'} */
class Block_17868263825cc04ce50c1119_33987695 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-top.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, false);
?>
          <?php
}
}
/* {/block 'product_list_top'} */
/* {block 'product_list_active_filters'} */
class Block_16061677915cc04ce50c1ee8_64976832 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <div id="" class="hidden-sm-down">
            <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_active_filters'];?>

          </div>
        <?php
}
}
/* {/block 'product_list_active_filters'} */
/* {block 'product_list'} */
class Block_14628172395cc04ce50c2d03_12453889 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, false);
?>
          <?php
}
}
/* {/block 'product_list'} */
/* {block 'product_list_bottom'} */
class Block_15679072055cc04ce50c3b81_08896552 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php $_smarty_tpl->_subTemplateRender('file:catalog/_partials/products-bottom.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('listing'=>$_smarty_tpl->tpl_vars['listing']->value), 0, false);
?>
          <?php
}
}
/* {/block 'product_list_bottom'} */
/* {block 'content'} */
class Block_12189528095cc04ce50bed32_60223350 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_12189528095cc04ce50bed32_60223350',
  ),
  'product_list_header' => 
  array (
    0 => 'Block_7065616885cc04ce50bf216_21932673',
  ),
  'product_list_top' => 
  array (
    0 => 'Block_17868263825cc04ce50c1119_33987695',
  ),
  'product_list_active_filters' => 
  array (
    0 => 'Block_16061677915cc04ce50c1ee8_64976832',
  ),
  'product_list' => 
  array (
    0 => 'Block_14628172395cc04ce50c2d03_12453889',
  ),
  'product_list_bottom' => 
  array (
    0 => 'Block_15679072055cc04ce50c3b81_08896552',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
          
  <section id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7065616885cc04ce50bf216_21932673', 'product_list_header', $this->tplIndex);
?>


<?php if (empty($_smarty_tpl->tpl_vars['subcategories']->value)) {?>
    <section id="products" class="umair">
      <?php if (count($_smarty_tpl->tpl_vars['listing']->value['products'])) {?>

        <div id="">
          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17868263825cc04ce50c1119_33987695', 'product_list_top', $this->tplIndex);
?>

        </div>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16061677915cc04ce50c1ee8_64976832', 'product_list_active_filters', $this->tplIndex);
?>


        <div id="">
          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14628172395cc04ce50c2d03_12453889', 'product_list', $this->tplIndex);
?>

        </div>

        <div id="js-product-list-bottom">
          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15679072055cc04ce50c3b81_08896552', 'product_list_bottom', $this->tplIndex);
?>

        </div>

      <?php } else { ?>

        <?php $_smarty_tpl->_subTemplateRender('file:errors/not-found.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

      <?php }?>
    </section>
 <?php }?>  

  </section>
<?php
}
}
/* {/block 'content'} */
}
