<?php
/* Smarty version 3.1.33, created on 2019-04-24 13:47:49
  from '/var/www/html/swisspackag.ch/themes/classic/templates/catalog/listing/category.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc04ce50badc1_76653783',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3b4c2e25fd632a85994efa33d050b02597ea61a8' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/catalog/listing/category.tpl',
      1 => 1555403165,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:./product-list.tpl' => 1,
    'file:./pagination.tpl' => 1,
  ),
),false)) {
function content_5cc04ce50badc1_76653783 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
     
                                           
<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2509397865cc04ce50a2660_15605511', 'product_list_header');
?>

  <?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'catalog/listing/product-list.tpl');
}
/* {block 'product_list_header'} */
class Block_2509397865cc04ce50a2660_15605511 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_list_header' => 
  array (
    0 => 'Block_2509397865cc04ce50a2660_15605511',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

 
    <?php if (isset($_smarty_tpl->tpl_vars['category']->value)) {?>     
        
        <?php if (isset($_smarty_tpl->tpl_vars['subcategories']->value)) {?>
        
              
 <div class="catgegory-head">
        <h1 class="h1"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['name'], ENT_QUOTES, 'UTF-8');?>
</h1>        
    </div> 
              
<?php if ($_smarty_tpl->tpl_vars['category']->value['description']) {?>
    <div id="category-description" class="text-muted"><?php echo $_smarty_tpl->tpl_vars['category']->value['description'];?>
</div>
<?php }?>
       
       <?php if (isset($_smarty_tpl->tpl_vars['listing']->value['rendered_facets'])) {?>
<div id="search_filters_wrapper" class="hidden-sm-down">
  <div id="search_filter_controls" class="hidden-md-up">
      <span id="_mobile_search_filters_clear_all"></span>
      <button class="btn btn-secondary ok">
        <i class="material-icons rtl-no-flip">&#xE876;</i>
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'OK','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

      </button>
  </div>
  <?php echo $_smarty_tpl->tpl_vars['listing']->value['rendered_facets'];?>

</div>
<?php }?>

              
        <!-- Subcategories -->
        <div id="subcategories" class="showcase">
            <ul class="inline_list">
            <?php $_smarty_tpl->_assignInScope('i', "0");?>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subcategories']->value, 'subcategory');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['subcategory']->value) {
?>


                            <li class="clearfix i<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['i']->value%3, ENT_QUOTES, 'UTF-8');?>
" id="subcat_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subcategory']->value['id_category'], ENT_QUOTES, 'UTF-8');?>
">
                     <h5><a class="subcategory-name normal-name" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['subcategory']->value['id_category'],$_smarty_tpl->tpl_vars['subcategory']->value['link_rewrite']),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['subcategory']->value['name'],125,'...' )),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</a></h5>
                     <h5><a class="subcategory-name hover-name" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['subcategory']->value['id_category'],$_smarty_tpl->tpl_vars['subcategory']->value['link_rewrite']),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['subcategory']->value['name'],125,'...' )),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span></a></h5>
                     
                        <div class="subcategory-image">
                            <a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getCategoryLink($_smarty_tpl->tpl_vars['subcategory']->value['id_category'],$_smarty_tpl->tpl_vars['subcategory']->value['link_rewrite']),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['subcategory']->value['name'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="img">
                                <?php if ($_smarty_tpl->tpl_vars['subcategory']->value['id_image']) {?>
                                    <img class="replace-2x" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getCatImageLink($_smarty_tpl->tpl_vars['subcategory']->value['link_rewrite'],$_smarty_tpl->tpl_vars['subcategory']->value['id_image'],'category_showcase'),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['subcategory']->value['name'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                <?php } else { ?>
                                    <img class="replace-2x" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['img_cat_dir']->value, ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['lang_iso']->value, ENT_QUOTES, 'UTF-8');?>
-default-category_default.jpg" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['subcategory']->value['name'],'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                <?php }?>
                            </a>
                        </div>
                </li>
              

                <?php $_smarty_tpl->_assignInScope('i', $_smarty_tpl->tpl_vars['i']->value+1);?>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <?php if (isset($_smarty_tpl->tpl_vars['Boxmaker']->value)) {?>
                <li class="clearfix i<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['i']->value%3, ENT_QUOTES, 'UTF-8');?>
" id="boxmaker">
                    <div class="title"><a class="categoryName" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['Boxmaker']->value, ENT_QUOTES, 'UTF-8');?>
" class="cat_name">Boxmaker</a></div>
                    <div class="image">
                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['Boxmaker']->value, ENT_QUOTES, 'UTF-8');?>
" title="Boxmaker" class="img">
                            <img src="themes/swisspack/img/theme/box.jpg" alt="" height="200" />
                        </a>
                    </div>
                </li>
            <?php }?>
            </ul>
            <br class="clear"/>
        </div>                  
        <?php } elseif ($_smarty_tpl->tpl_vars['products']->value) {?>            
                 <div id="subcategories">
            <ul class="inline_list">
                <li class="clearfix">
                <div class="content_sortPagiBar">
                    <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
                </div>

                <?php $_smarty_tpl->_subTemplateRender("file:./product-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('products'=>$_smarty_tpl->tpl_vars['products']->value,'cat_id'=>$_smarty_tpl->tpl_vars['category']->value->id), 0, false);
?>

                <div class="content_sortPagiBar">
                    <?php $_smarty_tpl->_subTemplateRender("file:./pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
                </div>
                </li>
            </ul>
        </div>
        <?php } else { ?>
            <p class="warning">No products in the category.</p>
       
     
    <?php }
}
}
}
/* {/block 'product_list_header'} */
}
