<?php
/* Smarty version 3.1.33, created on 2019-04-18 15:28:59
  from '/var/www/html/swisspackag.ch/themes/classic/templates/errors/not-found.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb87b9bb38691_76417599',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '15a2399106408f871d5e19eb2e47f40e884be3e9' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/errors/not-found.tpl',
      1 => 1546949589,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cb87b9bb38691_76417599 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<section id="content" class="page-content page-not-found">
  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4712129545cb87b9bb357b1_13649725', 'page_content');
?>

</section>


<?php }
/* {block 'hook_not_found'} */
class Block_5788744825cb87b9bb37886_98472207 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayNotFound'),$_smarty_tpl ) );?>

    <?php
}
}
/* {/block 'hook_not_found'} */
/* {block 'page_content'} */
class Block_4712129545cb87b9bb357b1_13649725 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content' => 
  array (
    0 => 'Block_4712129545cb87b9bb357b1_13649725',
  ),
  'hook_not_found' => 
  array (
    0 => 'Block_5788744825cb87b9bb37886_98472207',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

     <?php if (!empty($_smarty_tpl->tpl_vars['search_string']->value)) {?>
        <h4><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Keine Ergebnise gefunden ','d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>
"<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search_string']->value, ENT_QUOTES, 'UTF-8');?>
"</h4>
     <?php } else { ?> 
        <h4><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Bitte geben Sie ein Stichwort für die Suche ein.','d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>
</h4>
     <?php }?>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5788744825cb87b9bb37886_98472207', 'hook_not_found', $this->tplIndex);
?>


  <?php
}
}
/* {/block 'page_content'} */
}
