<?php
/* Smarty version 3.1.33, created on 2019-05-01 03:20:40
  from '/var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/hooks/head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc8f4684ea393_49934281',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0d07b3749ef8c8e4c8002ffac8cebf61ec2c4f2c' => 
    array (
      0 => '/var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/hooks/head.tpl',
      1 => 1545651729,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc8f4684ea393_49934281 (Smarty_Internal_Template $_smarty_tpl) {
?> 
<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_news']->value != 0) {?>
        <meta property="fb:app_id" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvappid']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
        <meta property="fb:admins" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvappadmin']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
       <meta property="og:title" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvname']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
       <?php if (strlen($_smarty_tpl->tpl_vars['spmblocknewsadvimg']->value) > 0) {?>
	   <meta property="og:image" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
upload/spmblocknewsadv/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvimg']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
	   <?php }?>
	   <meta property="og:type" content="product"/>
       <meta property="og:url" content="<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrew_on']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_seo_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_id']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"/>
<?php }?>


<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrsson']->value == 1) {?>
<link rel="alternate" type="application/rss+xml" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvrss_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<?php }?>



<?php echo '<script'; ?>
 type="text/javascript">
    var process_url_spmblocknewsadv = '<?php echo $_smarty_tpl->tpl_vars['spmblocknewsadvprocess_url']->value;?>
';

<?php echo '</script'; ?>
>


<?php }
}
