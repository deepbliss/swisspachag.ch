<?php
/* Smarty version 3.1.33, created on 2019-04-24 13:48:01
  from '/var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/hooks/left.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc04cf17c78f3_29654622',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4ea8db386a35572295b28c354578508c6ec18b11' => 
    array (
      0 => '/var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/hooks/left.tpl',
      1 => 1545651729,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc04cf17c78f3_29654622 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/html/swisspackag.ch/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>
 
 <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvleftnews']->value == 1) {?>




     <div id="spmblocknewsadvnews_block_left"
          class="block
                <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvbnews_slider']->value == 1) {?>owl_news_latest_news_type_carousel<?php }?>
                <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>block-categories hidden-sm-down<?php }?>
                <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis16']->value == 1 && $_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 0) {?>blockmanufacturer16<?php }?>
                spmblocknewsadv-block" >
         <h4 class="title_block <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>text-uppercase<?php }?>">

             <a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Latest News','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
"
                     ><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Latest News','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</a>

             <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrsson']->value == 1) {?>
                 <a  class="margin-left-left-10" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvrss_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'RSS Feed','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
" target="_blank">
                     <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmblocknewsadv/views/img/feed.png" alt="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'RSS Feed','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
" />
                 </a>
             <?php }?>

         </h4>
         <div class="block_content">

             <?php if (count($_smarty_tpl->tpl_vars['spmblocknewsadvitemsblock']->value) > 0) {?>
                 <div class="items-articles-block">

                     <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvbnews_slider']->value == 1 && (count($_smarty_tpl->tpl_vars['spmblocknewsadvitemsblock']->value) > $_smarty_tpl->tpl_vars['spmblocknewsadvbnews_sl']->value)) {?><ul class="owl-carousel owl-theme"><?php }?>

                     <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['spmblocknewsadvitemsblock']->value, 'items', false, NULL, 'myLoop1', array (
  'index' => true,
  'first' => true,
  'last' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['items']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['total'];
?>
                     <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value['data'], 'item', false, NULL, 'myLoop', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
?>


                         <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvbnews_slider']->value == 1) {?>
                            <?php if (((isset($_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['index'] : null)%$_smarty_tpl->tpl_vars['spmblocknewsadvbnews_sl']->value == 0) || (isset($_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['first'] : null)) {?>
                                <div>
                            <?php }?>

                        <?php }?>

                             <div class="current-item-block">
                                 <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvblock_display_img']->value == 1) {?>
                                     <?php if (strlen($_smarty_tpl->tpl_vars['item']->value['img']) > 0) {?>
                                         <div class="block-side">
                                             <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvpic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['img'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                                  title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"  />

                                         </div>
                                     <?php }?>
                                 <?php }?>

                                 <div class="block-content">
                                     <a class="item-article" title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                        href="<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrew_on']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['seo_url'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"
                                             ><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</a>

                                     <div class="clr"></div>
                                     <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvb_display_date']->value == 1) {?>
                                         <span class="float-left block-item-date"><i class="fa fa-clock-o fa-lg"></i>&nbsp;<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value['time_add'],"%d/%m/%Y"),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
                                     <?php }?>
                                     <span class="float-right comment block-item-like">
                            <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_like']->value == 1) {?>
                            <?php if ($_smarty_tpl->tpl_vars['item']->value['is_liked_news']) {?>
                                <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['count_like'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)
                            <?php } else { ?>
                                <span class="block-item-like-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                <a onclick="spmblocknewsadv_like_news(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['count_like'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)</a>
                                </span>
                            <?php }?>
                            <?php }?>
                            &nbsp;
                            <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_unlike']->value == 1) {?>
                            <?php if ($_smarty_tpl->tpl_vars['item']->value['is_unliked_news']) {?>
                                 <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['count_unlike'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)
                            <?php } else { ?>
                                <span class="block-item-unlike-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                <a onclick="spmblocknewsadv_like_news(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['count_unlike'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)</a>
                                </span>
                            <?php }?>
                            <?php }?>
                        </span>

                                     <div class="clr"></div>
                                 </div>


                                 <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvbnews_slider']->value == 1) {?>

                                     <?php if (((isset($_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['index'] : null)%$_smarty_tpl->tpl_vars['spmblocknewsadvbnews_sl']->value == $_smarty_tpl->tpl_vars['spmblocknewsadvbnews_sl']->value-1) || (isset($_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['last'] : null)) {?>
                                        </div>
                                     <?php }?>

                                 <?php }?>

                             </div>

                         <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                     <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                         <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvbnews_slider']->value == 1 && (count($_smarty_tpl->tpl_vars['spmblocknewsadvitemsblock']->value) > $_smarty_tpl->tpl_vars['spmblocknewsadvbnews_sl']->value)) {?></ul><?php }?>

                     <p class="block-view-all">
                         <a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View all news','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
"
                            class="button <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>button-small-spmblocknewsadv<?php }?>"
                                 ><b><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View all news','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</b></a>
                     </p>

                 </div>
             <?php } else { ?>
                 <div class="block-no-items">
                     <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'News not found.','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>

                 </div>
             <?php }?>

         </div>
     </div>

<?php }?>


<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvarch_left_n']->value == 1) {?>
    <div id="spmblocknewsadvarch_block_left" class="block <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>block-categories hidden-sm-down<?php }?> <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis16']->value == 1 && $_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 0) {?>blockmanufacturer16<?php }?> search_items" >
        <h4 class="title_block <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>text-uppercase<?php }?>"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'News Archives','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</h4>

        <div class="block_content<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>17<?php }?>">
            <?php if (sizeof($_smarty_tpl->tpl_vars['spmblocknewsadvarch']->value) > 0) {?>
                <ul class="bullet">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['spmblocknewsadvarch']->value, 'items', false, 'year', 'myarch', array (
  'index' => true,
  'first' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['year']->value => $_smarty_tpl->tpl_vars['items']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_myarch']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_myarch']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_myarch']->value['index'];
?>
                        <li><a class="arch-category" href="javascript:void(0)"
                               onclick="show_arch(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_foreach_myarch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_myarch']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,'left')"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['year']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</a></li>
                        <div id="arch<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_foreach_myarch']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_myarch']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
left"
                             <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_myarch']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_myarch']->value['first'] : null)) {
} else { ?>class="display-none"<?php }?>
                                >
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['items']->value, 'item', false, NULL, 'myLoop1', array (
  'index' => true,
  'first' => true,
  'last' => true,
  'iteration' => true,
  'total' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['iteration']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['index']++;
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['first'] = !$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['index'];
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['last'] = $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['iteration'] === $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop1']->value['total'];
?>
                                <li class="arch-subcat">
                                    <a class="arch-subitem" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['spmblocknewsadvrew_on']->value == 1) {?>?<?php } else { ?>&<?php }?>y=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['year']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
&m=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['month'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( smarty_modifier_date_format($_smarty_tpl->tpl_vars['item']->value['time_add'],"%B"),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
&nbsp;(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['item']->value['total'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)
                                    </a>
                                </li>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </div>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </ul>
            <?php } else { ?>
                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'There are not Archives yet.','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>

            <?php }?>

        </div>

    </div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvsearch_left_n']->value == 1) {?>
    <div id="spmblocknewsadvsearch_block_left" class="block <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>block-categories hidden-sm-down<?php }?> <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis16']->value == 1 && $_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 0) {?>blockmanufacturer16<?php }?> search_items" >
        <h4 class="title_block <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>text-uppercase<?php }?>"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search News','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</h4>
        <form method="get" action="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
            <div class="block_content<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>17<?php }?>">
                <input type="text" value="" class="search_items <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>search-spmblocknewsadv17<?php }?> <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_ps15']->value == 0) {?>search_text<?php }?>" name="search" >
                <input type="submit" value="go" class="button_mini <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>button-mini-spmblocknewsadv<?php }?> <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_ps15']->value == 0) {?>search_go<?php }?>"/>
                <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_ps15']->value == 0) {?><div class="clear"></div><?php }?>
            </div>
        </form>
    </div>
<?php }
}
}
