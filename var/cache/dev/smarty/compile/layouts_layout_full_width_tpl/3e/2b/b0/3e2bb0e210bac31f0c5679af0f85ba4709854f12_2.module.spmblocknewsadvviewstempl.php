<?php
/* Smarty version 3.1.33, created on 2019-04-18 15:54:12
  from 'module:spmblocknewsadvviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb881846403d6_77680577',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3e2bb0e210bac31f0c5679af0f85ba4709854f12' => 
    array (
      0 => 'module:spmblocknewsadvviewstempl',
      1 => 1546609254,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cb881846403d6_77680577 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
<!-- begin /var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/front/news-item17.tpl -->


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19224400885cb881845d9105_92364245', 'content_wrapper');
?>

<!-- end /var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/front/news-item17.tpl --><?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'layouts/layout-left-column.tpl');
}
/* {block 'content_wrapper'} */
class Block_19224400885cb881845d9105_92364245 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_19224400885cb881845d9105_92364245',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/html/swisspackag.ch/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>


  <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">

    <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis17']->value == 1) {?>
        <nav data-depth="2" class="breadcrumb hidden-sm-down">
            <ol itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                    <a itemprop="item" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                        <span itemprop="name"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'News','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</span>
                    </a>
                    <meta itemprop="position" content="1">
                </li>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['posts']->value, 'post', false, NULL, 'myLoop', array (
  'index' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['index']++;
?>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                    <a itemprop="item" href="<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrew_on']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['seo_url'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>">
                        <span itemprop="name"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
                    </a>
                    <meta itemprop="position" content="2">
                </li>
            </ol>
        </nav>

    <?php }?>

    <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'path', null, null);?>
        <a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'News','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>

        </a>
        <span class="navigation-pipe">></span>
        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

    <?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>




    <?php if (count($_smarty_tpl->tpl_vars['posts']->value) > 0) {?>


        <div class="news-block-item">


        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['posts']->value, 'post', false, NULL, 'myLoop', array (
  'index' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['index']++;
?>
            <div class="item-page" itemscope itemtype="http<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_ssl']->value == 1) {?>s<?php }?>://schema.org/Article">

                <meta itemscope itemprop="mainEntityOfPage"  itemType="https://schema.org/WebPage"
                      itemid="<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrew_on']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['seo_url'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"

                        />

                <meta itemprop="datePublished" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['time_add_rss'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                <meta itemprop="dateModified" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['time_add_rss'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                <meta itemprop="headline" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                <meta itemprop="alternativeHeadline" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>

        <span itemprop="author" itemscope itemtype="https://schema.org/Person">
             <meta itemprop="name" content="admin"/>
        </span>


        <span itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
            <span itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
                <meta itemprop="url" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
img/logo.jpg">
                <meta itemprop="width" content="600">
                <meta itemprop="height" content="60">
            </span>
            <meta itemprop="name" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvsnip_publisher']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
        </span>

                <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
                    <?php if (strlen($_smarty_tpl->tpl_vars['post']->value['img']) > 0) {?>
                        <meta itemprop="url" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvpic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['img'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                        <meta itemprop="width" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvsnip_width']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                        <meta itemprop="height" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvsnip_height']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">

                    <?php } else { ?>
                        <meta itemprop="url" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
img/logo.jpg"/>
                        <meta itemprop="width" content="600">
                        <meta itemprop="height" content="60">
                    <?php }?>
                </div>

                <meta itemprop="description" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( substr(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['post']->value['content']),0,140),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>

               


                <div class="top-item">

                    <h1><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_title']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
  <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvi_display_date']->value == 1) {?>
                        <p class="float-right date-time">
                            <time datetime="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['time_add'],"%d/%m/%Y"),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" pubdate="pubdate"
                                    ><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['time_add'],"%d/%m/%Y"),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</time>
                        </p>
                    <?php }?></h1>
                     <?php if (strlen($_smarty_tpl->tpl_vars['post']->value['img']) > 0) {?>
                     <div class="detail-img">
                        <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvpic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['img'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                             alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="img-responsive" />   </div>
                <?php }?>
                    <div class="body-post">
                    <?php echo $_smarty_tpl->tpl_vars['post']->value['content'];?>

                </div>

                  
                    <div class="clear"></div>
                  

                    <p class="float-right comment">


                        <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_like']->value == 1) {?>
                            <?php if ($_smarty_tpl->tpl_vars['post']->value['is_liked_news']) {?>
                                <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['count_like'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)
                            <?php } else { ?>
                                <span class="block-item-like-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                <a onclick="spmblocknewsadv_like_news(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['count_like'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)</a>
                                </span>
                            <?php }?>
                        <?php }?>
                        &nbsp;
                        <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_unlike']->value == 1) {?>
                            <?php if ($_smarty_tpl->tpl_vars['post']->value['is_unliked_news']) {?>
                                <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['count_unlike'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)
                            <?php } else { ?>
                                <span class="block-item-unlike-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                <a onclick="spmblocknewsadv_like_news(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['count_unlike'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)</a>
                                </span>
                            <?php }?>
                        <?php }?>




                    </p>
                    <div class="clear"></div>



                </div>





              




                <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_soc_but']->value == 1) {?>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <div class="fb-like" data-href="http<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_ssl']->value == 1) {?>s<?php }?>://<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['HTTP_HOST'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['REQUEST_URI'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                     data-send="false" data-layout="button_count" data-width="50" data-show-faces="true"></div>
                            </td>
                            <td>
                                <a href="http<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_ssl']->value == 1) {?>s<?php }?>://twitter.com/share" class="twitter-share-button"
                                   data-url="http<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_ssl']->value == 1) {?>s<?php }?>://<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['HTTP_HOST'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['REQUEST_URI'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                   data-counturl="http<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_ssl']->value == 1) {?>s<?php }?>://<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['HTTP_HOST'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['REQUEST_URI'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                   data-via="#"
                                   data-count="horizontal">Tweet</a>
                            </td>
                            <td>
                                <!-- Place this render call where appropriate -->
                                
                                    <?php echo '<script'; ?>
 type="text/javascript">
                                        (function() {
                                            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                            po.src = 'https://apis.google.com/js/plusone.js';
                                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                                        })();
                                    <?php echo '</script'; ?>
>
                                

                                <div class="g-plusone" data-size="medium" href="http<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_ssl']->value == 1) {?>s<?php }?>://$smarty.server.HTTP_HOST|escape:'htmlall':'UTF-8'}<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['REQUEST_URI'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" data-count="true"></div>

                            </td>
                            <td>
                                
                                <?php echo '<script'; ?>
 type="IN/Share" data-url="http{if $spmblocknewsadvis_ssl == 1}s{/if}://<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['HTTP_HOST'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['REQUEST_URI'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                        data-counter="right"><?php echo '</script'; ?>
>
                                
                            </td>
                            <td>
                                <a href="http<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_ssl']->value == 1) {?>s<?php }?>://pinterest.com/pin/create/button/?url=http<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_ssl']->value == 1) {?>s<?php }?>://<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['HTTP_HOST'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['REQUEST_URI'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
&media=<?php if (strlen($_smarty_tpl->tpl_vars['post']->value['img']) > 0) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
upload/spmblocknewsadv/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['img'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
img/logo.jpg<?php }?>&description=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                   class="pin-it-button" count-layout="horizontal">
                                    <img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>
                            </td>
                        </tr>
                    </table>

                <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_comments']->value == 0) {?>
                <?php if ($_smarty_tpl->tpl_vars['post']->value['is_comments'] == 1) {?>
                    <div id="fb-root"></div>
                

                    <?php echo '<script'; ?>
>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvlng_iso']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
/all.js#xfbml=1&appid=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvappid']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));<?php echo '</script'; ?>
>
                
                <?php }?>
                <?php }?>

                

                    <?php echo '<script'; ?>
 type="text/javascript" async src="https://platform.twitter.com/widgets.js"><?php echo '</script'; ?>
>
                    <?php echo '<script'; ?>
 src="//platform.linkedin.com/in.js" async type="text/javascript"><?php echo '</script'; ?>
>
                    <?php echo '<script'; ?>
 type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"><?php echo '</script'; ?>
>
                

                <?php }?>


            </div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>




        <?php if (count($_smarty_tpl->tpl_vars['related_products']->value) > 0) {?>
            <div class="rel-products-block <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrelpr_slider']->value == 1) {?>owl_news_related_products_type_carousel<?php }?>">
                <h4 class="related-products-title"><i class="fa fa-book fa-lg"></i>&nbsp;<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Related Products','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</h4>


                <ul <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrelpr_slider']->value == 1) {?>class="owl-carousel owl-theme"<?php }?>>
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['related_products']->value, 'product', false, NULL, 'myLoop', array (
  'index' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['index']++;
?>
                        <li class="clearfix <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrelpr_slider']->value == 0) {?>no-slider<?php }?>">
                            <?php if ($_smarty_tpl->tpl_vars['product']->value['picture']) {?>
                                <a title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['product_url'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="products-block-image">
                                    <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['picture'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                         <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrelpr_slider']->value == 1) {?>class="img-responsive"<?php }?>/>
                                </a>
                            <?php }?>
                            <div class="clear"></div>
                            <div class="product-content">
                                <h5>
                                    <a title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['product_url'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                       class="product-name">
                                        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                                    </a>
                                </h5>
                                <p class="product-description"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( substr(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['product']->value['description']),0,$_smarty_tpl->tpl_vars['spmblocknewsadvitem_rp_tr']->value),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
if (strlen($_smarty_tpl->tpl_vars['product']->value['description']) > $_smarty_tpl->tpl_vars['spmblocknewsadvitem_rp_tr']->value) {?>...<?php }?></p>
                            </div>
                        </li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                </ul>

                <div class="clear"></div>
            </div>
        <?php }?>





        <?php if (count($_smarty_tpl->tpl_vars['related_posts']->value) > 0) {?>
            <div class="rel-posts-block <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrelp_slider']->value == 1) {?>owl_news_related_news_type_carousel<?php }?>">

                <h4 class="related-posts-title"><i class="fa fa-newspaper-o fa-lg"></i>&nbsp;<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Related News','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</h4>

                <div class="other-posts">


                    <ul class="<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrelp_slider']->value == 1) {?>owl-carousel owl-theme<?php } else { ?>row-custom<?php }?>">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['related_posts']->value, 'relpost', false, NULL, 'myLoop', array (
  'index' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['relpost']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['index']++;
?>
                            <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['index'] : null)%3 == 0) {?><div class="clear"></div><?php }?>
                            <li <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrelp_slider']->value == 0) {?>class="col-sm-4-custom"<?php }?>>
                                <?php if (strlen($_smarty_tpl->tpl_vars['relpost']->value['img']) > 0) {?>
                                    <div class="block-top">
                                        <a title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                           href="<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrew_on']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['url'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"
                                                >
                                            <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrelp_slider']->value == 1) {?>class="img-responsive"<?php }?>
                                                 src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvpic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['img'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                        </a>
                                    </div>
                                <?php }?>


                                <div class="block-content"><h4 class="block-heading">
                                        <a title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                           href="<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrew_on']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['url'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"

                                                ><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</a></h4></div>
                                <div class="block-footer">
                                    <p class="float-left">
                                        <time pubdate="pubdate" datetime="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( smarty_modifier_date_format($_smarty_tpl->tpl_vars['relpost']->value['time_add'],"%d/%m/%Y"),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                                ><i class="fa fa-clock-o fa-lg"></i>&nbsp;&nbsp;<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( smarty_modifier_date_format($_smarty_tpl->tpl_vars['relpost']->value['time_add'],"%d/%m/%Y"),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                                        </time>
                                    </p>
                                    <p class="float-right comment">


                                        <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_like']->value == 1) {?>
                                            <?php if ($_smarty_tpl->tpl_vars['relpost']->value['is_liked_news']) {?>
                                                <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['count_like'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)
                                            <?php } else { ?>
                                                <span class="block-item-like-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                <a onclick="spmblocknewsadv_like_news(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['count_like'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)</a>
                                </span>
                                            <?php }?>
                                        <?php }?>
                                        &nbsp;
                                        <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_unlike']->value == 1) {?>
                                            <?php if ($_smarty_tpl->tpl_vars['relpost']->value['is_unliked_news']) {?>
                                                <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['count_unlike'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)
                                            <?php } else { ?>
                                                <span class="block-item-unlike-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                <a onclick="spmblocknewsadv_like_news(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['relpost']->value['count_unlike'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)</a>
                                </span>
                                            <?php }?>
                                        <?php }?>






                                    </p>

                                    <div class="clear"></div>
                                </div>
                            </li>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                    </ul>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>


            </div>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_comments']->value == 1) {?>
            <?php if ($_smarty_tpl->tpl_vars['post']->value['is_comments'] == 0) {?>
                <div class="blocnewsadv-noitems">
                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Comments are сlosed for this post','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>

                </div>
            <?php } else { ?>
                <div class="fcomment-title"><i class="fa fa-facebook fa-lg"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Facebook comments','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
</div>

                <div class="fcomment-content">
                    <div id="fb-root"></div>
                    

                    <?php echo '<script'; ?>
 type="text/javascript">
                        (function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvlng_iso']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
/all.js#xfbml=1&appid=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvappid']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));<?php echo '</script'; ?>
>
                        <style>.fb_iframe_widget_fluid_desktop, .fb_iframe_widget_fluid_desktop span, .fb_iframe_widget_fluid_desktop iframe{width:100%!important}</style>
                    

                    <div class="fb-comments" data-href="<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrew_on']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['seo_url'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"
                         data-numposts="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnumber_fc']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" data-width="100%" data-mobile="false"></div>


                </div>

            <?php }?>
        <?php }?>


    <?php } else { ?>
        <div class="blocnewsadv-noitems">
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'There are not news yet','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>

        </div>
    <?php }?>

    </div>





    <?php echo '<script'; ?>
 type="text/javascript">

        <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrelpr_slider']->value == 1) {?>
        var spmblocknewsadv_number_product_related_slider = <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnpr_slider']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrelp_slider']->value == 1) {?>
        var spmblocknewsadv_number_news_slider = <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnp_slider']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;
        <?php }?>

    <?php echo '</script'; ?>
>



    </div>
<?php
}
}
/* {/block 'content_wrapper'} */
}
