<?php
/* Smarty version 3.1.33, created on 2019-05-01 03:20:40
  from '/var/www/html/swisspackag.ch/themes/classic/templates/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc8f46852ede0_09477898',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0804acace2caf25c717b53999a147254e932ce71' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/index.tpl',
      1 => 1550815112,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc8f46852ede0_09477898 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_819288365cc8f46852c932_09090960', 'page_content_container');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_content_top'} */
class Block_18092890605cc8f46852cfd9_87519706 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'hook_home'} */
class Block_13686990335cc8f46852dc88_23558564 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

            <?php echo $_smarty_tpl->tpl_vars['HOOK_HOME']->value;?>

          <?php
}
}
/* {/block 'hook_home'} */
/* {block 'page_content'} */
class Block_15250831225cc8f46852d814_32138457 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13686990335cc8f46852dc88_23558564', 'hook_home', $this->tplIndex);
?>

        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_819288365cc8f46852c932_09090960 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content_container' => 
  array (
    0 => 'Block_819288365cc8f46852c932_09090960',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_18092890605cc8f46852cfd9_87519706',
  ),
  'page_content' => 
  array (
    0 => 'Block_15250831225cc8f46852d814_32138457',
  ),
  'hook_home' => 
  array (
    0 => 'Block_13686990335cc8f46852dc88_23558564',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-home">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18092890605cc8f46852cfd9_87519706', 'page_content_top', $this->tplIndex);
?>


        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15250831225cc8f46852d814_32138457', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
}
