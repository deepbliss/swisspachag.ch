<?php
/* Smarty version 3.1.33, created on 2019-04-24 13:47:56
  from '/var/www/html/swisspackag.ch/themes/classic/templates/catalog/_partials/product-discounts.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc04cece4f0f1_81725318',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '018010325c21d43813278509b56170b1e2aae0a9' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/catalog/_partials/product-discounts.tpl',
      1 => 1553685353,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc04cece4f0f1_81725318 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<section class="product-discounts" >
  <?php if ($_smarty_tpl->tpl_vars['product']->value['quantity_discounts']) {?>    
    <h3 class="product-detail-desc"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Staffelpreise pro','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</h3>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1002563555cc04cece48cc1_01639783', 'product_discount_table');
?>

  <?php }?>
</section>
<?php }
/* {block 'product_discount_table'} */
class Block_1002563555cc04cece48cc1_01639783 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_discount_table' => 
  array (
    0 => 'Block_1002563555cc04cece48cc1_01639783',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <table class="table-product table-product-discounts">
        <thead>
        <tr>                                                  
        </tr>
        </thead>
        <tbody id="combinations">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['product']->value['quantity_discounts'], 'quantity_discount', false, NULL, 'quantity_discounts', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['quantity_discount']->value) {
?>
          <tr data-discount-type="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quantity_discount']->value['reduction_type'], ENT_QUOTES, 'UTF-8');?>
" data-discount="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quantity_discount']->value['real_value'], ENT_QUOTES, 'UTF-8');?>
" data-discount-quantity="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quantity_discount']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
">
            <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quantity_discount']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
</td>
            <td> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['sign'], ENT_QUOTES, 'UTF-8');?>
</td>
            <?php if (intval($_smarty_tpl->tpl_vars['product']->value['unity']) == '0') {?>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quantity_discount']->value['price'], ENT_QUOTES, 'UTF-8');?>
</td>
            <?php } else { ?>
                          <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quantity_discount']->value['price']*intval($_smarty_tpl->tpl_vars['product']->value['unity']), ENT_QUOTES, 'UTF-8');?>
</td>
            <?php }?>  
            <td>
                
            </td>
          </tr> 
          
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </tbody>
      </table>
    <?php
}
}
/* {/block 'product_discount_table'} */
}
