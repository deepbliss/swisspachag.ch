<?php
/* Smarty version 3.1.33, created on 2019-04-18 13:45:02
  from 'module:spmblocknewsadvviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb8633e0ae153_27591954',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8ba925a9eb7beeb96081569132ff73f441be641e' => 
    array (
      0 => 'module:spmblocknewsadvviewstempl',
      1 => 1546607060,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'file:modules/spmblocknewsadv/views/templates/front/news.tpl' => 1,
  ),
),false)) {
function content_5cb8633e0ae153_27591954 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
<!-- begin /var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/front/news17.tpl -->
 

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15524645995cb8633e0ac541_08408911', 'content_wrapper');
?>

<!-- end /var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/front/news17.tpl --><?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'layouts/layout-left-column.tpl');
}
/* {block 'content_wrapper'} */
class Block_15524645995cb8633e0ac541_08408911 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content_wrapper' => 
  array (
    0 => 'Block_15524645995cb8633e0ac541_08408911',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">

    <?php $_smarty_tpl->_subTemplateRender("file:modules/spmblocknewsadv/views/templates/front/news.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</div>
<?php
}
}
/* {/block 'content_wrapper'} */
}
