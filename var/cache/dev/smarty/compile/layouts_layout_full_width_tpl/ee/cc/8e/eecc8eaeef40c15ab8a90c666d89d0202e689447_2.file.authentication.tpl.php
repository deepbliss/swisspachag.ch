<?php
/* Smarty version 3.1.33, created on 2019-04-18 14:18:23
  from '/var/www/html/swisspackag.ch/themes/classic/templates/customer/authentication.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb86b0fa0bb49_37274363',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'eecc8eaeef40c15ab8a90c666d89d0202e689447' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/customer/authentication.tpl',
      1 => 1546937399,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cb86b0fa0bb49_37274363 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>




      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15966089635cb86b0fa04b70_78710346', 'page_title');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3079362625cb86b0fa05fd4_50866174', 'page_content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'page.tpl');
}
/* {block 'page_title'} */
class Block_15966089635cb86b0fa04b70_78710346 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_title' => 
  array (
    0 => 'Block_15966089635cb86b0fa04b70_78710346',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
 
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Registrieren','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

        <?php
}
}
/* {/block 'page_title'} */
/* {block 'form_buttons'} */
class Block_10270394455cb86b0fa07ff6_98351650 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

                                  <button id="submit-register" class="btn btn-primary"  data-link-action="display-register-form" type="submit" class="form-control-submit">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Erstellen Sie ein Konto','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

                                  </button>
                                 <?php
}
}
/* {/block 'form_buttons'} */
/* {block 'display_after_login_form'} */
class Block_8991667455cb86b0fa09cf3_84677520 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayCustomerLoginFormAfter'),$_smarty_tpl ) );?>

      <?php
}
}
/* {/block 'display_after_login_form'} */
/* {block 'login_form_container'} */
class Block_9939265895cb86b0fa08c41_24712338 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <h3 style="margin-bottom:0px;"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'BEREITS REGISTRIERT?','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>
</h3>
   <div class="login-user">
      <section class="login-form">
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['render'][0], array( array('file'=>'customer/_partials/login-form.tpl','ui'=>$_smarty_tpl->tpl_vars['login_form']->value),$_smarty_tpl ) );?>

      </section>
   
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8991667455cb86b0fa09cf3_84677520', 'display_after_login_form', $this->tplIndex);
?>

      <div class="no-account">
        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['register'], ENT_QUOTES, 'UTF-8');?>
" data-link-action="display-register-form">
          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No account? Create one here','d'=>'Shop.Theme.Customeraccount'),$_smarty_tpl ) );?>

        </a>
      </div>
      </div>
    <?php
}
}
/* {/block 'login_form_container'} */
/* {block 'page_content'} */
class Block_3079362625cb86b0fa05fd4_50866174 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'page_content' => 
  array (
    0 => 'Block_3079362625cb86b0fa05fd4_50866174',
  ),
  'form_buttons' => 
  array (
    0 => 'Block_10270394455cb86b0fa07ff6_98351650',
  ),
  'login_form_container' => 
  array (
    0 => 'Block_9939265895cb86b0fa08c41_24712338',
  ),
  'display_after_login_form' => 
  array (
    0 => 'Block_8991667455cb86b0fa09cf3_84677520',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <div class="login-info">
            <form action="#" method="post" id="create_account_form" class="std">
                    <fieldset>
                        <h3 style="margin-bottom:0px;"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'ERSTELLEN SIE EIN KONTO'),$_smarty_tpl ) );?>
</h3>
                        <div class="form_content clearfix login-user">
                            <p class="title_block"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Um ein Konto zu erstellen, geben Sie bitte Ihre E-Mail-Adresse ein.','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
.</p>
                            <div class="error" id="create_account_error" style="display:none"></div>
                            <div class="form-group row ">
                                <label class="form-control-label required">
                                          E-Mail
                                      </label>
                                <div class="form-field">
                                      <input class="form-control" name="email" type="email" id="email_regis" value="" required="">
                                </div>
                            </div>
                            <p class="submit">
                                <input type="hidden" name="datalink" class="datalink" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['register'], ENT_QUOTES, 'UTF-8');?>
&email=">
                                
                                 <input type="hidden" name="submitregister" value="1">
                                <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10270394455cb86b0fa07ff6_98351650', 'form_buttons', $this->tplIndex);
?>

                            </p>
                        </div>
                    </fieldset>
                </form>
    </div>

    <div class="login-info">        
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9939265895cb86b0fa08c41_24712338', 'login_form_container', $this->tplIndex);
?>

    </div>
<?php
}
}
/* {/block 'page_content'} */
}
