<?php
/* Smarty version 3.1.33, created on 2019-04-18 13:45:02
  from '/var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/front/news.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb8633e2486a7_23091369',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ea1758751cc729f88f1f795de67aafee201c083b' => 
    array (
      0 => '/var/www/html/swisspackag.ch/modules/spmblocknewsadv/views/templates/front/news.tpl',
      1 => 1545715155,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cb8633e2486a7_23091369 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/var/www/html/swisspackag.ch/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>

<?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'path', null, null);
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

<?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>

<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis16']->value == 0) {?>

    <?php $_smarty_tpl->smarty->ext->_capture->open($_smarty_tpl, 'path', null, null);?>
    <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

    <?php $_smarty_tpl->smarty->ext->_capture->close($_smarty_tpl);?>



<?php } else { ?>

    <h1 class="page-heading"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</h1>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_search']->value == 1) {?>
    <div class="clear"></div>
    <h3 class="float-right"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Results for','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
 "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvsearch']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"</h3>
    <div class="clear"></div>
<?php }?>


<?php if ($_smarty_tpl->tpl_vars['count_all']->value > 0) {?>

<p class="blocnewsadv-header"><span class="bold"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'News','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>
  ( <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['count_all']->value,"htmlall","UTF-8" )), ENT_QUOTES, 'UTF-8');?>
 )</span></p>


<ul id="manufacturers_list" class="spmblocknewsadv-list">
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['posts']->value, 'post', false, NULL, 'myLoop', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['post']->value) {
?>
	<li class="clearfix"> 
				<div class="left_side">
			     <h2>
                    <a href="<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrew_on']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['seo_url'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"

                       title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                              >
                        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                    </a>
                     <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvl_display_date']->value == 1) {?>
                <span class="float-right block-item-date date-time"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( smarty_modifier_date_format($_smarty_tpl->tpl_vars['post']->value['time_add'],"%d/%m/%Y"),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
            <?php }?>
                </h2>
                
			<div class="logo">
				<?php if (strlen($_smarty_tpl->tpl_vars['post']->value['img']) > 0) {?>
					
						<a class="lnk_img"
                           href="<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrew_on']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['seo_url'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"

                           title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">

							<img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvpic']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['img'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
							     title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" 
							      />
						</a>
					
				<?php }?>
			</div>
					
			
			<p class="description rte">
				<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( substr($_smarty_tpl->tpl_vars['post']->value['content'],0,50000),"htmlall","UTF-8" )), ENT_QUOTES, 'UTF-8');?>

				<?php if (strlen($_smarty_tpl->tpl_vars['post']->value['content']) > 50000) {?>...<?php }?>
				<a href="<?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvrew_on']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['seo_url'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmblocknewsadvnews_item_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"
					   title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['title'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="item-more">
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'more','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>

				</a>
				<br/>
			</p>
				
			</div>
			<div class="spmblocknewsadv-clear"></div>


            <div class="clr"></div>
           
            <span class="float-right comment block-item-like">

                             <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_like']->value == 1) {?>
                                 <?php if ($_smarty_tpl->tpl_vars['post']->value['is_liked_news']) {?>
                                     <i class="fa fa-thumbs-up fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['count_like'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)
                            <?php } else { ?>
                                <span class="block-item-like-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                <a onclick="spmblocknewsadv_like_news(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,1)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-up fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['count_like'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)</a>
                                </span>
                                 <?php }?>
                             <?php }?>
            &nbsp;
            <?php if ($_smarty_tpl->tpl_vars['spmblocknewsadvis_unlike']->value == 1) {?>
                <?php if ($_smarty_tpl->tpl_vars['post']->value['is_unliked_news']) {?>
                    <i class="fa fa-thumbs-down fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['count_unlike'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)
                            <?php } else { ?>
                                <span class="block-item-unlike-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                <a onclick="spmblocknewsadv_like_news(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,0)"
                                   href="javascript:void(0)"><i class="fa fa-thumbs-o-down fa-lg"></i>&nbsp;(<span class="the-number"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['post']->value['count_unlike'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>)</a>
                                </span>
                <?php }?>
            <?php }?>

            </span>

		</li>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
</ul>

			
		
<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['paging']->value,'quotes','UTF-8' ));?>

	

<?php } else { ?>
	<div class="blocnewsadv-noitems">
	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'There are not news yet','mod'=>'spmblocknewsadv'),$_smarty_tpl ) );?>

	</div>
<?php }?>



<?php }
}
