<?php
/* Smarty version 3.1.33, created on 2019-04-18 15:55:19
  from '/var/www/html/swisspackag.ch/themes/classic/templates/checkout/_partials/footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb881c729e7e4_60969947',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7b0a8402b87b53eada206ecf5bbe4666970130f9' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/checkout/_partials/footer.tpl',
      1 => 1549347073,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cb881c729e7e4_60969947 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10113710265cb881c729e0f8_67073622', 'footer');
?>

<?php }
/* {block 'footer'} */
class Block_10113710265cb881c729e0f8_67073622 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'footer' => 
  array (
    0 => 'Block_10113710265cb881c729e0f8_67073622',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="text-sm-center">
    <span>Swisspack AG | Industriestrasse 53 | CH-9443 Widnau | Tel. +41 71 722 85 85 | Fax +41 71 722 85 84</span>
</div>
<?php
}
}
/* {/block 'footer'} */
}
