<?php
/* Smarty version 3.1.33, created on 2019-05-01 03:20:40
  from '/var/www/html/swisspackag.ch/modules/staticblock/views/templates/hook/embedded.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc8f4684cc4a5_93843237',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '23e4fc361fe887912d83920371db0dc462d46bd4' => 
    array (
      0 => '/var/www/html/swisspackag.ch/modules/staticblock/views/templates/hook/embedded.tpl',
      1 => 1545400041,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc8f4684cc4a5_93843237 (Smarty_Internal_Template $_smarty_tpl) {
if (isset($_smarty_tpl->tpl_vars['style_sheet']->value) && !empty($_smarty_tpl->tpl_vars['style_sheet']->value)) {?>
<style type = "text/css">{$style_sheet|escape:'htmlall':'UTF-8'}</style><?php }
if ($_smarty_tpl->tpl_vars['ps_17']->value > 0) {
echo '<script'; ?>
 type="text/javascript" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['jQuery_path']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
><?php }
echo '<script'; ?>
 type="text/javascript">
$(document).ready(function() {
	$('.static_block_content').each(function() {
		var ids = $(this).attr('id').split('_');
		var id_static_block = ids[1];
		if (typeof static_blocks !== 'undefined' && static_blocks.length) {
			for (var i = 0; i < static_blocks.length; i++) {
				if (id_static_block == parseInt(static_blocks[i].id_static_block)) {
					if (parseInt(static_blocks[i].id_static_block_template) && static_blocks[i].template) {
						$(this).html(static_blocks[i].template);
					} else {
						$(this).html(static_blocks[i].content);
					}
				}
			}
		}
	});
});
<?php echo '</script'; ?>
>
<?php }
}
