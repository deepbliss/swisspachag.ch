<?php
/* Smarty version 3.1.33, created on 2019-04-24 13:48:01
  from 'module:pscategorytreeviewstempla' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc04cf1777b30_98060156',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8921007f54626fc7fe42cbff53f1d70828d3393d' => 
    array (
      0 => 'module:pscategorytreeviewstempla',
      1 => 1553687239,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cc04cf1777b30_98060156 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'categories' => 
  array (
    'compiled_filepath' => '/var/www/html/swisspackag.ch/var/cache/dev/smarty/compile/89/21/00/8921007f54626fc7fe42cbff53f1d70828d3393d_2.module.pscategorytreeviewstempla.php',
    'uid' => '8921007f54626fc7fe42cbff53f1d70828d3393d',
    'call_name' => 'smarty_template_function_categories_10721713015cc04cf172beb3_04494830',
  ),
));
?><!-- begin /var/www/html/swisspackag.ch/themes/classic/modules/ps_categorytree/views/templates/hook/ps_categorytree.tpl -->


<div class="block-categories hidden-sm-down">
  <ul class="category-top-menu">
    <li><a class="text-uppercase h6" href="<?php echo $_smarty_tpl->tpl_vars['categories']->value['link'];?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['categories']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a></li>
    <li><?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'categories', array('nodes'=>$_smarty_tpl->tpl_vars['categories']->value['children']), true);?>
</li>
  </ul>
  
              <?php echo '<script'; ?>
 type="text/javascript">
        // <![CDATA[
            // we hide the tree only if JavaScript is activated
           

            /* TODO: change this shitty code! */
            $('div.category-sub-menu a.selected').each(function()
            {
                if ($(this).parent().parent().is(":not(ul.Level1)")) {
                    $(this).closest('ul.Level3').parent("li").find("a:first").addClass('selectedParentLevel2');
                    $(this).closest('ul.Level2').parent("li").find("a:first").addClass('selectedParentLevel1');
                }
            });

        $('div.category-sub-menu a:not(.selected)').click(function(){
            if ($(this).parent().attr("id") != "id") {
                $('.category-sub-menu').find('a.clicked').removeClass('clicked');
                //$(this).addClass("clicked");
                $('#center_column').addClass("contentLoading");
            }
        });


        // ]]>
        <?php echo '</script'; ?>
>
</div>
<!-- end /var/www/html/swisspackag.ch/themes/classic/modules/ps_categorytree/views/templates/hook/ps_categorytree.tpl --><?php }
/* smarty_template_function_categories_10721713015cc04cf172beb3_04494830 */
if (!function_exists('smarty_template_function_categories_10721713015cc04cf172beb3_04494830')) {
function smarty_template_function_categories_10721713015cc04cf172beb3_04494830(Smarty_Internal_Template $_smarty_tpl,$params) {
$params = array_merge(array('nodes'=>array(),'depth'=>0), $params);
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

  <?php if (count($_smarty_tpl->tpl_vars['nodes']->value)) {?><ul class="category-sub-menu"><?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['nodes']->value, 'node');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['node']->value) {
if ($_smarty_tpl->tpl_vars['node']->value['id'] == 7) {
$_tmp_array = isset($_smarty_tpl->tpl_vars['node']) ? $_smarty_tpl->tpl_vars['node']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['name'] = "Aus Vollkarton";
$_smarty_tpl->_assignInScope('node', $_tmp_array);
} elseif ($_smarty_tpl->tpl_vars['node']->value['id'] == 67) {
$_tmp_array = isset($_smarty_tpl->tpl_vars['node']) ? $_smarty_tpl->tpl_vars['node']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['name'] = "Eckkantenschutz wasserfest";
$_smarty_tpl->_assignInScope('node', $_tmp_array);
} elseif ($_smarty_tpl->tpl_vars['node']->value['id'] == 38) {
$_tmp_array = isset($_smarty_tpl->tpl_vars['node']) ? $_smarty_tpl->tpl_vars['node']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['name'] = "PE-Abdeckfolie Oeko Recycling";
$_smarty_tpl->_assignInScope('node', $_tmp_array);
} elseif ($_smarty_tpl->tpl_vars['node']->value['id'] == 53) {
$_tmp_array = isset($_smarty_tpl->tpl_vars['node']) ? $_smarty_tpl->tpl_vars['node']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['name'] = "Klebeband PVC, braun/transparent";
$_smarty_tpl->_assignInScope('node', $_tmp_array);
} elseif ($_smarty_tpl->tpl_vars['node']->value['id'] == 82) {
$_tmp_array = isset($_smarty_tpl->tpl_vars['node']) ? $_smarty_tpl->tpl_vars['node']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['name'] = "Klebeband PP, braun/transparent";
$_smarty_tpl->_assignInScope('node', $_tmp_array);
} elseif ($_smarty_tpl->tpl_vars['node']->value['id'] == 164) {
$_tmp_array = isset($_smarty_tpl->tpl_vars['node']) ? $_smarty_tpl->tpl_vars['node']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['name'] = "Klebebänder bedruckt";
$_smarty_tpl->_assignInScope('node', $_tmp_array);
} elseif ($_smarty_tpl->tpl_vars['node']->value['id'] == 55) {
$_tmp_array = isset($_smarty_tpl->tpl_vars['node']) ? $_smarty_tpl->tpl_vars['node']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['name'] = "Spezialklebeband blau";
$_smarty_tpl->_assignInScope('node', $_tmp_array);
} elseif ($_smarty_tpl->tpl_vars['node']->value['id'] == 193) {
$_tmp_array = isset($_smarty_tpl->tpl_vars['node']) ? $_smarty_tpl->tpl_vars['node']->value : array();
if (!is_array($_tmp_array) || $_tmp_array instanceof ArrayAccess) {
settype($_tmp_array, 'array');
}
$_tmp_array['name'] = "Kantenschutzwinkel Vollkarton";
$_smarty_tpl->_assignInScope('node', $_tmp_array);
}?><li data-depth="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['depth']->value, ENT_QUOTES, 'UTF-8');?>
" class="<?php if ((isset($_smarty_tpl->tpl_vars['category']->value) && is_array($_smarty_tpl->tpl_vars['category']->value) && isset($_smarty_tpl->tpl_vars['category']->value['id']) && $_smarty_tpl->tpl_vars['category']->value['id'] == $_smarty_tpl->tpl_vars['node']->value['id']) || (isset($_smarty_tpl->tpl_vars['id_category_current']->value) && $_smarty_tpl->tpl_vars['id_category_current']->value == $_smarty_tpl->tpl_vars['node']->value['id'])) {?> current_cate <?php }?>"><?php if ($_smarty_tpl->tpl_vars['depth']->value === 0) {
if ((isset($_smarty_tpl->tpl_vars['category']->value) && is_array($_smarty_tpl->tpl_vars['category']->value) && isset($_smarty_tpl->tpl_vars['category']->value['id']) && $_smarty_tpl->tpl_vars['category']->value['id'] == $_smarty_tpl->tpl_vars['node']->value['id']) || (isset($_smarty_tpl->tpl_vars['id_category_current']->value) && $_smarty_tpl->tpl_vars['id_category_current']->value == $_smarty_tpl->tpl_vars['node']->value['id'])) {?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['link'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a><?php if ($_smarty_tpl->tpl_vars['node']->value['children']) {?><div class="navbar-toggler collapse-icons" data-toggle="collapse" data-target="#exCollapsingNavbar<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['id'], ENT_QUOTES, 'UTF-8');?>
" aria-expanded="true"><i class="material-icons add">&#xE145;</i><i class="material-icons remove">&#xE15B;</i></div><div class="collapse in" id="exCollapsingNavbar<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'categories', array('nodes'=>$_smarty_tpl->tpl_vars['node']->value['children'],'depth'=>$_smarty_tpl->tpl_vars['depth']->value+1), true);?>
</div><?php }
} else { ?><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['link'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a><?php if ($_smarty_tpl->tpl_vars['node']->value['children']) {?><div class="navbar-toggler collapse-icons<?php if ($_smarty_tpl->tpl_vars['c_tree_path']->value && !in_array($_smarty_tpl->tpl_vars['node']->value['id'],$_smarty_tpl->tpl_vars['c_tree_path']->value)) {?> collapsed<?php }?>" data-toggle="collapse" data-target="#exCollapsingNavbar<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['id'], ENT_QUOTES, 'UTF-8');?>
" aria-expanded="<?php if ($_smarty_tpl->tpl_vars['c_tree_path']->value && in_array($_smarty_tpl->tpl_vars['node']->value['id'],$_smarty_tpl->tpl_vars['c_tree_path']->value)) {?>true<?php } else { ?>false<?php }?>"><i class="material-icons add">&#xE145;</i><i class="material-icons remove">&#xE15B;</i></div><div class="collapse <?php if ($_smarty_tpl->tpl_vars['c_tree_path']->value && in_array($_smarty_tpl->tpl_vars['node']->value['id'],$_smarty_tpl->tpl_vars['c_tree_path']->value)) {?> in<?php }?>" id="exCollapsingNavbar<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'categories', array('nodes'=>$_smarty_tpl->tpl_vars['node']->value['children'],'depth'=>$_smarty_tpl->tpl_vars['depth']->value+1), true);?>
</div><?php }
}
} else {
if ((isset($_smarty_tpl->tpl_vars['category']->value) && is_array($_smarty_tpl->tpl_vars['category']->value) && isset($_smarty_tpl->tpl_vars['category']->value['id']) && $_smarty_tpl->tpl_vars['category']->value['id'] == $_smarty_tpl->tpl_vars['node']->value['id']) || (isset($_smarty_tpl->tpl_vars['id_category_current']->value) && $_smarty_tpl->tpl_vars['id_category_current']->value == $_smarty_tpl->tpl_vars['node']->value['id'])) {?><a class="category-sub-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['link'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a><?php if ($_smarty_tpl->tpl_vars['node']->value['children']) {?><span class="arrows" data-toggle="collapse" data-target="#exCollapsingNavbar<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['id'], ENT_QUOTES, 'UTF-8');?>
" aria-expanded="true"><i class="material-icons arrow-right">&#xE315;</i><i class="material-icons arrow-down">&#xE313;</i></span><div class="collapse in" id="exCollapsingNavbar<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['id'], ENT_QUOTES, 'UTF-8');?>
" id="exCollapsingNavbar<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'categories', array('nodes'=>$_smarty_tpl->tpl_vars['node']->value['children'],'depth'=>$_smarty_tpl->tpl_vars['depth']->value+1), true);?>
</div><?php }
} else { ?><a class="category-sub-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['link'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['name'], ENT_QUOTES, 'UTF-8');?>
</a><?php if ($_smarty_tpl->tpl_vars['node']->value['children']) {?><span class="arrows<?php if ($_smarty_tpl->tpl_vars['c_tree_path']->value && !in_array($_smarty_tpl->tpl_vars['node']->value['id'],$_smarty_tpl->tpl_vars['c_tree_path']->value)) {?> collapsed<?php }?>" data-toggle="collapse" data-target="#exCollapsingNavbar<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['id'], ENT_QUOTES, 'UTF-8');?>
" aria-expanded="<?php if ($_smarty_tpl->tpl_vars['c_tree_path']->value && in_array($_smarty_tpl->tpl_vars['node']->value['id'],$_smarty_tpl->tpl_vars['c_tree_path']->value)) {?>true<?php } else { ?>false<?php }?>"><i class="material-icons arrow-right">&#xE315;</i><i class="material-icons arrow-down">&#xE313;</i></span><div class="collapse <?php if ($_smarty_tpl->tpl_vars['c_tree_path']->value && in_array($_smarty_tpl->tpl_vars['node']->value['id'],$_smarty_tpl->tpl_vars['c_tree_path']->value)) {?> in<?php }?>" id="exCollapsingNavbar<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['id'], ENT_QUOTES, 'UTF-8');?>
" id="exCollapsingNavbar<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['node']->value['id'], ENT_QUOTES, 'UTF-8');?>
"><?php $_smarty_tpl->smarty->ext->_tplFunction->callTemplateFunction($_smarty_tpl, 'categories', array('nodes'=>$_smarty_tpl->tpl_vars['node']->value['children'],'depth'=>$_smarty_tpl->tpl_vars['depth']->value+1), true);?>
</div><?php }
}
}?></li><?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?></ul><?php }
}}
/*/ smarty_template_function_categories_10721713015cc04cf172beb3_04494830 */
}
