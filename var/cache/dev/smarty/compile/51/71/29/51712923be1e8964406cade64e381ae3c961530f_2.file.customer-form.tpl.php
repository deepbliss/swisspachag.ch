<?php
/* Smarty version 3.1.33, created on 2019-04-18 15:55:18
  from '/var/www/html/swisspackag.ch/themes/classic/templates/customer/_partials/customer-form.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cb881c6ce4224_69047462',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '51712923be1e8964406cade64e381ae3c961530f' => 
    array (
      0 => '/var/www/html/swisspackag.ch/themes/classic/templates/customer/_partials/customer-form.tpl',
      1 => 1554099509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/form-errors.tpl' => 1,
  ),
),false)) {
function content_5cb881c6ce4224_69047462 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5165412295cb881c6cde753_10995287', 'customer_form');
?>

<?php }
/* {block 'customer_form_errors'} */
class Block_16374830565cb881c6cdec12_58185372 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_smarty_tpl->_subTemplateRender('file:_partials/form-errors.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('errors'=>$_smarty_tpl->tpl_vars['errors']->value['']), 0, false);
?>
  <?php
}
}
/* {/block 'customer_form_errors'} */
/* {block 'customer_form_actionurl'} */
class Block_17187743545cb881c6cdfc17_90367046 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['action']->value, ENT_QUOTES, 'UTF-8');
}
}
/* {/block 'customer_form_actionurl'} */
/* {block "form_field"} */
class Block_12667869295cb881c6ce1502_99922053 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['form_field'][0], array( array('field'=>$_smarty_tpl->tpl_vars['field']->value),$_smarty_tpl ) );?>

        <?php
}
}
/* {/block "form_field"} */
/* {block "form_fields"} */
class Block_6775095205cb881c6ce07d4_49566952 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['formFields']->value, 'field');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['field']->value) {
?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12667869295cb881c6ce1502_99922053', "form_field", $this->tplIndex);
?>

      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      <?php echo $_smarty_tpl->tpl_vars['hook_create_account_form']->value;?>

    <?php
}
}
/* {/block "form_fields"} */
/* {block "form_buttons"} */
class Block_11594246605cb881c6ce3186_76147133 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <button class="btn btn-primary form-control-submit float-xs-right" data-link-action="save-customer" type="submit">
          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Bestätigen','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

        </button>
      <?php
}
}
/* {/block "form_buttons"} */
/* {block 'customer_form_footer'} */
class Block_3398166395cb881c6ce2be2_86870278 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <footer class="form-footer clearfix">
        <p class="req-text"><sup style="left:0px;">*</sup>Pflichtfeld</p>
      <input type="hidden" name="submitCreate" value="1">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11594246605cb881c6ce3186_76147133', "form_buttons", $this->tplIndex);
?>

    </footer>  
    <p id="security_informations">
                Laut Informationsfreiheitsgesetz besitzen Sie bezüglich Ihrer persönlichen Daten das Recht auf Zugriff, Berichtigung und Widerspruch.
            </p>
  <?php
}
}
/* {/block 'customer_form_footer'} */
/* {block 'customer_form'} */
class Block_5165412295cb881c6cde753_10995287 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'customer_form' => 
  array (
    0 => 'Block_5165412295cb881c6cde753_10995287',
  ),
  'customer_form_errors' => 
  array (
    0 => 'Block_16374830565cb881c6cdec12_58185372',
  ),
  'customer_form_actionurl' => 
  array (
    0 => 'Block_17187743545cb881c6cdfc17_90367046',
  ),
  'form_fields' => 
  array (
    0 => 'Block_6775095205cb881c6ce07d4_49566952',
  ),
  'form_field' => 
  array (
    0 => 'Block_12667869295cb881c6ce1502_99922053',
  ),
  'customer_form_footer' => 
  array (
    0 => 'Block_3398166395cb881c6ce2be2_86870278',
  ),
  'form_buttons' => 
  array (
    0 => 'Block_11594246605cb881c6ce3186_76147133',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16374830565cb881c6cdec12_58185372', 'customer_form_errors', $this->tplIndex);
?>

<form action="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17187743545cb881c6cdfc17_90367046', 'customer_form_actionurl', $this->tplIndex);
?>
" id="customer-form" class="js-customer-form" method="post">
  <section class="register-block">
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6775095205cb881c6ce07d4_49566952', "form_fields", $this->tplIndex);
?>

  </section>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3398166395cb881c6ce2be2_86870278', 'customer_form_footer', $this->tplIndex);
?>


</form>                                
<?php
}
}
/* {/block 'customer_form'} */
}
