<?php
/* Smarty version 3.1.33, created on 2019-05-01 03:20:40
  from 'module:psshoppingcartpsshoppingc' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cc8f4685a0e15_77730481',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '35655e6409b6198f29dd6e732ef9598dec599880' => 
    array (
      0 => 'module:psshoppingcartpsshoppingc',
      1 => 1553492378,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'module:ps_shoppingcart/ps_shoppingcart-product-line.tpl' => 1,
  ),
),false)) {
function content_5cc8f4685a0e15_77730481 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /var/www/html/swisspackag.ch/themes/classic/modules/ps_shoppingcart/ps_shoppingcart.tpl --><div id="_desktop_cart">
  <div class="blockcart cart-preview <?php if ($_smarty_tpl->tpl_vars['cart']->value['products_count'] > 0) {?>active<?php } else { ?>inactive<?php }?>" data-refresh-url="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['refresh_url']->value, ENT_QUOTES, 'UTF-8');?>
">
    <div class="header">    <a href="<?php echo htmlspecialchars(__PS_BASE_URI__, ENT_QUOTES, 'UTF-8');?>
warenkorb?action=show">     
        <div class="cart-product">
            <span class="hidden-sm-down"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Cart','d'=>'Shop.Theme.Checkout'),$_smarty_tpl ) );?>
:</span>
            <?php if (count($_smarty_tpl->tpl_vars['cart']->value['products']) > 0) {?>
              <?php if (count($_smarty_tpl->tpl_vars['cart']->value['products']) == 1) {?>
                <span class="cart-products-count"><?php echo htmlspecialchars(count($_smarty_tpl->tpl_vars['cart']->value['products']), ENT_QUOTES, 'UTF-8');?>
 Produkt</span>
              <?php } else { ?>
                <span class="cart-products-count"><?php echo htmlspecialchars(count($_smarty_tpl->tpl_vars['cart']->value['products']), ENT_QUOTES, 'UTF-8');?>
 Produkte</span>
              <?php }?>
            <?php } else { ?>
              <span class="cart-products-count">(Leer)</span>
            <?php }?>
        </div>    
        <i class="material-icons shopping-cart"><span class="basket"></span></i></a>
    </div>

    
    
    
  <?php if ($_smarty_tpl->tpl_vars['cart']->value['products_count'] > 0) {?>
    <div class="ht_cart cart-hover-content">
      <ul>
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cart']->value['products'], 'product');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
?>
            <li class="cart-wishlist-item">
              <?php $_smarty_tpl->_subTemplateRender('module:ps_shoppingcart/ps_shoppingcart-product-line.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
            </li>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      </ul>
      <div class="cart-summary">
        <div class="cart-subtotals">
          <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['cart']->value['subtotals'], 'subtotal');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['subtotal']->value) {
?>
            <div class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotal']->value['type'], ENT_QUOTES, 'UTF-8');?>
 cart-parameter">
              <span class="label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotal']->value['label'], ENT_QUOTES, 'UTF-8');?>
</span>
              <span class="value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subtotal']->value['value'], ENT_QUOTES, 'UTF-8');?>
</span>
            </div>
          <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
        </div>
        <div class="cart-total cart-parameter">
            <span class="label"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['totals']['total']['label'], ENT_QUOTES, 'UTF-8');?>
</span>
            <span class="value"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['totals']['total']['value'], ENT_QUOTES, 'UTF-8');?>
</span>
        </div>
      </div>
      <div class="cart-wishlist-action">
          <a class="cart-wishlist-checkout" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['order'], ENT_QUOTES, 'UTF-8');?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Bestellen','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</a>
      </div>
    </div> 
    <?php } else { ?>
      <div class="ht_cart cart-hover-content">                    
          <p class="no-item">There is no item in your cart.</p>
      </div>
  <?php }?>
    </div>
</div>  

   
<!-- end /var/www/html/swisspackag.ch/themes/classic/modules/ps_shoppingcart/ps_shoppingcart.tpl --><?php }
}
