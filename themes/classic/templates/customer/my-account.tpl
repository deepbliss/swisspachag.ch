{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}

{block name='page_title'}
  {l s='Mein Konto' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
  <p class="account-txt">Willkommen auf Ihrer persönlichen Startseite. Hier können Sie Ihre persönlichen Daten, Ihre Bestellungen und Ihre Adresse verwalten.</p>
 
    <div class="links">
           {if !$customer.addresses}
    <a class="" id="address-link" href="{$urls.pages.address}">
           <img src="img/addrbook.gif" alt="orders" class="icon">
           <span class="link-item">                                       
                 {l s='Ihre Haupt-Adresse einfügen' d='Shop.Theme.Customeraccount'}
          </span>
        </a>
    {/if}
     
      {if !$configuration.is_catalog}
        <a class="" id="history-link" href="{$urls.pages.history}">
          <img src="img/order.gif" alt="orders" class="icon">
            <span class="link-item">
            {l s='Order history and details' d='Shop.Theme.Customeraccount'}
          </span>
        </a>
      {/if}
     
       {if !$configuration.is_catalog}
        <a class="" id="order-slips-link" href="{$urls.pages.order_slip}">
           <img src="img/slip.gif" alt="orders" class="icon">
           <span class="link-item">
            {l s='Ihre Gutschriften' d='Shop.Theme.Customeraccount'}
          </span>
        </a>
      {/if}

      <a class="" id="addresses-link" href="{$urls.pages.addresses}">
          <img src="img/addrbook.gif" alt=""/>
            <span class="link-item">                                              
           {l s='Ihre Adressen' d='Shop.Theme.Customeraccount'}
           </span>
        </a>   
      
      <a class="" id="identity-link" href="{$urls.pages.identity}">
          <img src="img/userinfo.gif" alt="orders" class="icon">
          <span class="link-item">
          {l s='Ihre persönlichen Daten' d='Shop.Theme.Customeraccount'}
        </span>
      </a>

      {if $configuration.voucher_enabled && !$configuration.is_catalog}
        <a class="" id="discounts-link" href="{$urls.pages.discount}">
            <img src="img/voucher.gif" alt="orders" class="icon">
          <span class="link-item">
            {l s='Ihre Gutscheine' d='Shop.Theme.Customeraccount'}
          </span>
        </a>
      {/if}

      {if $configuration.return_enabled && !$configuration.is_catalog}
        <a class="" id="returns-link" href="{$urls.pages.order_follow}">
          <span class="link-item">
            <i class="material-icons">&#xE860;</i>
            {l s='Merchandise returns' d='Shop.Theme.Customeraccount'}
          </span>
        </a>
      {/if}

      {block name='display_customer_account'}
        {hook h='displayCustomerAccount'}
      {/block}

    </div>

{/block}


{block name='page_footer'}
  {block name='my_account_links'}
    <div class="home-link">
      <a href="{$urls.base_url}" >
        {l s='Startseite' d='Shop.Theme.Actions'}
      </a>
    </div>
  {/block}
{/block}
   