{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}



      {block name='page_title'} 
            {l s='Registrieren' d='Shop.Theme.Customeraccount'}
        {/block}
{block name='page_content'}
      <div class="login-info">
            <form action="#" method="post" id="create_account_form" class="std">
                    <fieldset>
                        <h3 style="margin-bottom:0px;">{l s='ERSTELLEN SIE EIN KONTO'}</h3>
                        <div class="form_content clearfix login-user">
                            <p class="title_block">{l s='Um ein Konto zu erstellen, geben Sie bitte Ihre E-Mail-Adresse ein.'  d='Shop.Theme.Actions'}.</p>
                            <div class="error" id="create_account_error" style="display:none"></div>
                            <div class="form-group row ">
                                <label class="form-control-label required">
                                          E-Mail
                                      </label>
                                <div class="form-field">
                                      <input class="form-control" name="email" type="email" id="email_regis" value="" required="">
                                </div>
                            </div>
                            <p class="submit">
                                <input type="hidden" name="datalink" class="datalink" value="{$urls.pages.register}&email=">
                                
                                 <input type="hidden" name="submitregister" value="1">
                                {block name='form_buttons'}
                                  <button id="submit-register" class="btn btn-primary"  data-link-action="display-register-form" type="submit" class="form-control-submit">
                                    {l s='Erstellen Sie ein Konto' d='Shop.Theme.Actions'}
                                  </button>
                                 {/block}
                            </p>
                        </div>
                    </fieldset>
                </form>
    </div>

    <div class="login-info">        
    {block name='login_form_container'}
        <h3 style="margin-bottom:0px;">{l s='BEREITS REGISTRIERT?' d='Shop.Theme.Customeraccount'}</h3>
   <div class="login-user">
      <section class="login-form">
        {render file='customer/_partials/login-form.tpl' ui=$login_form}
      </section>
   
      {block name='display_after_login_form'}
        {hook h='displayCustomerLoginFormAfter'}
      {/block}
      <div class="no-account">
        <a href="{$urls.pages.register}" data-link-action="display-register-form">
          {l s='No account? Create one here' d='Shop.Theme.Customeraccount'}
        </a>
      </div>
      </div>
    {/block}
    </div>
{/block}
