{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='customer/page.tpl'}
 
{block name='page_title'}
  {l s='Verlauf Ihrer Bestellungen' d='Shop.Theme.Customeraccount'}
{/block}

{block name='page_content'}
  <p class="history-line">{l s='Here are the orders you\'ve placed since your account was created.' d='Shop.Theme.Customeraccount'}</p>

  {if $orders}
    <table class="table table-labeled hidden-sm-down">
      <thead>
        <tr>
          <th width="245">{l s='Order reference' d='Shop.Theme.Checkout'}</th>
          <th width="202">{l s='Date' d='Shop.Theme.Checkout'}</th>
          <th width="257">{l s='Total price' d='Shop.Theme.Checkout'}</th>
          <th width="193" class="hidden-md-down">{l s='Payment' d='Shop.Theme.Checkout'}</th>
          <th width="254" class="hidden-md-down">{l s='Status' d='Shop.Theme.Checkout'}</th>          
          <!-- <th class="d-none">{l s='Invoice' d='Shop.Theme.Checkout'}</th> -->                 
          <th width="89">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        {foreach from=$orders item=order}
          <tr>
            <td scope="row">{$order.details.reference}</td>
            <td>{$order.details.order_date}</td>
            <td class="">{$order.totals.total.value}</td>
            <td class="hidden-md-down">{$order.details.payment}</td>
            <td class="hidden-md-down">
              <span
                class="label-pill"
                style=""
              >
                {$order.history.current.ostate_name}
              </span>
            </td>
           <!-- <td class="text-sm-center d-none">
              {if $order.details.invoice_url}
                <a href="{$order.details.invoice_url}"><i class="material-icons">&#xE415;</i></a>
              {else}
                -
              {/if}
            </td>        -->
            <td class="text-sm-center order-actions">
            <!--  <a href="{$order.details.details_url}" data-link-action="view-order-details">
                {l s='Details' d='Shop.Theme.Customeraccount'}
              </a> -->
              {if $order.details.reorder_url}
                <a href="{$order.details.reorder_url}" class="reorder-icon">{l s='Reorder' d='Shop.Theme.Actions'}</a>
              {/if}
            </td>
          </tr>
        {/foreach}
      </tbody>
    </table>

    <div class="orders hidden-md-up">
      {foreach from=$orders item=order}
        <div class="order">
          <div class="row d-flex">
            <div class="col-xs-10 mobile-order">
              <div class="link-order"><a href="{$order.details.details_url}">{$order.details.reference}</a></div>
              <div class="date">{$order.details.order_date}</div>
              <div class="total">{$order.totals.total.value}</div>
              <div class="status">
                <span
                  class="label-pill"
                  style=""
                >
                  {$order.history.current.ostate_name}
                </span>
              </div>
            </div>
            <div class="col-xs-2">
                <!--<div>
                  <a href="{$order.details.details_url}" data-link-action="view-order-details" title="{l s='Details' d='Shop.Theme.Customeraccount'}">
                    <i class="material-icons">&#xE8B6;</i>
                  </a>
                </div>  -->
                {if $order.details.reorder_url}
                  <div>
                    <a href="{$order.details.reorder_url}" title="{l s='Reorder' d='Shop.Theme.Actions'}" class="reorder-icon">
                      
                    </a>
                  </div>
                {/if}
            </div>
          </div>
        </div>
      {/foreach}
    </div>

  {/if}    
{/block}
