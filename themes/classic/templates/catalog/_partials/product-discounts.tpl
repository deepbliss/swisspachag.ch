{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<section class="product-discounts" >
  {if $product.quantity_discounts}    
    <h3 class="product-detail-desc">{l s='Staffelpreise pro' d='Shop.Theme.Catalog'}</h3>
    {block name='product_discount_table'}
      <table class="table-product table-product-discounts">
        <thead>
        <tr>                                                  
        </tr>
        </thead>
        <tbody id="combinations">
        {foreach from=$product.quantity_discounts item='quantity_discount' name='quantity_discounts'}
          <tr data-discount-type="{$quantity_discount.reduction_type}" data-discount="{$quantity_discount.real_value}" data-discount-quantity="{$quantity_discount.quantity}">
            <td>{$quantity_discount.quantity}</td>
            <td> {$currency.sign}</td>
            {if $product.unity|intval=='0'}
            {*<td>{l s='Up to %discount%' d='Shop.Theme.Catalog' sprintf=['%discount%' => $quantity_discount.price]}</td>*}
            <td>{$quantity_discount.price}</td>
            {else}
             {*<td>{l s='Up to %discount%' d='Shop.Theme.Catalog' sprintf=['%discount%' => $quantity_discount.price*$product.unity|intval]}</td>*}
             <td>{$quantity_discount.price*$product.unity|intval}</td>
            {/if}  
            <td>
                
            </td>
          </tr> 
          
        {/foreach}
        </tbody>
      </table>
    {/block}
  {/if}
</section>
