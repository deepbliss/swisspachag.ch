
text/x-generic product-list.tpl ( UTF-8 Unicode (with BOM) English text, with very long lines )

﻿﻿﻿{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{* List of all faetures: http://screencast.com/t/nHYjVPsxWc *}

{* Turn of debugging view. Can see some helpful data in web front-end and in browser's console for JS *}

{* KEEP DISABLE FOR PRODUCTIVE VERSION !! *}
{assign var="swissDebug"         value=false}
{assign var="getSwissDebug"      value=$smarty.get.ssDebug}
{assign var="catId"              value=$smarty.get.id_category}
{assign var="getCurrentCategory" value=$smarty.get.id_category}

{* Look for parameter to force debug mode *}
{if !$swissDebug}
    {if $getSwissDebug}
        {assign var="swissDebug" value=true}
        {assign var="swissDebugClass" value="swissDebugClass"}
    {/if}
{/if}

{* Show column Stk/VE for all categories *}
{assign var="alwaysShowStkVe" value=$smarty.get.ssStkve}






{if isset($products) && ($products|count > 0)}

    {foreach from=$products[0].features item=feature}
        {* Qualität (pack type) *}
        {if $feature.id_feature == 21}
            {$qtyMeasure = $feature.value}
            {$qtyLabel = $feature.name}
        {/if}
    {/foreach}

    {*******************************************************************************************************************
    * Exceptions
    *}
	{if in_array($cat_id, $pCat)}

		{$exc = 1}
		{$dis_count = 0}
        {assign var="excCodeName" value="1"}

	{elseif $cat_id == 48}

		{$exc = 2}
        {assign var="excCodeName" value="2"}
		{assign var=dis  value=[1,2,3,4,6]}

    {elseif $cat_id == 59}

        {$modExceptionV30 = true}
        {assign var=dis  value=[1,3,5,10]}

    {elseif $cat_id == 143 || $cat_id == 145 || $cat_id == 147 || $cat_id == 148 || $cat_id == 55  || $cat_id == 54 }
        {$modExceptionV30 = true}
        {assign var=dis  value=[1,3,6,12,24]}
    {elseif $cat_id == 146 || $cat_id == 161  || $cat_id == 149  || $cat_id == 162}
        {$modExceptionV30 = true}
        {assign var=dis  value=[3,5,10,20,24]}
    {elseif in_array($cat_id, $pCat2) }

        {$modExceptionV33 = true}
        {$virtualStkVe = true}
        {$virtualPreisangabe = "1'000 Stk."}
        {$virtualPreisangabeInt = 1000}

    {*{elseif $cat_id == 148   }*}

        {*{$modExceptionV33 = true}*}
        {*{$virtualStkVe = true}*}

    {*{else}*}
        {assign var="excCodeName" value="IV"}
	{/if}

    {*******************************************************************************************************************
    * Debug box over each table informing about the method used for calculating the price
    *}
    {if $swissDebug}
        {if $exc}
            <h3>Calculation mode: <i>Calculation by exception {$exc}; Codename {$excCodeName}; </i></h3>
        {else}
            <h3>Calculation mode: <i>Standard calculation; Codename {$excCodeName}; </i></h3>
        {/if}
    {/if}


    {*******************************************************************************************************************
    * Alert box
    *}
    <div class="alert-box2" style="position: fixed;z-index:10;display:none;left: 0px;width: 100%; top: 1px;box-shadow: 0px 4px 8px #ccc;">
        <div style="background: yellow !important;border: 1px solid #666; #FF0000;font-weight:bold; font-size: 12px;overflow: hidden;padding: 10px;text-align: center;">
            Dieser Artikel ist zu <span></span> Stück verpackt.
        </div>
    </div>


    {*******************************************************************************************************************
    * PRODUCTS TABLE
    *
    *}
    <table style="width:100%;" class="{$swissDebugClass}">

        {*
         * PRODUCTS LISTING
         * header
         *
         *}
        <thead>
            <tr>
                {if $swissDebug}
                    <th class="ac" style="width:200px;">Ref.</th>
                {/if}

                {* FEATURES: description *}
                {strip}
                    {foreach from=$products[0].datasheet item=sheet}
                        {if $sheet.id_group != 2 && $sheet.id_group != 3}
                            {foreach from=$sheet.features item=feat}
                                {foreach from=$products[0].sodprodfeat item=feature}
                                    {assign var="feature_name_clean"    value="{$feature.feature_name|replace:' ':''}"}
                                    {assign var="feature_name"          value="{$feature.feature_name}"}
                                    {assign var="feature_id"            value="{$feature.id_feature}"}
                                    {if ($feature.indetail!=1) and ($feature.id_feature == $feat) and ($feature.id_group == $sheet.id_group)}
                                        <th data-feature-id="{$feature_id}" data-feature-name="{$feature_name_clean}">
                                            {if $feature_id == 27}
                                                {* INNENMASSE IN MM *}
                                                {$feature_name|wordwrap:10:"<br/>"}
                                            {else}
                                                {$feature_name}
                                            {/if}
                                        </th>
                                    {/if}
                                {/foreach}
                            {/foreach}
                        {/if}
                    {/foreach}
                {/strip}

                {if $alwaysShowStkVe}
                        <th class="ac">Stk./VE.</th>
                {/if}


                {* FEATURE: UNITY *}
                <th style="width:88px;text-align:left;">Preisangabe</th>

                {* FEATURES: DISCOUNTS & SPECIAL PRICES *}
                {strip}
                    {if $exc == 2}
                        {foreach from=$dis item='count'}
                        <th class="ar" >
                            {$count} Karton
                        </th>
                        {/foreach}
                    {elseif $modExceptionV30}
                        {foreach from=$dis item='count'}
                            <th class="ar" >
                                {$count} Karton
                            </th>
                        {/foreach}
                    {else}
                        {if (isset($discounts_arr) && count($discounts_arr) > 0)}
                            {foreach from=$discounts_arr item='discount'}

                                {assign var="discountAmount"            value="{$discount|intval}"}
                                {assign var="discountWithMeasure"       value="{$discountAmount|cat:" {$qtyMeasure}"}"}
                                {assign var="quantityMeasure"           value="{$qtyMeasure}" }

                                {if $exc == 1 }
                                    {if $cat_id == 114}
                                        {if $discountAmount == 50 || $discountAmount == 100 || $discountAmount == 200 || $discountAmount == 400}
                                        <th class="ar">
                                            {$discountWithMeasure}
                                        </th>
                                        {/if}
									{elseif $cat_id == 38}
										{if $discountAmount <= 20}{$dis_count=$dis_count+1}
                                        <th class="ar">
                                            {$discountWithMeasure}
                                        </th>
                                        {/if}
                                    {else}
                                        {if $discountAmount <= 400}{$dis_count=$dis_count+1}
                                        <th class="ar">
                                            {$discountWithMeasure}
                                        </th>
                                        {/if}
                                    {/if}
                                {else}
                                <th class="ar">
                                    {$discountWithMeasure}
                                </th>
                                {/if}
                            {/foreach}
                        {/if}
                        {if $exc == 1}<th class="ar">1 Palette</th>{/if}
                    {/if}
                {/strip}

                {*Menge*}
                <th class="ac" style="width:90px;">Menge</th>

                {*Preis*}
                <th class="thPreis" colspan="2">Preis (CHF)</th>

                {*Add to basket *}
                {*<th class="thAddToBasket" style="max-width:50px;padding-left:0;"><img src="/themes/swisspack/img/theme/cart.png"/></th>*}
            </tr>
        </thead>



        {*
         * PRODUCTS LISTING
         * body
         *
         *}
        {assign var="foundQty" value="0"}
        <tbody>
        {foreach from=$products item=product name=products}

            {foreach from=$product.datasheet item=sheet}
                {if $sheet.id_group != 2 && $sheet.id_group != 3}
                    {foreach from=$sheet.features item=feat}
                        {foreach from=$product.sodprodfeat item=feature}
                            {if ($feature.id_feature == $feat) and ($feature.id_group == $sheet.id_group) }

                                {if $feature.id_feature == 19}
                                    {assign var="discountNew" value=$feature.featurevalue }
                                 {elseif $feature.id_feature ==37}
                                    {assign var="discountNew" value=$feature.featurevalue }
                                {/if}
                                </td>
                            {/if}
                        {/foreach}
                    {/foreach}
                {/if}
            {/foreach}

            <tr style="cursor:pointer;" data-test="{$discountNew}" class="ajax_block_product {if $smarty.foreach.products.first}first_item{elseif $smarty.foreach.products.last}last_item{/if} {if $smarty.foreach.products.index % 2}alternate_item{else}item{/if}">

                {* Stk./VE - ID of the feature *}
                {foreach from=$product.features item=feature}
                    {if $feature.id_feature == 10}
                        {$stkpal = $feature.value}
                    {/if}
                    {if $feature.id_feature == 19}
                        {$stkve = $feature.value}
                    {/if}
                    {if $feature.id_feature == 18}
                        {$stkkr = $feature.value}
                    {/if}
                {/foreach}


                {*******************************************************************************************************

                    PRINT SPECIAL PRICES
                    Print price per quantity. Number of quantities is flexible and can be different for different
                    products. In provided quantitie product can be
                                                                                                                      *}

                {* Availability status: [ true, false, onRequest ]*}
                {assign var="discountAvailability" value="false"}

                {* Availability status for products *}
                {assign var="statusTrue"            value=[ 'status' => 'true',         'value' => '']}
                {assign var="statusFalse"           value=[ 'status' => 'false',        'value' => '-']}
                {assign var="statusOnRequest"       value=[ 'status' => 'onrequest',    'value' => 'a.A.']}

                {* Build array with status for products *}
                {assign var="discountAvailabilityArray"  value=[ 'true'      =>  $statusTrue,
                                                                 'false'     =>  $statusFalse,
                                                                 'onrequest' =>  $statusOnRequest ]}

                {*{$discountAvailability|@print_r:true}*}

                {* Front-end value displayed inside a cell. Readable for humans.*}
                {assign var="discountCellValue" value=""}

                {*Special price for that quantity *}
                {assign var="discountPrice" value="0"}

                {* Translation *}
                {assign var="txAufAnfrage" value=['onRequest' => 'a.A.', 'onRequest']}
                {assign var="txNoDiscountForQuantitie" value="-"}

                {if $swissDebug}
                    <td>
                        {$product.reference}
                    </td>
                {/if}

                {*******************************************************************************************************

                    BODY CONTENT

                *********************************************************************************************************************************************************************************}

                {* FEATURES: description *}
                {* LINK COLUMNS of features. Display features which are also a link to the product details *}
                {strip}
                    {foreach from=$product.datasheet item=sheet}
                        {if $sheet.id_group != 2 && $sheet.id_group != 3}
                            {foreach from=$sheet.features item=feat}
                                {foreach from=$product.sodprodfeat item=feature}
                                    {if ($feature.id_feature == $feat) and ($feature.id_group == $sheet.id_group) }
                                        {if ($feature.indetail!=1)}
                                            {*<td onclick="document.location = '{$product.link|escape:'htmlall':'UTF-8'}'" class="ac attr {$feature.id_feature}">{if $feature.id_feature == 8}{$feature.featurevalue|wordwrap:11:"<br />\n"}{else if}{$feature.featurevalue}{/if}</td>*}

                                            {assign var="ftValue" value=""}
                                            {if  $feature.id_feature == 19 && $virtualStkVe}
                                                {$ftValue = $stkkr}
                                            {else}
                                                {$ftValue = $feature.featurevalue|replace:' ':''}
                                            {/if}

                                            <td data-feature-id="{$feature.id_feature}"
                                                data-feature-value="{$ftValue}"
                                                data-feature-realvalue="{$feature.featurevalue|replace:' ':''}"
                                                onclick="document.location = '{$product.link|escape:'htmlall':'UTF-8'}'"
                                                class="attr {$feature.id_feature}"
                                                style="padding-left: 5px;">
                                                {assign var="clearfit" value=$feature.featurevalue|replace:' ':''}

                                                {if $feature.id_feature == 10}
                                                    {$feature.featurevalue|number_format:0:".":"'"}
                                                {* {elseif $feature.id_feature == 8}
                                                    {$feature.featurevalue|wordwrap:11:"<br />\n"} *}
                                                {elseif $feature.id_feature == 26 && $getCurrentCategory == 70}
                                                    {$feature.featurevalue|replace:'(':'<br/>('}
                                                {elseif $feature.id_feature == 15 && $getCurrentCategory == 83}
                                                    {$feature.featurevalue|replace:',':',<br/>'}
                                                {elseif $feature.id_feature == 15 && $getCurrentCategory==71}
                                                    {assign var="clearfit" value=$feature.featurevalue|replace:' ':''}
                                                    {* ARTIKEL break line. Only in category id=71 *}
                                                    {if $clearfit == "Halbpalette(2-Wege)"}
                                                        Halbpalette <br/>(2-Wege)
                                                    {elseif $clearfit =="Überseeformat(7Deckbretter)"}
                                                        Überseeformat<br/>(7 Deckbretter)
                                                    {elseif $clearfit =="Euroformat(7Deckbretter)"}
                                                        Euroformat<br/>(7 Deckbretter)
                                                    {else}
                                                        {$feature.featurevalue}
                                                    {/if}
                                                {elseif $feature.id_feature == 19 && $virtualStkVe}
                                                    {$stkkr}
                                                {else}
                                                    {$feature.featurevalue}

                                                {/if}
                                            </td>
                                        {/if}
                                    {/if}
                                {/foreach}
                            {/foreach}
                        {/if}
                    {/foreach}


                    {* Stk/VE visible for all categories *}
                        {foreach from=$product.features item=feature}
                            {if $feature.id_feature == 18}
                                {assign var="feat_stkkarton" value="{$feature.value|escape:'htmlall':'UTF-8'}"}
                                {$feat_stkkarton = $feat_stkkarton / 1000}
                            {/if}
                        {/foreach}



                {*********************************************************************************************************************************************************************************}
                {* FEATURE: UNITY *}
                <td class="unity" onclick="document.location = '{$product.link|escape:'htmlall':'UTF-8'}'">
                    {if $exc == 2}
                        <i style="display:inline-block;padding-right:2px;">pro</i> <span>Stk.</span>
                    {elseif $modExceptionV33}
                        <i style="display:inline-block;padding-right:2px;">pro</i> <span class="pangabe" data-unity-value="{$virtualPreisangabeInt}">{$virtualPreisangabe}</span>
                    {else}
                        {if !empty($product.unity)}<i style="display:inline-block;padding-right:2px;">pro</i><span>{$product.unity|escape:'htmlall':'UTF-8'}</span> {/if}
                    {/if}
                </td>


                {*********************************************************************************************************************************************************************************}
                {* FEATURES: DISCOUNTS & SPECIAL PRICES *}
                {if (isset($discounts_arr) && count($discounts_arr) > 0)}


                    {*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                    {* TD > Content: Exception 1  *}
                    {if $exc == 1}

                        {* cat_id = 114 *}
						{if $cat_id == 114}
                            {if $swissDebug}A id: {$cat_id}{/if}
							{$qPrice=0} {$palPrice = 0}
							{foreach from=$discounts_arr item='discount' name="discounts"}
								{foreach from=$product.quantity_discounts item='quantity_discount'}
                                {* Set up variables *}
                                    {assign var="qdPrice"               value=$quantity_discount.price|floatval}
                                    {assign var="qdPricePromotion"      value=""}
                                    {assign var="qdPricePromotionValue" value=""}
                                    {assign var="qdPrice"               value=$quantity_discount.price|floatval}
                                    {assign var="qdReduction"           value=$quantity_discount.reduction|string_format:"%.2f"}
                                    {assign var="qdReductionBoolean"    value=false}
                                    {assign var="qdReductionType"       value=$quantity_discount.reduction_type}
                                    {assign var="qdQuantity"            value=$quantity_discount.quantity|intval}
                                    {assign var="proUnity"              value=$product.unity|string_format:"%.2f"}


                                {* Template's validation *}
                                    {assign var="qdValidationMessage"   value=""}
                                    {assign var="qdValidation"          value=""}
                                    {assign var="_priceReduced"         value=""}

                                    {if $qdReduction > 0}
                                        {assign var="_priceReduced"         value="priceReduced"}
                                        {assign var="_promotedQuantitie"    value="promotion"}
                                        {assign var="qdReductionBoolean"    value=true}

                                        {if $qdReductionType == "percentage"}

                                            {if $proUnity == 1}
                                                {math
                                                equation        =   "qdPrice - (qdReduction * qdPrice)"
                                                qdReduction     =   $qdReduction
                                                qdPrice         =   $qdPrice
                                                assign          =   "qdPricePromotion"
                                                }
                                                {math
                                                equation        =   "qdReduction * qdPrice"
                                                qdReduction     =   $qdReduction
                                                qdPrice         =   $qdPrice
                                                assign          =   "qdPricePromotionValue"
                                                }
                                            {else}
                                                {math
                                                equation        =   "qdReduction * qdPrice"
                                                qdReduction     =   $qdReduction
                                                qdPrice         =   $qdPrice
                                                assign          =   "qdPricePromotion"
                                                }
                                                {$qdPricePromotionValue = $qdPricePromotion}
                                            {/if}

                                        {elseif $qdReductionType == "amount"}

                                            {math
                                            equation        =   "qdPrice - qdReduction"
                                            qdPrice         =   $qdPrice
                                            qdReduction     =   $qdReduction
                                            format          =   "%.2f"
                                            assign          =   "qdPricePromotion"
                                            }
                                            {if $proUnity == 1}
                                                {$qdPricePromotionValue = $qdReduction}
                                            {else}
                                                {$qdPricePromotionValue = $qdReduction}
                                            {/if}

                                        {* Discount price can not be higher than stuck price *}
                                            {if $qdReduction > $qdPrice}
                                                {$qdValidation = "false"}
                                            {/if}
                                        {/if}

                                    {* Promotion Product price for fixed quantities *}
                                        {math
                                        equation            =   "qdPricePromotion * proUnity"
                                        qdPricePromotion    =   $qdPricePromotion
                                        proUnity            =   $proUnity
                                        format              =   "%.2f"
                                        assign              =   "TOTAL_PRICE_PROMOTION"
                                        }
                                        {assign var="JUST_PRICE" value=$TOTAL_PRICE_PROMOTION}

                                    {elseif $qdReduction == 0}

                                        {assign var="qdPricePromotion" value="0"}
                                        {assign var="qdReduction" value="NaN"}
                                    {*{$TOTAL_PRICE_PROMOTION = false}*}
                                        {math
                                        equation            =   "qdPrice * proUnity"
                                        qdPrice             =   $qdPrice
                                        proUnity            =   $proUnity
                                        format              =   "%.2f"
                                        assign              =   "TOTAL_PRICE"
                                        }
                                        {assign var="JUST_PRICE" value=$TOTAL_PRICE}

                                    {/if}


                                    {if ($discount|intval==50 || $discount|intval==100 || $discount|intval==200 || $discount|intval==400 ) && $discount|intval == $quantity_discount.from_quantity|intval}
                                        {if $qdPrice >= 0 OR $qdReductionType == 'amount'}
                                        {* A - normal price set*}
                                            {if $qdPrice != 0}
                                                {if $proUnity != 0}
                                                    {assign var="discountPrice" value=$JUST_PRICE}
                                                {else}
                                                    {assign var="discountPrice" value=$qdPrice}
                                                {/if}
                                                {assign var="discountAvailability" value=$discountAvailabilityArray['true']['status']}
                                                {assign var="discountCellValue" value=$discountPrice}
                                            {* B - auf anfrage, because price is zero and there are special prices set*}
                                            {else}
                                                {assign var="discountPrice" value=""}
                                                {assign var="discountAvailability" value=$discountAvailabilityArray['onrequest']['status']}
                                                {assign var="discountCellValue" value=$discountAvailabilityArray['onrequest']['value']}
                                            {/if}
                                        {* no price and no special prices. Product is not available for that quaintity *}
                                        {else}
                                            {assign var="discountPrice" value=$quantity_discount.real_value|floatval}
                                            {assign var="discountAvailability" value=$discountAvailabilityArray['false']['status']}
                                            {assign var="discountCellValue" value=$discountAvailabilityArray['false']['value']}
                                        {*-{$quantity_discount.real_value|floatval}%*}
                                        {/if}



                                        <td class                       =   "discount ar ac {$_priceReduced}"
                                            data-discount-availability  =   "{$discountAvailability}"
                                            data-discount-quantity      =   "{$qdQuantity|intval}"
                                            data-discount-price         =   "{$discountPrice}"
                                            onclick="$(this).parent().children('.quantity_input').children('input').val('{$discount}');priceUpdate($(this).parent().children('.quantity_input').children('input'));">
                                            {if !$discountPrice}
                                                <span class="auf-anfrage" title="Für die gewählte Produktmenge von {$discount} Stück bieten wir spezielle Preise auf Anfrage. Bitte nehmen Sie mit uns Kontakt auf.">{$discountAvailabilityArray['onrequest']['value']}</span>
                                            {else}
                                                <span class="totalPrice">{$discountCellValue}</span>
                                                {if $qdReductionBoolean}
                                                    <span class="totalPricePromotion">{$discountCellValue}</span>
                                                {/if}
                                            {/if}
                                        </td>

                                    {/if}
									{if $stkpal|intval == $quantity_discount.from_quantity|intval}
                                        {assign var="discountAvailability" value=$discountAvailabilityArray['true']['status']}
										{if $product.unity|string_format:"%.2f" != 0}
											{$palPrice = ($quantity_discount.price|floatval * $product.unity)|string_format:"%.2f"}
										{else}
											{$palPrice = $quantity_discount.price|floatval|string_format:"%.2f"}
										{/if}

									{/if}
								{/foreach}
							{/foreach}

                            <td class                       =   "discount ar ac pal"
                                data-discount-availability  =   "{$discountAvailability}"
                                data-discount-quantity      =   "{$stkpal}"
                                data-discount-price         =   "{$palPrice}"
                                onclick="$(this).parent().children('.quantity_input').children('input').val('{$stkpal}');priceUpdate($(this).parent().children('.quantity_input').children('input'));">
                                {$palPrice}
                            </td>

						{else}
                        {*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                        {* TD > Content: Exception 2  1a
                           WELLKARTON-FALTBOXEN,BRIEFBOXEN MIT SELBSTKLEBEVERSCHLUSS,BUCHVERPACKUNG *}
                            {if $swissDebug}B id:  {$cat_id}{/if}
							{$qPrice=0}
							{foreach from=$discounts_arr item='discount' name="discounts"}
								{foreach from=$product.quantity_discounts item='quantity_discount'}

                                {* Set up variables *}
                                    {assign var="qdPrice"               value=$quantity_discount.price|floatval}
                                    {assign var="qdPricePromotion"      value=""}
                                    {assign var="qdPricePromotionValue" value=""}
                                    {assign var="qdPrice"               value=$quantity_discount.price|floatval}
                                    {assign var="qdReduction"           value=$quantity_discount.reduction|string_format:"%.2f"}
                                    {assign var="qdReductionBoolean"    value=false}
                                    {assign var="qdReductionType"       value=$quantity_discount.reduction_type}
                                    {assign var="qdQuantity"            value=$quantity_discount.quantity|intval}
                                    {assign var="qdAufAnfrage"          value="a.A."}
                                    {assign var="proUnity"              value=$product.unity|intval}
                                    {assign var="stuckPriceForPalette"  value=""}
                                    {assign var="dataDiscountException" value=false}

                                {* Template's validation *}
                                    {assign var="qdValidationMessage"   value=""}
                                    {assign var="qdValidation"          value=""}
                                    {assign var="_priceReduced"         value=""}

                                    {if $proUnity == 0}
                                        {$proUnity = 1}
                                    {elseif $proUnity > 0}
                                        {if $qdQuantity < $proUnity}
                                            {assign var="quantitieLowerThanPreisangabe" value=true}
                                            {assign var="dataDiscountException2" value="lower-than-preisangabe"}
                                            {math
                                            equation        =   "qdQuantity / proUnity"
                                            qdQuantity      =   $qdQuantity
                                            proUnity        =   $proUnity
                                            assign          =   "qdHalfOfPreisangabe"
                                            }
                                            {if $qdHalfOfPreisangabe == 0.5}
                                                {math
                                                equation        =   "qdPrice / 2"
                                                qdPrice         =   $qdPrice
                                                assign          =   "qdPrice"
                                                }
                                            {/if}
                                        {/if}
                                    {/if}

                                    {if $qdReduction > 0}
                                        {assign var="_priceReduced"         value="priceReduced"}
                                        {assign var="_promotedQuantitie"    value="promotion"}
                                        {assign var="qdReductionBoolean"    value=true}
                                        {if $qdReductionType == "percentage"}
                                            {if $proUnity == 1}
                                                {math
                                                equation        =   "qdPrice - (qdReduction * qdPrice)"
                                                qdReduction     =   $qdReduction
                                                qdPrice         =   $qdPrice
                                                assign          =   "qdPricePromotion"
                                                }
                                                {math
                                                equation        =   "qdReduction * qdPrice"
                                                qdReduction     =   $qdReduction
                                                qdPrice         =   $qdPrice
                                                assign          =   "qdPricePromotionValue"
                                                }
                                            {else}
                                                {math
                                                equation        =   "qdPrice -(qdReduction * qdPrice)"
                                                qdReduction     =   $qdReduction
                                                qdPrice         =   $qdPrice
                                                assign          =   "qdPricePromotion"
                                                }
                                                {$qdPricePromotionValue = $qdPricePromotion}
                                            {/if}
                                        {elseif $qdReductionType == "amount"}
                                            {math
                                            equation        =   "qdPrice - qdReduction"
                                            qdPrice         =   $qdPrice
                                            qdReduction     =   $qdReduction
                                            format          =   "%.2f"
                                            assign          =   "qdPricePromotion"
                                            }
                                            {if $proUnity == 1}
                                                {$qdPricePromotionValue = $qdReduction}
                                            {else}
                                                {$qdPricePromotionValue = $qdReduction}
                                            {/if}

                                        {* Discount price can not be higher than stuck price *}
                                            {if $qdReduction > $qdPrice}
                                                {$qdValidation = "false"}
                                            {/if}
                                        {/if}

                                    {* Promotion Product price for fixed quantities *}
                                        {math
                                        equation            =   "qdPricePromotion * proUnity"
                                        qdPricePromotion    =   $qdPricePromotion
                                        proUnity            =   $proUnity
                                        format              =   "%.2f"
                                        assign              =   "TOTAL_PRICE_PROMOTION"
                                        }
                                        {assign var="JUST_PRICE" value=$TOTAL_PRICE_PROMOTION}

                                    {/if}



                                    {math
                                    equation            =   "qdPrice * proUnity"
                                    qdPrice             =   $qdPrice
                                    proUnity            =   $proUnity
                                    format              =   "%.2f"
                                    assign              =   "TOTAL_PRICE"
                                    }

                                    {if $qdReduction == 0}
                                        {assign var="qdPricePromotion" value="0"}
                                        {assign var="qdReduction" value="NaN"}
                                    {*{$TOTAL_PRICE_PROMOTION = false}*}
                                        {assign var="JUST_PRICE" value=$TOTAL_PRICE}
                                    {/if}

									{if $discount|intval == $quantity_discount.from_quantity|intval}

                                    {* CHECK IF IS SET RETAIL PRICE or IF THERE ARE SPECIAL PRICES *}
                                        {if $qdPrice >= 0 OR $qdReductionType == 'amount'}
                                        {* A - normal price set*}
                                            {if $qdPrice != 0}
                                                {if $proUnity != 0}
                                                    {assign var="discountPrice" value=$JUST_PRICE}
                                                {else}
                                                    {assign var="discountPrice" value=$qdPrice}
                                                {/if}

                                                {assign var="discountAvailability" value=$discountAvailabilityArray['true']['status']}
                                                {assign var="discountCellValue" value=$discountPrice}
                                            {* B - auf anfrage, because price is zero and there are special prices set*}
                                            {else}
                                                {assign var="discountPrice" value=""}
                                                {assign var="discountAvailability" value=$discountAvailabilityArray['onrequest']['status']}
                                                {assign var="discountCellValue" value=$discountAvailabilityArray['onrequest']['value']}
                                            {/if}
                                        {* no price and no special prices. Product is not available for that quaintity *}
                                        {else}
                                            {assign var="discountPrice" value=$quantity_discount.real_value|floatval}
                                            {assign var="discountAvailability" value=$discountAvailabilityArray['false']['status']}
                                            {assign var="discountCellValue" value=$discountAvailabilityArray['false']['value']}
                                        {*-{$quantity_discount.real_value|floatval}%*}
                                        {/if}

                                        {if $cat_id == 113 || $cat_id == 114 || $cat_id == 78 || $cat_id == 116  || $cat_id == 117}
                                            {if $qdQuantity == 50}
                                                {math
                                                    equation            =   "discountPrice * 2"
                                                    discountPrice       =   $discountPrice
                                                    format              =   "%.2f"
                                                    assign              =   "discountPrice"
                                                }
                                                {math
                                                    equation            =   "discountCellValue * 2"
                                                    discountCellValue   =   $discountCellValue
                                                    format              =   "%.2f"
                                                    assign              =   "discountCellValue"
                                                }
                                            {/if}
                                        {/if}

                                        <td class                       =   "discount ar ac {$_priceReduced}"
                                            data-discount-availability  =   "{$discountAvailability}"
                                            data-discount-quantity      =   "{$qdQuantity}"
                                            data-discount-price         =   "{$discountPrice}"
                                            onclick="$(this).parent().children('.quantity_input').children('input').val('{$discount}');priceUpdate($(this).parent().children('.quantity_input').children('input'));">
                                            {if !$discountPrice}
                                                <span class="auf-anfrage" title="Für die gewählte Produktmenge von {$discount} Stück bieten wir spezielle Preise auf Anfrage. Bitte nehmen Sie mit uns Kontakt auf.">{$discountAvailabilityArray['onrequest']['value']}</span>
                                            {else}
                                                <span class="totalPrice">{$discountCellValue}</span>
                                                {if $qdReductionBoolean}
                                                    <span class="totalPricePromotion">{$discountCellValue}</span>
                                                {/if}
                                            {/if}
                                        </td>

                                    {/if}
								{/foreach}
							{/foreach}

                            {* IF Special price dedicated quantity == Stk./Pallette *}
							{if count($product.quantity_discounts) == $dis_count}
                                {assign var="discountAvailability" value=$discountAvailabilityArray['false']['status']}
								<td class="discount ar ac"
                                    data-discount-availability  =   "{$discountAvailability}"
                                    data-discount-quantity      =   "{$stkpal}"
                                    data-discount-price         =   "{$discountPrice}"
                                    onclick="$(this).parent().children('.quantity_input').children('input').val('{$stkpal}');
                                    priceUpdate($(this).parent().children('.quantity_input').children('input'));">
                                    {$discountPrice}
                                </td>
							{/if}

						{/if}

                   {*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                     TD > Content: Exception 2 *}
					{elseif $exc == 2}
                        {if $swissDebug}C id: {$cat_id}{/if}
                        {foreach from=$product.quantity_discounts item='quantity_discount'}

                        {* Set up variables *}
                            {assign var="qdPrice"               value=$quantity_discount.price|floatval}
                            {assign var="qdPricePromotion"      value=""}
                            {assign var="qdPricePromotionValue" value=""}
                            {assign var="qdPrice"               value=$quantity_discount.price|floatval}
                            {assign var="qdReduction"           value=$quantity_discount.reduction|string_format:"%.2f"}
                            {assign var="qdReductionBoolean"    value=false}
                            {assign var="qdReductionType"       value=$quantity_discount.reduction_type}
                            {assign var="qdQuantity"            value=$quantity_discount.quantity|intval}
                            {assign var="qdAufAnfrage"          value="a.A."}
                            {assign var="proUnity"              value=$product.unity|intval}
                            {assign var="stuckPriceForPalette"  value=""}
                            {assign var="dataDiscountException" value=false}

                            {* Template's validation *}
                            {assign var="qdValidationMessage"   value=""}
                            {assign var="qdValidation"          value=""}
                            {assign var="_priceReduced"         value=""}

                            {* WTF ? *}
                           {*  {$qdPrice = $qdPrice / $stkve} *}

                            {if $proUnity == 0}
                                {$proUnity = 1}
                            {elseif $proUnity > 0}
                                {if $qdQuantity < $proUnity}
                                    {assign var="quantitieLowerThanPreisangabe" value=true}
                                    {assign var="dataDiscountException2" value="lower-than-preisangabe"}
                                    {math
                                    equation        =   "qdQuantity / proUnity"
                                    qdQuantity      =   $qdQuantity
                                    proUnity        =   $proUnity
                                    assign          =   "qdHalfOfPreisangabe"
                                    }
                                    {if $qdHalfOfPreisangabe == 0.5}
                                        {math
                                        equation        =   "qdPrice / 2"
                                        qdPrice         =   $qdPrice
                                        assign          =   "qdPrice"
                                        }
                                    {/if}
                                {/if}
                            {/if}

                            {if $qdReduction > 0}
                                {assign var="_priceReduced"         value="priceReduced"}
                                {assign var="_promotedQuantitie"    value="promotion"}
                                {assign var="qdReductionBoolean"    value=true}
                                {if $qdReductionType == "percentage"}
                                    {if $proUnity == 1}
                                        {math
                                        equation        =   "qdPrice - (qdReduction * qdPrice)"
                                        qdReduction     =   $qdReduction
                                        qdPrice         =   $qdPrice
                                        assign          =   "qdPricePromotion"
                                        }
                                        {math
                                        equation        =   "qdReduction * qdPrice"
                                        qdReduction     =   $qdReduction
                                        qdPrice         =   $qdPrice
                                        assign          =   "qdPricePromotionValue"
                                        }
                                    {else}
                                        {math
                                        equation        =   "qdPrice -(qdReduction * qdPrice)"
                                        qdReduction     =   $qdReduction
                                        qdPrice         =   $qdPrice
                                        assign          =   "qdPricePromotion"
                                        }
                                        {$qdPricePromotionValue = $qdPricePromotion}
                                    {/if}
                                {elseif $qdReductionType == "amount"}
                                    {math
                                    equation        =   "qdPrice - qdReduction"
                                    qdPrice         =   $qdPrice
                                    qdReduction     =   $qdReduction
                                    format          =   "%.2f"
                                    assign          =   "qdPricePromotion"
                                    }
                                    {if $proUnity == 1}
                                        {$qdPricePromotionValue = $qdReduction}
                                    {else}
                                        {$qdPricePromotionValue = $qdReduction}
                                    {/if}

                                {* Discount price can not be higher than stuck price *}
                                    {if $qdReduction > $qdPrice}
                                        {$qdValidation = "false"}
                                    {/if}
                                {/if}

                            {* Promotion Product price for fixed quantities *}
                                {math
                                    equation            =   "qdPricePromotion * proUnity"
                                    qdPricePromotion    =   $qdPricePromotion
                                    proUnity            =   $proUnity
                                    format              =   "%.2f"
                                    assign              =   "TOTAL_PRICE_PROMOTION"
                                }
                                {assign var="JUST_PRICE" value=$TOTAL_PRICE_PROMOTION}

                            {/if}



                            {math
                                equation            =   "qdPrice * proUnity"
                                qdPrice             =   $qdPrice
                                proUnity            =   $proUnity
                                format              =   "%.2f"
                                assign              =   "TOTAL_PRICE"
                            }

                            {if $qdReduction == 0}
                                {assign var="qdPricePromotion" value="0"}
                                {assign var="qdReduction" value="NaN"}
                                {*{$TOTAL_PRICE_PROMOTION = false}*}
                                {assign var="JUST_PRICE" value=$TOTAL_PRICE}
                            {/if}


                            {if $qdPrice >= 0 OR $qdReductionType == 'amount'}
                            {* A - normal price set*}
                                {if $qdPrice != 0}
                                    {if $proUnity != 0}
                                        {assign var="discountPrice" value=$JUST_PRICE}
                                    {else}
                                        {assign var="discountPrice" value=$qdPrice}
                                    {/if}
                                    {assign var="discountAvailability" value=$discountAvailabilityArray['true']['status']}
                                    {assign var="discountCellValue" value=$discountPrice}
                                {* B - auf anfrage, because price is zero and there are special prices set*}
                                {else}
                                    {assign var="discountPrice" value=""}
                                    {assign var="discountAvailability" value=$discountAvailabilityArray['onrequest']['status']}
                                    {assign var="discountCellValue" value=$discountAvailabilityArray['onrequest']['value']}
                                {/if}
                            {* no price and no special prices. Product is not available for that quaintity *}
                            {else}
                                {assign var="discountPrice" value=$quantity_discount.real_value|floatval}
                                {assign var="discountAvailability" value=$discountAvailabilityArray['false']['status']}
                                {assign var="discountCellValue" value=$discountAvailabilityArray['false']['value']}
                            {*-{$quantity_discount.real_value|floatval}%*}
                            {/if}

                            <td class                       =   "discount ar ac {$_priceReduced}"
                                data-discount-availability  =   "{$discountAvailability}"
                                data-discount-quantity      =   "{$qdQuantity}"
                                data-discount-price         =   "{$discountPrice}"
                                onclick="$(this).parent().children('.quantity_input').children('input').val('{$quantity_discount.from_quantity|intval}');priceUpdate($(this).parent().children('.quantity_input').children('input'));">

                                {if !$discountPrice}
                                    <span class="auf-anfrage"  title="Für die gewählte Produktmenge von {$discount} Stück bieten wir spezielle Preise auf Anfrage. Bitte nehmen Sie mit uns Kontakt auf.">{$discountAvailabilityArray['onrequest']['value']}</span>
                                {else}
                                    <span class="totalPrice">{$discountCellValue}</span>
                                    {if $qdReductionBoolean}
                                        <span class="totalPricePromotion">{$discountCellValue}</span>
                                    {/if}
                                {/if}

                            </td>
						{/foreach}

                    {elseif $modExceptionV30 }

                        {if $swissDebug}D id: {$cat_id}{/if}

                        {* CALCULATION MODE: EXCEPTION @TROCKENMITTEL*}
                        {foreach from=$product.quantity_discounts item='quantity_discount'}

                        {* Set up variables *}
                            {assign var="qdPrice"               value=$quantity_discount.price|floatval}
                            {assign var="qdPricePromotion"      value=""}
                            {assign var="qdPricePromotionValue" value=""}
                            {assign var="qdPrice"               value=$quantity_discount.price|floatval}
                            {assign var="qdReduction"           value=$quantity_discount.reduction|string_format:"%.2f"}
                            {assign var="qdReductionBoolean"    value=false}
                            {assign var="qdReductionType"       value=$quantity_discount.reduction_type}
                            {assign var="qdQuantity"            value=$quantity_discount.quantity|intval}
                            {assign var="qdAufAnfrage"          value="a.A."}
                            {assign var="proUnity"              value=$product.unity|intval}
                            {assign var="stuckPriceForPalette"  value=""}
                            {assign var="dataDiscountException" value=false}


                        {* Template's validation *}
                            {assign var="qdValidationMessage"   value=""}
                            {assign var="qdValidation"          value=""}
                            {assign var="_priceReduced"         value=""}

                            {if $proUnity == 0}
                                {$proUnity = 1}
                            {elseif $proUnity > 0}
                                {if $qdQuantity < $proUnity}
                                    {assign var="quantitieLowerThanPreisangabe" value=true}
                                    {assign var="dataDiscountException2" value="lower-than-preisangabe"}
                                    {math
                                    equation        =   "qdQuantity / proUnity"
                                    qdQuantity      =   $qdQuantity
                                    proUnity        =   $proUnity
                                    assign          =   "qdHalfOfPreisangabe"
                                    }
                                    {if $qdHalfOfPreisangabe == 0.5}
                                        {math
                                        equation        =   "qdPrice / 2"
                                        qdPrice         =   $qdPrice
                                        assign          =   "qdPrice"
                                        }
                                    {/if}
                                {/if}
                            {/if}

                            {if $qdReduction > 0}
                                {assign var="_priceReduced"         value="priceReduced"}
                                {assign var="_promotedQuantitie"    value="promotion"}
                                {assign var="qdReductionBoolean"    value=true}
                                {if $qdReductionType == "percentage"}
                                    c
                                    {if $proUnity == 1}
                                        {math
                                        equation        =   "qdPrice - (qdReduction * qdPrice)"
                                        qdReduction     =   $qdReduction
                                        qdPrice         =   $qdPrice
                                        assign          =   "qdPricePromotion"
                                        }
                                        {math
                                        equation        =   "qdReduction * qdPrice"
                                        qdReduction     =   $qdReduction
                                        qdPrice         =   $qdPrice
                                        assign          =   "qdPricePromotionValue"
                                        }
                                    {else}
                                        {math
                                        equation        =   "qdPrice -(qdReduction * qdPrice)"
                                        qdReduction     =   $qdReduction
                                        qdPrice         =   $qdPrice
                                        assign          =   "qdPricePromotion"
                                        }
                                        {$qdPricePromotionValue = $qdPricePromotion}
                                    {/if}
                                {elseif $qdReductionType == "amount"}
                                    d
                                    {math
                                    equation        =   "qdPrice - qdReduction"
                                    qdPrice         =   $qdPrice
                                    qdReduction     =   $qdReduction
                                    format          =   "%.2f"
                                    assign          =   "qdPricePromotion"
                                    }
                                    {if $proUnity == 1}
                                        {$qdPricePromotionValue = $qdReduction}
                                    {else}
                                        {$qdPricePromotionValue = $qdReduction}
                                    {/if}

                                {* Discount price can not be higher than stuck price *}
                                    {if $qdReduction > $qdPrice}
                                        {$qdValidation = "false"}
                                    {/if}
                                {/if}

                            {* Promotion Product price for fixed quantities *}
                                {math
                                equation            =   "qdPricePromotion * proUnity"
                                qdPricePromotion    =   $qdPricePromotion
                                proUnity            =   $proUnity
                                format              =   "%.2f"
                                assign              =   "TOTAL_PRICE_PROMOTION"
                                }
                                {assign var="JUST_PRICE" value=$TOTAL_PRICE_PROMOTION}

                            {/if}



                            {math
                                equation            =   "qdPrice * proUnity"
                                qdPrice             =   $qdPrice
                                proUnity            =   $proUnity
                                format              =   "%.2f"
                                assign              =   "TOTAL_PRICE"
                            }

                            {if $qdReduction == 0}
                                {assign var="qdPricePromotion" value="0"}
                                {assign var="qdReduction" value="NaN"}
                            {*{$TOTAL_PRICE_PROMOTION = false}*}
                                {assign var="JUST_PRICE" value=$TOTAL_PRICE}
                            {/if}


                            {if $qdPrice >= 0 OR $qdReductionType == 'amount'}
                            {* A - normal price set*}
                                {if $qdPrice != 0}
                                    {if $proUnity != 0}
                                        {assign var="discountPrice" value=$JUST_PRICE}
                                    {else}

                                        {assign var="discountPrice" value=$qdPrice}
                                    {/if}
                                    {assign var="discountAvailability" value=$discountAvailabilityArray['true']['status']}
                                    {assign var="discountCellValue" value=$discountPrice}
                                {* B - auf anfrage, because price is zero and there are special prices set*}
                                {else}
                                    {assign var="discountPrice" value=""}
                                    {assign var="discountAvailability" value=$discountAvailabilityArray['onrequest']['status']}
                                    {assign var="discountCellValue" value=$discountAvailabilityArray['onrequest']['value']}
                                {/if}
                            {* no price and no special prices. Product is not available for that quaintity *}
                            {else}
                                {assign var="discountPrice" value=$quantity_discount.real_value|floatval}
                                {assign var="discountAvailability" value=$discountAvailabilityArray['false']['status']}
                                {assign var="discountCellValue" value=$discountAvailabilityArray['false']['value']}
                            {*-{$quantity_discount.real_value|floatval}%*}
                            {/if}

                            <td class                       =   "discount ar ac {$_priceReduced}"
                                data-discount-availability  =   "{$discountAvailability}"
                                data-discount-quantity      =   "{$qdQuantity}"
                                data-discount-price         =   "{$discountPrice}"
                                onclick="$(this).parent().children('.quantity_input').children('input').val('{$quantity_discount.from_quantity|intval}');priceUpdate($(this).parent().children('.quantity_input').children('input'));">

                                {if !$discountPrice}
                                    <span class="auf-anfrage"  title="Für die gewählte Produktmenge von {$discount} Stück bieten wir spezielle Preise auf Anfrage. Bitte nehmen Sie mit uns Kontakt auf.">{$discountAvailabilityArray['onrequest']['value']}</span>
                                {else}
                                    <span class="totalPrice">{$discountCellValue}</span>
                                    {if $qdReductionBoolean}
                                        <span class="totalPricePromotion">{$discountCellValue}</span>
                                    {/if}
                                {/if}

                            </td>
                        {/foreach}

                    {*--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                      TD > Content: Standard calculation *}
					{else}
                        {if $swissDebug}E id: {$cat_id}{/if}
                        {* CALCULATION MODE: STANDARD CALCULATION 3 used to be |V *}
                        {foreach from=$discounts_arr item='discount' name="discounts"}
                            {$foundQty = 0}
                            {foreach from=$product.quantity_discounts item='quantity_discount'}

                                {* Set up variables *}
                                {assign var="qdPrice"               value=$quantity_discount.price|floatval}
                                {assign var="qdPricePromotion"      value=""}
                                {assign var="qdPricePromotionValue" value=""}
                                {assign var="qdPrice"               value=$quantity_discount.price|floatval}
                                {assign var="qdReduction"           value=$quantity_discount.reduction|string_format:"%.2f"}
                                {assign var="qdReductionBoolean"    value=false}
                                {assign var="qdReductionType"       value=$quantity_discount.reduction_type}
                                {assign var="qdQuantity"            value=$quantity_discount.quantity|intval}
                                {assign var="qdAufAnfrage"          value="a.A."}
                                {assign var="proUnity"              value=$product.unity|intval}
                                {assign var="stuckPriceForPalette"  value=""}
                                {assign var="dataDiscountException" value=false}

                                {* Template's validation *}
                                {assign var="qdValidationMessage"   value=""}
                                {assign var="qdValidation"          value=""}
                                {assign var="_priceReduced"         value=""}


                                {if $proUnity == 0}
                                    {$proUnity = 1}
                                {elseif $proUnity > 0}
                                    {if $qdQuantity < $proUnity}
                                        {assign var="quantitieLowerThanPreisangabe" value=true}
                                        {assign var="dataDiscountException2" value="lower-than-preisangabe"}
                                        {math
                                        equation        =   "qdQuantity / proUnity"
                                        qdQuantity      =   $qdQuantity
                                        proUnity        =   $proUnity
                                        assign          =   "qdHalfOfPreisangabe"
                                        }
                                        {if $qdHalfOfPreisangabe == 0.5}
                                            {math
                                            equation        =   "qdPrice / 2"
                                            qdPrice         =   $qdPrice
                                            assign          =   "qdPrice"
                                            }
                                        {/if}
                                    {/if}
                                {/if}

                                {if $qdReduction > 0}
                                    {assign var="_priceReduced"         value="priceReduced"}
                                    {assign var="_promotedQuantitie"    value="promotion"}
                                    {assign var="qdReductionBoolean"    value=true}
                                    {if $qdReductionType == "percentage"}
                                        {if $proUnity == 1}
                                            {math
                                                equation        =   "qdPrice - (qdReduction * qdPrice)"
                                                qdReduction     =   $qdReduction
                                                qdPrice         =   $qdPrice
                                                assign          =   "qdPricePromotion"
                                            }
                                            {math
                                                equation        =   "qdReduction * qdPrice"
                                                qdReduction     =   $qdReduction
                                                qdPrice         =   $qdPrice
                                                assign          =   "qdPricePromotionValue"
                                            }
                                        {else}
                                            {math
                                                equation        =   "qdPrice -(qdReduction * qdPrice)"
                                                qdReduction     =   $qdReduction
                                                qdPrice         =   $qdPrice
                                                assign          =   "qdPricePromotion"
                                            }
                                            {$qdPricePromotionValue = $qdPricePromotion}
                                        {/if}
                                    {elseif $qdReductionType == "amount"}
                                        {math
                                            equation        =   "qdPrice - qdReduction"
                                            qdPrice         =   $qdPrice
                                            qdReduction     =   $qdReduction
                                            format          =   "%.2f"
                                            assign          =   "qdPricePromotion"
                                        }
                                        {if $proUnity == 1}
                                            {$qdPricePromotionValue = $qdReduction}
                                        {else}
                                            {$qdPricePromotionValue = $qdReduction}
                                        {/if}

                                    {* Discount price can not be higher than stuck price *}
                                        {if $qdReduction > $qdPrice}
                                            {$qdValidation = "false"}
                                        {/if}
                                    {/if}

                                    {* Promotion Product price for fixed quantities *}
                                    {math
                                        equation            =   "qdPricePromotion * proUnity"
                                        qdPricePromotion    =   $qdPricePromotion
                                        proUnity            =   $proUnity
                                        format              =   "%.2f"
                                        assign              =   "TOTAL_PRICE_PROMOTION"
                                    }

                                    {if ($TOTAL_PRICE_PROMOTION * 100) % 5}
                                        {assign var="tempRound" value="0"}
                                        {assign var="tempPrice" value="0"}
                                        {$tempPrice = ($TOTAL_PRICE_PROMOTION * 100)|intval}
                                        {if $tempPrice%5<3}
                                            {$tempRound = $tempPrice - ($tempPrice%5)}
                                        {else}
                                            {$tempRound = $tempPrice + 5-($tempPrice%5)}
                                        {/if}

                                        {$TOTAL_PRICE_PROMOTION = ($tempRound/100)|string_format:"%.2f"}
                                    {/if}

                                    {assign var="JUST_PRICE" value=$TOTAL_PRICE_PROMOTION}

                                {/if}

                                {math
                                    equation            =   "qdPrice * proUnity"
                                    qdPrice             =   $qdPrice
                                    proUnity            =   $proUnity
                                    format              =   "%.2f"
                                    assign              =   "TOTAL_PRICE"
                                }

                                {if $qdReduction == 0}
                                    {assign var="qdPricePromotion" value="0"}
                                    {assign var="qdReduction" value="NaN"}
                                    {*{$TOTAL_PRICE_PROMOTION = false}*}
                                    {assign var="JUST_PRICE" value=$TOTAL_PRICE}
                                {/if}



                            {* ---- ATTENTION! -------------------------------------------------------------
                             Here is a little trick. In the backend of Prestashop, if user set
                             price for the product to `0` it means that this product is possible to order
                             only on request - `auf anfrage`.
                            ------------------------------------------------------------------------------*}
                                {if $foundQty == 0 AND $discount|intval == $quantity_discount.from_quantity|intval}
                                    {$foundQty = 1}

                                    {* CHECK IF IS SET RETAIL PRICE or IF THERE ARE SPECIAL PRICES *}
                                    {if $qdPrice >= 0 OR $qdReductionType== 'amount'}
                                        {* A - normal price set*}
                                        {if $qdPrice != 0}
                                            {if $proUnity != 0}
                                                {assign var="discountPrice" value=$JUST_PRICE}
                                            {else}
                                                {assign var="discountPrice" value=$qdPrice}
                                            {/if}

                                            {* Devife discount price by Stk/Kartn *}
                                            {assign var="exLHOTSE" value=[ 127, 128, 129, 130, 131, 86, 87 ]}

                                            {if in_array( $cat_id , $exLHOTSE )}
                                                {math
                                                    equation        =   "discountPrice / stkkarton"
                                                    discountPrice   =   $TOTAL_PRICE
                                                    stkkarton       =   $feat_stkkarton
                                                    format          =   "%.2f"
                                                    assign          =   "TOTAL_PRICE"
                                                }
                                            {/if}


                                            {assign var="discountAvailability" value=$discountAvailabilityArray['true']['status']}
                                            {assign var="discountCellValue" value=$discountPrice}
                                        {* B - auf anfrage, because price is zero and there are special prices set*}
                                        {else}
                                            {assign var="discountPrice" value=""}
                                            {assign var="discountAvailability" value=$discountAvailabilityArray['onrequest']['status']}
                                            {assign var="discountCellValue" value=$discountAvailabilityArray['onrequest']['value']}
                                        {/if}
                                    {* no price and no special prices. Product is not available for that quaintity *}
                                    {else}
                                        {assign var="discountPrice" value=$quantity_discount.real_value|floatval}
                                        {assign var="discountAvailability" value=$discountAvailabilityArray['false']['status']}
                                        {assign var="discountCellValue" value=$discountAvailabilityArray['false']['value']}
                                        {*-{$quantity_discount.real_value|floatval}%*}
                                    {/if}

                                {strip}
                                <td class                       =   "discount ar ac {$_priceReduced}"
                                        data-discount-availability  =   "{$discountAvailability}"
                                        data-discount-quantity      =   "{$qdQuantity}"
                                        data-discount-price         =   "{$discountPrice}"
                                        onclick                     =   "$(this).parent().children('.quantity_input').children('input').val( {$discount} );priceUpdate($(this).parent().children('.quantity_input').children('input'));">
                                        {if !$discountPrice}
                                            <span class="auf-anfrage" title="Für die gewählte Produktmenge von {$discount} Stück bieten wir spezielle Preise auf Anfrage. Bitte nehmen Sie mit uns Kontakt auf.">{$discountAvailabilityArray['onrequest']['value']}</span>
                                        {else}
                                            <span class="totalPrice">{$TOTAL_PRICE}</span>
                                            {if $qdReductionBoolean}
                                                <span class="totalPricePromotion">{$TOTAL_PRICE_PROMOTION}</span>
                                            {/if}
                                        {/if}
                                    </td>
                                {/strip}
                                {/if}
                            {/foreach}
                            {if $foundQty == 0}
                                <td class="ar notav"  data-discount-available={$discountAvailabilityArray['false']['status']} >
                                   <span>{$discountAvailabilityArray['false']['value']}</span>
                                </td>
                            {/if}
                        {/foreach}
					{/if}
                {/if}

                {*********************************************************************************************************************************************************************************}
                {* Quantity *}
                <td class="ac quantity_input">
                    <input type="text" name="ajax_qty_to_add_to_cart[{$product.id_product|intval}]" id="quantity_wanted_{$product.id_product|intval}" class="text" value="0" size="5" maxlength="5" onkeyup="priceUpdate($(this))"/>

                </td>

                {*********************************************************************************************************************************************************************************}
                {* Price *}
                <td>

					{$product.price_tax_exc = $product.price_tax_exc|number_format:3}
                    {if ($product.price_tax_exc * 100) % 5}
                        {assign var="tempRound" value="0"}
                        {assign var="tempPrice" value="0"}
                        {$tempPrice = $product.price_tax_exc * 100}
                        {if $tempPrice%5<3}
                            {$tempRound = $tempPrice - ($tempPrice%5)}
                        {else}
                            {$tempRound = $tempPrice + 5-($tempPrice%5)}
                        {/if}
                        {$product.price_tax_exc = ($tempRound/100)}
						{assign var="rounded_price" value="0"}
                    {/if}

                    {if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
                        <span class="price" style="display: inline;">
                        {if $product.unity|string_format:"%.2f" != 0}
                            {*{$product.price_tax_exc * $product.unity|string_format:"%.2f"}*}
                            {assign var="totalPrice" value=$product.price_tax_exc * $product.unity|string_format:"%.2f"}
                            {$totalPrice|string_format:"%.2f"}
                        {else}
                            {$product.price_tax_exc|string_format:"%.2f"}
                        {/if}
                        </span>
                        <br />
                    {/if}
                </td>

                {*********************************************************************************************************************************************************************************}
                {* Basket *}
                <td class="last">
                    {if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.customizable != 2 && !$PS_CATALOG_MODE}
                        {if ($product.allow_oosp || $product.quantity > 0)}
                            {if isset($static_token)}
                                <a class="button ajax_add_to_cart_button exclusive" rel="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)}" title="{l s='Add to cart'}"><span></span>{l s='Add to cart'}</a>
                            {else}
                                <a class="button ajax_add_to_cart_button exclusive" rel="ajax_id_product_{$product.id_product|intval}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}", false)}" title="{l s='Add to cart'}"><span></span>{l s='Add to cart'}</a>
                            {/if}
                        {else}
                            <span class="exclusive"><span></span>{l s='Add to cart'}</span><br />
                        {/if}
                    {/if}
                </td>

                {*********************************************************************************************************************************************************************************}
                {* Addditional feature helper *}
				{foreach from=$product.features item=feature}
					{if $feature.id_feature == 19}
						<td class="attr {$feature.id_feature} not-displayable">{$feature.value|escape:'htmlall':'UTF-8'}</td>
					{/if}
				{/foreach}

            </tr>
        {/foreach}

        </tbody>

    </table>
    {* /Products table *}






    <script>
        jQuery.fn.reverse = [].reverse;
        var price;
        var noPriceSign = "-"
        var calculated;
        var multiplicity;
        var itemsMultiplicity; // Stk/Ve || Rll/VE
        var amount;
        var invalid;
		var pos;
        var aufAnfrage;
        var aufAnfrageClicked;
        var onlyOnRequest;
        var exLhotse = new Array();


        $(document).ready(function() {
            $('table tr').each(function() {
                var i=0;
                var j=0;
                $.each(this.cells, function(){
                   if( $(this).hasClass("discount") ) {
                       ++i;
                   }
                });
                $.each(this.cells, function(){
                    if( $(this).children().html() == 'a.A.' ) {
                        ++j;
                    }
                });

                // Only one discount and the discount isauf anfrage
                if(i==j && i!=0 && j!=0){
                    $(this).addClass("onlyOnRequest");
                    console.log(i);
                    console.log(j);

                    console.log("Product only on request!")
                }
                i = j = 0;
            });

            $("tr.onlyOnRequest td.quantity_input input").attr('disabled','disabled');

        });

        function setAttributeSelector( $value ){
            var $output = "div[data-role=\"" + $value +" \"]";
            return $output;
        }


        /**
         * calculate product price
         * @param $obj
         */
        function priceUpdate($obj) {

            invalid             = false;
            price               = 0;
            calculated          = false;
            aufAnfrageClicked   = false;
            onlyOnRequest       = true;
            exLhotse            = "{$exLHOTSE}";


            var basePrice;

            // Quantity value
            amount = parseInt($obj.val());

            // If `amount` is not a number return 0
            // TODO: Insert here validation
            if (isNaN(amount))
                amount = null;

            $obj.val(amount);

            console.clear();

//            console.log("aa "+ jQuery.inArray("128",exLhotse) );

            // Condition is running when feature Stk./VE is asssign to the product and it's set.
            // LUFTPOLSTERFOLIEN AUF ROLLE cat_id=29

            var $StkVeCheck = true;
            var $StkKartonCheck = false;
            var $modExceptionV30 = false;
            var $modExceptionV31 = false;
            var $modExceptionV32 = false;
            var $modExceptionV4 = false;

            var $feat_stkve = $obj.parent().parent().children("td[data-feature-id='19']").attr("data-feature-value");
            var $feat_rllve = $obj.parent().parent().children("td[data-feature-id='37']").attr("data-feature-value");
            var $feat_stkka = $obj.parent().parent().children("td[data-feature-id='18']").attr("data-feature-value");
            var $feat_unity = $obj.parent().parent().children("td.unity").children("span.pangabe").attr("data-unity-value");

            if( $feat_stkve )
                itemsMultiplicity = $feat_stkve;
            else if ( $feat_rllve )
                itemsMultiplicity = $feat_rllve;
				


            // cat_id = 75,110,111 - don't check Stk/VE
            $catId = $obj.parent().parent().parent().parent().parent().parent().attr("id");

            if ($catId == 'subcat_75' || $catId == 'subcat_110' || $catId == 'subcat_111' ) {
                $StkVeCheck = false;
            }

//            else if($catId == 'subcat_134') {
//                console.log("MODE: sortierung konzept v3.1");
//                $StkVeCheck = false;
//                $modExceptionV31 = true;
//            }
            else if(
                        $catId == 'subcat_128' ||
                        $catId == 'subcat_129' ||
                        $catId == 'subcat_130' ||
                        $catId == 'subcat_86'  ||
                        $catId == 'subcat_131' ||
                        $catId == 'subcat_87'
                    )
            {
                console.log("MODE: sortierung konzept v4");
                $StkVeCheck = false;

                $modExceptionV4 = true;
            }
            else if(

            // Stk/VE - ignored in price calculation
            // use when Stk/VE exists only in from presentation's purpose.
            // Requires also to do an exception in Cart.Controller.php for add to cart validation.

                    $catId == 'subcat_48' ||
                            $catId == 'subcat_31' ||
                            $catId == 'subcat_132' ||
                            $catId == 'subcat_133' ||
                            $catId == 'subcat_134' ||
                            $catId == 'subcat_135'
                    )
            {
                console.log("MODE: sortierung konzept v3.2");
                $StkVeCheck = false;
                $StkKartonCheck = true;
                $modExceptionV32 = true;
            }
            if ($catId == 'subcat_127' ) {
                $StkVeCheck = false;
            }


            console.log("-----------------------------------");

            console.log("xx"+exLhotse);

            if ( $StkVeCheck && $feat_stkve && $feat_stkve != '-' )
            {
                multiplicity = $feat_stkve;
                console.log("ATTENTION! Stk./VE found! Multiplicity changed!")

            }
            else {
                multiplicity = 1;
            }

            // If Stk./VE is assign.....
            // WTF!!!!!!!
            if ( multiplicity > 1) {
                if (amount % multiplicity != 0 || multiplicity > amount)
                    invalid = true;
                else
                    invalid = false;
            }


            // TODO: DEMOVE IT
            if ( !$StkVeCheck && $modExceptionV30) {
//                multiplicity = itemsMultiplicity;
//                invalid = false;
            }
            else if  ( !$StkVeCheck && $modExceptionV31) {
                multiplicity = 1;
                invalid = false;
            }

            if($StkKartonCheck){
                multiplicity = $feat_stkka / $feat_unity;
                console.log("$StkKartonChecked. Multiplicity assigned.");
            }

            // Remove special style (for prices) added for selecting quaintity
            // Adds style to any all cells with class `ar` in parent row
            // TODO: move it to the css
            $obj.parent().parent().children(".ar").reverse().each(function() {
                $(this).css('font-weight','normal');
            });

            // Na dziendobry sprawdzimy invalid..
            console.log("Preisangabe: "+ $feat_unity);
            console.log("Stk/ve: "+ $feat_stkve);
            console.log("Rll/ve: "+ $feat_rllve);
            console.log("Stk/Karton: "+ $feat_stkka);
            console.log("Multiplicity: "+multiplicity);
            console.log("I  tems Multiplicity: "+itemsMultiplicity);
            console.log("User quantity: "+amount);
            console.log("Invalid: "+invalid);

            // Find base price. Base price is smallest quantity for fixed price.
            $obj.parent().parent().each(function(){
                $(this).find('td').each(function(){
                    if( $(this).hasClass("discount") && $(this).data("discount-availability") == true) {
//                        basePrice = $(this).html();
                          basePrice = parseFloat($(this).attr("data-discount-price"));
                        console.log("Base price: " + basePrice);
                    }
                    // First td has base price
                    if (typeof basePrice != 'undefined')
                        return false;
                })
            })



            //
            // If amount validated than calculate the price
            //
            if (amount > 0) {

                // Row and cell relative to the selected amount
                var pCell = $obj.parent();
                var pRow  = $obj.parent().parent();

                // I guess it's old code, please remove this condition
                if (2==3 && !isNaN(parseInt($obj.parent().parent().children('.unity').children("span").html())) && parseInt($obj.parent().parent().children('.unity').children("span").html()) > amount ) {
                    invalid = true;
                    console.log("WARNING!! Super strange condition is running!")
                }
                else
                {
                    console.log("================================")
                    console.log("= ITERATE THROUGH ROW ==========")
                    console.log("================================")

                    var i = 0;

                    // Find all rows with a class `discount`
                    // TODO: change to work with data- attribute
                    pRow.children(".discount").reverse().each(function() {

                        i++;

                        // Available quantities for this product
                        var discountQty = parseInt($(this).attr("data-discount-quantity"));
                        // Float price format
                        var discountPrice = parseFloat($(this).attr("data-discount-price"));

                        console.log("Q: "+discountQty+ ", P: "  +discountPrice);



                        if (!calculated && amount >= discountQty) {
                            console.log("Calculated: "+calculated);
                            console.log("Init Invalid: "+invalid);


							if ($(this).children().html() == 'a.A.') {
                                aufAnfrageClicked = true;
                                calculated = true;
                                pos = $obj.position();
                                off = $obj.offset();
                                $(".alert-box").css("top",off.top-190);
                                $(".alert-box").css("left",pos.left-105);
                                $(".alert-box").find("strong").html(discountQty);
                                $(".alert-box").show(200);

                                if(pRow.hasClass("onlyOnRequest")){
                                    $obj.val( '-' );
                                }
                                else {
                                    $obj.val( amount );
                                }

                                price = 0;
                                console.log("Activated a.A. box");
                                console.log("Calculated: "+calculated);
							}
                            else
                            {
                                onlyOnRequest == true;

								if ($obj.parent().parent().children(".pal").length > 0 && parseInt($obj.parent().parent().children(".pal").attr("class").split(" ")[0]) <= amount) {
									$obj.parent().parent().children(".pal").each(function() {

										discountQty = parseInt($(this).attr("class").split(" ")[0]);
										discountPrice = parseFloat($(this).html());

										if ( !isNaN(parseInt($(this).parent().children(".unity").children("span").html())) && parseInt($(this).parent().children(".unity").children("span").html()) != 0 )
											price = discountPrice * amount / parseInt($(this).parent().children(".unity").children("span").html());
										else
											price = discountPrice * amount;
										calculated = true;



										$(this).css('font-weight','bold');
                                        $(this).css('font-size','15px');
//                                        $(this).css('color','red');

                                        console.log("Cos tam sie dzieje");
									});
								}
                                else
                                {
                                    // Sales unit = Preisangabe
                                    var salesUnit = parseInt($(this).parent().children(".unity").children("span").html());

                                    // If sales unit is set and it's grater than 0
                                    // For marketer: determine if product is sailed in packages or in single pieces
									if ( !isNaN( salesUnit ) && salesUnit != 0 )
									{
                                        if( $StkKartonCheck ) {
                                            price = discountPrice * amount * multiplicity;
                                            console.log("Calculation mode: $StkKartonCheck");
                                            console.log("Calculation: "+discountPrice+" * "+amount + " * " + multiplicity);
                                        }
                                        else {
                                            price = discountPrice * amount / salesUnit;
                                        }

                                        console.log("Calculation mode: packages (preisangabe is int!");
                                        console.log("Sales unit: Packages ("+ salesUnit +")")
                                        console.log("Calculation: "+discountPrice+" * "+amount+" / " + salesUnit);
                                    }
									else
                                    {
                                        // Product is sell in single pieces.

//                                        if ( !$StkVeCheck && $modExceptionV30) {
//                                            price = discountPrice * amount * itemsMultiplicity;
//                                            console.log("Calculation mode: modExceptionV30");
//                                            console.log("Calculation: "+discountPrice+" * "+amount + " * " + itemsMultiplicity);
//                                        }
                                        if ( !$StkVeCheck && $modExceptionV31) {
                                            price = discountPrice * amount;
                                            console.log("Calculation mode: modExceptionV31");
                                            console.log("Calculation: "+discountPrice+" * "+amount );
                                        }
                                        else if ( !$StkVeCheck && $modExceptionV32) {
                                            //price = discountPrice * amount * itemsMultiplicity;
                                            price = discountPrice * amount;
                                            console.log("Calculation mode: modExceptionV32");
                                            console.log("Calculation: "+discountPrice+" * "+amount);
                                        }
                                        else {
                                            price = discountPrice * amount;
                                            console.log("Calculation mode: standard");
                                            console.log("Calculation: "+discountPrice+" * "+amount );
                                        }

                                        console.log("Price: "+ parseFloat(price) + " CHF");
                                        console.log("Sales unit: Single ("+ salesUnit +") ")
                                    }
									calculated = true;
                                    console.log("Calculated: "+calculated);

									$(this).css('font-weight','bold');
                                    $(this).css('font-size','12px');
//                                    $(this).css('color','blue');
								}
							}
                        }

                        console.log("___________________.");
                    });


                    if( i == 1 && onlyOnRequest == true){
                        onlyOnRequest = true;
                        console.log("Product only on request: "+onlyOnRequest);
                    }

                    console.log("Discount combinations: "+i);
                    console.log("===========================.");


                    var discountQtyNew = $obj.parent().parent().children(".discount").first().attr("data-discount-quantity");

//                    if ( !$StkVeCheck && $modExceptionV30) {
//                        discountQtyNew = $feat_stkve;
//                    }

                    var discountPrice = parseFloat( $obj.parent().parent().children(".discount").first().attr("data-discount-price") );
                    var discountQty = discountQtyNew;

                    // Old. Depreciated.v
                    // var discountQty = $obj.parent().parent().children(".discount").first();
                    // var discountPrice = parseFloat(discountQty.html());
                    // discountQty = parseInt(discountQty.attr("class").split(" ")[0]);

					if (!calculated && amount < discountQty) {
						price = discountPrice * amount / parseInt($obj.parent().parent().children(".unity").children("span").html());
						calculated = true;
                        console.log("Warning! Additonal calculation!")
					}

                    // If price is correct number, change to correct fixed-point notation
                    if (!isNaN(price) && price > 0){
                        price = price.toFixed(2);
                        console.log("Price: "+ price + " (Valid!)")
                    }
                    else {
                        invalid = true;
                        price = noPriceSign;
                        console.log("Price: "+ price + " (Invalid!)")
                    }

                    // Insert price to proper cell
                    $obj.parent().parent().find(".price").html(price);
                    console.log("Price inserted into cell.")

                    if (!calculated) {
                        invalid = true;
                        console.log("Not calculated. Some problem!");
                    }
                }

                // Handle exception when calculated price is not correct!
                if (invalid) {

                    aufAnfrage  = $obj.parent().parent().find('td.discount').find('span').hasClass('auf-anfrage');

                    //
                    if (!aufAnfrageClicked){
                        $obj.parent().parent().addClass('activePrice');

                        $('.alert-box2 span').text(discountQtyNew);

                        if($('#subcategories .alert-box2').length == 0){
                            $('body').find('.alert-box2').css("display", "block");
                        }
                        else {
                            $('#subcategories').find('.alert-box2').css("display", "block").appendTo("body");
                        }

                        console.log("Wrong Quantity: "+discountQtyNew)
                    }
                    else {
                        console.log("auf anfrage clicked")
                    }

                    // Insert the price
                    $obj.parent().parent().find(".price").html( noPriceSign).css("text-align", "center");
                    $obj.parent().parent().children(".discount").css('color','');
                }
                else
                {
                    $obj.parent().parent().removeClass('activePrice');
                    $('body').find('.alert-box2').css("display", "none");
                }

            }
            // Selected amount is not a number or == 0. Validation falied.
            else {
                console.log("ERROR! Amonut of product is not defined!")
                $obj.parent().parent().removeClass('activePrice');
                $obj.parent().parent().find(".price").html( basePrice );
                $('body').find('.alert-box2').css("display", "none");
            }
        };






        function changePrice($obj) {
            var qty = parseInt($obj.val());
            if (qty < 0) {
                qty = Math.abs(qty);
                $obj.val(qty);
            }
            price = 0;
            calculated = false;
            $obj.parent().parent().children(".discount").reverse().each(function() {
                var discountQty = parseInt($(this).attr("class").split(" ")[0]);
                var discountPrice = parseFloat($(this).html());

                if (!calculated && qty >= discountQty) {
                    price = discountPrice * qty;
                    calculated = true;
                }

            });
            if (!calculated) {
                if(qty > 1){
                    price = parseFloat($obj.parent().parent().find(".10").html()) * qty;
                }
                else if (qty > 5){
                    price = parseFloat($obj.parent().parent().find(".5").html()) * qty;
                }
                else if (qty > 10){
                    price = parseFloat($obj.parent().parent().find(".10").html()) * qty;
                }
                else if (qty > 20){
                    price = parseFloat($obj.parent().parent().find(".20").html()) * qty;
                }

                calculated = true;
            }
            if (!isNaN(price))
                price = price.toFixed(2);
            else
                price = $obj.parent().parent().find(".real_price").html();
            $obj.parent().parent().find(".price").html(price);

        if(qty > 1 && qty < 5){
            $obj.parent().parent().find(".discount").removeClass('activePrice');
            $obj.parent().parent().find(".1").addClass('activePrice');
        }
        else if (qty > 4 && qty < 10){
            $obj.parent().parent().find(".discount").removeClass('activePrice');
            $obj.parent().parent().find(".5").addClass('activePrice');
        }
        else if (qty > 9 && qty < 20){
            $obj.parent().parent().find(".discount").removeClass('activePrice');
            $obj.parent().parent().find(".10").addClass('activePrice');
        }
        else if (qty > 19){
            $obj.parent().parent().find(".discount").removeClass('activePrice');
            $obj.parent().parent().find(".20").addClass('activePrice');
        }

//		$obj.parent().parent().find(".20").addClass('activePrice');

        }
    </script>
{else}
    <table style="width:100%;">
        <thead style="display:none">
        <tr>
            <th style="width:88px;">Preisangabe</th>
            {if (isset($discounts_arr) && count($discounts_arr) > 0)}
                {foreach from=$discounts_arr item='discount'}
                    <th class="ar">
                        {$discount|intval} {$qtyMeasure}
                    </th>
                {/foreach}
            {/if}
            <th class="ac" style="width:90px;">Menge</th>
            <th>Preis</th>
            <th class="ac" style="width:80px;">Cart</th>
        </tr>
        </thead>
		<tbody>
		<tr style="border:none;">
		<td style="border:none;" colspan="4"><p class="warning">No products in the category</p></td>
		</tr>
		</tbody>
	</table>


{/if}

