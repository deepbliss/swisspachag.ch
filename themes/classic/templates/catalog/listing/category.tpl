{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='catalog/listing/product-list.tpl'}     
                                           
{block name='product_list_header'}
 
    {if isset($category)}     
        
        {if isset($subcategories)}
        
              
 <div class="catgegory-head">
        <h1 class="h1">{$category.name}</h1>        
    </div> 
              
{if $category.description}
    <div id="category-description" class="text-muted">{$category.description nofilter}</div>
{/if}
       
       {if isset($listing.rendered_facets)}
<div id="search_filters_wrapper" class="hidden-sm-down">
  <div id="search_filter_controls" class="hidden-md-up">
      <span id="_mobile_search_filters_clear_all"></span>
      <button class="btn btn-secondary ok">
        <i class="material-icons rtl-no-flip">&#xE876;</i>
        {l s='OK' d='Shop.Theme.Actions'}
      </button>
  </div>
  {$listing.rendered_facets nofilter}
</div>
{/if}

              
        <!-- Subcategories -->
        <div id="subcategories" class="showcase">
            <ul class="inline_list">
            {assign var="i" value="0"}
            {foreach from=$subcategories item=subcategory}


            {* {if ($subcategories[0].level_depth lt '4') } *}
                <li class="clearfix i{$i%3}" id="subcat_{$subcategory.id_category}">
                     <h5><a class="subcategory-name normal-name" href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}">{$subcategory.name|truncate:125:'...'|escape:'html':'UTF-8'}</a></h5>
                     <h5><a class="subcategory-name hover-name" href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}"><span>{$subcategory.name|truncate:125:'...'|escape:'html':'UTF-8'}</span></a></h5>
                     
                        <div class="subcategory-image">
                            <a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}" title="{$subcategory.name|escape:'html':'UTF-8'}" class="img">
                                {if $subcategory.id_image}
                                    <img class="replace-2x" src="{$link->getCatImageLink($subcategory.link_rewrite, $subcategory.id_image, 'category_showcase')|escape:'html':'UTF-8'}" alt="{$subcategory.name|escape:'html':'UTF-8'}"/>
                                {else}
                                    <img class="replace-2x" src="{$img_cat_dir}{$lang_iso}-default-category_default.jpg" alt="{$subcategory.name|escape:'html':'UTF-8'}"/>
                                {/if}
                            </a>
                        </div>
                </li>
              {* {/if}  *}


                {$i=$i+1}
            {/foreach}
            {if isset($Boxmaker)}
                <li class="clearfix i{$i%3}" id="boxmaker">
                    <div class="title"><a class="categoryName" href="{$Boxmaker}" class="cat_name">Boxmaker</a></div>
                    <div class="image">
                        <a href="{$Boxmaker}" title="Boxmaker" class="img">
                            <img src="themes/swisspack/img/theme/box.jpg" alt="" height="200" />
                        </a>
                    </div>
                </li>
            {/if}
            </ul>
            <br class="clear"/>
        </div>                  
        {elseif $products}            
                 <div id="subcategories">
            <ul class="inline_list">
                <li class="clearfix">
                <div class="content_sortPagiBar">
                    {include file="$tpl_dir./pagination.tpl"}
                </div>

                {include file="./product-list.tpl" products=$products cat_id=$category->id}

                <div class="content_sortPagiBar">
                    {include file="./pagination.tpl"}
                </div>
                </li>
            </ul>
        </div>
        {else}
            <p class="warning">No products in the category.</p>
       
     
    {/if}
{/if}
{/block}
  