{**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

{function name="categories" nodes=[] depth=0}
  {strip}
    {if $nodes|count}
      <ul class="category-sub-menu">
        {foreach from=$nodes item=node}
        {if $node.id == 7}
            {$node.name = "Aus Vollkarton"}
        {elseif $node.id == 67 }
            {$node.name = "Eckkantenschutz wasserfest" }
        {elseif $node.id == 38 }
            {$node.name = "PE-Abdeckfolie Oeko Recycling" }
        {elseif $node.id == 53 }
            {$node.name = "Klebeband PVC, braun/transparent" }
        {elseif $node.id == 82 }
            {$node.name = "Klebeband PP, braun/transparent" }
        {elseif $node.id == 164 }
            {$node.name = "Klebebänder bedruckt" }
        {elseif $node.id == 55 }
            {$node.name = "Spezialklebeband blau" }
        {elseif $node.id == 193 }
            {$node.name = "Kantenschutzwinkel Vollkarton" }
        
        {/if}
          <li data-depth="{$depth}" class="{if (isset($category) && is_array($category) && isset($category.id) && $category.id==$node.id) || (isset($id_category_current) && $id_category_current==$node.id)} current_cate {/if}">
            {if $depth===0}
              {if (isset($category) && is_array($category) && isset($category.id) && $category.id==$node.id) || (isset($id_category_current) && $id_category_current==$node.id)}
              <a href="{$node.link}">{$node.name}</a>
              {if $node.children}
                <div class="navbar-toggler collapse-icons" data-toggle="collapse" data-target="#exCollapsingNavbar{$node.id}" aria-expanded="true">
                  <i class="material-icons add">&#xE145;</i>
                  <i class="material-icons remove">&#xE15B;</i>
                </div>
                <div class="collapse in" id="exCollapsingNavbar{$node.id}">
                  {categories nodes=$node.children depth=$depth+1}
                </div>
              {/if}
              {else}
                <a href="{$node.link}">{$node.name}</a>
              {if $node.children}
                <div class="navbar-toggler collapse-icons{if $c_tree_path && !in_array($node.id, $c_tree_path)} collapsed{/if}" data-toggle="collapse" data-target="#exCollapsingNavbar{$node.id}" aria-expanded="{if $c_tree_path && in_array($node.id, $c_tree_path)}true{else}false{/if}">
                  <i class="material-icons add">&#xE145;</i>
                  <i class="material-icons remove">&#xE15B;</i>
                </div>
                <div class="collapse {if $c_tree_path && in_array($node.id, $c_tree_path)} in{/if}" id="exCollapsingNavbar{$node.id}">
                  {categories nodes=$node.children depth=$depth+1}
                </div>
              {/if}
              {/if}
            {else}
                          {if (isset($category) && is_array($category) && isset($category.id) && $category.id==$node.id) || (isset($id_category_current) && $id_category_current==$node.id)}
              <a class="category-sub-link" href="{$node.link}">{$node.name}</a>
              {if $node.children}
                <span class="arrows" data-toggle="collapse" data-target="#exCollapsingNavbar{$node.id}" aria-expanded="true">
                  <i class="material-icons arrow-right">&#xE315;</i>
                  <i class="material-icons arrow-down">&#xE313;</i>
                </span>
                <div class="collapse in" id="exCollapsingNavbar{$node.id}" id="exCollapsingNavbar{$node.id}">
                  {categories nodes=$node.children depth=$depth+1}
                </div>
              {/if}
              {else}
                <a class="category-sub-link" href="{$node.link}">{$node.name}</a>
              {if $node.children}
                <span class="arrows{if $c_tree_path && !in_array($node.id, $c_tree_path)} collapsed{/if}" data-toggle="collapse" data-target="#exCollapsingNavbar{$node.id}" aria-expanded="{if $c_tree_path && in_array($node.id, $c_tree_path)}true{else}false{/if}">
                  <i class="material-icons arrow-right">&#xE315;</i>
                  <i class="material-icons arrow-down">&#xE313;</i>
                </span>
                <div class="collapse {if $c_tree_path && in_array($node.id, $c_tree_path)} in{/if}" id="exCollapsingNavbar{$node.id}" id="exCollapsingNavbar{$node.id}">
                  {categories nodes=$node.children depth=$depth+1}
                </div>
              {/if}
              {/if}
            {/if}
          </li>
        {/foreach}
      </ul>
    {/if}
  {/strip}
{/function}

<div class="block-categories hidden-sm-down">
  <ul class="category-top-menu">
    <li><a class="text-uppercase h6" href="{$categories.link nofilter}">{$categories.name}</a></li>
    <li>{categories nodes=$categories.children}</li>
  </ul>
  
      {* Javascript moved here to fix bug #PSCFI-151 *}
        <script type="text/javascript">
        // <![CDATA[
            // we hide the tree only if JavaScript is activated
           

            /* TODO: change this shitty code! */
            $('div.category-sub-menu a.selected').each(function()
            {
                if ($(this).parent().parent().is(":not(ul.Level1)")) {
                    $(this).closest('ul.Level3').parent("li").find("a:first").addClass('selectedParentLevel2');
                    $(this).closest('ul.Level2').parent("li").find("a:first").addClass('selectedParentLevel1');
                }
            });

        $('div.category-sub-menu a:not(.selected)').click(function(){
            if ($(this).parent().attr("id") != "id") {
                $('.category-sub-menu').find('a.clicked').removeClass('clicked');
                //$(this).addClass("clicked");
                $('#center_column').addClass("contentLoading");
            }
        });


        // ]]>
        </script>
</div>
